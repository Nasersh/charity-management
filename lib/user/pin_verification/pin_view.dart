import 'dart:async';
import 'package:charitysys/user/pin_verification/pin_controller.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:pin_code_fields/pin_code_fields.dart';

import '../../configrations/const/backgroundimage.dart';
import '../../configrations/locale/locale_controller.dart';

class PinVerification extends StatefulWidget {

  @override

  _PinVerificationState createState() => _PinVerificationState();
}

class _PinVerificationState extends State<PinVerification> {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Stack(
        children:[
          BackgroundImage(),
          Scaffold(
            body:
            OtpScreen(),
          ),
        ]
      ),
    );
  }
}

class OtpScreen extends StatelessWidget {
  StreamController<ErrorAnimationType> errorController =
  StreamController<ErrorAnimationType>();
  OtpScreen({Key? key}) : super(key: key);

  List<String> currentPin = ["", "", "", ""];
  List<TextEditingController> controllers = [
    TextEditingController(),
    TextEditingController(),
    TextEditingController(),
    TextEditingController(),
  ];

  int pinIndex = 0;

  MyLocalController langController = Get.put<MyLocalController>(MyLocalController());
  PinController controller=Get.put(PinController());

  @override
  Widget build(BuildContext context) {

    return Column(
      children: [
        Obx(
          ()=> Expanded(
            child: Container(
              //color: Colors.red,
              // alignment: const Alignment(0, 0.5),
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: [
                  SizedBox(height: MediaQuery.of(context).size.height/10,),
                  buildSecurityText(),
                   SizedBox(height: MediaQuery.of(context).size.height/6,),
                  Container(
                    height: MediaQuery.of(context).size.height/12,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(20),
                      color: Colors.white.withOpacity(.2)
                    ),
                    child: Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 30.0),
                      child: PinCodeTextField(
                        length: 4,
                        //obscureText: true,
                        animationType: AnimationType.fade,
                        animationDuration: Duration(milliseconds: 200),
                        // controller: PINController,
                        errorAnimationController: errorController,
                        // enableActiveFill: true,
                        onChanged: (value) {
                          // controller.checkcode(value);
                          controller.code=value;

                        },
                        appContext: context,
                        keyboardType: TextInputType.number,
                        pinTheme: PinTheme(
                          // borderWidth: 4,
                          shape: PinCodeFieldShape.box,
                          borderRadius: BorderRadius.circular(15),
                          fieldHeight: 60,
                          fieldWidth: 60,
                          selectedFillColor: Colors.black26,
                          inactiveColor: Colors.white,
                          selectedColor: Colors.blue,
                          activeFillColor:Colors.black26,
                          activeColor: Colors.black26,
                        ), // Pass it here
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 100,
                  ),

                  Container(
                    width: MediaQuery.of(context).size.width/4,
                    child: ElevatedButton(
                        onPressed: (){
                            controller.checkcode();
                        },
                        style: ButtonStyle(
                          // backgroundColor: Colors.blue
                        ),
                        child: Center(
                          child: Text(
                            'submit',style: TextStyle(
                            color: Colors.white
                          ),
                          ),
                        )),
                  ),
                 controller.verified.value? Padding(padding: EdgeInsets.all(10),
                  child: Text(
                    'enter the right code',
                    style: TextStyle(
                      color: Colors.red
                    ),
                  ),
                  ):SizedBox()

                ],
              ),
            ),
          ),
        ),
        // buildNumberPin(),
      ],
    );
  }

  buildSecurityText() {
    return Text(
      "Enter the pin code".tr,
      style: const TextStyle(
          color: Colors.white, fontWeight: FontWeight.bold, fontSize: 30),
    );
  }

  buildPinRow() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: [
        pinNumber(controllers[0]),
        pinNumber(controllers[1]),
        pinNumber(controllers[2]),
        pinNumber(controllers[3]),
      ],
    );
  }

  pinNumber(TextEditingController control) {
    return SizedBox(
      width: 60,
      child: TextFormField(
        decoration: InputDecoration(
          filled: true,
          fillColor: Colors.white.withOpacity(.2),
          contentPadding: const EdgeInsets.all(10),
        ),
        controller: control,
        textAlign: TextAlign.center,
        obscureText: true,
        style: const TextStyle(
            color: Colors.white, fontSize: 30, fontWeight: FontWeight.bold),
      ),
    );
  }

  buildNumberPin() {
    return Expanded(
      child: Container(
        alignment: Alignment.bottomCenter,
        child: Padding(
          padding: const EdgeInsets.all(8),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  keyboardNumber(1, () {
                    pinIndexSetup("1");
                    langController.changeLang("ar");
                  }),
                  keyboardNumber(2, () {
                    pinIndexSetup("2");
                    langController.changeLang("en");
                  }),
                  keyboardNumber(3, () {
                    pinIndexSetup("3");
                  }),
                ],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  keyboardNumber(4, () {
                    pinIndexSetup("4");
                  }),
                  keyboardNumber(5, () {
                    pinIndexSetup("5");
                  }),
                  keyboardNumber(6, () {
                    pinIndexSetup("6");
                  }),
                ],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  keyboardNumber(7, () {
                    pinIndexSetup("7");
                  }),
                  keyboardNumber(8, () {
                    pinIndexSetup("8");
                  }),
                  keyboardNumber(9, () {
                    pinIndexSetup("9");
                  }),
                ],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  const SizedBox(
                    width: 60,
                    height: 60,
                  ),
                  keyboardNumber(0, () {
                    pinIndexSetup("0");
                  }),
                  GestureDetector(
                    onTap: () {
                      clearPin();
                    },
                    child: Container(
                        width: 60,
                        height: 60,
                        alignment: Alignment.center,
                        decoration: BoxDecoration(
                            color: Colors.white.withOpacity(.2),
                            shape: BoxShape.circle),
                        child: const Icon(
                          Icons.subdirectory_arrow_left_rounded,
                          color: Colors.white,
                          size: 30,
                        )),
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }

  keyboardNumber(int num, onPressed) {
    return GestureDetector(
      onTap: onPressed,
      child: Container(
        width: 100,
        height: 60,
        alignment: Alignment.center,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(20),
            color: Colors.white.withOpacity(.2), shape: BoxShape.rectangle),
        child: Text(
          "$num",
          style: const TextStyle(
              color: Colors.white, fontSize: 25, fontWeight: FontWeight.bold),
        ),
      ),
    );
  }

  pinIndexSetup(String text) async {
    if (pinIndex == 0) {
      print(pinIndex);
      setPin(pinIndex, text);
      currentPin[pinIndex] = text;
      pinIndex = 1;
    } else if (pinIndex < 4) {
      print(pinIndex);
      setPin(pinIndex, text);
      currentPin[pinIndex] = text;
      pinIndex++;
      print(pinIndex);
    }

    String strPin = "";
    currentPin.forEach((element) {
      strPin += element;
    });

    if (pinIndex == 4 && strPin == "1256") {
      Get.toNamed('/dashboard');
    } else if (pinIndex == 4) {
      Get.snackbar(
        "error_pin".tr,
        "error_pin_desc".tr,
        colorText: Colors.white,
        backgroundColor: Colors.white.withOpacity(.2),
      );
      Timer(const Duration(milliseconds: 250), () {
        for (int i = 0; i < 5; i++) {
          clearPin();
        }
      });
    }
  }

  setPin(int index, String text) {
    switch (index) {
      case 0:
        controllers[0].text = text;
        break;
      case 1:
        controllers[1].text = text;
        break;
      case 2:
        controllers[2].text = text;
        break;
      case 3:
        controllers[3].text = text;
        break;
    }
  }

  clearPin() {
    if (pinIndex == 0) {
      print(pinIndex);
      setPin(pinIndex, "");
      currentPin[pinIndex] = "";
      pinIndex = 0;
    } else if (pinIndex == 4) {
      pinIndex--;
      //clearPin()
      print(pinIndex);
    } else {
      print(pinIndex);
      setPin(pinIndex, "");
      currentPin[pinIndex] = "";
      pinIndex--;
    }
  }
}

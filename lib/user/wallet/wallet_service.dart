import 'dart:convert';
import 'dart:io';
import 'package:charitysys/config/config.dart';
import 'package:charitysys/secureStorage.dart';
import 'package:http/http.dart' as http;

class WalletService
{
  var message;
  String wallet='';
  Future<int> ShowWallet() async{
    var _storage=SecureStorage();
    String? token = await _storage.read('token');
    var url=Uri.parse(ServerConfig.domainNameServer+ServerConfig.ShowWallet);

    var response = await http.get(url,
        headers: {
      'Accept' : 'application/json',
      'Authorization'  :  'Bearer $token'
    });
    print(response.statusCode);

    if(response.statusCode == 200){
      var jsonres=jsonDecode(response.body);
      wallet=jsonres["wallet"].toString();
      return int.parse(wallet);
    }
    else {
      return 0;
    }
  }

  Future<int> ChargeWallet(money) async{
    var url=Uri.parse(ServerConfig.domainNameServer+ServerConfig.ChargeWallet);
    var _storage=SecureStorage();
    String? token = await _storage.read('token');
    var response = await http.post(url,
        headers:
    {
      'Accept' : 'application/json',
      'Authorization'  :  'Bearer $token'
    },
      body:
      {
        'money' : money
      }
    );
    print(response.statusCode);
    var jsonres=jsonDecode(response.body);

    if(response.statusCode == 200){
      print('successful');
      return wallet=jsonres["wallet"];
    }
    else {
      print('fail');
      return 0;
    }
  }
}
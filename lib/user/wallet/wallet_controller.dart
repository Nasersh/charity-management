
import 'package:charitysys/user/wallet/wallet_service.dart';
import 'package:get/get.dart';


class WalletController extends GetxController
{
  var wallet=0.obs;
  // dynamic wallet=''.obs;
   WalletService _service=WalletService();

   @override
  void onInit() {
     showWallet();
    // TODO: implement onInit
  }
   showWallet() async
  {
  wallet.value=await _service.ShowWallet();
  print('current wallet   ${wallet}');
  }
  ChargeWallet(money) async
  {
    wallet.value=await _service.ChargeWallet(money);
    print('charged wallet   ${wallet}');
  }
}
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';

import '../../configrations/const/const.dart';


class AboutUs extends StatelessWidget {
  const AboutUs({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: kprimary,
      body: Stack(
        children:[

          Container(
            color: kprimary,
            height: double.infinity,
            width: double.infinity,
            child: Column(
                children:[
                  Container(
                    height: 300,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(20),
                      image: DecorationImage(
                        image:AssetImage("images/orphan.jpg"),
                        fit: BoxFit.cover
                      )
                    ),
                  ),
                  // SizedBox(
                  //   height: MediaQuery.of(context).size.height/12,
                  // ),
                  // Container(
                  //   width: MediaQuery.of(context).size.width/3.5,
                  //   height: MediaQuery.of(context).size.height/20,
                  //     decoration:BoxDecoration(
                  //       color: kblue,
                  //       borderRadius: BorderRadius.only(bottomRight: Radius.circular(20))
                  //     ),
                  //     child: Center(child: Text("About Us",style: TextStyle(color: kprimary,fontSize: 20,fontWeight: FontWeight.bold),))),
                  // SizedBox(
                  //   height: MediaQuery.of(context).size.height/20,
                  // ),
                  Expanded(
                    child: ListView(
                      shrinkWrap: true,
                      children:  [
                        const Card(
                          child: ExpansionTile(
                              title: Text(
                                "where does the association locate?",style: TextStyle(
                                color: kblue,
                                fontSize: 17
                              ),

                              ),
                              children: <Widget>[
                                ListTile(
                                  title: Text(
                                    "we are in Ain mneen",
                                    style: TextStyle(fontWeight: FontWeight.w700),
                                  ),
                                )
                              ]),
                        ),
                        SizedBox(
                          height: MediaQuery.of(context).size.height/40,
                        ),
                        const Card(
                          child: ExpansionTile(
                              title: Text(
                                "what is this app for?",style: TextStyle(
                              color: kblue,
                              fontSize: 17
                              ),


    ),
                              children: <Widget>[
                                ListTile(
                                  title: Text(
                                    "it provides the needs of the orphans and make them feel good",
                                    style: TextStyle(fontWeight: FontWeight.w700),
                                  ),
                                )
                              ]),
                        ),
                        SizedBox(
                          height: MediaQuery.of(context).size.height/40,
                        ),
                        const Card(
                          child: ExpansionTile(
                              title: Text(
                                "developers ?",style: TextStyle(
                                  color: kblue,
                                  fontSize: 17
                              ),

                              ),
                              children: <Widget>[
                                ListTile(
                                  title: Text(
                                    "naser shridm \n ali ismael \n naser dahadal\n lady fahmeh",
                                    style: TextStyle(fontWeight: FontWeight.w700),
                                  ),
                                )
                              ]),
                        ),
                        SizedBox(
                          height: MediaQuery.of(context).size.height/40,
                        ),
                        const Card(
                          child: ExpansionTile(
                              title: Text(
                                "when did we establish this system?",style: TextStyle(
                                  color: kblue,
                                  fontSize: 17
                              ),

                              ),
                              children: <Widget>[
                                ListTile(
                                  title: Text(
                                    "2001/11/21",
                                    style: TextStyle(fontWeight: FontWeight.w700),
                                  ),
                                )
                              ]),
                        ),
                        SizedBox(
                          height: MediaQuery.of(context).size.height/40,
                        ),
                        Card(
                            child: TextButton(
                              onPressed:(){
                                launch(
                                    "mailto:shridmnaser@gmail.com?subject=Hi&body=How are you%20plugin");

                              } ,
                              child: const Text(
                                  "Email to Contact ?",style: TextStyle(
                                color: kstorm
                              ),
                              ),
                            )

                        ),


                      ],
                    ),
                  ),
                ]
            ),
          ),
        //   Padding(
        //     padding: EdgeInsets.only(top: MediaQuery.of(context).size.height/6),
        //     child: Container(
        //     decoration: BoxDecoration(
        //       color: kprimary,
        //       borderRadius: BorderRadius.only(topLeft: Radius.circular(30),topRight: Radius.circular(30))
        //     ),
        //     child: Column(
        //       children:[
        //         SizedBox(
        //           height: MediaQuery.of(context).size.height/12,
        //         ),
        //         Text("About Us",style: TextStyle(color: kstorm,fontSize: 20,fontWeight: FontWeight.bold),),
        //         SizedBox(
        //          height: MediaQuery.of(context).size.height/20,
        //         ),
        //         Expanded(
        //           child: ListView(
        //             shrinkWrap: true,
        //             children:  [
        //               const Card(
        //                   child: ExpansionTile(
        //                       title: Text(
        //                         "where does the association locate?",
        //
        //                       ),
        //                       children: <Widget>[
        //                         ListTile(
        //                           title: Text(
        //                             "we are in Ain mneen",
        //                             style: TextStyle(fontWeight: FontWeight.w700),
        //                           ),
        //                         )
        //                       ]),
        //                 ),
        //               SizedBox(
        //                 height: MediaQuery.of(context).size.height/40,
        //               ),
        //               Card(
        //                 child: ExpansionTile(
        //                     title: Text(
        //                       "where does the association locate?",
        //
        //                     ),
        //                     children: const <Widget>[
        //                       ListTile(
        //                         title: Text(
        //                           "we are in Ain mneen",
        //                           style: TextStyle(fontWeight: FontWeight.w700),
        //                         ),
        //                       )
        //                     ]),
        //               ),
        //               SizedBox(
        //                 height: MediaQuery.of(context).size.height/40,
        //               ),
        //               Card(
        //                 child: ExpansionTile(
        //                     title: Text(
        //                       "where does the association locate?",
        //
        //                     ),
        //                     children: const <Widget>[
        //                       ListTile(
        //                         title: Text(
        //                           "we are in Ain mneen",
        //                           style: TextStyle(fontWeight: FontWeight.w700),
        //                         ),
        //                       )
        //                     ]),
        //               ),
        //               SizedBox(
        //                 height: MediaQuery.of(context).size.height/40,
        //               ),
        //               Card(
        //                 child: ExpansionTile(
        //                     title: Text(
        //                       "where does the association locate?",
        //
        //                     ),
        //                     children: const <Widget>[
        //                       ListTile(
        //                         title: Text(
        //                           "we are in Ain mneen",
        //                           style: TextStyle(fontWeight: FontWeight.w700),
        //                         ),
        //                       )
        //                     ]),
        //               ),
        //               SizedBox(
        //                 height: MediaQuery.of(context).size.height/40,
        //               ),
        //               Card(
        //                 child: TextButton(
        //                   onPressed:(){
        //                     launch(
        //                         "mailto:shridmnaser@gmail.com?subject=Hi&body=How are you%20plugin");
        //
        //                   } ,
        //                   child: Text(
        //                     "Email to Contact ?"
        //                   ),
        //                 )
        //
        //               ),
        //
        //
        //             ],
        //           ),
        //         ),
        //       ]
        // ),
        //       ),
        //   ),

        ]
      ),

    );
  }

  Future _launchEmailURL() async {
    launch(
        "mailto:shridmnaser@androidcoding.in?subject=Hi&body=How are you%20plugin");
  }
}

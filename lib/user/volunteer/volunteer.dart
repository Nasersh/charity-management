import 'package:charitysys/user/volunteer/volunteer_controller.dart';
import 'package:charitysys/user/volunteer/volunteer_service.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:image_picker/image_picker.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get/get_core/src/get_main.dart';
import '../../config/userinfo.dart';
import '../../configrations/const/const.dart';
import '../../configrations/const/widgets/text-input.dart';
import '../../configrations/the_helper_widget/the_helper_widget.dart';
import '../../dashboard/volunteers_model.dart';
import '../../secureStorage.dart';

class Volunteer extends StatelessWidget {
  Volunteer({Key? key}) : super(key: key);

  VolunteerController vcontroller = Get.put(VolunteerController());
  SecureStorage storage = SecureStorage();
  Rx<DateTime> currentDate = DateTime.now().obs;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: kprimary,
      body: Stack(children: [
        Container(
          color: kblue,
          height: double.infinity,
          width: double.infinity,
        ),
        Padding(
          padding: EdgeInsets.only(top: MediaQuery.of(context).size.height / 6),
          child: Container(
            width: double.infinity,
            height: MediaQuery.of(context).size.height / 1.3,
            decoration:  BoxDecoration(
                color: kprimary,
                borderRadius: BorderRadius.all(Radius.circular(20))),
            child: Padding(
              padding: EdgeInsets.all(10),
              child: SingleChildScrollView(

                child:Userinfo.Volunteer!='true'?
                Column(
                  children: [
                    SizedBox(
                      height: MediaQuery.of(context).size.height / 12,
                    ),
                    Text(
                      "Volunteer",
                      style: TextStyle(
                          color: kblue,
                          fontSize: 25,
                          fontWeight: FontWeight.bold),
                    ),
                    SizedBox(
                      height: MediaQuery.of(context).size.height / 16,
                    ),
                    TextField(
                      decoration: const InputDecoration(
                        enabledBorder: OutlineInputBorder(
                          borderSide: BorderSide(width: 3, color: kblue),
                          borderRadius: BorderRadius.all(Radius.circular(20)),
                        ),
                        hintText: "name",
                        hintStyle: TextStyle(
                          color: Colors.grey,
                        ),
                      ),
                      onChanged: (value) {
                        vcontroller.name.value = value;
                      },
                    ),
                    SizedBox(
                      height: MediaQuery.of(context).size.height / 16,
                    ),
                    TextField(
                      decoration: const InputDecoration(
                          enabledBorder: OutlineInputBorder(
                            borderSide: BorderSide(width: 3, color: kblue),
                            borderRadius: BorderRadius.all(Radius.circular(20)),
                          ),
                          hintText: "description",
                          hintStyle: TextStyle(
                            color: Colors.grey,
                          )),
                      onChanged: (value) {
                        vcontroller.description.value = value;
                      },
                    ),
                    SizedBox(
                      height: MediaQuery.of(context).size.height / 16,
                    ),
                    SizedBox(
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          GestureDetector(
                            child: Container(
                              alignment: Alignment.center,
                              child: const Text('galley'),
                              padding: const EdgeInsets.symmetric(
                                  horizontal: 10, vertical: 5),
                              decoration: BoxDecoration(
                                  border: Border.all(color: kblue, width: 3),
                                  borderRadius: BorderRadius.circular(10)),
                              width: MediaQuery.of(context).size.height / 7,
                              height: 60,
                            ),
                            onTap: () async {
                              vcontroller.identicalImage.value =
                              await takeImages(ImageSource.gallery);
                            },
                          ),
                          GestureDetector(
                            child: Container(
                              alignment: Alignment.center,
                              child: const Text('date'),
                              padding: const EdgeInsets.symmetric(
                                  horizontal: 10, vertical: 5),
                              decoration: BoxDecoration(
                                  border: Border.all(color: kblue, width: 3),
                                  borderRadius: BorderRadius.circular(10)),
                              width: MediaQuery.of(context).size.height / 7,
                              height: 60,
                            ),
                            onTap: () async {
                              await openCylinder(context);
                            },
                          ),
                          GestureDetector(
                            child: Container(
                              alignment: Alignment.center,
                              child: const Text('gallery'),
                              padding: const EdgeInsets.symmetric(
                                  horizontal: 10, vertical: 5),
                              width: MediaQuery.of(context).size.height / 7,
                              height: 60,
                              decoration: BoxDecoration(
                                  border: Border.all(color: kblue, width: 3),
                                  borderRadius: BorderRadius.circular(10)),
                            ),
                            onTap: () async {
                              vcontroller.certificateImage.value =
                              await takeImages(ImageSource.gallery);
                              // certificateImage.value = image.value;
                            },
                          )
                        ],
                      ),
                    ),
                    SizedBox(
                      height: MediaQuery.of(context).size.height / 20,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Container(
                          padding: const EdgeInsets.symmetric(
                            horizontal: 10,
                            vertical: 5,
                          ),
                          decoration: BoxDecoration(
                              border: Border.all(color: kblue, width: 3),
                              color: Colors.white,
                              borderRadius: BorderRadius.circular(10)),
                          width: MediaQuery.of(context).size.height / 7,

                          // dropdown below..
                          child: Obx(
                                () => DropdownButton<String>(
                              value: vcontroller.selectedgender.value,
                              onChanged: (value) {
                                vcontroller.selectedgender.value = value!;
                              },
                              hint: const Center(
                                  child: Text(
                                    'gender',
                                    style: TextStyle(color: kblue),
                                  )),
                              // Hide the default underline
                              underline: Container(),
                              // set the color of the dropdown menu
                              dropdownColor: kprimary,
                              icon: const Icon(
                                Icons.arrow_downward,
                                color: kstorm,
                              ),
                              isExpanded: true,

                              // The list of options
                              items: vcontroller.gender
                                  .map((e) => DropdownMenuItem(
                                value: e,
                                child: Container(
                                  alignment: Alignment.centerLeft,
                                  child: Text(
                                    e,
                                    style:
                                    const TextStyle(fontSize: 18),
                                  ),
                                ),
                              ))
                                  .toList(),

                              // Customize the selected item
                              selectedItemBuilder: (BuildContext context) =>
                                  vcontroller.gender
                                      .map((e) => Center(
                                    child: Text(
                                      '$e',
                                      style: const TextStyle(
                                          fontSize: 15,
                                          color: kblue,
                                          fontWeight: FontWeight.bold),
                                    ),
                                  ))
                                      .toList(),
                            ),
                          ),
                        ),
                        Container(
                          padding: const EdgeInsets.symmetric(
                              horizontal: 10, vertical: 5),
                          decoration: BoxDecoration(
                              border: Border.all(color: kblue, width: 3),
                              color: Colors.white,
                              borderRadius: BorderRadius.circular(10)),
                          width: MediaQuery.of(context).size.height / 7,

                          // dropdown below..
                          child: Obx(
                                () => DropdownButton<String>(
                              value: vcontroller.selectedjob.value,
                              onChanged: (value) {
                                vcontroller.selectedjob.value = value!;
                                if(value == 'Health'){
                                  vcontroller.cat_id.value = 1;
                                }else if(value == 'Food'){
                                  vcontroller.cat_id.value = 2;
                                }else if(value == 'Education'){
                                  vcontroller.cat_id.value = 3;
                                }
                              },
                              hint: const Center(
                                  child: Text(
                                    'Section',
                                    style: TextStyle(color: kblue),
                                  )),
                              // Hide the default underline
                              underline: Container(),
                              // set the color of the dropdown menu
                              dropdownColor: kprimary,
                              icon: const Icon(
                                Icons.arrow_downward,
                                color: kstorm,
                              ),
                              isExpanded: true,

                              // The list of options
                              items: vcontroller.job
                                  .map((e) => DropdownMenuItem(
                                value: e,
                                child: Container(
                                  alignment: Alignment.centerLeft,
                                  child: Text(
                                    e,
                                    style:
                                    const TextStyle(fontSize: 18),
                                  ),
                                ),
                              ))
                                  .toList(),

                              // Customize the selected item
                              selectedItemBuilder: (BuildContext context) =>
                                  vcontroller.job
                                      .map((e) => Center(
                                    child: Text(
                                      '$e',
                                      style: const TextStyle(
                                          fontSize: 15,
                                          color: kblue,
                                          fontWeight: FontWeight.bold),
                                    ),
                                  ))
                                      .toList(),
                            ),
                          ),
                        ),
                      ],
                    ),
                    SizedBox(
                      height: MediaQuery.of(context).size.height / 26,
                    ),
                    GestureDetector(
                      onTap: () {
                        VolunteerService().addVolunteerRequest(
                          VolunteerRequestModel(
                            0,
                            vcontroller.cat_id.value,
                            1,
                            currentDate.value.toString(),
                            vcontroller.selectedgender.value,
                            vcontroller.selectedjob.value,
                            vcontroller.certificateImage.value,
                            vcontroller.identicalImage.value,
                            '09876534',
                          ),
                        );
                      },
                      child: Container(
                        height: MediaQuery.of(context).size.height / 20,
                        width: MediaQuery.of(context).size.width / 4,
                        decoration: const BoxDecoration(
                            color: kstorm,
                            borderRadius:
                            BorderRadius.all(Radius.circular(30))),
                        child: Container(
                          width: MediaQuery.of(context).size.width / 4,
                          color: kstorm,
                          child: const Center(
                            child: Text("submit"),
                          ),
                        ),
                      ),
                    )
                  ],
                ):
                Column(
                children: [
                  SizedBox(
                    height: MediaQuery.of(context).size.height/10,
                  ),
                  Text('you are already in our crew ',style: TextStyle(
                    color: kblue,
                    fontSize: 25,
                    fontWeight: FontWeight.bold
                  ),),
                  SizedBox(
                    height: MediaQuery.of(context).size.height/20,
                  ),
                  Text('You can request a resignation \n\n      if you are unconfortable ',
                    style: TextStyle(
                      // color: kblue,
                      fontSize: 20,
                  ),),
                  SizedBox(
                    height: MediaQuery.of(context).size.height/20,
                  ),
                  TextInput(icon: FontAwesomeIcons.person, hint: 'why would you resign',
                      onchange: (value)
                      {
                        vcontroller.reason=value;
                      },
                      validator: (value){},
                      inputType: TextInputType.name,
                    inputAction: TextInputAction.next,
                  ),
                  SizedBox(
                    height: MediaQuery.of(context).size.height/20,
                  ),
                  MaterialButton(
                    onPressed: () {
                        vcontroller.resign();
                        Get.back();
                  },
                    child: Container(
                      width: MediaQuery.of(context).size.width/3,
                      height: MediaQuery.of(context).size.height/15,
                      decoration: BoxDecoration(
                        color: kstorm,
                        borderRadius: BorderRadius.circular(20)
                      ),
                      child: Center(
                        child: Text('resign request',style: TextStyle(
                            color: kprimary
                        ),),
                      ),
                    ),
                  )

                ],
                ),
              ),
            ),
          ),
        )
      ]),
    );
  }

  Future<void> openCylinder(BuildContext context) async {
    final DateTime? pickedDate = await showDatePicker(
        context: context,
        initialDate: currentDate.value,
        firstDate: currentDate.value,
        lastDate: DateTime(2050));
    if (pickedDate != null && pickedDate != currentDate) {
      currentDate.value = pickedDate;
    }
  }

  Future<String> takeImages(ImageSource source) async {
    print('${image.value}okkkkkkkkkkkkkkkkkkkkkkkkkkkkk');
    final picker = ImagePicker();
    final getImage = await picker.pickImage(source: source);
    if (getImage != null) {
      return getImage.path;
      print('${image.value}okkkkkkkkkkkkkkkkkkkkkkkkkkkkk');
      isGet.value = true;
    } else {
      return '';
    }
  }
}

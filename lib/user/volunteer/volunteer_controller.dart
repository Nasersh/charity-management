import 'package:charitysys/user/volunteer/volunteer_service.dart';
import 'package:get/get.dart';
class VolunteerController extends GetxController {
  var reason='no reason';
  VolunteerService service=VolunteerService();

  Future <void>resign()async
  {
      await service.resign(reason);
  }
  final List<String> gender = ["Male", "Female"];
  final List<String> job = ["Health", "Food","Education"];
  RxString selectedjob = 'Health'.obs;
  RxString selectedgender = 'Female'.obs;
  RxString name = ''.obs;
  RxString description = ''.obs;
  RxString certificateImage = ''.obs;
  RxString identicalImage = ''.obs;
  RxInt cat_id = 1.obs;
}
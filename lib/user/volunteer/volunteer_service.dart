
import '../../config/config.dart';
import '../../dashboard/volunteers_model.dart';
import '../../secureStorage.dart';
import 'package:http/http.dart' as http;

class VolunteerService {

  var urlAddProduct = Uri.parse(
      ServerConfig.domainNameServer + ServerConfig.VolunteerSubmit);

  addVolunteerRequest(VolunteerRequestModel volunteer) async {
    var _storage = SecureStorage();
    String? token = await _storage.read('token');
    print(volunteer.gender);
    print('${volunteer.cat_id} this is the cat_id');
    var data = {
      "category_id": volunteer.cat_id.toString(),
      'gender': volunteer.gender,
      'job': volunteer.job,
      'birth': volunteer.birth_date,
      'phone': volunteer.phone,
      'identity': volunteer.identicalImage,
      'certificate': volunteer.certificateImage
    };

    var headers = {
      'Authorization': 'Bearer $token',
    };
    // List<dynamic> images = [
    //   volunteer.certificateImage,
    //   volunteer.identicalImage
    // ];
    print(volunteer.certificateImage);
    print(volunteer.identicalImage);
    var request = http.MultipartRequest('POST',urlAddProduct);
    request.fields.addAll(data);
    request.files.addAll(
        [
          await http.MultipartFile.fromPath('certificate', volunteer.certificateImage),
          await http.MultipartFile.fromPath('identity', volunteer.identicalImage)
        ]
    );
    request.headers.addAll(headers);
    var response = await request.send();

    // print(volunteer.certificateImage);
    print(response.statusCode);
    print(response.reasonPhrase);
    //   print(response.body);


  }
  Future<void> resign(reason) async {
    var _storage=SecureStorage();
    String? token = await _storage.read('token');
    print(token);

    var url = Uri.parse(
        ServerConfig.domainNameServer + ServerConfig.VolunteerResign);

    var response = await http.post(url,
        headers: {
      'Authorization': 'Bearer ${token}',
      'Accept': 'application/json'
    },
      body: {
      'description' : reason
      }
    );
    print(response.statusCode);
    if(response.statusCode==200)
    {
      print('success');
    }
  }

}
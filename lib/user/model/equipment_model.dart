// To parse this JSON data, do
//
//     final equipModel = equipModelFromJson(jsonString);

import 'dart:convert';

List<EquipModel> equipModelFromJson(String str) => List<EquipModel>.from(json.decode(str).map((x) => EquipModel.fromJson(x)));

String equipModelToJson(List<EquipModel> data) => json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class EquipModel {
  EquipModel({
    required this.id,
    required this.categoryId,
    required this.name,
    required this.cost,
    this.donate,
    required this.count,
    this.image,
    this.createdAt,
    this.updatedAt,
  });

  int id;
  int categoryId;
  String name;
  int cost;
  dynamic donate;
  int count;
  dynamic image;
  dynamic createdAt;
  dynamic updatedAt;

  factory EquipModel.fromJson(Map<String, dynamic> json) => EquipModel(
    id: json["id"],
    categoryId: json["category_id"],
    name: json["name"],
    cost: json["cost"],
    donate: json["donate"],
    count: json["count"],
    image: json["image"],
    createdAt: json["created_at"],
    updatedAt: json["updated_at"],
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "category_id": categoryId,
    "name": name,
    "cost": cost,
    "donate": donate,
    "count": count,
    "image": image,
    "created_at": createdAt,
    "updated_at": updatedAt,
  };
}

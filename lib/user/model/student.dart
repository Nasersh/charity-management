// To parse this JSON data, do
//
//     final students = studentsFromJson(jsonString);

import 'dart:convert';

Students studentsFromJson(String str) => Students.fromJson(json.decode(str));

String studentsToJson(Students data) => json.encode(data.toJson());

class Students {
  Students({
    required this.students,
  });

  List<Student> students;

  factory Students.fromJson(Map<String, dynamic> json) => Students(
    students: List<Student>.from(json["Students"].map((x) => Student.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "Students": List<dynamic>.from(students.map((x) => x.toJson())),
  };
}

class Student {
  Student({
    this.id=0,
   required this.name,
    required this.birthDate,
    required this.academicYear,
    required this.cost,
    this.time,
    this.expDate,
    this.guaranteed=0,
    this.image,
    this.createdAt,
    this.updatedAt,
  });

  int id;
  String name;
  DateTime birthDate;
  String academicYear;
  int cost;
  dynamic time;
  dynamic expDate;
  int guaranteed;
  dynamic image;
  dynamic createdAt;
  dynamic updatedAt;

  factory Student.fromJson(Map<String, dynamic> json) => Student(
    id: json["id"],
    name: json["name"],
    birthDate: DateTime.parse(json["birth_date"]),
    academicYear: json["academic_year"],
    cost: json["cost"],
    time: json["time"],
    expDate: json["exp_date"],
    guaranteed: json["guaranteed"],
    image: json["image"],
    createdAt: json["created_at"],
    updatedAt: json["updated_at"],
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "name": name,
    "birth_date": "${birthDate.year.toString().padLeft(4, '0')}-${birthDate.month.toString().padLeft(2, '0')}-${birthDate.day.toString().padLeft(2, '0')}",
    "academic_year": academicYear,
    "cost": cost,
    "time": time,
    "exp_date": expDate,
    "guaranteed": guaranteed,
    "image": image,
    "created_at": createdAt,
    "updated_at": updatedAt,
  };
}

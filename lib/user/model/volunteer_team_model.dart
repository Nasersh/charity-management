// To parse this JSON data, do
//
//     final medicalTeam = medicalTeamFromJson(jsonString);

import 'dart:convert';

MedicalTeam medicalTeamFromJson(String str) => MedicalTeam.fromJson(json.decode(str));

String medicalTeamToJson(MedicalTeam data) => json.encode(data.toJson());

class MedicalTeam {
  MedicalTeam({
   required this.medicalTeam,
  });

  List<MedicalTeamElement> medicalTeam;

  factory MedicalTeam.fromJson(Map<String, dynamic> json) => MedicalTeam(
    medicalTeam: List<MedicalTeamElement>.from(json["MEDICAL_TEAM"].map((x) => MedicalTeamElement.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {

    "MEDICAL_TEAM": List<dynamic>.from(medicalTeam.map((x) => x.toJson())),
  };
}

class MedicalTeamElement {
  MedicalTeamElement({
   required this.id,
    this.categoryId=0,
   required this.userId,
   required this.birth,
   required this.gender,
   required this.job,
   this.identity='',
    this.certificate='',
    required this.phone,
    required this.accepted,
   required this.user,
  });

  int id;
  int categoryId;
  int userId;
  DateTime birth;
  String gender;
  String job;
  String identity;
  String certificate;
  String phone;
  int accepted;
  User user;

  factory MedicalTeamElement.fromJson(Map<String, dynamic> json) => MedicalTeamElement(
    id: json["id"],
    categoryId: json["category_id"],
    userId: json["user_id"],
    birth: DateTime.parse(json["birth"]),
    gender: json["gender"],
    job: json["job"],
    identity: json["identity"],
    certificate: json["certificate"],
    phone: json["phone"],
    accepted: json["accepted"],
    user: User.fromJson(json["user"]),
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "category_id": categoryId,
    "user_id": userId,
    "birth": "${birth.year.toString().padLeft(4, '0')}-${birth.month.toString().padLeft(2, '0')}-${birth.day.toString().padLeft(2, '0')}",
    "gender": gender,
    "job": job,
    "identity": identity,
    "certificate": certificate,
    "phone": phone,
    "accepted": accepted,
    "user": user.toJson(),
  };
}

class User {
  User({
    required this.id,
    required this.name,
    required this.email,
  });

  int id;
  String name;
  String email;

  factory User.fromJson(Map<String, dynamic> json) => User(
    id: json["id"],
    name: json["name"],
    email: json["email"],
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "name": name,
    "email": email,
  };
}



//////////////////////////////


CookTeam cookTeamFromJson(String str) => CookTeam.fromJson(json.decode(str));

String cookTeamToJson(CookTeam data) => json.encode(data.toJson());

class CookTeam {
  CookTeam({
    required this.cookTeam,
  });

  List<CookTeamElement> cookTeam;

  factory CookTeam.fromJson(Map<String, dynamic> json) => CookTeam(
    cookTeam: List<CookTeamElement>.from(json["FOOD_TEAM"].map((x) => CookTeamElement.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {

    "FOOD_TEAM": List<dynamic>.from(cookTeam.map((x) => x.toJson())),
  };
}

class CookTeamElement {
  CookTeamElement({
    required this.id,
    this.categoryId=0,
    required this.userId,
    required this.birth,
    required this.gender,
    required this.job,
    this.identity='',
    this.certificate='',
    required this.phone,
    required this.accepted,
    required this.user,
  });

  int id;
  int categoryId;
  int userId;
  DateTime birth;
  String gender;
  String job;
  String identity;
  String certificate;
  String phone;
  int accepted;
  User user;

  factory CookTeamElement.fromJson(Map<String, dynamic> json) => CookTeamElement(
    id: json["id"],
    categoryId: json["category_id"],
    userId: json["user_id"],
    birth: DateTime.parse(json["birth"]),
    gender: json["gender"],
    job: json["job"],
    identity: json["identity"],
    certificate: json["certificate"],
    phone: json["phone"],
    accepted: json["accepted"],
    user: User.fromJson(json["user"]),
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "category_id": categoryId,
    "user_id": userId,
    "birth": "${birth.year.toString().padLeft(4, '0')}-${birth.month.toString().padLeft(2, '0')}-${birth.day.toString().padLeft(2, '0')}",
    "gender": gender,
    "job": job,
    "identity": identity,
    "certificate": certificate,
    "phone": phone,
    "accepted": accepted,
    "user": user.toJson(),
  };
}

///////////////////////////////////

EducationTeam EducationTeamFromJson(String str) => EducationTeam.fromJson(json.decode(str));

String EducationTeamToJson(EducationTeam data) => json.encode(data.toJson());

class EducationTeam {
  EducationTeam({
    required this.educationTeam,
  });

  List<EducationTeamElement> educationTeam;

  factory EducationTeam.fromJson(Map<String, dynamic> json) => EducationTeam(
    educationTeam: List<EducationTeamElement>.from(json["EDUCATIONAL_TEAM"].map((x) => EducationTeamElement.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {

    "EDUCATIONAL_TEAM": List<dynamic>.from(educationTeam.map((x) => x.toJson())),
  };
}

class EducationTeamElement {
  EducationTeamElement({
    required this.id,
    this.categoryId=0,
    required this.userId,
    required this.birth,
    required this.gender,
    required this.job,
    this.identity='',
    this.certificate='',
    required this.phone,
    required this.accepted,
    required this.user,
  });

  int id;
  int categoryId;
  int userId;
  DateTime birth;
  String gender;
  String job;
  String identity;
  String certificate;
  String phone;
  int accepted;
  User user;

  factory EducationTeamElement.fromJson(Map<String, dynamic> json) => EducationTeamElement(
    id: json["id"],
    categoryId: json["category_id"],
    userId: json["user_id"],
    birth: DateTime.parse(json["birth"]),
    gender: json["gender"],
    job: json["job"],
    identity: json["identity"],
    certificate: json["certificate"],
    phone: json["phone"],
    accepted: json["accepted"],
    user: User.fromJson(json["user"]),
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "category_id": categoryId,
    "user_id": userId,
    "birth": "${birth.year.toString().padLeft(4, '0')}-${birth.month.toString().padLeft(2, '0')}-${birth.day.toString().padLeft(2, '0')}",
    "gender": gender,
    "job": job,
    "identity": identity,
    "certificate": certificate,
    "phone": phone,
    "accepted": accepted,
    "user": user.toJson(),
  };
}




// To parse this JSON data, do
//
//     final stationeryModel = stationeryModelFromJson(jsonString);

import 'dart:convert';

StationeryModel stationeryModelFromJson(String str) => StationeryModel.fromJson(json.decode(str));

String stationeryModelToJson(StationeryModel data) => json.encode(data.toJson());

class StationeryModel {
  StationeryModel({
    required this.stationery,
  });

  List<Stationery> stationery;

  factory StationeryModel.fromJson(Map<String, dynamic> json) => StationeryModel(
    stationery: List<Stationery>.from(json["stationery"].map((x) => Stationery.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "stationery": List<dynamic>.from(stationery.map((x) => x.toJson())),
  };
}

class Stationery {
  Stationery({
    required this.id,
    required this.categoryId,
    required this.name,
    this.count,
    required this.cost,
    required this.size,
    this.donate,
    required this.content,
    this.image,
    this.createdAt,
    this.updatedAt,
  });

  int id;
  int categoryId;
  String name;
  dynamic count;
  int cost;
  String size;
  dynamic donate;
  String content;
  dynamic image;
  dynamic createdAt;
  dynamic updatedAt;

  factory Stationery.fromJson(Map<String, dynamic> json) => Stationery(
    id: json["id"],
    categoryId: json["category_id"],
    name: json["name"],
    count: json["count"],
    cost: json["cost"],
    size: json["size"],
    donate: json["donate"],
    content: json["content"],
    image: json["image"],
    createdAt: json["created_at"],
    updatedAt: json["updated_at"],
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "category_id": categoryId,
    "name": name,
    "count": count,
    "cost": cost,
    "size": size,
    "donate": donate,
    "content": content,
    "image": image,
    "created_at": createdAt,
    "updated_at": updatedAt,
  };
}

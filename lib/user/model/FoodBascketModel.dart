// To parse this JSON data, do
//
//     final foodBascket = foodBascketFromJson(jsonString);

import 'dart:convert';

List<FoodBascket> foodBascketFromJson(String str) => List<FoodBascket>.from(json.decode(str).map((x) => FoodBascket.fromJson(x)));

String foodBascketToJson(List<FoodBascket> data) => json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class FoodBascket {
  FoodBascket({
    required this.id,
    required this.categoryId,
    required this.size,
     this.count,
    required this.cost,
    required this.donate,
    required this.content,
    this.image,
    this.createdAt,
    this.updatedAt,
  });

  int id;
  int categoryId;
  String size;
  dynamic count;
  int cost;
  int donate;
  String content;
  dynamic image;
  dynamic createdAt;
  dynamic updatedAt;

  factory FoodBascket.fromJson(Map<String, dynamic> json) => FoodBascket(
    id: json["id"],
    categoryId: json["category_id"],
    size: json["size"],
    count: json["count"],
    cost: json["cost"],
    donate: json["donate"],
    content: json["content"],
    image: json["image"],
    createdAt: json["created_at"],
    updatedAt: json["updated_at"],
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "category_id": categoryId,
    "size": size,
    "count": count,
    "cost": cost,
    "donate": donate,
    "content": content,
    "image": image,
    "created_at": createdAt,
    "updated_at": updatedAt,
  };
}

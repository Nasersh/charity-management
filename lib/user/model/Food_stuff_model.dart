// To parse this JSON data, do
//
//     final foodStuffModel = foodStuffModelFromJson(jsonString);

import 'dart:convert';

List<FoodStuffModel> foodStuffModelFromJson(String str) => List<FoodStuffModel>.from(json.decode(str).map((x) => FoodStuffModel.fromJson(x)));

String foodStuffModelToJson(List<FoodStuffModel> data) => json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class FoodStuffModel {
  FoodStuffModel({
   required this.id,
   required this.categoryId,
   required this.name,
   required this.cost,
   required this.count,
    this.donate,
    this.image,
    this.createdAt,
    this.updatedAt,
  });

  int id;
  int categoryId;
  String name;
  int cost;
  int count;
  dynamic donate;
  dynamic image;
  dynamic createdAt;
  dynamic updatedAt;

  factory FoodStuffModel.fromJson(Map<String, dynamic> json) => FoodStuffModel(
    id: json["id"],
    categoryId: json["category_id"],
    name: json["name"],
    cost: json["cost"],
    count: json["count"],
    donate: json["donate"],
    image: json["image"],
    createdAt: json["created_at"],
    updatedAt: json["updated_at"],
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "category_id": categoryId,
    "name": name,
    "cost": cost,
    "count": count,
    "donate": donate,
    "image": image,
    "created_at": createdAt,
    "updated_at": updatedAt,
  };
}

// To parse this JSON data, do
//
//     final patient = patientFromJson(jsonString);

import 'dart:convert';

import 'package:get/get.dart';

Patients patientFromJson(String str) => Patients.fromJson(json.decode(str));

String patientToJson(Patients data) => json.encode(data.toJson());

class Patients {
  Patients({
    required this.patients,
  });

  List<Patient> patients;

  factory Patients.fromJson(Map<String, dynamic> json) => Patients(
    patients: List<Patient>.from(json["patients"].map((x) => Patient.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "patients": List<dynamic>.from(patients.map((x) => x.toJson())),
  };
}

class Patient {
  Patient({
    required this.id,
    required this.categoryId,
    required this.name,
    required this.birthDate,
    required this.medicalCondition,
    required this.cost,
    required this.donate,
    required this.gender,
     this.description,
    this.image,
    this.createdAt,
    this.updatedAt,
  });

  int id;
  int categoryId;
  String name;
  DateTime birthDate;
  String medicalCondition;
  int cost;
  int donate;
  String gender;
  dynamic image;
  dynamic createdAt;
  dynamic updatedAt;
  dynamic description;

  factory Patient.fromJson(Map<String, dynamic> json) => Patient(
    id: json["id"],
    categoryId: json["category_id"],
    name: json["name"],
    birthDate: DateTime.parse(json["birth_date"]),
    medicalCondition: json["medical_condition"],
    cost: json["cost"],
    donate: json["donate"],
    gender: json["gender"],
    description: json["description"],
    image: json["image"],
    createdAt: json["created_at"],
    updatedAt: json["updated_at"],
  );
//ايماااااااااااااالو
  Map<String, dynamic> toJson() => {
    "id": id,
    "category_id": categoryId,
    "name": name,
    "birth_date": "${birthDate.year.toString().padLeft(4, '0')}-${birthDate.month.toString().padLeft(2, '0')}-${birthDate.day.toString().padLeft(2, '0')}",
    "medical_condition": medicalCondition,
    "cost": cost,
    "donate": donate,
    "gender": gender,
    "description": description,
    "image": image,
    "created_at": createdAt,
    "updated_at": updatedAt,
  };
}

// To parse this JSON data, do
//
//     final post = postFromJson(jsonString);

import 'dart:convert';

Post postFromJson(String str) => Post.fromJson(json.decode(str));

String postToJson(Post data) => json.encode(data.toJson());

class Post {
  Post({
   required this.gallery,
  });

  List<Gallery> gallery;

  factory Post.fromJson(Map<String, dynamic> json) => Post(
    gallery: List<Gallery>.from(json["gallery"].map((x) => Gallery.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "gallery": List<dynamic>.from(gallery.map((x) => x.toJson())),
  };
}

class Gallery {
  Gallery({
    this.id=0,
    this.userId,
  required  this.name,
    this.image='',
    this.createdAt,
    this.updatedAt,
    this.count=0,
  });

  int id;
  dynamic userId;
  String name;
  String image;
  dynamic createdAt;
  dynamic updatedAt;
  int count;

  factory Gallery.fromJson(Map<String, dynamic> json) => Gallery(
    id: json["id"],
    userId: json["user_id"],
    name: json["name"],
    image: json["image"],
    createdAt: json["created_at"],
    updatedAt: json["updated_at"],
    count: json["count"],
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "user_id": userId,
    "name": name,
    "image": image,
    "created_at": createdAt,
    "updated_at": updatedAt,
    "count": count,
  };
}

// To parse this JSON data, do
//
//     final pin = pinFromJson(jsonString);

import 'dart:convert';

Pin pinFromJson(String str) => Pin.fromJson(json.decode(str));

String pinToJson(Pin data) => json.encode(data.toJson());

class Pin {
  Pin({
   required this.code,
  });

  int code;

  factory Pin.fromJson(Map<String, dynamic> json) => Pin(
    code: json["code"],
  );

  Map<String, dynamic> toJson() => {
    "code": code,
  };
}
// To parse this JSON data, do
//
//     final kitchenSet = kitchenSetFromJson(jsonString);

import 'dart:convert';

KitchenSet kitchenSetFromJson(String str) => KitchenSet.fromJson(json.decode(str));

String kitchenSetToJson(KitchenSet data) => json.encode(data.toJson());

class KitchenSet {
  KitchenSet({
    required this.kitchenSet,
  });

  List<KitchenSetElement> kitchenSet;

  factory KitchenSet.fromJson(Map<String, dynamic> json) => KitchenSet(
    kitchenSet: List<KitchenSetElement>.from(json["kitchen set"].map((x) => KitchenSetElement.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "kitchen set": List<dynamic>.from(kitchenSet.map((x) => x.toJson())),
  };
}

class KitchenSetElement {
  KitchenSetElement({
    required this.id,
    required this.categoryId,
    required this.name,
    required this.count,
    required this.cost,
    this.donate,
    this.image,
    this.createdAt,
    this.updatedAt,
  });

  int id;
  int categoryId;
  String name;
  int count;
  int cost;
  dynamic donate;
  dynamic image;
  dynamic createdAt;
  dynamic updatedAt;

  factory KitchenSetElement.fromJson(Map<String, dynamic> json) => KitchenSetElement(
    id: json["id"],
    categoryId: json["category_id"],
    name: json["name"],
    count: json["count"],
    cost: json["cost"],
    donate: json["donate"],
    image: json["image"],
    createdAt: json["created_at"],
    updatedAt: json["updated_at"],
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "category_id": categoryId,
    "name": name,
    "count": count,
    "cost": cost,
    "donate": donate,
    "image": image,
    "created_at": createdAt,
    "updated_at": updatedAt,
  };
}

// // To parse this JSON data, do
// //
// //     final Medicine = MedicineFromJson(jsonString);
//
// import 'dart:convert';
//
// Medicins MedicineFromJson(String str) => Medicins.fromJson(json.decode(str));
//
// String MedicineToJson(Medicins data) => json.encode(data.toJson());
//
// class Medicins {
//   Medicins({
//     required this.medicins,
//   });
//
//   List<Medicine> medicins;
//
//   factory Medicins.fromJson(Map<String, dynamic> json) => Medicins(
//     medicins: List<Medicine>.from(json["Medicins"].map((x) => Medicine.fromJson(x))),
//   );
//
//   Map<String, dynamic> toJson() => {
//     "Medicins": List<dynamic>.from(medicins.map((x) => x.toJson())),
//   };
// }
//
// class Medicine {
//   Medicine({
//     required this.id,
//     required this.categoryId,
//     required this.name,
//     required this.cost,
//     required this.donate,
//     required this.gender,
//     required this.description,
//     this.image,
//     this.createdAt,
//     this.updatedAt,
//   });
//
//   int id;
//   int categoryId;
//   String name;
//   int cost;
//   int donate;
//   String gender;
//   String description;
//   dynamic image;
//   dynamic createdAt;
//   dynamic updatedAt;
//
//   factory Medicine.fromJson(Map<String, dynamic> json) => Medicine(
//     id: json["id"],
//     categoryId: json["category_id"],
//     name: json["name"],
//     cost: json["cost"],
//     donate: json["donate"],
//     gender: json["gender"],
//     description: json["description"],
//     image: json["image"],
//     createdAt: json["created_at"],
//     updatedAt: json["updated_at"],
//   );
//
//   Map<String, dynamic> toJson() => {
//     "id": id,
//     "category_id": categoryId,
//     "name": name,
//     "cost": cost,
//     "donate": donate,
//     "gender": gender,
//     "description": description,
//     "image": image,
//     "created_at": createdAt,
//     "updated_at": updatedAt,
//   };
// }
// To parse this JSON data, do
//
//     final medicine = medicineFromJson(jsonString);

import 'dart:convert';

List<Medicine> medicineFromJson(String str) => List<Medicine>.from(json.decode(str).map((x) => Medicine.fromJson(x)));

String medicineToJson(List<Medicine> data) => json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class Medicine {
  Medicine({
    required this.id,
    required this.categoryId,
    required this.name,
    required this.cost,
    this.count=0,
    this.donate=0,
    this.image,
    this.createdAt,
    this.updatedAt,
  });

  int id;
  int categoryId;
  String name;
  int cost;
  int count;
  int donate;
  dynamic image;
  dynamic createdAt;
  dynamic updatedAt;

  factory Medicine.fromJson(Map<String, dynamic> json) => Medicine(
    id: json["id"],
    categoryId: json["category_id"],
    name: json["name"],
    cost: json["cost"],
    count: json["count"],
    donate: json["donate"],
    image: json["image"],
    createdAt: json["created_at"],
    updatedAt: json["updated_at"],
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "category_id": categoryId,
    "name": name,
    "cost": cost,
    "count": count,
    "donate": donate,
    "image": image,
    "created_at": createdAt,
    "updated_at": updatedAt,
  };
}

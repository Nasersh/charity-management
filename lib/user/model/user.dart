// To parse this JSON data, do
//
//     final user = userFromJson(jsonString);

import 'dart:convert';

User userFromJson(String str) => User.fromJson(json.decode(str));

String userToJson(User data) => json.encode(data.toJson());

class User {
  User({
    required this.user,
  });

  Usermodel user;

  factory User.fromJson(Map<String, dynamic> json) => User(
    user: Usermodel.fromJson(json["user"]),
  );

  Map<String, dynamic> toJson() => {
    "user": user.toJson(),
  };
}

class Usermodel {
  Usermodel({
    this.id=0,
    this.name='',
    this.password='',
    this.passwordConfirmation='',
    this.email='',
    this.emailVerifiedAt,
    this.profileImgUrl='',
    this.phone='',
    this.facebookUrl='',
    this.whatsappUrl='',

  });

  int id;
  String name;
  String password;
  String passwordConfirmation;
  String email;
  dynamic emailVerifiedAt;
  String profileImgUrl;
  String phone;
  String facebookUrl;
  String whatsappUrl;


  factory Usermodel.fromJson(Map<String, dynamic> json) => Usermodel(
    id: json["id"],
    name: json["name"],
    email: json["email"],
    emailVerifiedAt: json["email_verified_at"],
    profileImgUrl: json["profile_img_url"],
    phone: json["phone"],
    facebookUrl: json["facebook_url"],
    whatsappUrl: json["whatsapp_url"],

  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "name": name,
    "email": email,
    "email_verified_at": emailVerifiedAt,
    "profile_img_url": profileImgUrl,
    "phone": phone,
    "facebook_url": facebookUrl,
    "whatsapp_url": whatsappUrl,

  };
}

// import 'dart:async';
//
// import 'package:animator/animator.dart';
// import 'package:flutter/material.dart';
// import 'package:get/get.dart';
// import 'package:animated_text_kit/animated_text_kit.dart';
//
// import '../configrations/consts.dart';
//
// class SplashScreen extends StatefulWidget {
//   const SplashScreen({Key? key}) : super(key: key);
//
//   @override
//   _SplashScreenState createState() => _SplashScreenState();
// }
//
// class _SplashScreenState extends State<SplashScreen> {
//   @override
//   RxBool syrian = false.obs;
//
//   Widget build(BuildContext context) {
//
//     Timer(
//       const Duration(seconds: 6),
//         ()=> Get.toNamed("/pageV")
//     );
//
//     Size size = MediaQuery.of(context).size;
//
//     return Scaffold(
//       body: SafeArea(
//         child: Container(
//           height: double.infinity,
//           width: double.infinity,
//           decoration: BoxDecoration(
//             gradient: LinearGradient(
//                 begin: Alignment.topLeft,
//                 end: Alignment.bottomRight,
//                 colors: [littleGrey, Colors.white.withOpacity(.2), deepGrey]),
//           ),
//           child: Column(
//             children: [
//               Expanded(
//                 flex: 2,
//                 child: Column(
//                   children: [
//                     const Spacer(flex: 2),
//                     Expanded(
//                       child: Animator(
//                         duration: const Duration(seconds: 1),
//                         cycles: 1,
//                         tweenMap: {
//                           'motion': Tween<Offset>(
//                             begin: const Offset(100, -100),
//                             end: const Offset(0, 0),
//                           ),
//                           'rotation': Tween<double>(begin: 0, end: 2 * 3.14),
//                           'opacity': Tween<double>(begin: 0, end: 1)
//                         },
//                         endAnimationListener: (anim) {
//                           syrian.value = true;
//                           setState(() {
//                             check['syria'] = true;
//                           });
//                           print(check['syria']);
//                         },
//                         builder: (_, anim, __) => Transform.translate(
//                           offset: anim.getValue('motion'),
//                           child: FadeTransition(
//                             opacity: anim.getAnimation('opacity'),
//                             child: Container(
//                               width: double.infinity,
//                               alignment: const Alignment(-0.8, 0),
//                               child: Transform.rotate(
//                                 angle: anim.getValue('rotation'),
//                                 child: Text(
//                                   "Syrian",
//                                   style: style1,
//                                 ),
//                               ),
//                             ),
//                           ),
//                         ),
//                       ),
//                     ),
//                     Expanded(
//                       child: check['syria'] ?? true
//                           ? Animator(
//                               duration: const Duration(seconds: 1),
//                               cycles: 1,
//                               tweenMap: {
//                                 'opacity': Tween<double>(begin: 0, end: 1),
//                                 'motion': Tween<Offset>(
//                                     begin: const Offset(45, 0),
//                                     end: const Offset(0, 0))
//                               },
//                               endAnimationListener: (_) {
//                                 setState(() {
//                                   check['charity'] = true;
//                                 });
//                               },
//                               builder: (_, anim, __) => FadeTransition(
//                                 opacity: anim.getAnimation('opacity'),
//                                 child: Transform.translate(
//                                   offset: anim.getValue('motion'),
//                                   child: Container(
//                                     width: double.infinity,
//                                     alignment: const Alignment(-0.6, 0),
//                                     child: Text(
//                                       "Charity",
//                                       style: style1,
//                                     ),
//                                   ),
//                                 ),
//                               ),
//                             )
//                           : Container(),
//                     ),
//                     Expanded(
//                       child: check['charity'] ?? true
//                           ? AnimatedTextKit(
//                               onFinished: () => setState(() {
//                                 check['rest'] = true;
//                               }),
//                               pause: const Duration(seconds: 0),
//                               totalRepeatCount: 1,
//                               animatedTexts: [
//                                 TyperAnimatedText(
//                                   "Always with you",
//                                   textStyle: style1,
//                                 )
//                               ],
//                             )
//                           : Container(),
//                     ),
//                     const Spacer(flex: 2)
//                   ],
//                 ),
//               ),
//               Expanded(
//                 flex: 1,
//                 child: Stack(
//                   clipBehavior: Clip.none,
//                   children: [
//                     Positioned(
//                       top: -80,
//                       right: 0,
//                       child: check['rest'] ?? true
//                           ? Animator(
//                               duration: const Duration(seconds: 2),
//                               cycles: 1,
//                               tweenMap: {
//                                 'scale': Tween<double>(begin: 2, end: 1)
//                               },
//                               builder: (_, anim, __) => Transform.scale(
//                                 scale: anim.getValue('scale'),
//                                 child: Image.asset(
//                                   'assets/familyy.png',
//                                   width: 300,
//                                   height: 300,
//                                 ),
//                               ),
//                             )
//                           : Container(),
//                     ),
//                   ],
//                 ),
//               )
//             ],
//           ),
//         ),
//       ),
//     );
//   }
// }
//
// class ClapIt extends CustomClipper<Path> {
//   Path getClip(Size size) {
//     var path = Path();
//
//     path.lineTo(0, size.height);
//     path.quadraticBezierTo(
//         size.width / 2, size.height - 200, size.width, size.height);
//     path.lineTo(size.width, 0);
//     path.close();
//
//     return path;
//   }
//
//   @override
//   bool shouldReclip(covariant CustomClipper<Path> oldClipper) {
//     // TODO: implement shouldReclip
//     throw UnimplementedError();
//   }
// }

import 'package:get/get.dart';

import 'equip_donatio_service.dart';

class EquipDonationController extends GetxController
{
  var id;
  var current;
  var total;
  var count=0.obs;
  @override
  void onInit() {

    // TODO: implement onInit
    total=0;
    super.onInit();
  }
  EquipDonationService _service=new EquipDonationService();
  Donation() async
  {
    current=await _service.Donate(count.toString(),id);
  }
}
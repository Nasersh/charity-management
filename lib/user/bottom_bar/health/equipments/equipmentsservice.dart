
import 'package:http/http.dart' as http;

import '../../../../config/config.dart';
import '../../../../secureStorage.dart';
import '../../../model/equipment_model.dart';

class EquipService
{
  var message;
  var url = Uri.parse(ServerConfig.domainNameServer + ServerConfig.showallEquip);

  Future<List<EquipModel>> getEquip() async {
    var _storage = SecureStorage();
    String? token = await _storage.read('token');
    var response = await http.get(url, headers: {
      'Accept': 'application/json',
      'Authorization': 'Bearer $token'
    },);
    print(response.statusCode);

    if (response.statusCode == 200) {
      var Equips = equipModelFromJson(response.body);
      return Equips;
    }
    else {
      return [];
    }
  }
}
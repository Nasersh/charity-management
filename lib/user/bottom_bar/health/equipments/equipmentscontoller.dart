import 'package:get/get.dart';
import '../../../model/equipment_model.dart';
import 'equipmentsservice.dart';

class EquipController extends GetxController
{
  var equiplist =<EquipModel>[].obs;
  var Equip=<EquipModel>[];

  var id=0.obs;

  EquipService _service= EquipService();
  var isloading=true.obs;

  @override
  void onInit() {
    showEquip();
    //loadProductsFromRepo(id.value);
    super.onInit();
  }

  void showEquip() async {
    try {
      isloading(true);
      Equip= await _service.getEquip();
      if (Equip != null) {
        equiplist.value = Equip;
      }
    } finally {
      isloading(false);
    }
  }

}
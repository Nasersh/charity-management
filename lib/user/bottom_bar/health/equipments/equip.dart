import 'dart:math';
import 'package:confetti/confetti.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:get/get.dart';
import 'package:get/get_core/src/get_main.dart';
import 'package:percent_indicator/linear_percent_indicator.dart';
import '../../../../confetti_controller.dart';
import '../../../../configrations/const/const.dart';
import 'donations/equip_donation_controller.dart';
import 'equipmentscontoller.dart';
class Equipments extends StatelessWidget {
   Equipments({Key? key}) : super(key: key);
   EquipController controller = Get.put(EquipController());
   EquipDonationController donatecont =Get.put(EquipDonationController());
   final cf = Get.find<ConfController>();

   @override
  Widget build(BuildContext context) {
    return Stack(
      children:[
      Scaffold(
          backgroundColor: kprimary,
          body: SafeArea(
            child: Column(
              children: [
                Stack(

                    children: [
                      Container(
                        width: double.infinity,
                        height: MediaQuery.of(context).size.height/4,
                        decoration: BoxDecoration(
                            color: kblue,
                            borderRadius: BorderRadius.only(bottomLeft:Radius.circular(30),bottomRight:Radius.circular(30))
                        ),
                      ),
                      Column(
                        children: [
                          SizedBox(
                            height: MediaQuery.of(context).size.height/9,
                          ),
                          Center(
                            child: Text(
                              'Equipmentss . . .',
                              style: TextStyle(
                                  color: kprimary,
                                  fontSize: 30,
                                  fontStyle: FontStyle.italic,
                                  fontWeight: FontWeight.bold),
                            ),
                          ),
                        ],
                      ),
                    ]
                ),
                Expanded(child: Obx(() => _buildProductsList())),
              ],
            ),
          ),
      ),
        Align(
          alignment: Alignment.topCenter,
          child: ConfettiWidget(
            colors: const [
              kprimary,
              kblue,
              kstorm,
              Colors.white12

            ],
            confettiController: cf.topController,
            blastDirection: pi / 2,
            maxBlastForce: 5,
            minBlastForce: 1,
            emissionFrequency: 0.03,
            numberOfParticles: 10,
            // shouldLoop:true,
            gravity: .1,
          ),
        ),
      ]
      );
  }

  ListView _buildProductsList() {
    return ListView.builder(
      shrinkWrap: true,
      itemCount: controller.equiplist.length,
      padding: EdgeInsets.only(top: 16),
      itemBuilder: (context, index) => Padding(
        padding: const EdgeInsets.symmetric(vertical: 10),
        child: Container(
          height: MediaQuery.of(context).size.height / 6,
          decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(15),
              boxShadow: [kDefaultShadow]),
          child: Padding(
            padding: const EdgeInsets.symmetric(vertical: 8.0),
            child: ListTile(
              leading: const CircleAvatar(
                radius: 30,
                backgroundColor: Colors.black45,
              ),
              title: Padding(
                padding: const EdgeInsets.only(top: 10),
                child: Text('${controller.equiplist[index].name}'),
              ),
              subtitle: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Padding(
                    padding: const EdgeInsets.only(top: 5, bottom: 10),
                    child:
                    Text('${controller.equiplist[index].cost}'),
                  ),
                  LinearPercentIndicator(
                    barRadius: Radius.circular(040),
                    width: MediaQuery.of(context).size.width / 2.4,
                    animation: true,
                    animationDuration: 800,
                    lineHeight: 10,
                    percent: controller.equiplist[index].donate/controller.equiplist[index].cost,
                    progressColor: kblue,
                    backgroundColor: kbblue,
                  ),
                  SizedBox(
                    height: MediaQuery.of(context).size.height / 100,
                  ),
                  Text(
                    '${controller.equiplist[index].donate}/${controller.equiplist[index].count}',
                    style: TextStyle(color: kstorm),
                  ),
                ],
              ),
              trailing: Padding(
                padding: const EdgeInsets.only(top: 8.0, right: 5),
                child: Container(
                  height: 50,
                  width: 75,
                  decoration: BoxDecoration(
                      color: kstorm, borderRadius: BorderRadius.circular(10)),
                  child: MaterialButton(
                      onPressed: () {
                        var id=controller.equiplist[index].id;
                        showModalBottomSheet(
                            context: context,
                            builder: (BuildContext context) {
                              return Container(
                                height: MediaQuery.of(context).size.height / 2,
                                decoration:   const BoxDecoration(
                                    image: DecorationImage(image: AssetImage('images/naser.jpg'),fit: BoxFit.cover),
                                    color: kprimary,
                                    borderRadius: BorderRadius.only(
                                        topRight: Radius.circular(25),
                                        topLeft: Radius.circular(25))),
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    Padding(
                                        padding: const EdgeInsets.only(
                                            top: 15.0, right: 10, left: 10),
                                        child: Obx(()=> Row(
                                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                          children: [
                                            IconButton(
                                                color: kblue,
                                                onPressed: (){donatecont.count--;}, icon: const Icon(
                                              FontAwesomeIcons.minus,
                                            )),
                                            Text(
                                              '${donatecont.count}',style: const TextStyle(
                                                fontSize: 30
                                            ),
                                            ),
                                            IconButton(onPressed: (){donatecont.count++;}, icon: const Icon(
                                                FontAwesomeIcons.plus
                                            )),
                                          ],
                                        ),
                                        )
                                    ),
                                    SizedBox(
                                      height:
                                      MediaQuery.of(context).size.height /
                                          20,
                                    ),
                                    ElevatedButton(
                                        onPressed: () {
                                          if(donatecont.count.value*controller.equiplist[index].cost<=controller.equiplist[index].cost-controller.equiplist[index].donate) {
                                            donatecont.id = id;
                                            _Equpdonate();
                                            cf.topController.play();
                                          }
                                        },
                                        child: Container(
                                          decoration: BoxDecoration(
                                              color: kstorm,
                                              borderRadius:
                                              BorderRadius.circular(20)),
                                          height: MediaQuery.of(context)
                                              .size
                                              .height /
                                              14,
                                          width: MediaQuery.of(context)
                                              .size
                                              .width /
                                              3,
                                          child: const Center(
                                            child: Text(
                                              'Donate',
                                              style: TextStyle(
                                                  color: kprimary,
                                                  fontSize: 20),
                                            ),
                                          ),
                                        )),
                                    SizedBox(
                                      height:
                                      MediaQuery.of(context).size.height /
                                          40,
                                    ),
                                    ElevatedButton(
                                        onPressed: () {
                                          Get.back();
                                        },
                                        child: Container(
                                          decoration: BoxDecoration(
                                              color: kblue,
                                              borderRadius:
                                              BorderRadius.circular(20)),
                                          height: MediaQuery.of(context)
                                              .size
                                              .height /
                                              15,
                                          width: MediaQuery.of(context)
                                              .size
                                              .width /
                                              4,
                                          child: Center(
                                            child: Text(
                                              'cancel',
                                              style: TextStyle(color: kprimary),
                                            ),
                                          ),
                                        ))
                                  ],
                                ),
                              );
                            });
                      },
                      child:  Center(
                          child: Icon(
                            FontAwesomeIcons.plus,
                            color: kprimary,
                          ))),
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
   void _Equpdonate()
   {
     donatecont.Donation();
   }
}

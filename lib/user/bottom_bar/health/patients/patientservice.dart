import 'package:http/http.dart' as http;

import '../../../../config/config.dart';
import '../../../../dashboard/completed/health/patients/patient_model.dart';
import '../../../../secureStorage.dart';




class PatientsService
{
  var message;
  var url = Uri.parse(ServerConfig.domainNameServer + ServerConfig.showallPatients);

  Future<List<Patient>> getPatient() async{
    var _storage=SecureStorage();
    String? token = await _storage.read('token');
    var response = await http.get(url,headers:  {
      'Accept' : 'application/json',
      'Authorization'  :  'Bearer $token'
    },);
    print(response.statusCode);

    if(response.statusCode == 200){
      var patients = patientFromJson(response.body);
      return patients.patients;
    }
    else {
      return [];
    }
  }
  //
  // Future<List<Patient>> loadProductsFromApi(int index) async {
  //   var response = await http.get(Uri.parse(ServerConfig.domainNameServer+ServerConfig.showCategoryProducts + '${index}'),
  //     headers: {
  //       'Accept': 'application/json',
  //     },
  //   );
  //   print(response.statusCode);
  //   //  print(response.body);
  //
  //   if(response.statusCode == 200){
  //     var porductscat = productsFromJson(response.body);
  //     return porductscat.products;
  //   }
  //   else {
  //     return [];
  //   }
  }

  //
  // Future<List<Patient>> SearchProducts(String name) async {
  //   var response = await http.get(Uri.parse(ServerConfig.domainNameServer+ServerConfig.SearchProducts + '${name}'),
  //     headers: {
  //       'Accept': 'application/json',
  //     },
  //   );
  //   print(response.statusCode);
  //   //  print(response.body);
  //
  //   if(response.statusCode == 200){
  //     var porductsearch = productsFromJson(response.body);
  //     return porductsearch.products;
  //   }
  //   else {
  //     return [];
  //   }
  // }

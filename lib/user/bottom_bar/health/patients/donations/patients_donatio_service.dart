import 'dart:convert';
import 'package:http/http.dart' as http;

import '../../../../../config/config.dart';
import '../../../../../secureStorage.dart';


class PatientDonationService
{
  var donate;
  var message;
  var index;
  Future<String?> Donate(donation,id) async{
    index=id.toString();
    print(donation);
    donate=int.parse(donation);
    print(index);
    var url=Uri.parse(ServerConfig.domainNameServer+ServerConfig.DonatePatient+'${index}');
    var _storage=SecureStorage();
    String? token = await _storage.read('token');
    print(token);
    var response = await http.post(url,
        headers:
        {
          'Accept' : 'application/json',
          'Authorization'  :  'Bearer $token'
        },
        body:
        {
          'donate' : donation.toString()
        }
    );
    print(response.statusCode);
    var jsonres=jsonDecode(response.body);

    if(response.statusCode == 200){
      print('successful');
      // return ' ';
      return message=jsonres["message"];
    }
    else {
      print('fail');
      return '';
    }
  }
}
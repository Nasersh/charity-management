import 'package:get/get.dart';

import 'patients_donatio_service.dart';

class PatientsDonationController extends GetxController
{
  var id;
  var donation;
  var current;
  @override
  void onInit() {
    // TODO: implement onInit
    donation=0;
    super.onInit();
  }
  PatientDonationService _service=new PatientDonationService();
  Donation(money) async
  {
    current=await _service.Donate(donation,id);
  }
}
import 'package:charitysys/user/bottom_bar/health/patients/patientservice.dart';
import 'package:get/get.dart';

import '../../../../dashboard/completed/health/patients/patient_model.dart';


class Patientscontroller extends GetxController
{
  var patientslist =<Patient>[].obs;
  var patients=<Patient>[];
  var donate=0;
  var dnt=0.obs;
  var id=0.obs;

  PatientsService _service= PatientsService();
  var isloading=true.obs;

  @override
  void onInit() {
    showPatients();
    //loadProductsFromRepo(id.value);
    super.onInit();
  }


  void showPatients() async {
    try {
      isloading(true);
       patients = await _service.getPatient();
      if (patients != null) {
        patientslist.value = patients;
      }
    } finally {
      isloading(false);
    }
  }


}
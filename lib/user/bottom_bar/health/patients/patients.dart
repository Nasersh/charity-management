import 'dart:math';
import 'package:charitysys/confetti_controller.dart';
import 'package:charitysys/user/bottom_bar/health/patients/patientscontroller.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:get/get.dart';
import 'package:get/get_core/src/get_main.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:percent_indicator/linear_percent_indicator.dart';
import '../../../../configrations/const/const.dart';
import '../../../../configrations/const/widgets/text-input.dart';
import '../../../wallet/wallet_controller.dart';
import 'donations/patients_donation_controller.dart';
import 'package:confetti/confetti.dart';


class Patients extends StatelessWidget {
  Patients({Key? key}) : super(key: key);

  Patientscontroller controller = Get.put(Patientscontroller());
  WalletController wc=Get.put(WalletController());
  PatientsDonationController donatecont=Get.put(PatientsDonationController());
  final cf = Get.find<ConfController>();
  final _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
      Scaffold(
      backgroundColor: kprimary,
      body: SafeArea(
        child: Obx(()=>
            Column(
              children: [
                Stack(

                    children: [
                      Container(
                        width: double.infinity,
                        height: MediaQuery.of(context).size.height/4,
                        decoration: const BoxDecoration(
                            color: kblue,
                            borderRadius: BorderRadius.only(bottomLeft:Radius.circular(30),bottomRight:Radius.circular(30))
                        ),
                      ),
                      Column(
                        children: [
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Padding(
                                padding: const EdgeInsets.only(left: 15),
                                child:  IconButton(
                                  icon:const Icon(
                                    FontAwesomeIcons.arrowLeft,
                                    color: kprimary,
                                  ),
                                  // Icons.arrow_back,
                                  onPressed: () {
                                    Get.back();
                                  },
                                ),
                              ),


                              // child: Obx(() => _buildProductsList())
                              Padding(
                                padding: EdgeInsets.all(MediaQuery.of(context).size.height/30),
                                child: Obx(()=>Text('wallet ${wc.wallet.value}',style: GoogleFonts.varela(
                                    color: kprimary,
                                    fontSize: 16
                                ),),)
                              ),
                            ],
                          ),
                          SizedBox(
                            height: MediaQuery.of(context).size.height/15,
                          ),
                          const Padding(
                            padding: EdgeInsets.only(bottom: 10,left: 40),
                            child: Align(
                              alignment: Alignment.bottomLeft,
                              child: Text(
                                'Patients . . .',
                                style: TextStyle(
                                    color: kprimary,
                                    fontSize: 30,
                                    fontStyle: FontStyle.italic,
                                    fontWeight: FontWeight.bold),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ]
                ),
                Expanded(child:!controller.isloading.value?
                 Obx(()=> _buildProductsList()) : Center(child:CircularProgressIndicator()
                )
                )
                  ],
            ),
        ),
      ),
    ),
        Align(
          alignment: Alignment.topCenter,
          child: ConfettiWidget(
            colors: const [
              kprimary,
              kblue,
              kstorm,
              Colors.white12

            ],
            confettiController: cf.topController,
            blastDirection: pi / 2,
            maxBlastForce: 5,
            minBlastForce: 1,
            emissionFrequency: 0.03,
            numberOfParticles: 10,
            // shouldLoop:true,
            gravity: .1,
          ),
        ),

      ],
    );
  }

  Widget _buildProductsList() {
    return ListView.builder(
      shrinkWrap: true,
      itemCount: controller.patientslist.value.length,
      padding: EdgeInsets.only(top: 16),
      itemBuilder: (context, index) => Padding(
        padding: const EdgeInsets.symmetric(vertical: 10),
        child: Container(
          height: MediaQuery.of(context).size.height / 5,
          decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(15),
              boxShadow: [kDefaultShadow]),
          child: Padding(
            padding: const EdgeInsets.symmetric(vertical: 8.0),
            child: ExpansionTile(
              children: [
                Text('psycho needs your help !!')
              ],
              leading: const CircleAvatar(
                radius: 30,
                backgroundColor: Colors.black45,
              ),
              title: Padding(
                padding: const EdgeInsets.only(top: 10),
                child: Text('${controller.patientslist[index].name}',style: TextStyle(
                  fontWeight:  FontWeight.bold,color: kblue
                ),),
              ),
              subtitle: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Padding(
                    padding: const EdgeInsets.only(top: 5, bottom: 10),
                    child:
                        Text('${controller.patientslist[index].medicalCondition}',style: TextStyle(
                          fontWeight: FontWeight.w300
                        ),),
                  ),
                  SizedBox(
                    height: MediaQuery.of(context).size.height/100,
                  ),
                  Obx(
                      ()=> LinearPercentIndicator(
                      barRadius: Radius.circular(040),
                      width: MediaQuery.of(context).size.width / 2.3,
                      animation: true,
                      animationDuration: 800,
                      lineHeight: 7,
                      percent:(controller.patientslist[index].donate+controller.dnt.value) /controller.patientslist[index].cost,
                      progressColor: kblue,
                      backgroundColor: kbblue,
                    ),
                  ),
                  SizedBox(
                    height: MediaQuery.of(context).size.height / 100,
                  ),
                     Obx(()=>
                       Text(
                         '${controller.patientslist[index].donate}\$/${controller.patientslist[index].cost}',
                         style: TextStyle(color: kstorm),
                       )
                     )
                ],
              ),
              trailing: Padding(
                padding: const EdgeInsets.only(top: 20.0),
                child: Container(
                  height: MediaQuery.of(context).size.height/20,
                  width: MediaQuery.of(context).size.width/4.8,
                  decoration: BoxDecoration(
                      color: kstorm, borderRadius: BorderRadius.circular(10)),
                  child: MaterialButton(
                      onPressed: () {
                        var id=controller.patientslist[index].id;
                        showModalBottomSheet(
                            context: context,
                            builder: (BuildContext context) {
                              return Container(
                                height: MediaQuery.of(context).size.height / 2,
                                decoration:   const BoxDecoration(
                                    // border: Border.all(
                                    //     color: kstorm,
                                    //     width: 6.0
                                    // ),
                                  image: DecorationImage(image: AssetImage('images/naser.jpg'),fit: BoxFit.cover),
                                    borderRadius: BorderRadius.only(
                                        topRight: Radius.circular(25),
                                        topLeft: Radius.circular(25))),
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                     Padding(
                                      padding: const EdgeInsets.only(
                                            left: 10,right: 10),
                                       child:

                                         Form(
                                           autovalidateMode: AutovalidateMode.onUserInteraction,
                                           key: _formKey,
                                           child: TextInput(
                                             onchange: (value ) { donatecont.donation=value;},
                                             inputType: TextInputType.number,
                                             hint: 'enter the amount',
                                             icon: FontAwesomeIcons.moneyCheckDollar,
                                             inputAction: TextInputAction.next,
                                             textAlign: TextAlign.center,
                                             validator: (value) {
                                               if(int.parse(value!) > controller.patientslist[index].cost-controller.patientslist[index].donate)
                                              return 'you added alot of money';
                                             },),
                                         )
                                    ),
                                    SizedBox(
                                      height:
                                          MediaQuery.of(context).size.height /
                                              20,
                                    ),
                                    ElevatedButton(
                                        onPressed: () {
                                          // print(controller.patientslist[index].donate);
                                        if(int.parse(donatecont.donation)<=controller.patientslist[index].cost-controller.patientslist[index].donate)
                                          {
                                            donatecont.id=id;
                                            _patientdonate(donatecont.donation);
                                            wc.wallet.value=wc.wallet.value-int.parse(donatecont.donation);
                                            cf.topController.play();
                                            Get.back();
                                          }
                                        },
                                        child: Container(
                                          decoration: BoxDecoration(
                                              color: kstorm,
                                              borderRadius:
                                                  BorderRadius.circular(20)),
                                          height: MediaQuery.of(context)
                                                  .size
                                                  .height /
                                              14,
                                          width: MediaQuery.of(context)
                                                  .size
                                                  .width /
                                              3,
                                          child: const Center(
                                            child: Text(
                                              'Donate',
                                              style: TextStyle(
                                                  color: kprimary,
                                                  fontSize: 20),
                                            ),
                                          ),
                                        )),
                                    SizedBox(
                                      height:
                                          MediaQuery.of(context).size.height /
                                              40,
                                    ),
                                    ElevatedButton(
                                        onPressed: () {
                                          Get.back();
                                        },
                                        child: Container(
                                          decoration: BoxDecoration(
                                              color: kblue,
                                              borderRadius:
                                                  BorderRadius.circular(20)),
                                          height: MediaQuery.of(context)
                                                  .size
                                                  .height /
                                              15,
                                          width: MediaQuery.of(context)
                                                  .size
                                                  .width /
                                              4,
                                          child: const Center(
                                            child: Text(
                                              'cancel',
                                              style: TextStyle(color: kprimary),
                                            ),
                                          ),
                                        )),
                                  ],
                                ),
                              );
                            });
                      },
                      child: const Center(
                          child: Text(
                        'Donate',
                        style: TextStyle(color: kprimary, fontSize: 13.2),
                      ))),
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }

  void _patientdonate(donate)
  {
    donatecont.Donation(donate);
  }
}

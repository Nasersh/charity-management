import 'dart:math';
import 'package:confetti/confetti.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:get/get.dart';
import 'package:get/get_core/src/get_main.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:percent_indicator/linear_percent_indicator.dart';
import '../../../../confetti_controller.dart';
import '../../../../configrations/const/const.dart';
import '../../../../configrations/const/widgets/text-input.dart';
import '../../../wallet/wallet_controller.dart';
import 'donations/med_donation_controller.dart';
import 'medcontroller.dart';

class Med extends StatelessWidget {
   Med({Key? key}) : super(key: key);
   MedicineController controller = Get.put(MedicineController());
   MedDonationController donatecont=Get.put(MedDonationController());
   final wc = Get.find<WalletController>();
   final cf = Get.find<ConfController>();

   @override
  Widget build(BuildContext context) {
    return Stack(
      children:[
        Scaffold(
          backgroundColor: kprimary,
          body: SafeArea(
            child: Column(
              children: [
                Stack(

                    children: [
                      Container(
                        width: double.infinity,
                        height: MediaQuery.of(context).size.height/4,
                        decoration: BoxDecoration(
                            color: kblue,
                            borderRadius: BorderRadius.only(bottomLeft:Radius.circular(30),bottomRight:Radius.circular(30))
                        ),
                      ),
                      Column(
                        children: [
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Padding(
                                padding: const EdgeInsets.only(left: 15),
                                child:  IconButton(
                                  icon:Icon(
                                    FontAwesomeIcons.arrowLeft,
                                    color: kprimary,
                                  ),
                                  // Icons.arrow_back,
                                  onPressed: () {
                                    Get.back();
                                  },
                                ),
                              ),

                              // child: Obx(() => _buildProductsList())
                              Padding(
                                padding: EdgeInsets.all(MediaQuery.of(context).size.height/30),
                                child: Text('wallet: ${wc.wallet}',style: GoogleFonts.varela(
                                    color: kprimary,
                                    fontSize: 16
                                ),),
                              ),
                            ],
                          ),
                          SizedBox(
                            height: MediaQuery.of(context).size.height/15,
                          ),
                          const Padding(
                            padding: EdgeInsets.only(bottom: 10,left: 40),
                            child: Align(
                              alignment: Alignment.bottomLeft,
                              child: Text(
                                'Medicine . . .',
                                style: TextStyle(
                                    color: kprimary,
                                    fontSize: 30,
                                    fontStyle: FontStyle.italic,
                                    fontWeight: FontWeight.bold),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ]
                ),
                Expanded(child: Obx(() => controller.Medicineslist.length>0?_buildProductsList() : Center(child: Text("no elements to show"),))),
              ],
            ),
          ),
        ),
        Align(
          alignment: Alignment.topCenter,
          child: ConfettiWidget(
            colors: const [
              kprimary,
              kblue,
              kstorm,
              Colors.white12

            ],
            confettiController: cf.topController,
            blastDirection: pi / 2,
            maxBlastForce: 5,
            minBlastForce: 1,
            emissionFrequency: 0.03,
            numberOfParticles: 10,
            // shouldLoop:true,
            gravity: .1,
          ),
        ),

      ]

    );
  }

  ListView _buildProductsList() {
    return ListView.builder(
      shrinkWrap: true,
      itemCount: controller.Medicineslist.length,
      padding: EdgeInsets.only(top: 16),
      itemBuilder: (context, index) => Padding(
        padding: const EdgeInsets.symmetric(vertical: 10),
        child: Container(
          height: MediaQuery.of(context).size.height / 6,
          decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(15),
              boxShadow: [kDefaultShadow]),
          child: Padding(
            padding: const EdgeInsets.symmetric(vertical: 8.0),
            child: ListTile(
              leading: const CircleAvatar(
                radius: 30,
                backgroundColor: Colors.black45,
              ),
              title: Padding(
                padding: const EdgeInsets.only(top: 10),
                child: Text('${controller.Medicineslist[index].name}'),
              ),
              subtitle: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Padding(
                    padding: const EdgeInsets.only(top: 5, bottom: 10),
                    child:
                    Text('${controller.Medicineslist[index].count}'),
                  ),
                  LinearPercentIndicator(
                    barRadius: Radius.circular(040),
                    width: MediaQuery.of(context).size.width / 2.4,
                    animation: true,
                    animationDuration: 800,
                    lineHeight: 10,
                    percent: controller.Medicineslist[index].donate/controller.Medicineslist[index].count ,
                    progressColor: kblue,
                    backgroundColor: kbblue,
                  ),
                  SizedBox(
                    height: MediaQuery.of(context).size.height / 100,
                  ),
                  Text(
                    '${controller.Medicineslist[index].donate}\$/${controller.Medicineslist[index].cost}',
                    style: TextStyle(color: kstorm),
                  ),
                ],
              ),
              trailing: Padding(
                padding: const EdgeInsets.only(top: 8.0, right: 5),
                child: Container(
                  height: 50,
                  width: 75,
                  decoration: BoxDecoration(
                      color: kstorm, borderRadius: BorderRadius.circular(10)),
                  child:MaterialButton(
                      onPressed: () {
                        var id=controller.Medicineslist[index].id;
                        showModalBottomSheet(
                            context: context,
                            builder: (BuildContext context) {
                              return Container(

                                height: MediaQuery.of(context).size.height / 2,
                                decoration:  const BoxDecoration(
                                    image: DecorationImage(image: AssetImage('images/naser.jpg'),fit: BoxFit.cover),

                                    color: kprimary,
                                    borderRadius: BorderRadius.only(
                                        topRight: Radius.circular(25),
                                        topLeft: Radius.circular(25))),
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    Padding(
                                      padding: const EdgeInsets.only(
                                          top: 15.0, right: 10, left: 10),
                                      child:TextInput(onchange: (value ) { donatecont.donation=value;}, inputType: TextInputType.number, hint: 'enter the amount', icon: FontAwesomeIcons.moneyCheckDollar, inputAction: TextInputAction.next,textAlign: TextAlign.center,
                                        validator: (String?value ) {
                                        if(int.parse(value!)>wc.wallet.value)
                                          {
                                            return 'you dont have enough money';
                                          }
                                        if(int.parse(value)>controller.Medicineslist[index].count-controller.Medicineslist[index].count)
                                        {
                                              return 'you added more than needed';
                                        }
                                        },)

                                    ),
                                    SizedBox(
                                      height:
                                      MediaQuery.of(context).size.height /
                                          20,
                                    ),
                                    ElevatedButton(
                                        onPressed: () {
                                          if(int.parse(donatecont.donation)<=controller.Medicineslist[index].cost-controller.Medicineslist[index].donate)
                                          {
                                            donatecont.id=id;
                                            _Meddonate(donatecont.donation);
                                            wc.wallet.value=wc.wallet.value-int.parse(donatecont.donation);
                                            // controller.patientslist[index].donate=controller.patientslist[index].donate+int.parse(donatecont.donation);
                                            // controller.dnt.value=
                                            //      controller.patientslist[index].donate+
                                            //         int.parse(donatecont.donation);
                                            cf.topController.play();
                                            Get.back();

                                          }
                                        },
                                        child: Container(
                                          decoration: BoxDecoration(
                                              color: kstorm,
                                              borderRadius:
                                              BorderRadius.circular(20)),
                                          height: MediaQuery.of(context)
                                              .size
                                              .height /
                                              14,
                                          width: MediaQuery.of(context)
                                              .size
                                              .width /
                                              3,
                                          child: const Center(
                                            child: Text(
                                              'Donate',
                                              style: TextStyle(
                                                  color: kprimary,
                                                  fontSize: 20),
                                            ),
                                          ),
                                        )),
                                    SizedBox(
                                      height:
                                      MediaQuery.of(context).size.height /
                                          40,
                                    ),
                                    ElevatedButton(
                                        onPressed: () {
                                          Get.back();
                                        },
                                        child: Container(
                                          decoration: BoxDecoration(
                                              color: kblue,
                                              borderRadius:
                                              BorderRadius.circular(20)),
                                          height: MediaQuery.of(context)
                                              .size
                                              .height /
                                              15,
                                          width: MediaQuery.of(context)
                                              .size
                                              .width /
                                              4,
                                          child: const Center(
                                            child: Text(
                                              'cancel',
                                              style: TextStyle(color: kprimary),
                                            ),
                                          ),
                                        ))
                                  ],
                                ),
                              );
                            });
                      },
                      child: const Center(
                          child: Text(
                            'Donate',
                            style: TextStyle(color: kprimary, fontSize: 13.2),
                          ))),
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }

   void _Meddonate(donate)
   {
     donatecont.Donation(donate);
   }
}

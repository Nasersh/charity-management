import 'package:get/get.dart';
import '../../../model/medicine.dart';
import 'medservice.dart';

class MedicineController extends GetxController
{
  var Medicineslist =<Medicine>[].obs;
  var Medicines=<Medicine>[];

  var id=0.obs;

  MedicineService _service= MedicineService();
  var isloading=true.obs;

  @override
  void onInit() {
    showMedicines();
    super.onInit();
  }

  void showMedicines() async {
    try {
      isloading(true);
      Medicines = await _service.getMedicine();
      if (Medicines != null) {
        Medicineslist.value = Medicines;
      }
    } finally {
      isloading(false);
    }
  }


}
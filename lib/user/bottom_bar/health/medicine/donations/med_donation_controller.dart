import 'package:get/get.dart';

import 'med_donatio_service.dart';

class MedDonationController extends GetxController
{
  var id;
  var donation;
  var current;
  MedDonationService _service=new MedDonationService();
  Donation(money) async
  {
    current=await _service.Donate(donation,id);
  }
}
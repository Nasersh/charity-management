import 'package:charitysys/user/bottom_bar/health/patients/patients.dart';
import 'package:charitysys/user/bottom_bar/widgets/volunteer_comp.dart';
import 'package:charitysys/user/volunteer/volunteer.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get/get_core/src/get_main.dart';
import '../../../configrations/const/const.dart';
import '../widgets/category_card.dart';
import 'equipments/equip.dart';
import 'medicine/medicine.dart';

class Health extends StatelessWidget {
  Health({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Stack(
        children: [
          Container(
            height: MediaQuery.of(context).size.height / 5,
            decoration: const BoxDecoration(
                color: kblue,
                borderRadius: BorderRadius.only(
                  bottomRight: Radius.circular(20),
                  bottomLeft: Radius.circular(20),
                ),
                boxShadow: [kDefaultShadow]),
          ),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const Padding(
                padding: EdgeInsets.all(8.0),
                child: Text(
                  'Health',
                  style: TextStyle(
                      color: kprimary,
                      fontSize: 40,
                      fontWeight: FontWeight.w600),
                ),
              ),
              SizedBox(
                height: MediaQuery.of(context).size.height / 50,
              ),
              const Padding(
                padding: EdgeInsets.symmetric(horizontal: 12),
                child: Text(
                  'Together to Help orphans . . .',
                  style: TextStyle(
                      color: kprimary,
                      fontSize: 20,
                      fontWeight: FontWeight.w400),
                ),
              ),
              SizedBox(
                height: MediaQuery.of(context).size.height / 10,
              ),
              VolunteerComponent(
                  title:
                      "If you are a doctor you can volunteer to help orphans!"),
              SizedBox(
                height: MediaQuery.of(context).size.height / 40,
              ),
              const Padding(
                padding: EdgeInsets.all(8.0),
                child: Text(
                  'Categories:',
                  style: TextStyle(
                      color: kblue, fontSize: 20, fontWeight: FontWeight.w600),
                ),
              ),
              SizedBox(
                height: MediaQuery.of(context).size.height / 60,
              ),
              SingleChildScrollView(
                scrollDirection: Axis.horizontal,
                child: Row(
                  children: [
                    CategoryCard(
                      name: 'Patients',
                      image: 'images/patients.png',
                      onPressed: () {
                        Get.to(Patients());
                      },
                    ),
                    CategoryCard(
                      name: 'Medicine',
                      image: 'iimages/med.png',
                      onPressed: () {
                        Get.to(() => Med());
                      },
                    ),
                    CategoryCard(
                      name: 'Equipments',
                      image: 'images/eq.png',
                      onPressed: () {
                        Get.to(Equipments());
                      },
                    ),
                  ],
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}

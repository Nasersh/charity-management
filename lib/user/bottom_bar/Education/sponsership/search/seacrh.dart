import 'package:charitysys/user/bottom_bar/Education/sponsership/search/searchcontroller.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:get/get.dart';
import '../../../../../confetti_controller.dart';
import '../../../../../configrations/const/const.dart';
import '../../../../../configrations/const/search_box.dart';
import '../../../../../configrations/const/widgets/text-input.dart';
import '../../../../wallet/wallet_controller.dart';
import '../donations/guarantee_donation_controller.dart';
import '../sponcontroller.dart';
class ssearch extends StatelessWidget {
  ssearch({Key? key}) : super(key: key);
  String name = '';
  SearchController scontroller = Get.put(SearchController());
  Sponsershipcontroller controller = Get.put(Sponsershipcontroller());
  WalletController wc=Get.put(WalletController());
  SponsershipDonationController donatecont=Get.put(SponsershipDonationController());
  final cf = Get.find<ConfController>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: kprimary,
      body: Stack(
        children: [

          Flex(

            direction: Axis.vertical,
            children:[Expanded(
              child: Column(
                  children: [
                    Column(
                      children: [
                        const SizedBox(
                          height: 45,
                        ),

                        SearchBox(
                          onChanged: (value) {
                            controller.sssearch(value);
                          },
                        ),
                        const SizedBox(
                          height: 1,
                        ),
                        const SizedBox(
                          height: 10,
                        ),

                        const SizedBox(
                          height: 10,
                        ),
                      ],
                    ),
                    Expanded(
                      child: Column(
                          children: [

                            Container(
                              // margin: EdgeInsets.only(top: 0),
                              decoration:  const BoxDecoration(
                                color: kblue,
                                borderRadius: BorderRadius.only(
                                  topLeft: Radius.circular(40),
                                  topRight: Radius.circular(40),
                                ),
                              ),
                            ),
                            Obx(() {
                                return Expanded(child:
                                _buildProductsList()
                                ) ;
                              })
                          ]),
                    ),
                  ]),
            ),]
          ),
        ],
      ),
    );
  }
  Widget _buildProductsList() {
    return ListView.builder(

      shrinkWrap: true,
      itemCount: controller.search.length,
      padding: EdgeInsets.only(top: 16),
      itemBuilder: (context, index) => Padding(
        padding: const EdgeInsets.symmetric(vertical: 10),
        child: Container(
          height: MediaQuery.of(context).size.height / 5,
          decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(15),
              boxShadow: [kDefaultShadow]),
          child: Padding(
            padding: const EdgeInsets.symmetric(vertical: 8.0),
            child: ExpansionTile(
              children: [
                Text('psycho needs your help !!')
              ],
              leading: const CircleAvatar(
                radius: 30,
                backgroundColor: Colors.black45,
              ),
              title: Padding(
                padding: const EdgeInsets.only(top: 10),
                child: Text('${controller.search[index].name}',style: TextStyle(
                    fontWeight:  FontWeight.bold,color: kblue
                ),),
              ),
              subtitle: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Padding(
                    padding: const EdgeInsets.only(top: 5, bottom: 10),
                    child:
                    Text('${controller.search[index].academicYear}',style: TextStyle(
                        fontWeight: FontWeight.w300
                    ),),
                  ),
                  SizedBox(
                    height: MediaQuery.of(context).size.height/100,
                  ),
                  // LinearPercentIndicator(
                  //   barRadius: Radius.circular(040),
                  //   width: MediaQuery.of(context).size.width / 2.3,
                  //   animation: true,
                  //   animationDuration: 800,
                  //   lineHeight: 7,
                  //   percent:controller.Sponsershiplist[index].donate /controller.Sponsershiplist[index].cost,
                  //   progressColor: kblue,
                  //   backgroundColor: kbblue,
                  // ),
                  SizedBox(
                    height: MediaQuery.of(context).size.height / 100,
                  ),
                  Obx(()=>
                      Text(
                        '${controller.search[index].cost}',
                        style: TextStyle(color: kstorm),
                      )
                  )
                ],
              ),
              trailing: Padding(
                padding: const EdgeInsets.only(top: 20.0),
                child: Container(
                  height: MediaQuery.of(context).size.height/20,
                  width: MediaQuery.of(context).size.width/4.8,
                  decoration: BoxDecoration(
                      color: kstorm, borderRadius: BorderRadius.circular(10)),
                  child: MaterialButton(
                      onPressed: () {
                        var id=controller.search[index].id;
                        showModalBottomSheet(
                            context: context,
                            builder: (BuildContext context) {
                              return Container(
                                height: MediaQuery.of(context).size.height / 2,
                                decoration:   const BoxDecoration(
                                  // border: Border.all(
                                  //     color: kstorm,
                                  //     width: 6.0
                                  // ),
                                    image: DecorationImage(image: AssetImage('images/naser.jpg'),fit: BoxFit.cover),
                                    borderRadius: BorderRadius.only(
                                        topRight: Radius.circular(25),
                                        topLeft: Radius.circular(25))),
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    Padding(
                                        padding: const EdgeInsets.only(
                                            left: 10,right: 10),
                                        child:

                                        TextInput(onchange: (value ) { donatecont.donation=value;}, inputType: TextInputType.number, hint: 'enter the amount', icon: FontAwesomeIcons.moneyCheckDollar, inputAction: TextInputAction.next,textAlign: TextAlign.center, validator: (String? value) {  },)
                                    ),
                                    SizedBox(
                                      height:
                                      MediaQuery.of(context).size.height /
                                          20,
                                    ),
                                    ElevatedButton(

                                        onPressed: () {
                                          donatecont.id=id;
                                          _studentdonate();
                                          wc.wallet.value=wc.wallet.value-int.parse(donatecont.donation);
                                          // scontroller.Sponsershiplist[index].donate=scontroller.Sponsershiplist[index].donate+int.parse(donatecont.donation);
                                          cf.topController.play();
                                          Get.back();

                                        },
                                        child: Container(
                                          decoration: BoxDecoration(
                                              color: kstorm,
                                              borderRadius:
                                              BorderRadius.circular(20)),
                                          height: MediaQuery.of(context)
                                              .size
                                              .height /
                                              14,
                                          width: MediaQuery.of(context)
                                              .size
                                              .width /
                                              3,
                                          child: const Center(
                                            child: Text(
                                              'guarentee',
                                              style: TextStyle(
                                                  color: kprimary,
                                                  fontSize: 20),
                                            ),
                                          ),
                                        )),
                                    SizedBox(
                                      height:
                                      MediaQuery.of(context).size.height /
                                          40,
                                    ),
                                    ElevatedButton(
                                        onPressed: () {
                                          Get.back();
                                        },
                                        child: Container(
                                          decoration: BoxDecoration(
                                              color: kblue,
                                              borderRadius:
                                              BorderRadius.circular(20)),
                                          height: MediaQuery.of(context)
                                              .size
                                              .height /
                                              15,
                                          width: MediaQuery.of(context)
                                              .size
                                              .width /
                                              4,
                                          child: const Center(
                                            child: Text(
                                              'cancel',
                                              style: TextStyle(color: kprimary),
                                            ),
                                          ),
                                        )),
                                  ],
                                ),
                              );
                            });
                      },
                      child: const Center(
                          child: Text(
                            'guarente',
                            style: TextStyle(color: kprimary, fontSize: 13.2),
                          ))),
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }

  void _studentdonate()
  {
    donatecont.Donation();
  }
}

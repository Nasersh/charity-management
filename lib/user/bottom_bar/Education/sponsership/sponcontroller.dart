import 'package:charitysys/user/bottom_bar/Education/sponsership/sponservice.dart';
import 'package:charitysys/user/model/student.dart';
import 'package:get/get.dart';



class Sponsershipcontroller extends GetxController
{
  var Sponsershiplist =<Student>[].obs;
  var MYSponsershiplist =<Student>[].obs;
  var search =<Student>[].obs;
  var searchresult =<Student>[].obs;

  var Sponsership=<Student>[];
  var MySponsership=<Student>[];

  var id=0.obs;

  SponsershipService _service= SponsershipService();
  var isloading=true.obs;

  @override
  void onInit() {
    showSponsership();
    showMySponsership();
    //loadProductsFromRepo(id.value);
    super.onInit();
  }

  void showSponsership() async {
    try {
      isloading(true);
       Sponsership = await _service.getStudent();
      if (Sponsership != null) {
        Sponsershiplist.value = Sponsership;
        isloading(false);
      }
    } finally {
      isloading(false);
    }
  }
  void SerchStudent(String value)
  {
    search.value= [ ];
    search.value=Sponsershiplist..where((element) => (element.name.contains(value)));
    if(value.isNotEmpty)
      {
       search.value.forEach((item)
           {
            if(item.name.contains(value))
            {
              search.add(item);
            }
           }
       );
       search.value.clear();
       search.addAll(search);
      }
    else {

    }

  }
  sssearch(String text) async {
    List<Student> results =<Student> [];
    if (text.isEmpty) {
      results = Sponsershiplist.value;
    } else {
      results = Sponsershiplist.value.where((element) => element.name.toLowerCase().contains(text.toLowerCase())|| element.cost.toString().contains(text.toLowerCase())||element.academicYear.toLowerCase().contains(text.toLowerCase())).toList();
    }
    search.value = results;
  }
  // void filterSearchResults(String query) {
  //   List<String> dummySearchList = List<String>();
  //   dummySearchList.addAll(duplicateItems);
  //   if(query.isNotEmpty) {
  //     List<String> dummyListData = List<String>();
  //
  //     dummySearchList.forEach((item) {
  //       if(item.contains(query)) {
  //         dummyListData.add(item);
  //       }
  //     });
  //     setState(() {
  //       items.clear();
  //       items.addAll(dummyListData);
  //     });
  //     return;
  //   } else {
  //     setState(() {
  //       items.clear();
  //       items.addAll(duplicateItems);
  //     });
  //   }
  // }

  void showMySponsership() async {

      MySponsership = await _service.getMyStudent();
      if (MySponsership != null) {
        MYSponsershiplist.value = MySponsership;
        print(
           ' ${MYSponsershiplist.length}'
        );
      }
    }

  searchstudentsFromRepo(String name) async {
    Sponsershiplist.value = await _service.SearchStudent(name);
    return Sponsershiplist;

  }


}
import 'dart:math';
import 'package:charitysys/user/bottom_bar/Education/sponsership/search/seacrh.dart';
import 'package:charitysys/user/bottom_bar/Education/sponsership/sponcontroller.dart';
import 'package:confetti/confetti.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:get/get.dart';
import 'package:get/get_core/src/get_main.dart';
import 'package:google_fonts/google_fonts.dart';
import '../../../../confetti_controller.dart';
import '../../../../configrations/const/const.dart';
import '../../../wallet/wallet_controller.dart';
import 'donations/guarantee_donation_controller.dart';

class Sponsership extends StatelessWidget {
  Sponsership({Key? key}) : super(key: key);
  Sponsershipcontroller controller = Get.put(Sponsershipcontroller());
  WalletController wc = Get.put(WalletController());
  SponsershipDonationController donatecont =
      Get.put(SponsershipDonationController());
  final cf = Get.find<ConfController>();
  var id;
  var name;

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        Scaffold(
          backgroundColor: kprimary,
          body: SafeArea(
            child: Obx(
              () => Column(
                children: [
                  Stack(children: [
                    Container(
                      width: double.infinity,
                      height: MediaQuery.of(context).size.height / 4,
                      decoration: const BoxDecoration(
                          color: kblue,
                          borderRadius: BorderRadius.only(
                              bottomLeft: Radius.circular(30),
                              bottomRight: Radius.circular(30))),
                    ),
                    Column(
                      children: [
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Padding(
                              padding: const EdgeInsets.only(left: 15),
                              child: IconButton(
                                icon: const Icon(
                                  FontAwesomeIcons.arrowLeft,
                                  color: kprimary,
                                ),
                                // Icons.arrow_back,
                                onPressed: () {
                                  Get.back();
                                },
                              ),
                            ),

                            // child: Obx(() => _buildProductsList())
                            Padding(
                                padding: EdgeInsets.all(
                                    MediaQuery.of(context).size.height / 50),
                                child: Obx(
                                  () => Text(
                                    'wallet: ${wc.wallet.value}',
                                    style: GoogleFonts.varela(
                                        color: kprimary, fontSize: 16),
                                  ),
                                )),
                          ],
                        ),
                        SizedBox(
                          height: MediaQuery.of(context).size.height / 100,
                        ),
                        const Padding(
                          padding: EdgeInsets.only(left: 30, top: 20),
                          child: Align(
                            alignment: Alignment.topLeft,
                            child: Text(
                              'Students',
                              style: TextStyle(
                                  color: kprimary,
                                  fontSize: 30,
                                  fontStyle: FontStyle.italic,
                                  fontWeight: FontWeight.bold),
                            ),
                          ),
                        ),
                      ],
                    ),
                    Positioned(
                      top: MediaQuery.of(context).size.height / 7,
                      left: MediaQuery.of(context).size.width / 2,
                      child: Padding(
                        padding: const EdgeInsets.all(20.0),
                        child: Container(
                            // margin: EdgeInsets.all(kDefaultPadding),
                            width: MediaQuery.of(context).size.width / 2.5,
                            padding: const EdgeInsets.symmetric(
                              horizontal: kDefaultPadding / 4,
                              // vertical: kDefaultPadding / 4,
                            ),
                            decoration: BoxDecoration(
                              color: Colors.white.withOpacity(0.4),
                              borderRadius: BorderRadius.circular(12),
                            ),
                            child: ElevatedButton(
                                onPressed: () {
                                  Get.to(ssearch());
                                },
                                child: Row(
                                  children: const [
                                    Icon(
                                      Icons.search,
                                      color: kprimary,
                                    ),
                                    SizedBox(
                                      width: 10,
                                    ),
                                    Text(
                                      'Search',
                                      style: TextStyle(
                                          color: kprimary, fontSize: 20),
                                    ),
                                  ],
                                ))),
                      ),
                    ),
                  ]),

                  Expanded(
                      child: !controller.isloading.value
                          ? _buildProductsList()
                          : Center(child: CircularProgressIndicator())),
                ],
              ),
            ),
          ),
        ),
        Align(
          alignment: Alignment.topCenter,
          child: ConfettiWidget(
            colors: const [kprimary, kblue, kstorm, Colors.white12],
            confettiController: cf.topController,
            blastDirection: pi / 2,
            maxBlastForce: 5,
            minBlastForce: 1,
            emissionFrequency: 0.03,
            numberOfParticles: 10,
            // shouldLoop:true,
            gravity: .1,
          ),
        ),
      ],
    );
  }

  Widget _buildProductsList() {
    return ListView.builder(
      shrinkWrap: true,
      itemCount: controller.Sponsershiplist.value.length,
      padding: EdgeInsets.only(top: 16),
      itemBuilder: (context, index) => Padding(
        padding: const EdgeInsets.symmetric(vertical: 10),
        child: Container(
          height: MediaQuery.of(context).size.height / 5,
          decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(15),
              boxShadow: [kDefaultShadow]),
          child: Padding(
            padding: const EdgeInsets.symmetric(vertical: 8.0),
            child: ExpansionTile(
              children: [Text('${controller.Sponsershiplist[index].birthDate}')],
              leading: const CircleAvatar(
                radius: 30,
                backgroundColor: Colors.black45,
              ),
              title: Padding(
                padding: const EdgeInsets.only(top: 10),
                child: Text(
                  '${controller.Sponsershiplist[index].name}',
                  style: TextStyle(fontWeight: FontWeight.bold, color: kblue),
                ),
              ),
              subtitle: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Padding(
                    padding: const EdgeInsets.only(top: 5, bottom: 10),
                    child: Text(
                      '${controller.Sponsershiplist[index].academicYear}',
                      style: TextStyle(fontWeight: FontWeight.w300),
                    ),
                  ),
                  SizedBox(
                    height: MediaQuery.of(context).size.height / 100,
                  ),
                  // LinearPercentIndicator(
                  //   barRadius: Radius.circular(040),
                  //   width: MediaQuery.of(context).size.width / 2.3,
                  //   animation: true,
                  //   animationDuration: 800,
                  //   lineHeight: 7,
                  //   percent:controller.Sponsershiplist[index].donate /controller.Sponsershiplist[index].cost,
                  //   progressColor: kblue,
                  //   backgroundColor: kbblue,
                  // ),
                  SizedBox(
                    height: MediaQuery.of(context).size.height / 100,
                  ),
                  Obx(() => Text(
                        '${controller.Sponsershiplist[index].cost}',
                        style: TextStyle(color: kstorm),
                      ))
                ],
              ),
              trailing: Padding(
                padding: const EdgeInsets.only(top: 20.0),
                child: Container(
                  height: MediaQuery.of(context).size.height / 20,
                  width: MediaQuery.of(context).size.width / 4.8,
                  decoration: BoxDecoration(
                      color: kstorm, borderRadius: BorderRadius.circular(10)),
                  child: MaterialButton(
                      onPressed: () {
                         id = controller.Sponsershiplist[index].id;
                        showAlertDialog(context,index);
                      },
                      child: const Center(
                          child: Text(
                        'guarente',
                        style: TextStyle(color: kprimary, fontSize: 10),
                      ))),
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }

  void _studentdonate() {
    donatecont.Donation();
  }

  showAlertDialog(BuildContext context, index) {
    // set up the buttons
    Widget cancelButton = ElevatedButton(
      child: Text("Cancel",style: TextStyle(color: kblue),),
      onPressed: () {
        Get.back();
      },
    );
    Widget continueButton = ElevatedButton(
      child: Text("Continue",style: TextStyle(color: kblue)),
      onPressed: () {
        if(controller.Sponsershiplist[index].cost<wc.wallet.value) {
          donatecont.id = id;
          _studentdonate();
        }  Get.back();

      },
    );

    // set up the AlertDialog
    AlertDialog alert = AlertDialog(
      title: Text("Guarantee",style: TextStyle(color: kstorm),),
      content: Text(
          "Would you like to continue learning how to use Flutter alerts?",style: TextStyle(color: kblue),),
      actions: [
        cancelButton,
        continueButton,
      ],
    );

    // show the dialog
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }
}

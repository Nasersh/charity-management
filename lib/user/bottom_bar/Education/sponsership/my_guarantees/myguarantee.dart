import 'dart:math';import 'package:confetti/confetti.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:get/get.dart';
import 'package:get/get_core/src/get_main.dart';
import 'package:google_fonts/google_fonts.dart';

import '../../../../../confetti_controller.dart';
import '../../../../../configrations/const/const.dart';
import '../../../../../configrations/const/widgets/card.dart';
import '../sponcontroller.dart';

class MyGuarantees extends StatelessWidget {
  MyGuarantees({Key? key}) : super(key: key);
  Sponsershipcontroller controller = Get.put(Sponsershipcontroller());
  final cf = Get.put(ConfController());
  var id;
  var name;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: kprimary,
      body: SafeArea(
        child: Obx(
              () => Column(
            children: [
              Stack(children: [
                Container(
                  width: double.infinity,
                  height: MediaQuery.of(context).size.height / 5,
                  decoration: const BoxDecoration(
                      color: kblue,
                      borderRadius: BorderRadius.only(
                          bottomLeft: Radius.circular(30),
                          bottomRight: Radius.circular(30))),
                ),
                Column(
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Padding(
                          padding: const EdgeInsets.only(left: 15),
                          child: IconButton(
                            icon: const Icon(
                              FontAwesomeIcons.arrowLeft,
                              color: kprimary,
                            ),
                            // Icons.arrow_back,
                            onPressed: () {
                              Get.back();
                            },
                          ),
                        ),

                        // child: Obx(() => _buildProductsList())
                      ],
                    ),
                    SizedBox(
                      height: MediaQuery.of(context).size.height / 20,
                    ),
                    const Align(
                      alignment: Alignment.bottomCenter,
                      child: Text(
                        'My guarantees',
                        style: TextStyle(
                            color: kprimary,
                            fontSize: 30,
                            fontStyle: FontStyle.italic,
                            fontWeight: FontWeight.bold),
                      ),
                    ),
                  ],
                ),
              ]),
              SizedBox(
                height: MediaQuery.of(context).size.height/20,
              ),

              Expanded(
                  child: controller.MYSponsershiplist.length > 0
                      ? _buildStudentsList()
                      : Center(child: CircularProgressIndicator())),
            ],
          ),
        ),
      ),
    );
  }

  Widget _buildStudentsList() {
    return  GridView.count(
      physics: ClampingScrollPhysics(),
      padding: EdgeInsets.fromLTRB(8, 0, 8, 0),
      shrinkWrap: true,
      crossAxisCount: 2,
      mainAxisSpacing: 30,
      crossAxisSpacing: 20,
      children:
      List.generate(controller.MYSponsershiplist.length, (index) {
        return Buildcard(
            controller.MYSponsershiplist[index].name,
            controller.MYSponsershiplist[index].cost,
            controller.MYSponsershiplist[index].academicYear);
      }),
    );
  }

}

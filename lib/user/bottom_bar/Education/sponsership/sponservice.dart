import 'dart:convert';

import 'package:http/http.dart' as http;

import '../../../../config/config.dart';
import '../../../../secureStorage.dart';
import '../../../model/student.dart';



class SponsershipService
{
  var message;
  // var index=0;
  var url = Uri.parse(ServerConfig.domainNameServer + ServerConfig.showallStudent);
  var url1 = Uri.parse(ServerConfig.domainNameServer + ServerConfig.showMyStudent);

  Future<List<Student>> getStudent() async{
    var _storage=SecureStorage();
    String? token = await _storage.read('token');
    print('$token');
    var response = await http.get(url,headers:  {
      'Accept' : 'application/json',
      'Authorization'  :  'Bearer $token'
    },
    );
    print(response.statusCode);

    if(response.statusCode == 200){
      var Sponsership = studentsFromJson(response.body);
      return Sponsership.students;
    }
    else {
      return [];
    }
  }
  Future<List<Student>> getMyStudent() async{
    var _storage=SecureStorage();
    String? token = await _storage.read('token');
    print('$token');
    var response = await http.get(url1,headers:  {
      'Accept' : 'application/json',
      'Authorization'  :  'Bearer $token'
    },
    );
    print(response.statusCode);

    if(response.statusCode == 200){
      var Sponsership = jsonDecode(response.body);
      var sp=List<Student>.from(Sponsership["guarantee"].map((x) => Student.fromJson(x)));
      return sp;
    }
    else {
      return [];
    }
  }
  //
  // Future<List<Student>> loadProductsFromApi(int index) async {
  //   var response = await http.get(Uri.parse(ServerConfig.domainNameServer+ServerConfig.showCategoryProducts + '${index}'),
  //     headers: {
  //       'Accept': 'application/json',
  //     },
  //   );
  //   print(response.statusCode);
  //   //  print(response.body);
  //
  //   if(response.statusCode == 200){
  //     var porductscat = productsFromJson(response.body);
  //     return porductscat.products;
  //   }
  //   else {
  //     return [];
  //   }


  Future<List<Student>> SearchStudent(String name) async {
    var _storage=SecureStorage();
    String? token = await _storage.read('token');
    var response = await http.post(Uri.parse(ServerConfig.domainNameServer+ServerConfig.SearchStudents),
        headers: {
          'Accept': 'application/json',
          'Authorization'  :  'Bearer $token'
        },
        body: {
             'name' : name
        }
    );
    print(response.statusCode);
    //  print(response.body);

    if(response.statusCode == 200){
      var Sponsership = studentsFromJson(response.body);
      return Sponsership.students;
    }
    else {
      return [];
    }
  }

  }


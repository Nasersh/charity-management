import 'dart:ui';
import 'package:charitysys/user/bottom_bar/Education/sponsership/sposership.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get/get_core/src/get_main.dart';
import '../../../configrations/const/const.dart';
import '../../volunteer/volunteer.dart';
import '../widgets/category_card.dart';
import '../widgets/volunteer_comp.dart';
import 'stationery/stationery.dart';

class Education extends StatelessWidget {
  const Education({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Stack(
        children: [
          Container(
            height: MediaQuery.of(context).size.height / 5,
            // margin: EdgeInsets.only(top: 0),
            decoration: const BoxDecoration(
              color: kblue,
              boxShadow: [kDefaultShadow],
              borderRadius: BorderRadius.only(
                bottomRight: Radius.circular(20),
                bottomLeft: Radius.circular(20),
              ),
            ),
          ),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              // SizedBox(height: MediaQuery.of(context).size.width/15,),
              const Padding(
                padding: EdgeInsets.all(8.0),
                child: Text(
                  'Education',
                  style: TextStyle(
                      color: kprimary,
                      fontSize: 40,
                      fontWeight: FontWeight.w600),
                ),
              ),
              SizedBox(height: MediaQuery.of(context).size.height / 50),
              const Padding(
                padding: EdgeInsets.symmetric(horizontal: 12),
                child: Text(
                  'provide meals for kids',
                  style: TextStyle(
                      color: kprimary,
                      fontSize: 20,
                      fontWeight: FontWeight.w400),
                ),
              ),
              SizedBox(
                height: MediaQuery.of( context).size.height / 10,
              ),
               VolunteerComponent(title:" If you are a teacher you can volunteer to help orphans!",),
              SizedBox(
                height: MediaQuery.of(context).size.height / 40,
              ),
              const Padding(
                padding: EdgeInsets.all(8.0),
                child: Text(
                  'Categories:',
                  style: TextStyle(
                      color: kblue, fontSize: 20, fontWeight: FontWeight.w600),
                ),
              ),
              SizedBox(
                height: MediaQuery.of(context).size.height / 60,
              ),
              SingleChildScrollView(
                scrollDirection: Axis.horizontal,
                child: Row(
                  children: [
                    CategoryCard(
                      name: 'Sponsorship',
                      image: 'images/edu.png',
                      onPressed: () {
                        Get.to(Sponsership());
                      },
                    ),
                    CategoryCard(
                      name: 'stationery',
                      image: 'images/sta.png',
                      onPressed: () {
                        Get.to(() => Stationery());
                      },
                    ),
                  
                  ],
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}

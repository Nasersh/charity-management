import 'package:get/get.dart';

import 'stationery_donation_service.dart';

class StationerysDonationController extends GetxController
{
  var id;
  var donation;
  var current;
  var count=0.obs;

  @override
  void onReady() {
     count.value=0;

    super.onReady();
  }
  StationeryDonationService _service=new StationeryDonationService();
  Donation() async
  {
    current=await _service.Donate(count.toString(),id);
  }
}
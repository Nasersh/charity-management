import 'package:charitysys/user/bottom_bar/Education/stationery/staionery_service.dart';
import 'package:get/get.dart';

import '../../../model/stationery_model.dart';







class StationeryController extends GetxController
{
  var Stationeryslist =<Stationery>[].obs;
  var stationery=<Stationery>[];

  var id=0.obs;

  StationeryService _service= StationeryService();
  var isloading=true.obs;

  @override
  void onInit() {
    showStationery();
    super.onInit();
  }

  void showStationery() async {
    try {
      isloading(true);
      stationery = await _service.getStationery();
      if (stationery != null) {
        Stationeryslist.value = stationery;
      }
    } finally {
      isloading(false);
    }
  }


}
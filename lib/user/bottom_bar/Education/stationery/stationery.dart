import 'dart:math';
import 'package:charitysys/user/bottom_bar/Education/stationery/staionery_controller.dart';
import 'package:confetti/confetti.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:get/get.dart';
import 'package:get/get_core/src/get_main.dart';
import '../../../../confetti_controller.dart';
import '../../../../configrations/const/const.dart';
import '../../../wallet/wallet_controller.dart';
import 'donations/stationery_donation_controller.dart';

class Stationery extends StatelessWidget {
  Stationery({Key? key}) : super(key: key);
  WalletController wc=Get.put(WalletController());
  StationeryController controller=Get.put(StationeryController());
  StationerysDonationController donatecont=Get.put(StationerysDonationController());
  final cf = Get.find<ConfController>();


  @override
  Widget build(BuildContext context) {
    return  Stack(
      children: [
        Scaffold(
          backgroundColor: kprimary,
          body: SafeArea(
            child: Obx(()=> Column(
              children: [
                Stack(
                    children: [
                      Container(
                        width: double.infinity,
                        height: MediaQuery.of(context).size.height/4,
                        decoration: const BoxDecoration(
                            color: kblue,
                            borderRadius: BorderRadius.only(bottomLeft:Radius.circular(30),bottomRight:Radius.circular(30))
                        ),
                      ),
                      Column(
                        children: [
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Padding(
                                padding: const EdgeInsets.only(left: 15),
                                child:  IconButton(
                                  icon:const Icon(
                                    FontAwesomeIcons.arrowLeft,
                                    color: kprimary,
                                  ),
                                  // Icons.arrow_back,
                                  onPressed: () {
                                    Get.back();
                                  },
                                ),
                              ),
                              Padding(
                                padding: EdgeInsets.all(MediaQuery.of(context).size.height/30),
                                child: Text('wallet: ${wc.wallet}',style: GoogleFonts.varela(
                                    color: kprimary,
                                    fontSize: 16
                                ),),
                              ),
                            ],
                          ),
                          SizedBox(
                            height: MediaQuery.of(context).size.height/15,
                          ),
                          const Padding(
                            padding: EdgeInsets.only(bottom: 10,left: 40),
                            child: Align(
                              alignment: Alignment.bottomLeft,
                              child: Text(
                                'Stationery . . .',
                                style: TextStyle(
                                    color: kprimary,
                                    fontSize: 30,
                                    fontStyle: FontStyle.italic,
                                    fontWeight: FontWeight.bold),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ]
                ),
                SizedBox(
                  height:MediaQuery.of(context).size.height/100,
                )
                ,
                Expanded(child: _buildProductsList()),
              ],
            ),
            ),
          ),
        ),
        Align(
          alignment: Alignment.topCenter,
          child: ConfettiWidget(
            colors: const [
              kprimary,
              kblue,
              kstorm,
              Colors.white12

            ],
            confettiController: cf.topController,
            blastDirection: pi / 2,
            maxBlastForce: 5,
            minBlastForce: 1,
            emissionFrequency: 0.03,
            numberOfParticles: 10,
            // shouldLoop:true,
            gravity: .1,
          ),
        ),

      ]
    );
  }
  Widget _buildProductsList() {

    return ListView.builder(
      shrinkWrap: true,
      itemCount: controller.Stationeryslist.length,
      padding: EdgeInsets.only(top: 16),
      itemBuilder: (context, index) =>  Padding(
        padding: const EdgeInsets.symmetric(vertical: 10),
        child: Container(
          height: MediaQuery.of(context).size.height / 4.7,
          decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(15),
              boxShadow: [kDefaultShadow]),
          child: Padding(
            padding: const EdgeInsets.symmetric(vertical: 8.0),
            child: ListTile(
              leading: const CircleAvatar(
                radius: 30,
                backgroundColor: Colors.black45,
              ),
              title: Padding(
                padding: const EdgeInsets.only(top: 10),
                child: Text(controller.Stationeryslist[index].size,
                  style: const TextStyle(
                      color: kblue,
                      fontWeight: FontWeight.bold
                  )
                  ,),
              ),
              subtitle: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Padding(
                    padding: const EdgeInsets.only(top: 5, bottom: 10),
                    child:
                    Text(controller.Stationeryslist[index].content,maxLines: 3,),
                  ),
                  SizedBox(
                    height: MediaQuery.of(context).size.height / 100,
                  ),
                  Text(
                    '${controller.Stationeryslist[index].cost}',
                    style: TextStyle(color: kstorm),
                  ),
                ],
              ),
              trailing: Padding(
                padding: const EdgeInsets.only(top: 8.0, right: 5),
                child: Container(
                  height: 50,
                  width: 75,
                  decoration: BoxDecoration(
                      color: kstorm, borderRadius: BorderRadius.circular(10)),
                  child: MaterialButton(
                      onPressed: () {
                        var id=controller.Stationeryslist[index].id;
                        showModalBottomSheet(
                            context: context,
                            builder: (BuildContext context) {
                              return Container(
                                height: MediaQuery.of(context).size.height / 2,
                                decoration:  const BoxDecoration(
                                    image: DecorationImage(image: AssetImage('images/naser.jpg'),fit: BoxFit.cover),
                                    color: kprimary,
                                    borderRadius: BorderRadius.only(
                                        topRight: Radius.circular(25),
                                        topLeft: Radius.circular(25))),
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    Padding(
                                        padding: const EdgeInsets.only(
                                            top: 15.0, right: 10, left: 10),
                                        child: Obx(()=> Row(
                                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                          children: [
                                            IconButton(
                                                color: kblue,
                                                onPressed: (){donatecont.count--;}, icon: const Icon(
                                              FontAwesomeIcons.minus,
                                            )),
                                            Text(
                                              '${donatecont.count}',style: const TextStyle(
                                                fontSize: 30
                                            ),
                                            ),
                                            IconButton(onPressed: (){donatecont.count++;}, icon: const Icon(
                                                FontAwesomeIcons.plus
                                            )),
                                          ],
                                        ),
                                        )
                                    ),
                                    SizedBox(
                                      height:
                                      MediaQuery.of(context).size.height /
                                          20,
                                    ),
                                    ElevatedButton(
                                        onPressed: () {
                                          if(donatecont.count*controller.Stationeryslist[index].cost>=wc.wallet.value) {
                                            donatecont.id = id;
                                            _Stationerydonate();
                                          }
                                        },
                                        child: Container(
                                          decoration: BoxDecoration(
                                              color: kstorm,
                                              borderRadius:
                                              BorderRadius.circular(20)),
                                          height: MediaQuery.of(context)
                                              .size
                                              .height /
                                              14,
                                          width: MediaQuery.of(context)
                                              .size
                                              .width /
                                              3,
                                          child: const Center(
                                            child: Text(
                                              'Donate',
                                              style: TextStyle(
                                                  color: kprimary,
                                                  fontSize: 20),
                                            ),
                                          ),
                                        )),
                                    SizedBox(
                                      height:
                                      MediaQuery.of(context).size.height /
                                          40,
                                    ),
                                    ElevatedButton(
                                        onPressed: () {
                                          Get.back();
                                        },
                                        child: Container(
                                          decoration: BoxDecoration(
                                              color: kblue,
                                              borderRadius:
                                              BorderRadius.circular(20)),
                                          height: MediaQuery.of(context)
                                              .size
                                              .height /
                                              15,
                                          width: MediaQuery.of(context)
                                              .size
                                              .width /
                                              4,
                                          child: Center(
                                            child: Text(
                                              'cancel',
                                              style: TextStyle(color: kprimary),
                                            ),
                                          ),
                                        ))
                                  ],
                                ),
                              );
                            });
                      },
                      child:  const Center(
                          child: Icon(
                            FontAwesomeIcons.plus,
                            color: kprimary,
                          ))),
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
  void _Stationerydonate()
  {
    donatecont.Donation();
  }
}


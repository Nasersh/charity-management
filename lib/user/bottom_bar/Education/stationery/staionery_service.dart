import 'package:http/http.dart' as http;

import '../../../../config/config.dart';
import '../../../../secureStorage.dart';
import '../../../model/stationery_model.dart';


class StationeryService
{
  var message;
  var url = Uri.parse(ServerConfig.domainNameServer + ServerConfig.showallStationery);

  Future<List<Stationery>> getStationery() async{
    var _storage=SecureStorage();
    String? token = await _storage.read('token');
    var response = await http.get(url,
        headers: {
          'Accept' : 'application/json',
          'Authorization'  :  'Bearer $token'
        }
    );
    print(response.statusCode);

    if(response.statusCode == 200){
      var Stationerys = stationeryModelFromJson(response.body);
      return Stationerys.stationery;
    }
    else {
      return [];
    }
  }
//
// Future<List<Patient>> loadProductsFromApi(int index) async {
//   var response = await http.get(Uri.parse(ServerConfig.domainNameServer+ServerConfig.showCategoryProducts + '${index}'),
//     headers: {
//       'Accept': 'application/json',
//     },
//   );
//   print(response.statusCode);
//   //  print(response.body);
//
//   if(response.statusCode == 200){
//     var porductscat = productsFromJson(response.body);
//     return porductscat.products;
//   }
//   else {
//     return [];
//   }
}
import 'package:get/get.dart';

import 'kit_donatio_service.dart';

class kitDonationController extends GetxController
{
  var id;
  var current;
  var total;
  var count=0.obs;
@override
  void onReady() {
    count.value=0;
    total=0;
    super.onReady();
  }
  KitDonationService _service=new KitDonationService();
  Donation() async
  {
    current=await _service.Donate(count.toString(),id);
  }
}
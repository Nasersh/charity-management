
import 'package:http/http.dart' as http;

import '../../../../config/config.dart';
import '../../../../secureStorage.dart';
import '../../../model/kitchen_needs_model.dart';

class kitService
{

  var message;
  var url = Uri.parse(ServerConfig.domainNameServer + ServerConfig.showallkitchenNeeds);

  Future<List<KitchenSetElement>> getkitchenNeeds() async {
    var _storage = SecureStorage();
    String? token = await _storage.read('token');
    var response = await http.get(url,
        headers: {
          'Accept': 'application/json',
          'Authorization': 'Bearer $token'
        }
    );
    print(response.statusCode);

    if (response.statusCode == 200) {
      var kitchenNeeds = kitchenSetFromJson(response.body);
      return kitchenNeeds.kitchenSet;
    }
    else {
      return [];
    }
  }}
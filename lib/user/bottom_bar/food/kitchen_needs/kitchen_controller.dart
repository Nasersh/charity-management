import 'package:get/get.dart';
import '../../../model/kitchen_needs_model.dart';
import 'kitchen_needs_service.dart';

class KitController extends GetxController
{
  var kitchenNeedslist =<KitchenSetElement>[].obs;
  var kitchenNeeds=<KitchenSetElement>[];

  var id=0.obs;

  kitService _service= kitService();
  var isloading=true.obs;

  @override
  void onInit() {
    showkitchenNeeds();
    super.onInit();
  }

  void showkitchenNeeds() async {
    try {
      isloading(true);
      kitchenNeeds = await _service.getkitchenNeeds();
      if (kitchenNeeds != null) {
        kitchenNeedslist.value = kitchenNeeds;
      }
    } finally {
      isloading(false);
    }
  }

}
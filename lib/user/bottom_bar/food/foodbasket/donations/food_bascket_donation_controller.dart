import 'package:get/get.dart';

import 'food_bascket_donatio_service.dart';

class FoodBascketsDonationController extends GetxController
{
  var id;
  var donation;
  var current;
  var count=0.obs;

  @override
  void onReady() {
     count.value=0;

    super.onReady();
  }
  FoodBascketDonationService _service=new FoodBascketDonationService();
  Donation() async
  {
    current=await _service.Donate(count.toString(),id);
  }
}
import 'package:get/get.dart';
import '../../../model/FoodBascketModel.dart';
import 'food_bascket_service.dart';



class FoodBascketController extends GetxController
{
  var FoodBascketslist =<FoodBascket>[].obs;
  var Food=<FoodBascket>[];

  var id=0.obs;

  FoodBascketService _service= FoodBascketService();
  var isloading=true.obs;

  @override
  void onInit() {
    showFood();
    super.onInit();
  }

  void showFood() async {
    try {
      isloading(true);
      Food = await _service.getFoodBascket();
      if (Food != null) {
        FoodBascketslist.value = Food;
      }
    } finally {
      isloading(false);
    }
  }


}
import 'package:get/get.dart';
import '../../../model/Food_stuff_model.dart';
import 'food_stuff_service.dart';

class FoodStuffController extends GetxController
{
  var FoodStufflist =<FoodStuffModel>[].obs;
  var FoodStuff=<FoodStuffModel>[];

  var id=0.obs;

  FoodStuffService _service= FoodStuffService();
  var isloading=true.obs;

  @override
  void onInit() {
    showFoodStuff();
    super.onInit();
  }

  void showFoodStuff() async {
    try {
      isloading(true);
      FoodStuff = await _service.getFoodStuff();
      if (FoodStuff != null) {
        FoodStufflist.value = FoodStuff;
      }
    } finally {
      isloading(false);
    }
  }


}
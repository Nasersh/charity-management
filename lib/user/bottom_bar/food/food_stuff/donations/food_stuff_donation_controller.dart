import 'package:get/get.dart';

import 'food_stuff_donatio_service.dart';

class FoodStuffsDonationController extends GetxController
{
  var id;
  var donation;
  var current;

  @override
  void onReady() {

    super.onReady();
  }
  FoodStuffDonationService _service=new FoodStuffDonationService();
  Donation(money) async
  {
    current=await _service.Donate(money,id);
  }
}
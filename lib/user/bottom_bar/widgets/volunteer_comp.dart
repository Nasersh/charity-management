
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get/get_core/src/get_main.dart';

import '../../../configrations/const/const.dart';
import '../../volunteer/volunteer.dart';

class VolunteerComponent extends StatelessWidget {
  VolunteerComponent({
    required this.title,
    Key? key,
  }) : super(key: key);
  String title;
  @override
  Widget build(BuildContext context) {
    return Padding(
        padding: const EdgeInsets.symmetric(horizontal: 20),
        child: Container(
          height: MediaQuery.of(context).size.height / 6,
          width: MediaQuery.of(context).size.width,
          decoration: BoxDecoration(
            color: kblue,
            boxShadow: const [kDefaultShadow],
            borderRadius: BorderRadius.circular(20),
          ),
          child: Column(
            children: [
              Padding(
                padding: const EdgeInsets.symmetric(
                    horizontal: 10, vertical: 10),
                child: Text(
                  title,
                  style: TextStyle(color: kongreen, fontSize: 20),
                ),
              ),
              SizedBox(
                height: MediaQuery.of(context).size.height / 60,
              ),
              TextButton(
                  onPressed: () {
                    Get.to(Volunteer());
                  },
                  child: const Text(
                    'Volunteer',
                    style: TextStyle(color: kprimary, fontSize: 20),
                  ))
            ],
          ),
        ));
  }
}


import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get/get_core/src/get_main.dart';

import '../../../configrations/const/const.dart';
import '../health/patients/patients.dart';

class CategoryCard extends StatelessWidget {
  CategoryCard({
    required this.name,
    required this.image,
    required this.onPressed,
    Key? key,
  }) : super(key: key);
  String name;
  String image;
  Function()? onPressed;
  @override
  Widget build(BuildContext context) {
    return Padding(padding: EdgeInsets.all(10),
        child: Container(
          height: MediaQuery.of(context).size.height/4.5,
          width: MediaQuery.of(context).size.width/2.3,
          decoration: BoxDecoration(
            color: kblue,
            boxShadow: const [
              kDefaultShadow
            ],
            borderRadius: BorderRadius.circular(20),
          ),
          child: ElevatedButton(
            onPressed: (){
              Get.to(() => Patients());
            },
            child: Column(
              children: [
                Padding(
                  padding: const EdgeInsets.symmetric(vertical: 13),
                  child: Container(
                    height: MediaQuery.of(context).size.height/7,
                    child: Image.asset("images/patients.png"),
                  ),
                ),
                Text('Patients',style: TextStyle(color: kprimary,fontSize: 20),)

              ],),
          ),
        )
    );
  }
}

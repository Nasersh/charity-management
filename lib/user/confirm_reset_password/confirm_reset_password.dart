import 'package:flutter/material.dart';
import 'package:flutter_neumorphic/flutter_neumorphic.dart';
import 'package:flutter_svg/flutter_svg.dart';

class ConfirmPassword extends StatelessWidget {
  const ConfirmPassword({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    double widthMedia = MediaQuery.of(context).size.width;
    double heightMedia = MediaQuery.of(context).size.height;
    double widthRatio = 1.152;
    double heightRatio = 1.22;

    return SafeArea(
      child: Scaffold(
        backgroundColor: const Color.fromRGBO(236, 240, 243, 1),
        body: Stack(
          children: [
            buildTheCircle2(
                319, 52, const Color.fromRGBO(151, 167, 195, 1), Colors.white),
            Positioned(
              top: 38.96 * (1.22),
              left: 134.2 * (1.152),
              child: Container(
                alignment: Alignment.center,
                padding: EdgeInsets.only(
                    top: 1.36 * heightRatio,
                    right: 7.37 * widthRatio,
                    left: 6.68 * widthRatio,
                    bottom: 0.68 * heightRatio),
                width: 21.04 * (1.152),
                height: 21.04 * (1.22),
                color: const Color.fromRGBO(25, 53, 102, 1),
                child: const Text(
                  "T",
                  style: TextStyle(color: Color.fromRGBO(252, 252, 252, 1)),
                ),
              ),
            ),
            Positioned(
              top: 38.96 * heightRatio,
              left: 164.76 * widthRatio,
              child: Container(
                height: 21.04 * heightRatio,
                width: 76.04 * widthRatio,
                child: const FittedBox(
                  child: Text(
                    "R  A  C  K",
                    style: TextStyle(
                      color: Color.fromRGBO(25, 53, 102, 1),
                      fontWeight: FontWeight.w900,
                      fontSize: 25,
                    ),
                  ),
                ),
              ),
            ),
            buildTheCircle(
              55,
              49,
              Colors.white,
              const Color.fromRGBO(151, 167, 195, 1),
            ),
            Positioned(
              top: 168 * heightRatio,
              left: 34 * widthRatio,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                      height: 35 * heightRatio,
                      width: 121 * widthRatio,
                      margin: EdgeInsets.only(
                        bottom: 8 * heightRatio,
                      ),
                      child: const FittedBox(
                        child: Text(
                          "Welcome",
                          style: TextStyle(
                            fontSize: 24,
                            color: Color.fromRGBO(25, 53, 102, 1),
                            fontWeight: FontWeight.w900,
                          ),
                        ),
                      )),
                  Container(
                      height: 35 * heightRatio,
                      width: 169 * widthRatio,
                      child: const FittedBox(
                        child: Text(
                          "Lets get started",
                          style: TextStyle(
                            fontSize: 24,
                            color: Color.fromRGBO(25, 53, 102, 1),
                            fontWeight: FontWeight.w900,
                          ),
                        ),
                      )),
                  Neumorphic(
                    style: NeumorphicStyle(
                        depth: -10,
                        boxShape: NeumorphicBoxShape.roundRect(
                            BorderRadius.circular(40))),
                    child: Container(
                      padding: EdgeInsets.only(
                          left: 20 * widthRatio,
                          top: 13,
                          bottom: 13 * heightRatio),
                      width: 308 * widthRatio,
                      height: 51 * heightRatio,
                      color: const Color.fromRGBO(236, 240, 243, 1),
                      child: Row(
                        children: [
                          const Text(
                            "Insert code",
                            style: TextStyle(
                                color: Color.fromRGBO(98, 124, 168, 1),
                                fontSize: 16,
                                fontWeight: FontWeight.w600),
                          ),
                          SizedBox(width: (142 + 22) * widthRatio),
                          SvgPicture.asset(
                            'assets/delete.svg',
                            height: 18.6 * heightRatio,
                            width: 31 * widthRatio,
                            color: const Color.fromRGBO(25, 53, 102, 1),
                          ),
                        ],
                      ),
                    ),
                  ),
                  SizedBox(height: 12 * heightRatio),
                  Container(
                    margin: EdgeInsets.only(
                      left: 56 * widthRatio,
                    ),
                    height: 213 * heightRatio,
                    width: 195 * widthRatio,
                    child: Column(
                      children: [
                        Row(
                          children: [
                            buildCalculator(widthRatio, heightRatio, "1"),
                            SizedBox(width: 39 * widthRatio),
                            buildCalculator(widthRatio, heightRatio, "2"),
                            SizedBox(width: 39 * widthRatio),
                            buildCalculator(widthRatio, heightRatio, "3")
                          ],
                        ),
                        SizedBox(height: 19 * heightRatio),
                        Row(
                          children: [
                            buildCalculator(widthRatio, heightRatio, "4"),
                            SizedBox(width: 39 * widthRatio),
                            buildCalculator(widthRatio, heightRatio, "5"),
                            SizedBox(width: 39 * widthRatio),
                            buildCalculator(widthRatio, heightRatio, "6")
                          ],
                        ),
                        SizedBox(height: 19 * heightRatio),
                        Row(
                          children: [
                            buildCalculator(widthRatio, heightRatio, "7"),
                            SizedBox(width: 39 * widthRatio),
                            buildCalculator(widthRatio, heightRatio, "8"),
                            SizedBox(width: 39 * widthRatio),
                            buildCalculator(widthRatio, heightRatio, "9")
                          ],
                        ),
                        SizedBox(height: 19 * heightRatio),
                        Row(
                          children: [
                            buildCalculator(widthRatio, heightRatio, "*"),
                            SizedBox(width: 39 * widthRatio),
                            buildCalculator(widthRatio, heightRatio, "0"),
                            SizedBox(width: 39 * widthRatio),
                            buildCalculator(widthRatio, heightRatio, "#")
                          ],
                        ),
                      ],
                    ),
                  ),
                  SizedBox(height: 19  * heightRatio),
                  Container(
                    margin: EdgeInsets.only(
                      left: 105 * widthRatio,
                    ),
                    width: 98 * widthRatio * heightRatio,
                    height: 21 * heightRatio,
                    child: Text(
                      "Resend code",
                      style: TextStyle(
                          color: const Color.fromRGBO(25, 53, 102, 1),
                          fontSize: 15 * widthRatio * heightRatio,
                          fontWeight: FontWeight.w600,
                          decoration: TextDecoration.underline,
                          decorationStyle: TextDecorationStyle.solid,
                          decorationColor:
                              const Color.fromRGBO(25, 53, 102, 1)),
                      textAlign: TextAlign.center,
                    ),
                  ),
                  SizedBox(height: 14 * heightRatio),
                  Container(
                    margin: EdgeInsets.only(
                      left: 96 * widthRatio,
                    ),
                    alignment: Alignment.center,
                    width: 134 * widthRatio,
                    height: 57 * heightRatio,
                    decoration: BoxDecoration(
                        color: const Color.fromRGBO(236, 240, 243, 1),
                        borderRadius: BorderRadius.circular(70),
                        boxShadow: const [
                          BoxShadow(
                            offset: Offset(10, 16),
                            color: Color.fromRGBO(151, 167, 195, 0.5),
                            blurRadius: 12,
                            spreadRadius: 1,
                          ),
                          BoxShadow(
                            offset: Offset(-13, -16),
                            color: Colors.white,
                            blurRadius: 22,
                            spreadRadius: 6,
                          )
                        ]),
                    child:  Text(
                      "Confirm",
                      style: TextStyle(
                        fontSize: 20 * widthRatio ,
                        fontWeight: FontWeight.bold,
                        color: const Color.fromRGBO(
                          25,
                          53,
                          102,
                          1,
                        ),
                      ),
                    ),
                  ),
                  SizedBox(height: 17 * heightRatio),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  Container buildCalculator(double widthRatio, double heightRatio, String num) {
    return Container(
      alignment: Alignment.center,
      width: 39 * widthRatio,
      height: 38 * heightRatio,
      child: Text(num,
          style: TextStyle(
              fontSize: 24 * widthRatio * heightRatio,
              color: const Color.fromRGBO(25, 53, 102, 1))),
      decoration: const BoxDecoration(
          shape: BoxShape.circle,
          color: Color.fromRGBO(236, 240, 243, 1),
          boxShadow: [
            BoxShadow(
                color: Color.fromRGBO(151, 167, 195, 0.5),
                blurRadius: 30,
                spreadRadius: 3,
                offset: Offset(15, 10)),
            BoxShadow(
                color: Colors.white,
                blurRadius: 30,
                spreadRadius: 3,
                offset: Offset(-15, -10))
          ]),
    );
  }

  Positioned buildTheCircle(double xOff, double yOff, Color up, Color down) {
    return Positioned(
      top: -xOff * (1.152),
      left: -yOff * (1.22),
      child: Container(
        width: 160 * (1.152),
        height: 159 * (1.22),
        decoration: const BoxDecoration(
          color: Color.fromRGBO(236, 240, 243, 1),
          shape: BoxShape.circle,
          boxShadow: [
            BoxShadow(
                color: Color.fromRGBO(151, 167, 195, 1),
                offset: Offset(3, 3),
                blurRadius: 45,
                spreadRadius: 6),
            BoxShadow(
              color: Colors.white,
              offset: Offset(-4, -4),
              blurRadius: 15,
              spreadRadius: 2,
            ),
          ],
        ),
      ),
    );
  }

  Positioned buildTheCircle2(double xOff, double yOff, Color up, Color down) {
    return Positioned(
      top: yOff * (1.152),
      left: xOff * (1.22),
      child: Container(
        width: 160 * (1.152),
        height: 159 * (1.22),
        decoration: const BoxDecoration(
          color: Color.fromRGBO(236, 240, 243, 1),
          shape: BoxShape.circle,
          boxShadow: [
            BoxShadow(
                color: Color.fromRGBO(151, 167, 195, 1),
                offset: Offset(2, 4),
                blurRadius: 2,
                spreadRadius: 3),
            BoxShadow(
              color: Colors.white,
              offset: Offset(-20, -20),
              blurRadius: 40,
              spreadRadius: 1,
            ),
          ],
        ),
      ),
    );
  }
}

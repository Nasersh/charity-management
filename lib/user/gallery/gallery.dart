import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:get/get.dart';
import 'package:get/get_core/src/get_main.dart';

import '../../configrations/const/const.dart';
import 'gallery_controller.dart';

class Gallery extends StatelessWidget {
   Gallery({Key? key}) : super(key: key);
  GalleryController controller=Get.put(GalleryController());
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: kprimary,
    appBar: AppBar(
      centerTitle: true,
    title: Text('orphans art'),
        ),
      body: Column(
        children:[
          SizedBox(

          ),
          Obx(()=> Expanded(child: _buildProductsList())
          )

      ]),
    );
  }
  Widget _buildProductsList() {
    return ListView.builder(
      shrinkWrap: true,
      itemCount: controller.posts.value.length,
      padding: EdgeInsets.only(top: 16),
      itemBuilder: (context, index) =>   Padding( padding: EdgeInsets.symmetric(vertical: 5),
            child: Obx(()=> InkWell(
                child: Column(
                  children: <Widget>
                  [
                    ListTile( leading: CircleAvatar(
                      backgroundImage: AssetImage( "images/handmade${controller.postlist[index].id}.jpg", ),
                    ),
                      contentPadding: EdgeInsets.all(0),
                      title: Text( '${controller.postlist[index].name}',
                        style: TextStyle( fontWeight: FontWeight.bold, ), ),
                      trailing: const Padding(
                        padding: EdgeInsets.symmetric(horizontal: 40.0),
                        child: Icon(
                          FontAwesomeIcons.heart
                        ),
                      ), ),
                    // const Padding(padding: EdgeInsets.all(10),
                    // child: Text('joel having fun with colors',style: TextStyle(
                    //   color: Colors.black54
                    // ),
                            // ),
                            //
                    //             ),
                    Image.asset( "images/handmade${index+1}.jpg", height: 500,
                      width: MediaQuery.of(context).size.width,
                      fit: BoxFit.cover, ),
                      // SizedBox(
                      //   height: 20,
                      // ),
                      Container(
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.only(bottomLeft: Radius.circular(20),bottomRight: Radius.circular(20)),
                          color: Colors.white
                        ),
                        child: Column(
                          children: [
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                              children: [
                                IconButton(onPressed: (){
                                  var indexx =controller.postlist[index].id;
                                  controller.Like(indexx);
                                  !controller.liked.value?controller.postlist[index].count++
                                      : controller.postlist[index].count--
                                  ;
                                },
                                    icon:Icon(
                                      FontAwesomeIcons.thumbsUp,
                                      color: controller.liked.value? Colors.blueAccent:Colors.grey,
                                    )
                                ),
                                //تلت كومنت
                                ////////////////////////////////////////////
                                IconButton(onPressed:
                                // !controller.sold.value?
                                    (){
                                  var indexx =controller.postlist[index].id;
                                  controller.buy(indexx);
                                },
                                    // :(){
                                    //
                                    // },
                                    icon: Icon(
                                      FontAwesomeIcons.wallet,color: kstorm,
                                    )),

                              ],
                            ),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                              children: [
                                Text('likes ${controller.postlist[index].count}'),
                                Obx(()=> Text(controller.sold.value?'sold':'buy'))
                              ],
                            ),
                          ],
                        ),
                      )
                    ,
                    Divider(
                      thickness: 1,
                      height: 10,
                    ),
                  ], ),
                onTap: (){}, ),
            )),
    );
  }

}




// import 'package:flutter/material.dart';
// class PostItem extends StatefulWidget
// {
//   final String dp; final String name;
//   final String time; final String img;
//   PostItem(
//       { Key key,
//     @required this.dp, @required this.name, @required this.time, @required this.img }) :
//         super(key: key); @override _PostItemState createState() => _PostItemState();}
//         class _PostItemState extends State<PostItem> {
//   @override Widget build(BuildContext context)
//   {
//           return Padding( padding: EdgeInsets.symmetric(vertical: 5),
//             child: InkWell(
//               child: Column(
//                 children: <Widget>
//                 [ ListTile( leading: CircleAvatar(
//                     backgroundImage: AssetImage( "${widget.dp}", ),
//                   ), contentPadding: EdgeInsets.all(0),
//                     title: Text( "${widget.name}",
//                       style: TextStyle( fontWeight: FontWeight.bold, ), ),
//                     trailing: Text( "${widget.time}",
//                       style: TextStyle(
//                         fontWeight: FontWeight.w300, fontSize: 11, ), ), ),
//                   Image.asset( "${widget.img}", height: 170, width: MediaQuery.of(context).size.width,
//                     fit: BoxFit.cover, ), ], ), onTap: (){}, ), ); }}
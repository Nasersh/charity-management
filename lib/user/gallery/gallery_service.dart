import 'dart:convert';

import '../../config/config.dart';
import '../../secureStorage.dart';
import 'package:http/http.dart' as http;

import '../model/post.dart';

class GalleryService {
  var message;
  var index;
  var url = Uri.parse(
      ServerConfig.domainNameServer + ServerConfig.showallposts);

  Future<List<Gallery>> getposts() async {
    var _storage = SecureStorage();
    String? token = await _storage.read('token');
    print('${token}');
    var response = await http.get(url,
        headers: {
          'Authorization': 'Bearer $token',
          'Accept': 'application/json',
        }
    );
    print(response.statusCode);

    if (response.statusCode == 200) {
      var posts = postFromJson(response.body);
      return posts.gallery;
    }
    else {
      return [];
    }
  }

  Future<String?> likepost(id) async {
    index = id.toString();

    var url = Uri.parse(
        ServerConfig.domainNameServer + ServerConfig.like + '${index}');
    var _storage = SecureStorage();
    String? token = await _storage.read('token');
    print(token);
    var response = await http.get(url,
      headers:
      {
        'Accept': 'application/json',
        'Authorization': 'Bearer $token'
      },
    );
    print(response.statusCode);
    if (response.statusCode == 200) {
      print('successful');
      // return ' ';
    }
    else {
      print('fail');
      return '';
    }
  }

  Future<bool> buypost(id) async {
    index = id.toString();

    var url = Uri.parse(
        ServerConfig.domainNameServer + ServerConfig.Buy + '${index}');
    var _storage = SecureStorage();
    String? token = await _storage.read('token');
    print(token);
    var response = await http.get(url,
      headers:
      {
        'Accept': 'application/json',
        'Authorization': 'Bearer $token'
      },
    );
    print(response.statusCode);
    var jsonresponse = jsonDecode(response.body);
    var nunez = jsonresponse["message"];
    if (response.statusCode == 200) {
      if (nunez == 'done') {
        print('$nunez');
        return false;
      }

      else {
        print('${nunez}');
        return true;
      }
    }
    else {
      return false;
    }
  }
// To parse this JSON data, do
//
//     final issold = issoldFromJson(jsonString);
}
// To parse this JSON data, do
//
//     final issold = issoldFromJson(jsonString);

//
// Issold issoldFromJson(String str) => Issold.fromJson(json.decode(str));
//
// String issoldToJson(Issold data) => json.encode(data.toJson());
//
// class Issold {
//   Issold({
//     required this.meaasge,
//   });
//
//   String meaasge;
//
//   factory Issold.fromJson(Map<String, dynamic> json) => Issold(
//     meaasge: json["meaasge"],
//   );
//
//   Map<String, dynamic> toJson() => {
//     "meaasge": meaasge,
//   };
// }

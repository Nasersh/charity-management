import 'package:get/get.dart';
import '../model/post.dart';
import 'gallery_service.dart';

class GalleryController extends GetxController
{
  List<Gallery> postlist=<Gallery>[].obs;
  var posts=<Gallery>[].obs;
  RxBool sold=false.obs;
  var liked=false.obs;
  var id=0.obs;
  var count=0.obs;

  GalleryService _service= GalleryService();
  var isloading=true.obs;

  @override
  void onInit()async {
    showposts();
    super.onInit();
  }
  @override
  void onReady() async{
    // TODO: implement onReady

    showposts();
  }

  void showposts() async {
    try {
      isloading(true);
      posts.value = await _service.getposts();
      if (posts != null) {
        postlist= posts;
      }
    } finally {
      isloading(false);
    }
  }
  Like(index) async
  {
   await _service.likepost(index);
   liked.value=!liked.value;
  }

  buy(index) async
  {
   sold.value= await _service.buypost(index);
  }

}
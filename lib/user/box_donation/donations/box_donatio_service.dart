import 'dart:convert';
import 'package:http/http.dart' as http;

import '../../../../config/config.dart';
import '../../../../secureStorage.dart';

class BoxDonationService
{
  var donate;
  var message;
  var index;
  Future<void> Donate(donation) async{
    // index=id.toString();
    donate=int.parse(donation);
    // print(index);
    var url=Uri.parse(ServerConfig.domainNameServer+ServerConfig.BoxDonate);
    var _storage=SecureStorage();
    String? token = await _storage.read('token');
    print(token);
    var response = await http.post(url,
        headers:
        {
          'Accept' : 'application/json',
          'Authorization'  :  'Bearer $token'
        },
        body:
        {
          'amount' : donation
        }
    );
    // print(response.statusCode);
    // var jsonres=jsonDecode(response.body);

    if(response.statusCode == 200){
      print('successful');
    }
    else {
      print('fail');
    }
  }
}
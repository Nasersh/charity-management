import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get/get_core/src/get_main.dart';
import 'package:percent_indicator/circular_percent_indicator.dart';
import '../../configrations/const/const.dart';
import '../wallet/wallet_controller.dart';
import 'donations/box_donation_controller.dart';

class Box extends StatelessWidget {
  Box({Key? key}) : super(key: key);
  BoxDonationController controller = Get.put(BoxDonationController());
  WalletController wc = Get.put(WalletController());
  GlobalKey _key = GlobalKey();
  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: Stack(
      children: [
        Expanded(child: Container(
          color: kblue,
        )),
        Positioned(
          child: Container(
            alignment: Alignment.bottomCenter,
            decoration: const BoxDecoration(
              borderRadius: BorderRadius.only(topRight:Radius.circular(20),topLeft:Radius.circular(20)),
              color: kprimary
            ),
            height:  MediaQuery.of(context).size.height*.9,
            child: Column(children: [
              Padding(
                padding: EdgeInsets.symmetric(
                    vertical: MediaQuery.of(context).size.height / 50,
                    horizontal: MediaQuery.of(context).size.width / 3.5),
                child: Column(
                  children: const [
                    Text(
                      'Direct Donation',
                      style: TextStyle(
                          color: kblue, fontWeight: FontWeight.w500, fontSize: 20),
                    ),
                  ],
                ),
              ),
              const Text(
                'for compaines and users who want to donte directly',
              ),
              SizedBox(
                height: MediaQuery.of(context).size.height / 20,
              ),
              CircularPercentIndicator(
                radius: 100.0,
                animation: true,
                animationDuration: 1200,
                lineWidth: 15.0,
                percent: 0.24,
                center: const Text(
                  "100%",
                  style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20.0),
                ),
                circularStrokeCap: CircularStrokeCap.butt,
                backgroundColor: kblue,
                progressColor: kstorm,
              ),
              SizedBox(
                height: MediaQuery.of(context).size.height / 20,
              ),
              ListTile(
                leading: Image.asset(
                  'images/bluedonate.png',
                  scale: 0.1,
                ),
                trailing: const Text(
                  '76%',
                  style: TextStyle(
                      fontSize: 20, color: kblue, fontWeight: FontWeight.bold),
                ),
                title: const Text(
                  'donation for orphan necessarites',
                  style: TextStyle(
                      // color: kblue,
                      fontSize: 15,
                      fontWeight: FontWeight.w500),
                ),
              ),
              const Divider(
                height: 30,
                thickness: 0.9,
                color: Colors.grey,
              ),
              ListTile(
                leading: Image.asset(
                  'images/orangedonation.png',
                  scale: 0.1,
                ),
                trailing: const Text(
                  '24%',
                  style: TextStyle(
                      fontSize: 20, color: kstorm, fontWeight: FontWeight.bold),
                ),
                title: const Text(
                  'donation for orphans activities',
                  style: TextStyle(
                      // color: kblue,
                      fontSize: 15,
                      fontWeight: FontWeight.w500),
                ),
              ),
              const Divider(
                height: 30,
                thickness: 0.9,
                color: Colors.grey,
              ),
              SizedBox(
                height: MediaQuery.of(context).size.height / 15,
              ),
              Material(
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(300.0)),
                elevation: 10.0,
                color: kstorm,
                clipBehavior: Clip.antiAlias, // Add This
                child: MaterialButton(
                  minWidth: MediaQuery.of(context).size.width - 160,
                  height: 35,
                  color: kblue,

                  ////فكرة ليدي
                  //التافع//
                  //رز
                  child: const Text('quick donate',
                      style: TextStyle(fontSize: 16.0, color: kprimary)),
                  onPressed: () {
                    _displayTextInputDialog(context);
                  },
                ),
              ),
            ]),
          ),
        ),
      ],
    ));
  }

  Future<void> _displayTextInputDialog(BuildContext context) async {
    return showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            shape: const RoundedRectangleBorder(
                borderRadius: BorderRadius.all(Radius.circular(32.0))),
            title: const Text(
              'Donate for Orphans',
              style: TextStyle(color: kblue),
            ),
            content: Form(
              key: _key,
              autovalidateMode: AutovalidateMode.onUserInteraction,
              child: TextFormField(
                validator: (value) {
                  if (int.parse(value!) > wc.wallet.value) {
                    return 'you dont have enough money';
                  }
                },
                keyboardType: TextInputType.number,
                onChanged: (value) {
                  controller.donation = value;
                },
                decoration: InputDecoration(hintText: "Enter the amount"),
              ),
            ),
            actions: [
              Center(
                child: ElevatedButton(
                  onPressed: () {
                    if (int.parse(controller.donation) < wc.wallet.value) {
                      controller.Donation(controller.donation);
                      Get.back();
                    }
                  },
                  child: Container(
                    width: MediaQuery.of(context).size.width / 6,
                    child: const Center(
                      child: Text(
                        'donate',
                        style: TextStyle(color: kprimary),
                      ),
                    ),
                  ),
                ),
              )
            ],
          );
        });
  }
}

import 'package:charitysys/login/login.dart';
import 'package:charitysys/services/local_notification.dart';
import 'package:charitysys/user/gallery/gallery.dart';
import 'package:flutter/material.dart';
import 'package:get/get_navigation/src/root/get_material_app.dart';
import 'package:get/get_navigation/src/routes/get_route.dart';
import 'InApp.dart';
import 'configrations/const/const.dart';
import 'configrations/functions_status/functions_status.dart';
import 'configrations/select_lang/select_lang.dart';
// <<<<<<< HEAD
// =======
// >>>>>>> 2c6a0d2 (new file)
// <<<<<<< HEAD
// =======
// >>>>>>> 2c6a0d2 (new file)
import 'dashboard/completed/education/complete_guarantee/complete_guarantee.dart';
import 'dashboard/completed/food/complete_food_section/complete_food_parcel_screen.dart';
import 'dashboard/completed/food/complete_kitchen_sets/complete_kitchen_sets.dart';
import 'dashboard/completed/health/medical_tools/complete_medical_tools.dart';
import 'dashboard/completed/health/medicines/complete_medicines.dart';
import 'dashboard/completed/health/patients/complete_patients.dart';
// <<<<<<< HEAD
// =======
// >>>>>>> 2c6a0d2 (new file)
import 'dashboard/dashboard.dart';
import 'dashboard/incomplete/Education/guarantee/guarantee.dart';
import 'dashboard/incomplete/Education/in_complete_stationery/incomplete_stationety.dart';
import 'dashboard/incomplete/Education/in_guarantee/incomplete_guarantee.dart';
import 'dashboard/incomplete/Education/stationery/add_stationery.dart';
import 'dashboard/incomplete/Health/in_complete_category/in_complete_category.dart';
import 'dashboard/incomplete/Health/incomplete_medical_tools/incomplete_medical_tools.dart';
import 'dashboard/incomplete/Health/incomplete_medicines/incomplete_medicines.dart';
import 'dashboard/incomplete/Health/incomplete_patients/incomplete_patients.dart';
import 'dashboard/incomplete/Health/medical_tools/medical_tools.dart';
import 'dashboard/incomplete/Health/medicines/add_medicine.dart';
import 'dashboard/incomplete/food/food_parcel/add_food_parcel/add_food_parcel.dart';
import 'dashboard/incomplete/food/food_parcel/incomplete_food_parcel/incomplete_food_parcel_screen.dart';
import 'dashboard/incomplete/food/food_section/add_food_section/add_food_section.dart';
import 'dashboard/incomplete/food/food_section/incomplete_food_section/incomplete_food_section.dart';
import 'dashboard/incomplete/food/kitchen_sets/incomplete_kitchen_sets/incomplete_kitchen_sets.dart';
import 'dashboard/incomplete/food/kitchen_sets/kitchen_sets/kitchen_sets_screen.dart';
import 'dashboard/volunteer/accept_reject/accept_reject.dart';
import 'dashboard/volunteer/show volunteers/medical_team/medical_team.dart';
import 'dashboard/volunteer/show volunteers/showvolunteers.dart';
import 'intro_oage/into_page.dart';
import 'user/bottom_bar/Education/sponsership/my_guarantees/myguarantee.dart';
import 'user/bottom_bar/health/patients/patients.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  LocalNotification.intialize();
  // await Firebase.initializeApp();
  // await FirebaseMessaging.instance.getToken().then((value) => print(value));

  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      theme: ThemeData(
          canvasColor: Colors.transparent,
          primarySwatch: buildMaterialColor(Color(0xff093d65))),
      debugShowCheckedModeBanner: false,
      color: kblue,
      initialRoute: '/login',
      getPages: [
        GetPage(name: '/inapp', page: () => InApp()),
        GetPage(name: '/gallery', page: () => Gallery()),
        GetPage(name: '/my', page: () => MyGuarantees()),
        GetPage(name: '/login', page: () => LoginPage()),
        GetPage(name: '/patients', page: () => Patients()),
        GetPage(name: '/medicalteam', page: () => MedicalVol()),
        GetPage(name: '/volunteers', page: () => ShowVolunteers()),
        GetPage(name: "/pageV", page: () => const IntroPage()),
        GetPage(name: "/login", page: () => LoginPage()),
        GetPage(name: "/dashboard", page: () => const Dashboard()),
        GetPage(name: "/acceptReject", page: () => const AcceptReject()),
        GetPage(name: '/lang', page: () => SelectLang()),
        GetPage(name: '/add_medicine', page: () => const AddMedicine()),
        GetPage(name: '/add_medical', page: () => const AddMedicalTool()),
        GetPage(name: '/inCompPatientsAdmin', page: () => IncompletePatients()),
        GetPage(name: '/inComp_medicines', page: () => IncompleteMedicines()),
        GetPage(
            name: '/inComp_medical_tools',
            page: () => IncompleteMedicalTools()),
        GetPage(name: '/inCompCat', page: () => InCompleteCategory()),
        GetPage(name: '/add_stationery', page: () => const AddStationery()),
        GetPage(
            name: '/stationery_incomplete', page: () => InCompleteStationery()),
        GetPage(name: '/function_status', page: () => FunctionStatus()),
        GetPage(name: '/add_guarantee', page: () => const AddGuarantee()),
        GetPage(name: '/inComp_guarantee', page: () => InCompleteGuarantee()),
        GetPage(name: '/add_food_parcel', page: () => const AddFoodParcel()),
        GetPage(name: '/add_food_item', page: () => const AddFoodSection()),
        GetPage(name: '/add_kitchen_item', page: () => const AddKitchenSet()),
        GetPage(name: '/inCompFoodParcel', page: () => InCompleteFoodParcel()),
        GetPage(
            name: '/inCompFoodSection', page: () => InCompleteFoodSection()),
        GetPage(name: '/inCompKitchenSet', page: () => InCompleteKitchenSet()),
        GetPage(name: '/compMedicine', page: () => CompleteMedicines()),
        GetPage(name: '/comMedical', page: () => CompleteMedicalTools()),
        GetPage(name: '/comPatient', page: () => CompletePatients()),
        GetPage(name: '/comFoodSection', page: () => CompleteFoodSection()),
        GetPage(name: '/comKitchenSet', page: () => CompleteKitchenSet()),
        GetPage(name: '/comGuarantee', page: () => CompleteGuarantee()),
      ],
    );
  }

  MaterialColor buildMaterialColor(Color color) {
    List strengths = <double>[.05];
    Map<int, Color> swatch = {};
    final int r = color.red, g = color.green, b = color.blue;

    for (int i = 1; i < 10; i++) {
      strengths.add(0.1 * i);
    }
    strengths.forEach((strength) {
      final double ds = 0.5 - strength;
      swatch[(strength * 1000).round()] = Color.fromRGBO(
        r + ((ds < 0 ? r : (255 - r)) * ds).round(),
        g + ((ds < 0 ? g : (255 - g)) * ds).round(),
        b + ((ds < 0 ? b : (255 - b)) * ds).round(),
        1,
      );
    });
    return MaterialColor(color.value, swatch);
  }
}

class StationeryModel {
  late String name;
  late String cost;
  late String count;
  late String size;
  late String image;
  late String content;

  StationeryModel(
      this.name,
      this.cost,
      this.count,
      this.size,
      this.image,
      this.content
      );
}

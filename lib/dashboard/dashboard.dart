import 'package:charitysys/dashboard/completed/education/complete_guarantee/consts.dart';
import 'package:charitysys/dashboard/completed/health/patients/complete_patients.dart';
import 'package:charitysys/dashboard/volunteer/resign_volunteer/resign_volunteer.dart';
import 'package:charitysys/dashboard/volunteer/show%20volunteers/Cook_team/cook_team.dart';
import 'package:charitysys/dashboard/volunteer/show%20volunteers/medical_team/medical_team.dart';
import 'package:charitysys/dashboard/volunteer/show%20volunteers/teachers_team/teachers_team.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:get/get.dart';
import 'completed/education/complete_guarantee/complete_guarantee.dart';
import 'completed/food/complete_food_section/complete_food_parcel_screen.dart';
import 'completed/food/complete_kitchen_sets/complete_kitchen_sets.dart';
import 'completed/health/medical_tools/complete_medical_tools.dart';
import 'completed/health/medicines/complete_medicines.dart';
import 'incomplete/Education/in_guarantee/incomplete_guarantee.dart';
import 'incomplete/Health/incomplete_medical_tools/incomplete_medical_tools.dart';
import 'incomplete/Health/incomplete_medicines/incomplete_medicines.dart';
import 'incomplete/Health/incomplete_patients/incomplete_patients.dart';
import 'incomplete/food/food_section/incomplete_food_section/incomplete_food_section.dart';
import 'incomplete/food/kitchen_sets/incomplete_kitchen_sets/incomplete_kitchen_sets.dart';
class Dashboard extends StatelessWidget {
  const Dashboard({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    List<Widget> tapbar=[
      Column(
        children: [
          SizedBox(
            height: MediaQuery.of(context).size.height/20,
          ),

          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Container(
                alignment: Alignment.centerLeft,
                child: const Text('Health:',style: TextStyle(color: Colors.black45,fontWeight: FontWeight.w600,fontSize: 18),)),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: GestureDetector(
                  onTap: ()=> Get.to(IncompleteMedicalTools()),
                  child: Container(
                    decoration: BoxDecoration(
                      // border: Border.all(),
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(10),
                        boxShadow: [
                          BoxShadow(
                            color: Colors.grey.withOpacity(.4),
                            spreadRadius: 2,
                            blurRadius: 4,
                            offset: Offset(0, 3), // changes position of shadow
                          )
                        ]
                    ),
                    width: 120,
                    height: 100,
                    child: Center(
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          const Icon(
                            FontAwesomeIcons.kitMedical,color: kstorm,
                          ),
                          SizedBox(
                            height: MediaQuery.of(context).size.height/40,
                          ),
                          const Text(
                            "Medical Tools",
                            style: TextStyle(
                                color: kblue,
                                fontSize: 14
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: GestureDetector(
                  onTap: ()=> Get.to(IncompletePatients()),
                  child: Container(
                    decoration: BoxDecoration(
                      // border: Border.all(
                      //     color: kblue,
                      //     width: 3
                      // ),
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(10),
                        boxShadow: [
                          BoxShadow(
                            color: Colors.grey.withOpacity(.4),
                            spreadRadius: 2,
                            blurRadius: 4,
                            offset: Offset(0, 3), // changes position of shadow
                          )
                        ]
                    ),
                    width: 120,
                    height: 100,
                    child: Center(
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          const Icon(
                            FontAwesomeIcons.person,color: kstorm,
                          ),
                          SizedBox(
                            height: MediaQuery.of(context).size.height/40,
                          ),
                          const Text(
                            "Patients",
                            style: TextStyle(
                                color: kblue,
                                fontSize: 14
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: GestureDetector(
                  onTap: ()=> Get.to(IncompleteMedicines()),
                  child: Container(
                    decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(10),
                        boxShadow: [
                          BoxShadow(
                            color: Colors.grey.withOpacity(.4),
                            spreadRadius: 2,
                            blurRadius: 4,
                            offset: Offset(0, 3), // changes position of shadow
                          )
                        ]
                    ),
                    width: 120,
                    height: 100,
                    child: Center(
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          const Icon(
                            FontAwesomeIcons.houseMedical,color: kstorm,
                          ),
                          SizedBox(height: MediaQuery.of(context).size.height/40,),
                          const Text(
                            'Medicine',
                            style: TextStyle(
                                color: kblue,
                                fontSize: 14
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              ),

            ],
          ),
          SizedBox(
            height: MediaQuery.of(context).size.height/20,
          ),



          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Container(
                alignment: Alignment.centerLeft,
                child: const Text('Food:',style: TextStyle(color: Colors.black45,fontWeight: FontWeight.w600,fontSize: 18),)),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: GestureDetector(
                  onTap: ()=> Get.to(InCompleteKitchenSet()),
                  child: Container(
                    decoration: BoxDecoration(
                      // border: Border.all(
                      //     color: kblue,
                      //     width: 3
                      // ),
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(10),
                        boxShadow: [
                          BoxShadow(
                            color: Colors.grey.withOpacity(.4),
                            spreadRadius: 2,
                            blurRadius: 4,
                            offset: Offset(0, 3), // changes position of shadow
                          )
                        ]
                    ),
                    width: 160,
                    height: 100,
                    child: Center(
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          const Icon(
                            FontAwesomeIcons.kitchenSet,color: kstorm,
                          ),
                          SizedBox(
                            height: MediaQuery.of(context).size.height/40,
                          ),
                          const Text(
                            "Kitchen set",
                            style: TextStyle(
                                color: kblue,
                                fontSize: 14
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: GestureDetector(
                  onTap: ()=> Get.to(InCompleteFoodSection()),
                  child: Container(
                    decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(10),
                        boxShadow: [
                          BoxShadow(
                            color: Colors.grey.withOpacity(.4),
                            spreadRadius: 2,
                            blurRadius: 4,
                            offset: Offset(0, 3), // changes position of shadow
                          )
                        ]
                    ),
                    width: 160,
                    height: 100,
                    child: Center(
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          const Icon(
                            FontAwesomeIcons.toolbox,color: kstorm,
                          ),
                          SizedBox(height: MediaQuery.of(context).size.height/40,),
                          const Text(
                            'Kitchen Needs',
                            style: TextStyle(
                                color: kblue,
                                fontSize: 14
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              ),

            ],
          ),
          SizedBox(
            height: MediaQuery.of(context).size.height/20,
          ),



          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Container(
                alignment: Alignment.centerLeft,
                child: const Text('Education:',style: TextStyle(color: Colors.black45,fontWeight: FontWeight.w600,fontSize: 18),)),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: GestureDetector(
                  onTap: ()=> Get.to(InCompleteGuarantee()),
                  child: Container(
                    decoration: BoxDecoration(
                      // border: Border.all(),
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(10),
                        boxShadow: [
                          BoxShadow(
                            color: Colors.grey.withOpacity(.4),
                            spreadRadius: 2,
                            blurRadius: 4,
                            offset: Offset(0, 3), // changes position of shadow
                          )
                        ]
                    ),
                    width: 220,
                    height: 100,
                    child: Center(
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          const Icon(
                            FontAwesomeIcons.bagShopping,color: kstorm,
                          ),
                          SizedBox(
                            height: MediaQuery.of(context).size.height/40,
                          ),
                          const Text(
                            "Guarentees",
                            style: TextStyle(
                                color: kblue,
                                fontSize: 14
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              ),

            ],
          ),
        ],
      ),
      Column(
        children: [
          SizedBox(
            height: MediaQuery.of(context).size.height/20,
          ),

          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Container(
                alignment: Alignment.centerLeft,
                child: const Text('Health:',style: TextStyle(color: Colors.black45,fontWeight: FontWeight.w600,fontSize: 18),)),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: GestureDetector(
                  onTap: ()=> Get.to(CompleteMedicalTools()),
                  child: Container(
                    decoration: BoxDecoration(
                      // border: Border.all(),
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(10),
                        boxShadow: [
                          BoxShadow(
                            color: Colors.grey.withOpacity(.4),
                            spreadRadius: 2,
                            blurRadius: 4,
                            offset: Offset(0, 3), // changes position of shadow
                          )
                        ]
                    ),
                    width: 120,
                    height: 100,
                    child: Center(
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          const Icon(
                            FontAwesomeIcons.kitMedical,color: kstorm,
                          ),
                          SizedBox(
                            height: MediaQuery.of(context).size.height/40,
                          ),
                          const Text(
                            "Medical Tools",
                            style: TextStyle(
                                color: kblue,
                                fontSize: 14
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: GestureDetector(
                  onTap: ()=> Get.to(CompletePatients()),
                  child: Container(
                    decoration: BoxDecoration(
                      // border: Border.all(
                      //     color: kblue,
                      //     width: 3
                      // ),
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(10),
                        boxShadow: [
                          BoxShadow(
                            color: Colors.grey.withOpacity(.4),
                            spreadRadius: 2,
                            blurRadius: 4,
                            offset: Offset(0, 3), // changes position of shadow
                          )
                        ]
                    ),
                    width: 120,
                    height: 100,
                    child: Center(
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          const Icon(
                            FontAwesomeIcons.person,color: kstorm,
                          ),
                          SizedBox(
                            height: MediaQuery.of(context).size.height/40,
                          ),
                          const Text(
                            "Patients",
                            style: TextStyle(
                                color: kblue,
                                fontSize: 14
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: GestureDetector(
                  onTap: ()=> Get.to(CompleteMedicines()),
                  child: Container(
                    decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(10),
                        boxShadow: [
                          BoxShadow(
                            color: Colors.grey.withOpacity(.4),
                            spreadRadius: 2,
                            blurRadius: 4,
                            offset: Offset(0, 3), // changes position of shadow
                          )
                        ]
                    ),
                    width: 120,
                    height: 100,
                    child: Center(
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          const Icon(
                            FontAwesomeIcons.houseMedical,color: kstorm,
                          ),
                          SizedBox(height: MediaQuery.of(context).size.height/40,),
                          const Text(
                            'Medicine',
                            style: TextStyle(
                                color: kblue,
                                fontSize: 14
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              ),

            ],
          ),
          SizedBox(
            height: MediaQuery.of(context).size.height/20,
          ),



          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Container(
                alignment: Alignment.centerLeft,
                child: const Text('Food:',style: TextStyle(color: Colors.black45,fontWeight: FontWeight.w600,fontSize: 18),)),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: GestureDetector(
                  onTap: ()=> Get.to(CompleteKitchenSet()),
                  child: Container(
                    decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(10),
                        boxShadow: [
                          BoxShadow(
                            color: Colors.grey.withOpacity(.4),
                            spreadRadius: 2,
                            blurRadius: 4,
                            offset: Offset(0, 3), // changes position of shadow
                          )
                        ]
                    ),
                    width: 120,
                    height: 100,
                    child: Center(
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          const Icon(
                            FontAwesomeIcons.kitchenSet,color: kstorm,
                          ),
                          SizedBox(
                            height: MediaQuery.of(context).size.height/40,
                          ),
                          const Text(
                            "Kitchen set",
                            style: TextStyle(
                                color: kblue,
                                fontSize: 14
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: GestureDetector(
                  onTap: ()=> Get.to(CompleteFoodSection()),
                  child: Container(
                    decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(10),
                        boxShadow: [
                          BoxShadow(
                            color: Colors.grey.withOpacity(.4),
                            spreadRadius: 2,
                            blurRadius: 4,
                            offset: Offset(0, 3), // changes position of shadow
                          )
                        ]
                    ),
                    width: 120,
                    height: 100,
                    child: Center(
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          const Icon(
                            FontAwesomeIcons.toolbox,color: kstorm,
                          ),
                          SizedBox(height: MediaQuery.of(context).size.height/40,),
                          const Text(
                            'Kitchen Needs',
                            style: TextStyle(
                                color: kblue,
                                fontSize: 14
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: GestureDetector(
// <<<<<<< HEAD
//                   onTap: ()=> Get.to(MainCompleted()),
// =======
                  onTap: (){},
// >>>>>>> 2c6a0d2 (new file)
                  child: Container(
                    decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(10),
                        boxShadow: [
                          BoxShadow(
                            color: Colors.grey.withOpacity(.4),
                            spreadRadius: 2,
                            blurRadius: 4,
                            offset: Offset(0, 3), // changes position of shadow
                          )
                        ]
                    ),
                    width: 120,
                    height: 100,
                    child: Center(
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          const Icon(
                            FontAwesomeIcons.bowlFood,color: kstorm,
                          ),
                          SizedBox(height: MediaQuery.of(context).size.height/40,),
                          const Text(
                            'Food Basket',
                            style: TextStyle(
                                color: kblue,
                                fontSize: 14
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              ),


            ],
          ),
          SizedBox(
            height: MediaQuery.of(context).size.height/20,
          ),



          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Container(
                alignment: Alignment.centerLeft,
                child: const Text('Education:',style: TextStyle(color: Colors.black45,fontWeight: FontWeight.w600,fontSize: 18),)),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: GestureDetector(
                  onTap: ()=> Get.to(CompleteGuarantee()),
                  child: Container(
                    decoration: BoxDecoration(
                      // border: Border.all(),
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(10),
                        boxShadow: [
                          BoxShadow(
                            color: Colors.grey.withOpacity(.4),
                            spreadRadius: 2,
                            blurRadius: 4,
                            offset: Offset(0, 3), // changes position of shadow
                          )
                        ]
                    ),
                    width: 150,
                    height: 100,
                    child: Center(
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          const Icon(
                            FontAwesomeIcons.bagShopping,color: kstorm,
                          ),
                          SizedBox(
                            height: MediaQuery.of(context).size.height/40,
                          ),
                          const Text(
                            "Guarentees",
                            style: TextStyle(
                                color: kblue,
                                fontSize: 14
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: GestureDetector(
                  onTap: ()=> Get.to(IncompleteMedicalTools()),
                  child: Container(
                    decoration: BoxDecoration(
                      // border: Border.all(),
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(10),
                        boxShadow: [
                          BoxShadow(
                            color: Colors.grey.withOpacity(.4),
                            spreadRadius: 2,
                            blurRadius: 4,
                            offset: Offset(0, 3), // changes position of shadow
                          )
                        ]
                    ),
                    width: 150,
                    height: 100,
                    child: Center(
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          const Icon(
                            FontAwesomeIcons.penToSquare,color: kstorm,
                          ),
                          SizedBox(
                            height: MediaQuery.of(context).size.height/40,
                          ),
                          const Text(
                            "Staionery",
                            style: TextStyle(
                                color: kblue,
                                fontSize: 15
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              ),

            ],
          ),
        ],
      ),
      Column(
        children: [
          SizedBox(
            height: MediaQuery.of(context).size.height/20,
          ),

          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Container(
                alignment: Alignment.centerLeft,
                child: const Text('Demands:',style: TextStyle(color: Colors.black45,fontWeight: FontWeight.w600,fontSize: 18),)),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: GestureDetector(
                  onTap: ()=> Get.to(CompleteMedicalTools()),
                  child: Container(
                    decoration: BoxDecoration(
                      // border: Border.all(),
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(10),
                        boxShadow: [
                          BoxShadow(
                            color: Colors.grey.withOpacity(.4),
                            spreadRadius: 2,
                            blurRadius: 4,
                            offset: Offset(0, 3), // changes position of shadow
                          )
                        ]
                    ),
                    width: 120,
                    height: 100,
                    child: Center(
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          const Icon(
                            FontAwesomeIcons.personHalfDress,color: kstorm,
                          ),
                          SizedBox(
                            height: MediaQuery.of(context).size.height/40,
                          ),
                          const Text(
                            "volunteering",
                            style: TextStyle(
                                color: kblue,
                                fontSize: 14
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: GestureDetector(
                  onTap: ()=> Get.to(ResignVolunteer()),
                  child: Container(
                    decoration: BoxDecoration(
                      // border: Border.all(
                      //     color: kblue,
                      //     width: 3
                      // ),
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(10),
                        boxShadow: [
                          BoxShadow(
                            color: Colors.grey.withOpacity(.4),
                            spreadRadius: 2,
                            blurRadius: 4,
                            offset: Offset(0, 3), // changes position of shadow
                          )
                        ]
                    ),
                    width: 120,
                    height: 100,
                    child: Center(
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          const Icon(
                            FontAwesomeIcons.x,color: kstorm,
                          ),
                          SizedBox(
                            height: MediaQuery.of(context).size.height/40,
                          ),
                          const Text(
                            "resignation",
                            style: TextStyle(
                                color: kblue,
                                fontSize: 14
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              ),

            ],
          ),
          SizedBox(
            height: MediaQuery.of(context).size.height/20,
          ),



          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Container(
                alignment: Alignment.centerLeft,
                child: const Text('Volunteers crew:',style: TextStyle(color: Colors.black45,fontWeight: FontWeight.w600,fontSize: 18),)),
          ),
          SizedBox(
            height: MediaQuery.of(context).size.height/30,
          ),

          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: GestureDetector(
                  onTap: ()=> Get.to(CookVol()),
                  child: Container(
                    decoration: BoxDecoration(
                      // border: Border.all(
                      //     color: kblue,
                      //     width: 3
                      // ),
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(10),
                        boxShadow: [
                          BoxShadow(
                            color: Colors.grey.withOpacity(.4),
                            spreadRadius: 2,
                            blurRadius: 4,
                            offset: Offset(0, 3), // changes position of shadow
                          )
                        ]
                    ),
                    width: 120,
                    height: 100,
                    child: Center(
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          const Icon(
                            FontAwesomeIcons.kitchenSet,color: kstorm,
                          ),
                          SizedBox(
                            height: MediaQuery.of(context).size.height/40,
                          ),
                          const Text(
                            "Kitchen crew",
                            style: TextStyle(
                                color: kblue,
                                fontSize: 14
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: GestureDetector(
                  onTap: ()=> Get.to(TeachersVol()),
                  child: Container(
                    decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(10),
                        boxShadow: [
                          BoxShadow(
                            color: Colors.grey.withOpacity(.4),
                            spreadRadius: 2,
                            blurRadius: 4,
                            offset: Offset(0, 3), // changes position of shadow
                          )
                        ]
                    ),
                    width: 120,
                    height: 100,
                    child: Center(
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          const Icon(
                            FontAwesomeIcons.toolbox,color: kstorm,
                          ),
                          SizedBox(height: MediaQuery.of(context).size.height/40,),
                          const Text(
                            'Education crew',
                            style: TextStyle(
                                color: kblue,
                                fontSize: 14
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              ),
            ],
          ),
          SizedBox(
            height: MediaQuery.of(context).size.height/30,
          ),

          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: GestureDetector(
                  onTap: ()=> Get.to(MedicalVol()),
                  child: Container(
                    decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(10),
                        boxShadow: [
                          BoxShadow(
                            color: Colors.grey.withOpacity(.4),
                            spreadRadius: 2,
                            blurRadius: 4,
                            offset: Offset(0, 3), // changes position of shadow
                          )
                        ]
                    ),
                    width: 150,
                    height: 100,
                    child: Center(
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          const Icon(
                            FontAwesomeIcons.bowlFood,color: kstorm,
                          ),
                          SizedBox(height: MediaQuery.of(context).size.height/40,),
                          const Text(
                            'Health crew',
                            style: TextStyle(
                                color: kblue,
                                fontSize: 14
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              ),

            ],
          )
        ],
      ),
    ];
    return DefaultTabController(
      length:3,
      child: Scaffold(
        backgroundColor: kprimary,
        appBar: AppBar(
          centerTitle: true,
          title: const Text(
            'Admin dashboard'
          ),
          // bottom:
          elevation: 0,
          bottom:  TabBar(
            indicator: UnderlineTabIndicator(
                borderSide: BorderSide(width: 5.0,color: kprimary.withOpacity(.3)),
                insets: EdgeInsets.symmetric(horizontal:25.0)
            ),
            tabs: [
              Column(
                children: const[
                  Tab(icon: Icon(FontAwesomeIcons.squareFontAwesome),),
                  Text('incomplete')
                ],
              ),
              Column(
                children:const [
                  Tab(icon:  Icon(FontAwesomeIcons.checkToSlot),),
                  Text('complete')
                ],
              ),
              Column(
                children:const [
                  Tab(icon: Icon(FontAwesomeIcons.addressCard),),
                  Text('Volunteers')
                ],
              )
            ],
          ),

        ),
        drawer:Container(
          width: 80,
          child: Drawer(
            backgroundColor: kblue,
            elevation: 20,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [

                ListTile(
                  leading: const Icon(
                    Icons.add,size: 35,
                    color: kprimary,
                  ),
                  // title: const Text('Box ',style: TextStyle(
                  //   color: kprimary,
                  //   fontSize: 17
                  // ),),
                  onTap: () {
                    // Get.to(MyGuarantees());
                  },
                ),
               SizedBox(
                 height: MediaQuery.of(context).size.height/10,
               ),
                ListTile(
                  leading: const Icon(
                    Icons.star_border_outlined,size: 35,
                    color: kprimary,
                  ),
                  // title: const Text('gallery',style: TextStyle(color: kprimary,fontSize: 17),),
                  onTap: () {
                    // Get.to(Gallery());
                  },
                ),
                 SizedBox(
            height: MediaQuery.of(context).size.height/10,
          ),
                ListTile(
                  leading: Icon(
                    Icons.boy,size: 35,
                    color: kprimary,
                  ),
                  // title: const Text('sponsorship',style: TextStyle(color: kprimary,fontSize: 17),),
                  onTap: () {
                    // Get.to(Sponsership());
                  },
                ),
                SizedBox(
                  height: MediaQuery.of(context).size.height/10,
                ),
                ListTile(
                  leading: Icon(
                    FontAwesomeIcons.football,size: 35,
                    color: kprimary,
                  ),
                  // title: const Text('logout',style: TextStyle(color: kprimary,fontSize: 17),),
                  onTap: () {
                    // Get.to(Sponsership());
                  },
                ),
              ],
            ),
          ),
        ),
        body: TabBarView(
          children: [
            Stack(
                children:[
                  Container(
                    color: kblue,
                    height: MediaQuery.of(context).size.height/20,
                  ),
                  Column(
                      children: [
                        SizedBox(
                          height: MediaQuery.of(context).size.height/20,
                        ),
                        tapbar[0],

                      ]
                  ),
                ]),
            Stack(
                children:[
                  Container(
                    color: kblue,
                    height: MediaQuery.of(context).size.height/20,
                  ),
                  Column(
                      children: [
                        SizedBox(
                          height: MediaQuery.of(context).size.height/20,
                        ),
                        tapbar[1],

                      ]
                  ),
                ]),
            Stack(
                children:[
                  Container(
                    color: kblue,
                    height: MediaQuery.of(context).size.height/20,
                  ),
                  Column(
                      children: [
                        SizedBox(
                          height: MediaQuery.of(context).size.height/20,
                        ),
                        tapbar[2],

                      ]
                  ),
                ]),

          ],
        )
    )
    );
  }
}


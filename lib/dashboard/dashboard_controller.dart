import 'package:get/get.dart';

class DashboardController extends GetxController{


  List<String> categoriesLottie = [
    'assets/medicines/66841-medicine-capsule.json',
    'assets/patients/88284-doctor-prescription (1).json',
    'assets/medical_tool/56120-medical-shield.json'
  ];

  List<String> categoriesName = [
    "Medicines",
    "Patients",
    "Medical Tools"
  ];

  List<String> routeName = [
    'inComp_medicines',
    'inCompPatientsAdmin',
    'inComp_medical_tools'
  ];


}
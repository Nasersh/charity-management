import 'package:flutter/material.dart';
import 'package:get/get.dart';
import '../../../configrations/const/const.dart';
import 'confirm_password_admin_controller.dart';

class ConfirmAdminPassword extends StatelessWidget {
  const ConfirmAdminPassword({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    GlobalKey<FormState> _formKey = GlobalKey<FormState>();
    ConfirmAdminPasswordController controller =
        Get.put<ConfirmAdminPasswordController>(
            ConfirmAdminPasswordController());
    return SafeArea(
      child: Scaffold(
        body: Container(
          color: kblue,
          child: Align(
            alignment: Alignment.center,
            child: Container(
              width: size.width / 1.3,
              height: size.height / 2.5,
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.circular(30),
              ),
              child: Column(
                children: [
                  Container(
                    alignment: Alignment.center,
                    padding: const EdgeInsets.all(30),
                    child: Text(
                      'confirm password'.tr,
                      style: const TextStyle(
                        color: Colors.grey,
                        fontSize: 22,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(30.0),
                    child: Container(
                      padding: const EdgeInsets.symmetric(horizontal: 6),
                      decoration: BoxDecoration(
                          border: Border.all(width: 2, color: kblue),
                          borderRadius: BorderRadius.circular(10)),
                      child: Form(
                        autovalidateMode: AutovalidateMode.onUserInteraction,
                        child: TextFormField(
                          decoration: InputDecoration(
                              hintText: "enter your password".tr,
                              hintStyle: const TextStyle(fontSize: 15),
                              suffixIcon: const Icon(Icons.lock, color: Colors.grey),
                              border: InputBorder.none),
                          onChanged: (value) {
                            controller.password.value = value;
                          },
                          validator: (value) {
                            if (value != null && value.length < 4) {
                              return 'your password less than 3';
                            }
                            return null;
                          },
                        ),
                      ),
                    ),
                  ),
                  const Spacer(),
                  Obx(() => !controller.isRightPassword.value
                      ? const Text(
                          'wrong password',
                    style: TextStyle(color: Colors.red),
                        )
                      : const SizedBox()),
                  const SizedBox(height: 10),
                  GestureDetector(
                    onTap: () {
                      if (controller.password.value.length > 3) {
                        controller.isHisPassword();
                      }
                    },
                    child: Obx(
                      () => Container(
                        width: 120,
                        height: 50,
                        padding: const EdgeInsets.all(10),
                        alignment: Alignment.center,
                        decoration: BoxDecoration(
                          color: kblue.withOpacity(.5),
                          borderRadius: BorderRadius.circular(20),
                        ),
                        child: !controller.isSubmitted.value
                            ? Text(
                                "submit".tr,
                                style:const TextStyle(
                                    color: Colors.white, fontSize: 16),
                              )
                            : const CircularProgressIndicator(
                                color: Colors.white),
                      ),
                    ),
                  ),
                  const Spacer(flex: 2)
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}

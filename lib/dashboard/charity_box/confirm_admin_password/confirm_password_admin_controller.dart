import 'package:get/get.dart';

import 'confirm_password_admin_service.dart';

class ConfirmAdminPasswordController extends GetxController{

RxString password = ''.obs;
ConfirmPasswordAdminService service = ConfirmPasswordAdminService();
RxBool isSubmitted = false.obs;
RxBool isRightPassword = true.obs;

dynamic isHisPassword() async {

  isSubmitted.value = true;
  String status = await service.checkPassword(password.value);
  print(password.value);
  if(status == 'OK'){
    Get.offNamed('charity_box');
  }else{
    isSubmitted.value = false;
    isRightPassword.value = false;
  }

}

}
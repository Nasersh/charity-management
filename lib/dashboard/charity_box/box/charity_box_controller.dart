import 'package:get/get.dart';

import '../charity_box_model.dart';
import 'charity_box_service.dart';

class CharityBoxController extends GetxController{

  var charityBoxList = <CharityBoxModel>[].obs;
  RxBool isLoaded = true.obs;
  RxString amount = ''.obs;
  RxString reason = ''.obs;
  RxBool isSubmitted = false.obs;
  RxBool isTrue = false.obs;
  RxBool isFalse = false.obs;
  RxInt money = 0.obs;


  @override
  void onInit() async {
    // TODO: implement onInit

    await getBoxMoney();
    await showBoxDonation();

    super.onInit();
  }

  Future<void> showBoxDonation() async {

    CharityBoxService service = CharityBoxService();
    charityBoxList.value = await service.getCharityDonation();
    isLoaded.value = false;
    print(charityBoxList.map((e) => print(e.name)));

  }

  Future<String> submitTakeOut() async {

    CharityBoxService service = CharityBoxService();
    String status = await service.takeOut(amount.value, reason.value);
    isSubmitted.value = false;
    return status;

  }


  Future<void> getBoxMoney() async {

    CharityBoxService service = CharityBoxService();
    money.value = await service.getBoxMoney() != 'error' ? await service.getBoxMoney() : 0;

  }

}
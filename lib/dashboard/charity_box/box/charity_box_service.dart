import 'dart:convert';
// <<<<<<< HEAD

// =======
import '../../../config/config.dart';
// >>>>>>> 2c6a0d2 (new file)
import '../../../secureStorage.dart';
import 'package:http/http.dart' as http;
import '../charity_box_model.dart';

class CharityBoxService{


  Future<List<CharityBoxModel>> getCharityDonation() async{

// <<<<<<< HEAD
    var url = Uri.parse(ServerConfig.domainNameServer + ServerConfig.showBoxDonation);
// =======
//     var url = Uri.parse(ServerConfig.domainNameServer + ServerConfig.showBoxDonation);
// >>>>>>> 2c6a0d2 (new file)

    var _storage=SecureStorage();
    String? token = await _storage.read('token');
    var response = await http.get(url,headers:  {
      'Accept' : 'application/json',
      'Authorization'  :  'Bearer $token'
    },);
    print(response.statusCode);

    if(response.statusCode == 200){
      List<CharityBoxModel> boxDonationsList = boxDonationsCombine(json.decode(response.body));
      //print(boxDonationsList.map((e) => print(e.name)));
      return boxDonationsList;
    }
    else {
      return [];
    }
  }


  List<CharityBoxModel> boxDonationsCombine(List<dynamic> response) {
    List<CharityBoxModel> boxDonationsList = [];

    for (int i = 0; i < response.length; i++) {

      print(i);
      boxDonationsList.add(CharityBoxModel(
        response[i]['amount'],
        response[i]['user']['name'],
        response[i]['user']['email'],
      ));

    }
    return boxDonationsList;
  }

  Future<String> takeOut(String amount , String reason) async {
    var _storage=SecureStorage();
    String? token = await _storage.read('token');
// <<<<<<< HEAD
    var url = Uri.parse(ServerConfig.domainNameServer + ServerConfig.takeOutBox);
// =======
//     var url = Uri.parse(ServerConfig.domainNameServer + ServerConfig.takeOutBox);
// >>>>>>> 2c6a0d2 (new file)

    var response = await http.post(url, headers: {
      'Authorization': 'Bearer $token',
      'Accept': 'application/json'
    }, body: {
      'amount': amount,
      'reason': reason
    });
    print('${response.statusCode} ${response.body}');

    if(response.statusCode == 200){
      return 'OK';
    }else{
      return 'NO';
    }

  }


  Future<dynamic> getBoxMoney() async{

// <<<<<<< HEAD
    var url = Uri.parse(ServerConfig.domainNameServer + ServerConfig.getBoxMoney);
// =======
//     var url = Uri.parse(ServerConfig.domainNameServer + ServerConfig.getBoxMoney);
// >>>>>>> 2c6a0d2 (new file)

    var _storage=SecureStorage();
    String? token = await _storage.read('token');
    var response = await http.get(url,headers:  {
      'Accept' : 'application/json',
      'Authorization'  :  'Bearer $token'
    },);
    print(response.statusCode);

    if(response.statusCode == 200){
      int boxMoney = json.decode(response.body)['money'];
      print(boxMoney);
      print('200');
      //print(boxDonationsList.map((e) => print(e.name)));
      return boxMoney;
    }
    else {
      return 'error';
    }
  }

}
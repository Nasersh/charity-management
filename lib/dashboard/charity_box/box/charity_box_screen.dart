import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import '../../../configrations/const/const.dart';
import '../../shwimmer.dart';
import 'charity_box_controller.dart';

class CharityBoxScreen extends StatelessWidget {
  const CharityBoxScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    CharityBoxController controller =
        Get.put<CharityBoxController>(CharityBoxController());

    return SafeArea(
        child: Obx(
      () => Scaffold(
        body: !controller.isLoaded.value
            ? Column(
                children: [
                  Container(
                    width: size.width,
                    height: size.width / 4,
                    alignment: Alignment.bottomLeft,
                    padding: const EdgeInsets.all(10),
                    child: Container(
                      decoration: BoxDecoration(
                          color: Colors.orange.withOpacity(.99),
                          borderRadius: BorderRadius.circular(10)),
                      width: size.width,
                      height: 45,
                      padding: const EdgeInsets.symmetric(
                          horizontal: 20, vertical: 4),
                      alignment: Alignment.bottomLeft,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text(
                            "Charity Box".tr,
                            style: const TextStyle(fontSize: 24),
                          ),
                          const Icon(
                            Icons.monetization_on,
                            size: 35,
                          )
                        ],
                      ),
                    ),
                  ),
                  Expanded(
                    flex: 5,
                    child: ListView.builder(
                      itemBuilder: (_, index) => Container(
                        margin: const EdgeInsets.all(20),
                        width: size.width - (size.width / 10),
                        height: size.height / 5,
                        decoration: BoxDecoration(
                            color: kblue,
                            borderRadius: BorderRadius.circular(20)),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Padding(
                              padding: const EdgeInsets.symmetric(
                                  horizontal: 20, vertical: 45),
                              child: SizedBox(
                                child: Image.asset('assets/stationery.png'),
                                width: size.width / 3,
                                height: size.height / 5,
                              ),
                            ),
                            Expanded(
                              child: Padding(
                                padding: const EdgeInsets.symmetric(
                                    horizontal: 30, vertical: 10),
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Text(
                                      '${"amount :".tr}${controller.charityBoxList[index].amount}'.tr,
                                      style:
                                          const TextStyle(color: Colors.white),
                                    ),
                                    Text(
                                      'Details :'.tr,
                                      style: TextStyle(color: Colors.white),
                                      textAlign: TextAlign.left,
                                    ),
                                    Text(
                                      '${"name : ".tr}${controller.charityBoxList[index].name}'.tr,
                                      style:
                                          const TextStyle(color: Colors.white),
                                      textAlign: TextAlign.left,
                                    ),
                                    Text(
                                      '${"email : ".tr} ${controller.charityBoxList[index].amount}'.tr,
                                      style:
                                          const TextStyle(color: Colors.white),
                                      textAlign: TextAlign.left,
                                    ),
                                  ],
                                ),
                              ),
                            )
                          ],
                        ),
                      ),
                      itemCount: controller.charityBoxList.length,
                    ),
                  ),
                  Expanded(
                      child: Padding(
                    padding: const EdgeInsets.all(20.0),
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            const Icon(
                              Icons.monetization_on,
                              size: 35,
                              color: kblue,
                            ),
                            Text(
                              "money : ".tr,
                            ),
                            Container(
                              decoration: BoxDecoration(
                                  color: kblue,
                                  borderRadius: BorderRadius.circular(10)),
                              width: 80,
                              padding: const EdgeInsets.all(8),
                              alignment: Alignment.center,
                              child: Text(
                                "${controller.money.value}",
                                style: const TextStyle(color: Colors.white),
                              ),
                            )
                          ],
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            const Icon(
                              Icons.flight_takeoff,
                              size: 35,
                              color: kblue,
                            ),
                            Text("take out : ".tr),
                            GestureDetector(
                              onTap: () {
                                Get.toNamed('take_out_dialog');
                              },
                              child: Container(
                                decoration: BoxDecoration(
                                    color: kblue,
                                    borderRadius: BorderRadius.circular(10)),
                                width: 80,
                                padding: const EdgeInsets.all(8),
                                alignment: Alignment.center,
                                child: Text(
                                  "submit".tr,
                                  style: TextStyle(color: Colors.white),
                                ),
                              ),
                            )
                          ],
                        )
                      ],
                    ),
                  ))
                ],
              )
            : const Center(
                child: ShimmerWidget(),
              ),
      ),
    ));
  }
}

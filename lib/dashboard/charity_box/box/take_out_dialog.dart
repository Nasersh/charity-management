import 'dart:async';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import '../../../configrations/const/const.dart';
import '../../../configrations/functions_status/functions_status.dart';
import 'charity_box_controller.dart';

class TakeOutDialog extends StatelessWidget {
  const TakeOutDialog({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    GlobalKey<FormState> _formKey = GlobalKey<FormState>();
    CharityBoxController controller =
    Get.put<CharityBoxController>(CharityBoxController());

    return SafeArea(
      child: Scaffold(
        body:Container(
          color: kblue,
          child: Align(
            alignment: Alignment.center,
            child: Container(
              width: size.width / 1.2,
              height: size.height / 2,
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.circular(30),
              ),
              child: Column(
                children: [
                  Container(
                    alignment: Alignment.center,
                    padding: const EdgeInsets.all(30),
                    child:  Text(
                      'Take out money'.tr,
                      style: const TextStyle(
                        color: Colors.grey,
                        fontSize: 22,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(30.0),
                    child: Container(
                      padding: const EdgeInsets.symmetric(horizontal: 6),
                      decoration: BoxDecoration(
                          border: Border.all(
                              width: 2,
                              color: kblue
                          ),
                          borderRadius: BorderRadius.circular(10)
                      ),
                      child: Form(
                        autovalidateMode: AutovalidateMode.onUserInteraction,
                        child: TextFormField(
                          keyboardType: TextInputType.number,
                          decoration:  InputDecoration(
                              hintText: "enter your amount".tr,
                              hintStyle: const TextStyle(
                                  fontSize: 15
                              ),
                              suffixIcon: Icon(Icons.monetization_on,color:Colors.grey),
                              border: InputBorder.none
                          ),
                          onChanged: (value){
                            controller.amount.value = value;
                          },
                          validator: (value){

                            if(value != '' && int.parse(value!) <= 0){
                              return 'your amount is wrong'.tr;
                            }
                            return null;
                          },
                        ),
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 30.0),
                    child: Container(
                      padding: const EdgeInsets.symmetric(horizontal: 6),
                      decoration: BoxDecoration(
                          border: Border.all(
                              width: 2,
                              color: kblue
                          ),
                          borderRadius: BorderRadius.circular(10)
                      ),
                      child: Form(
                        autovalidateMode: AutovalidateMode.onUserInteraction,
                        child: TextFormField(
                          decoration: InputDecoration(
                              hintText: "enter your reason".tr,
                              hintStyle: const TextStyle(
                                  fontSize: 15
                              ),
                              suffixIcon: Icon(Icons.text_fields,color:Colors.grey),
                              border: InputBorder.none
                          ),
                          onChanged: (value){
                            controller.reason.value = value;
                          },
                          validator: (value){
                            if(value != '' && value!.length < 3){
                              return 'your reason less than 3'.tr;
                            }
                            return null;
                          },
                        ),
                      ),
                    ),
                  ),
                  const Spacer(),
                  GestureDetector(
                    onTap: () async {
                      if(controller.reason.value.length > 3 && int.parse(controller.amount.value) > 0 ){
                        controller.isSubmitted.value = true;
                        String status = await controller.submitTakeOut();
                        await controller.getBoxMoney();
                        if(status == 'OK'){
                          controller.isTrue.value = true;
                          Timer(const Duration(seconds: 0), () async {
                            Get.to(FunctionStatus(
                              controller: controller,
                              title: "money",
                              function: "took out",
                            ));
                          });
                        }else{
                          controller.isFalse.value = true;
                          Timer(const Duration(seconds: 1), () async {
                            await Get.to(FunctionStatus(
                              controller: controller,
                              title: "money",
                              function: "took out",
                            ));
                          });
                        }
                      }
                    },
                    child: Obx(
                          ()=> Container(
                        width: 120,
                        height: 50,
                        padding: const EdgeInsets.all(10),
                        alignment: Alignment.center,
                        decoration: BoxDecoration(
                          color: kblue.withOpacity(.5) ,
                          borderRadius: BorderRadius.circular(20),
                        ),
                        child: !controller.isSubmitted.value ? Text(
                          "submit".tr,
                          style: const TextStyle(
                              color: Colors.white,
                              fontSize: 16
                          ),
                        ) : const CircularProgressIndicator(color: Colors.white),
                      ),
                    ),
                  ),
                  const Spacer()
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}

import 'package:flutter/material.dart';
import 'package:shimmer/shimmer.dart';

class ShimmerWidget extends StatelessWidget {
  const ShimmerWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {

    Size size = MediaQuery.of(context).size;

    return SafeArea(
      child: Scaffold(
        backgroundColor: Colors.white,
        body: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 10.0,vertical: 40),
          child: Shimmer.fromColors(
            baseColor: Colors.grey,
            highlightColor: Colors.grey.withOpacity(.5),
            child: ListView.builder(
              itemBuilder: (_,index){
                return Column(
                  children: [
                    ListTile(
                      title: Align(
                        alignment: Alignment.bottomRight,
                        child: Container(
                          margin: const EdgeInsets.all(5),
                          color: Colors.white,
                          width: size.width / 2,
                          height: 20,
                        ),
                      ),
                      subtitle: Container(
                        margin: const EdgeInsets.all(5),
                        color: Colors.white,
                        width: size.width - 20,
                        height: 20,
                      ),
                      leading: Container(
                        width: 85,
                        height: 85,
                        decoration: const BoxDecoration(
                            color: Colors.white,
                            shape: BoxShape.circle
                        ),
                      ),
                    ),
                    const SizedBox(height: 20)
                  ],
                );
              },
            ),
          ),
        ),
      ),
    );
  }
}
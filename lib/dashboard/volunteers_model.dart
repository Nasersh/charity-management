class VolunteerModel{

  int id;
  int cat_id;
  int user_id;
  String birth_date;
  String gender;
  String job;
  String image;
  String phone;

  VolunteerModel(
      this.id,
      this.cat_id,
      this.user_id,
      this.birth_date,
      this.gender,
      this.job,
      this.image,
      this.phone
      );

}

class VolunteerRequestModel{

  int id;
  int cat_id;
  int user_id;
  String birth_date;
  String gender;
  String job;
  String certificateImage;
  String identicalImage;
  String phone;

  VolunteerRequestModel(
      this.id,
      this.cat_id,
      this.user_id,
      this.birth_date,
      this.gender,
      this.job,
      this.certificateImage,
      this.identicalImage,
      this.phone
      );

}
import 'package:get/get.dart';
import 'incomplete_kitchen_sets_model.dart';
import 'incomplete_kitchen_sets_services.dart';


class InCompleteKitchenSetController extends GetxController
{
  var kitchenList =<InCompleteKitchenSetModel>[].obs;
  var kitchenItems=<InCompleteKitchenSetModel>[];
  RxBool isTrue = false.obs;

  var id=0.obs;

  InCompleteKitchenSetServices service = InCompleteKitchenSetServices();
  var isloading=true.obs;

  @override
  void onInit() {
    showKitchenItem();
    super.onInit();
  }

  void showKitchenItem() async {
    try {
      isloading(true);
      kitchenItems = await service.getKitchenItems();
      kitchenItems.forEach((element) {print(element.donate);});
      if (kitchenItems != null) {
        kitchenList.value = kitchenItems;
      }
    } finally {
      isloading(false);
    }
  }


  Future<String> deleteKitchenItem(int id) async {

    String save;

    try {
      isloading(true);
      save = await service.deleteKitchenItem(id);
    }finally {
      isloading(false);
    }
    return save;
  }


}
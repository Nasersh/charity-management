import 'dart:async';
import 'dart:io';
import 'dart:math';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:get/get.dart';
import 'package:get/get_core/src/get_main.dart';
import 'package:image_picker/image_picker.dart';
import 'package:percent_indicator/linear_percent_indicator.dart';
import '../../../../../configrations/functions_status/functions_status.dart';
import '../../../Education/in_complete_stationery/consts.dart';
import '../../kitchen_sets_model.dart';
import '../kitchen_sets/kitchen_sets_consts.dart';
import 'incomplete_kitchen_sets_controller.dart';
import 'incomplete_kitchen_sets_services.dart';

class InCompleteKitchenSet extends StatelessWidget {
  InCompleteKitchenSet({Key? key}) : super(key: key);

  InCompleteKitchenSetController controller = Get.put(InCompleteKitchenSetController());
  Rx<DateTime> currentDate = DateTime.now().obs;

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;

    return Obx(
          () => controller.isloading.value
          ? const SafeArea(
          child: Scaffold(body: Center(child: CircularProgressIndicator())))
          : Stack(
        children: [
          Scaffold(
            backgroundColor: kprimary,
            body: SafeArea(
              child: Obx(
                    () => Column(
                  children: [
                    Stack(children: [
                      Container(
                        width: double.infinity,
                        height: MediaQuery.of(context).size.height / 4,
                        decoration: const BoxDecoration(
                            color: kblue,
                            borderRadius: BorderRadius.only(
                                bottomLeft: Radius.circular(30),
                                bottomRight: Radius.circular(30))),
                      ),
                      Column(
                        children: [
                          Row(
                            mainAxisAlignment:
                            MainAxisAlignment.spaceBetween,
                            children: [
                              Padding(
                                padding: const EdgeInsets.only(left: 15),
                                child: IconButton(
                                  icon: const Icon(
                                    FontAwesomeIcons.arrowLeft,
                                    color: kprimary,
                                  ),
                                  // Icons.arrow_back,
                                  onPressed: () {
                                    Get.back();
                                  },
                                ),
                              ),
                            ],
                          ),
                          SizedBox(
                            height:
                            MediaQuery.of(context).size.height / 15,
                          ),
                          const Padding(
                            padding:
                            EdgeInsets.only(bottom: 10, left: 40),
                            child: Align(
                              alignment: Alignment.bottomLeft,
                              child: Text(
                                'Kitchen Items . . .',
                                style: TextStyle(
                                    color: kprimary,
                                    fontSize: 30,
                                    fontStyle: FontStyle.italic,
                                    fontWeight: FontWeight.bold),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ]),
                    Expanded(child: _buildProductsList(size, context)),
                  ],
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget _buildProductsList(Size size, BuildContext context) {
    Random rnd;
    double min = 0;
    rnd = Random();
    double r = min + rnd.nextDouble();
    return ListView.builder(
      shrinkWrap: true,
      itemCount: controller.kitchenList.length,
      padding: const EdgeInsets.only(top: 16),
      itemBuilder: (context, index) => Padding(
        padding: const EdgeInsets.symmetric(vertical: 10),
        child: Container(
          height: MediaQuery.of(context).size.height / 5.3,
          decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(15),
              boxShadow: [kDefaultShadow]),
          child: Padding(
            padding: const EdgeInsets.symmetric(vertical: 8.0),
            child: SingleChildScrollView(
              child: ExpansionTile(
                children: [
                  Stack(
                    children: [
                      Align(
                        alignment: const Alignment(0, 0),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Padding(
                              padding: const EdgeInsets.all(12.0),
                              child: SizedBox(
                                width: size.width / 2 + size.width / 20,
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Text(
                                      '${controller.kitchenList[index].count}',
                                      style: const TextStyle(color: kstorm),
                                    ),
                                    const SizedBox(
                                      height: 10,
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                      Align(
                        alignment: const Alignment(-0.9, 0),
                        child: deleteButtonEffect(index, context, size),
                      ),
                    ],
                  ),
                ],
                leading: const CircleAvatar(
                  radius: 30,
                  backgroundColor: Colors.black45,
                ),
                title: Padding(
                  padding: const EdgeInsets.only(top: 10),
                  child: Text(
                    controller.kitchenList[index].name,
                    style: const TextStyle(
                      color: Colors.black,
                    ),
                  ),
                ),
                subtitle: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Padding(
                      padding: const EdgeInsets.only(top: 5, bottom: 10),
                      child: Text(
                        '${controller.kitchenList[index].cost}',
                        style: const TextStyle(
                          color: Colors.black,
                        ),
                      ),
                    ),
                    LinearPercentIndicator(
                      barRadius: const Radius.circular(040),
                      width: MediaQuery.of(context).size.width / 2,
                      animation: true,
                      animationDuration: 800,
                      lineHeight: 10,
                      percent: r,
                      progressColor: kblue,
                      backgroundColor: kbblue,
                    ),
                    SizedBox(
                      height: MediaQuery.of(context).size.height / 100,
                    ),
                    Text(
                      '${controller.kitchenList[index].donate}\$/${controller.kitchenList[index].cost}',
                      style: const TextStyle(color: kstorm),
                    ),
                  ],
                ),
                trailing: updateMedicine(index, context, size),
              ),
            ),
          ),
        ),
      ),
    );
  }

  Padding updateMedicine(int index, BuildContext context, Size size) {
    return Padding(
      padding: const EdgeInsets.only(top: 8.0, right: 5),
      child: Container(
        height: 50,
        width: 75,
        decoration: BoxDecoration(
          color: kstorm,
          borderRadius: BorderRadius.circular(10),
        ),
        child: MaterialButton(
          onPressed: () {
            var id = controller.kitchenList[index].id;
            showModalBottomSheet(
              context: context,
              builder: (BuildContext context) {
                return Container(
                  height: MediaQuery.of(context).size.height / 2,
                  decoration: const BoxDecoration(
                    color: kprimary,
                    borderRadius: BorderRadius.only(
                      topRight: Radius.circular(25),
                      topLeft: Radius.circular(25),
                    ),
                  ),
                  child: SingleChildScrollView(
                    child: Column(
                      children: [
                        Container(
                          height: MediaQuery.of(context).size.height / 5,
                          width: MediaQuery.of(context).size.width,
                          //color: Colors.purple,
                          alignment: const Alignment(-0.6, 0.4),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                            children: [
                              Stack(
                                clipBehavior: Clip.none,
                                children: [
                                  Obx(
                                        () => CircleAvatar(
                                      radius: 40,
                                      backgroundColor: Colors.black45,
                                      backgroundImage: image.value != ''
                                          ? FileImage(
                                        File(image.value),
                                      )
                                          : null,
                                    ),
                                  ),
                                  Positioned(
                                    top: 45,
                                    right: 0,
                                    child: GestureDetector(
                                      onTap: () => {
                                        Timer(const Duration(seconds: 1),
                                                () async {
                                              await takeImage(ImageSource.gallery);
                                            })
                                      },
                                      child: CircleAvatar(
                                        backgroundColor:
                                        Colors.black54.withOpacity(.5),
                                        child: const Icon(
                                          Icons.camera_alt,
                                          size: 30,
                                          color: Colors.white,
                                        ),
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                              //const Spacer(),
                              //const Spacer(),
                            ],
                          ),
                        ),
                        Form(
                          key: kitchenSetFormKey[0],
                          autovalidateMode: AutovalidateMode.onUserInteraction,
                          child: getInputKitchenItem(0, size, kitchenSetFormKey[0]),
                        ),
                        Form(
                          key: kitchenSetFormKey[1],
                          autovalidateMode: AutovalidateMode.onUserInteraction,
                          child: getInputKitchenItem(1, size, kitchenSetFormKey[1]),
                        ),
                        Form(
                          key: kitchenSetFormKey[2],
                          autovalidateMode: AutovalidateMode.onUserInteraction,
                          child: getInputKitchenItem(2, size, kitchenSetFormKey[2]),
                        ),
                        const SizedBox(height: 20),
                        GestureDetector(
                          onTap: () => {
                            if (checking())
                              {
                                Timer(const Duration(seconds: 0), () async {
                                  String save =
                                  await InCompleteKitchenSetServices().updateKitchenItem(
                                    KitchenSetModel(
                                      kitchenSetEditors[0].text != ''
                                          ? kitchenSetEditors[0].text
                                          : controller
                                          .kitchenList[index].name,
                                      kitchenSetEditors[1].text != ''
                                          ? kitchenSetEditors[1].text
                                          : controller.kitchenList[index].cost
                                          .toString(),
                                      kitchenSetEditors[2].text != ''
                                          ? kitchenSetEditors[2].text
                                          : controller
                                          .kitchenList[index].count
                                          .toString(),
                                      image.value != ''
                                          ? image.value
                                          : controller
                                          .kitchenList[index].image,
                                    ),
                                    id,
                                  );
                                  Get.back();

                                  if (save == 'OK') {
                                    Timer(const Duration(seconds: 1), () {
                                      Get.to(FunctionStatus(
                                        controller: controller,
                                        title: "Kitchen Item",
                                        function: "updated",
                                      ));
                                      controller.isTrue.value = true;
                                      controller.showKitchenItem();
                                    });
                                  } else {
                                    controller.isTrue.value = false;
                                    Get.to(FunctionStatus(
                                      controller: controller,
                                      title: "Kitchen Item",
                                      function: "updated",
                                    ));
                                    controller.showKitchenItem();
                                  }
                                }),
                              }
                          },
                          child: Container(
                            width: MediaQuery.of(context).size.width / 3,
                            height: MediaQuery.of(context).size.width / 9,
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(10),
                              color: kblue,
                            ),
                            alignment: Alignment.center,
                            child: const Text(
                              "Update",
                              style: TextStyle(
                                  color: Colors.white,
                                  fontWeight: FontWeight.bold,
                                  fontSize: 19),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                );
              },
            );
          },
          child: const Center(
            child: Text(
              'Update',
              style: TextStyle(color: kprimary, fontSize: 12.2),
            ),
          ),
        ),
      ),
    );
  }

  Container deleteButtonEffect(int index, BuildContext context, Size size) {
    return Container(
      height: 50,
      width: 75,
      decoration: BoxDecoration(
        color: kstorm,
        borderRadius: BorderRadius.circular(10),
      ),
      child: MaterialButton(
        onPressed: () {
          var id = controller.kitchenList[index].id;
          showModalBottomSheet(
            constraints: const BoxConstraints(maxHeight: 200),
            context: context,
            builder: (BuildContext context) {
              return Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  const Text(
                    "Would yo like to delete this element ? ",
                    style: TextStyle(
                        color: Colors.black54, fontWeight: FontWeight.bold),
                    //textAlign: TextAlign.center,
                  ),
                  const SizedBox(
                    height: 15,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      GestureDetector(
                        onTap: () => {
                          Timer(const Duration(seconds: 0), () async {
                            String save = await controller.deleteKitchenItem(id);
                            if (save == 'OK') {
                              controller.isTrue.value = true;
                              Get.back();
                              Get.to(FunctionStatus(
                                controller: controller,
                                title: "Kitchen Item",
                                function: "deleted",
                              ));
                              controller.showKitchenItem();
                            } else {
                              print('error');
                              controller.isTrue.value = false;
                              Get.to(FunctionStatus(
                                controller: controller,
                                title: "Kitchen Item",
                                function: "deleted",
                              ));
                              controller.showKitchenItem();
                            }
                          })
                        },
                        child: Container(
                          alignment: Alignment.center,
                          width: 80,
                          height: 50,
                          decoration: BoxDecoration(
                            color: kstorm,
                            borderRadius: BorderRadius.circular(10),
                          ),
                          child: const Text(
                            "Yes",
                            style: TextStyle(
                                color: Colors.white,
                                fontWeight: FontWeight.bold,
                                fontSize: 22),
                          ),
                        ),
                      ),
                      const SizedBox(
                        width: 15,
                      ),
                      GestureDetector(
                        onTap: () => Get.back(),
                        child: Container(
                          width: 80,
                          height: 50,
                          alignment: Alignment.center,
                          decoration: BoxDecoration(
                            color: kprimary,
                            borderRadius: BorderRadius.circular(10),
                          ),
                          child: const Text(
                            "No",
                            style: TextStyle(
                                color: Colors.white,
                                fontWeight: FontWeight.bold,
                                fontSize: 22),
                          ),
                        ),
                      ),
                    ],
                  ),
                ],
              );
            },
          );
        },
        child: const Center(
          child: Text(
            'Delete',
            style: TextStyle(color: kprimary, fontSize: 12.2),
          ),
        ),
      ),
    );
  }

  bool checking() {
    bool isValidate = true;
    for (int i = 0; i <= 2; i++) {
      print(kitchenSetEditors[i].toString());
      if (isValidate) {
        if (kitchenSetEditors[i].text != '') {
          isValidate = kitchenSetFormKey[i].currentState!.validate();
        }
      }
      if (!isValidate) {
        if (kitchenSetEditors[i].text != '') {
          kitchenSetFormKey[i].currentState!.validate();
        }
      }
    }
    print(isValidate);
    return isValidate;
  }


  Future<void> openCylinder(BuildContext context) async {
    final DateTime? pickedDate = await showDatePicker(
        context: context,
        initialDate: currentDate.value,
        firstDate: currentDate.value,
        lastDate: DateTime(2050));
    if (pickedDate != null && pickedDate != currentDate) {
      currentDate.value = pickedDate;
    }
  }

  Container CameraStudio(IconData icon, Size size) {
    return Container(
      margin: const EdgeInsets.symmetric(horizontal: 5, vertical: 10),
      child: Icon(
        icon,
        size: 30,
        color: kprimary,
      ),
      width: size.width * .3,
      height: 120,
      decoration: BoxDecoration(
        color: Colors.white.withOpacity(.2),
        border: Border.all(
          width: 2,
          color: kprimary,
        ),
        borderRadius: BorderRadius.circular(20),
      ),
    );
  }

  Widget getInputKitchenItem(int index, Size size, formKey) {
    return Container(
      alignment: Alignment.center,
      width: size.width * .8,
      height: 78,
      margin: const EdgeInsets.symmetric(horizontal: 20, vertical: 15),
      padding: const EdgeInsets.symmetric(horizontal: 10),
      decoration: BoxDecoration(
          color: Colors.grey.withOpacity(.2),
          borderRadius: BorderRadius.circular(10)),
      child: TextFormField(
        //key: formKey,
        controller: kitchenSetEditors[index],
        decoration: InputDecoration(
          labelText: kitchenSetAttributeName[index],
          border: InputBorder.none,
          icon: kitchenSetIcons[index],
          iconColor: Colors.red,
          labelStyle: TextStyle(
              fontSize: 16, fontWeight: FontWeight.w600, color: kprimary),
        ),
        keyboardType: kitchenSetTypes[index],
        validator: (value) {
          if (index == 0 && value != null && value.length < 7) {
            return kitchenSetErrorText[0];
          } else if (index == 1 && value != null && value == 0) {
            return kitchenSetErrorText[1];
          } else if (index == 2 && value != null && value == 0) {
            return kitchenSetErrorText[2];
          }
        },
      ),
    );
  }


  RxString image = ''.obs;
  RxBool isGet = false.obs;

  Future<void> takeImage(ImageSource source) async {
    final picker = ImagePicker();
    final getImage = await picker.pickImage(source: source);
    if (getImage != null) {
      image.value = getImage.path;
      isGet.value = true;
    }
  }


}

import 'dart:convert';
// <<<<<<< HEAD

import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:http/http.dart' as http;
// =======
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:http/http.dart' as http;
// >>>>>>> 2c6a0d2 (new file)
import '../../../../../config/config.dart';
import '../../kitchen_sets_model.dart';
import 'incomplete_kitchen_sets_model.dart';



class InCompleteKitchenSetServices {
  var message;
  var url = Uri.parse(
// <<<<<<< HEAD
      ServerConfig.domainNameServer + ServerConfig.inCompleteKitchenSet);
// =======
//       ServerConfig.domainNameServer + ServerConfig.inCompleteKitchenSet);
// >>>>>>> 2c6a0d2 (new file)
  FlutterSecureStorage storage = const FlutterSecureStorage();

  Future<List<InCompleteKitchenSetModel>> getKitchenItems() async {
    String? token = await storage.read(key: 'token');
    print(token);
    var response = await http.get(
      url,
      headers: {'Accept': 'application/json', 'Authorization': 'Bearer $token'},
    );

    print(response.body);
    if (response.statusCode == 200) {
      List<InCompleteKitchenSetModel> inComKitchenItems = combineKitchenItems(json.decode(response.body));
      print(inComKitchenItems);
      return inComKitchenItems;
    } else {
      return [];
    }
  }

  List<InCompleteKitchenSetModel> combineKitchenItems(response) {
    List<InCompleteKitchenSetModel> inComKitchenItems = [];

    for (int i = 0; i < response.length; i++) {

      if(!response['kitchen set'].isEmpty){
        print(i);
        inComKitchenItems.add(InCompleteKitchenSetModel(
          response['kitchen set'][i]['name'],
          response['kitchen set'][i]['cost'],
          response['kitchen set'][i]['count'],
          response['kitchen set'][i]['image'],
          response['kitchen set'][i]['donate'],
          response['kitchen set'][i]['id'],
        ));
      }

    }
    return inComKitchenItems;
  }

  Future<String> updateKitchenItem(KitchenSetModel kitchenItem, int id) async {

    String? token = await storage.read(key: 'token');
    print(token);

    var url = Uri.parse(
// <<<<<<< HEAD
        ServerConfig.domainNameServer + ServerConfig.updateKitchenSet + '$id');
// =======
//         ServerConfig.domainNameServer + ServerConfig.updateKitchenSet + '$id');
// >>>>>>> 2c6a0d2 (new file)

    print(kitchenItem.name);
    print(kitchenItem.count);print(kitchenItem.cost);print(kitchenItem.image);


    var response = await http.post(url, headers: {
      'Authorization': 'Bearer $token ',
      'Accept': 'application/json'
    }, body: {
      'name': kitchenItem.name,
      'cost': kitchenItem.cost,
      'count': kitchenItem.count,
      'image': kitchenItem.image,
    });

    String getResponse = response.body;
    print(getResponse);
    if (getResponse.contains('{"message":"updated successfully"')) {
      return 'OK';
    } else {
      return 'NO';
    }
  }


  Future<String> deleteKitchenItem(int id) async {

    String? token = await storage.read(key: 'token');
    print(token);

    var url = Uri.parse(
// <<<<<<< HEAD
        ServerConfig.domainNameServer + ServerConfig.deleteKitchenSet + '$id');
// =======
//         ServerConfig.domainNameServer + ServerConfig.deleteKitchenSet + '$id');
// >>>>>>> 2c6a0d2 (new file)

    print(id);

    var response = await http.delete(url, headers: {
      'Authorization': 'Bearer $token ',
      'Accept': 'application/json'
    });

    String getResponse = response.body;
    print(getResponse);
    if (response.statusCode == 200) {
      return 'OK';
    } else {
      return 'NO';
    }
  }


}

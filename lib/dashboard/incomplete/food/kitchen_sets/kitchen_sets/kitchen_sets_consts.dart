import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import '../../../../../configrations/consts.dart';



List<GlobalKey<FormState>> kitchenSetFormKey = [
  GlobalKey<FormState>(),
  GlobalKey<FormState>(),
  GlobalKey<FormState>(),
];

List<String> kitchenSetAttributeName = [
  "name",
  "cost",
  "count",
];

List<Icon> kitchenSetIcons = [
  Icon(
    Icons.person,
    color: mainColor,
  ),
  Icon(
    Icons.paypal,
    color: mainColor,
  ),
  Icon(
    Icons.numbers,
    color: mainColor,
  ),
];

List<String> kitchenSetErrorText = [
  "your Name is less than 7",
  "your cost is less than 1000",
  "your count must be more than 0",
];

List<TextInputType> kitchenSetTypes = [
  TextInputType.name,
  TextInputType.number,
  TextInputType.number,
];

List<TextEditingController> kitchenSetEditors = [
  TextEditingController(),
  TextEditingController(),
  TextEditingController(),
];

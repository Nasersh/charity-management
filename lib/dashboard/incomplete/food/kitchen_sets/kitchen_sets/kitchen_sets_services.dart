// <<<<<<< HEAD
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:http/http.dart' as http;
import '../../../../../config/config.dart';
import '../../kitchen_sets_model.dart';

class KitchenSetsServices {
  var url = Uri.parse(ServerConfig.domainNameServer + ServerConfig.addKitchenSets);
// >>>>>>> 2c6a0d2 (new file)

  Future<String> addStationery(KitchenSetModel kitchenSet) async {

    FlutterSecureStorage storage = const FlutterSecureStorage();
    String? token = await storage.read(key: 'token');
    print(token);

    print(kitchenSet.name);
    print(kitchenSet.cost);
    print(kitchenSet.count);
    print(kitchenSet.image);


    var response = await http.post(url, headers: {
      'Authorization': 'Bearer $token',
      'Accept': 'application/json'
    }, body: {
      'name': kitchenSet.name,
      'count':kitchenSet.count,
      'cost': kitchenSet.cost,
      'image': kitchenSet.image,
    });

    String getResponse = response.body;
    print(getResponse);
    if(getResponse.contains('{"message":"Kitchen set added successfully"}')){
      return 'OK';
    }else{
      return 'NO';
    }

  }

}

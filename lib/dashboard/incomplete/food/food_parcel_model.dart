class FoodParcelModel {
  late String cost;
  late String count;
  late String size;
  late String image;
  late String content;

  FoodParcelModel(
      this.cost,
      this.count,
      this.size,
      this.image,
      this.content
      );
}

class   InCompleteFoodParcelModel {
  late int cost;
  late int count;
  late String size;
  late String content;
  late int donate;
  late int id;

  InCompleteFoodParcelModel(
      this.cost,
      this.count,
      this.size,
      this.content,
      this.donate,
      this.id,
      );
}

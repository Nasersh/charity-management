import 'dart:convert';
// <<<<<<< HEAD

import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:http/http.dart' as http;
// =======
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:http/http.dart' as http;
// >>>>>>> 2c6a0d2 (new file)
import '../../../../../config/config.dart';
import '../../food_parcel_model.dart';
import 'incomplete_food_parcel_model.dart';



class InCompleteFoodPacrelServices {
  var message;
  var url = Uri.parse(
// <<<<<<< HEAD
      ServerConfig.domainNameServer + ServerConfig.inCompleteFoodParcels);
// =======
//       ServerConfig.domainNameServer + ServerConfig.inCompleteFoodParcels);
// >>>>>>> 2c6a0d2 (new file)
  FlutterSecureStorage storage = const FlutterSecureStorage();

  Future<List<InCompleteFoodParcelModel>> getFoodParcels() async {
    String? token = await storage.read(key: 'token');
    print(token);
    var response = await http.get(
      url,
      headers: {'Accept': 'application/json', 'Authorization': 'Bearer $token'},
    );

    print(response.body);
    if (response.statusCode == 200) {
      List<InCompleteFoodParcelModel> inComFoodParcels = combineFoodParcels(json.decode(response.body));
      print(inComFoodParcels);
      return inComFoodParcels;
    } else {
      return [];
    }
  }

  List<InCompleteFoodParcelModel> combineFoodParcels(var response) {
    List<InCompleteFoodParcelModel> inComFoodParcels = [];

    //print(response['stationery'][0]);
    //print(response[0].length);

    for (int i = 0; i < response.length; i++) {

      inComFoodParcels.add(InCompleteFoodParcelModel(
        response[i]['cost'],
        response[i]['count'],
        response[i]['size'],
        response[i]['content'],
        response[i]['donate'],
        response[i]['id'],
      ));

    }
    return inComFoodParcels;
  }

  Future<String> updateFoodParcel(FoodParcelModel foodParcel, int id) async {
    String? token = await storage.read(key: 'token');
    print(token);

    var url = Uri.parse(
// <<<<<<< HEAD
        ServerConfig.domainNameServer + ServerConfig.updateFoodParcel + '$id');
// =======
//         ServerConfig.domainNameServer + ServerConfig.updateFoodParcel + '$id');
// >>>>>>> 2c6a0d2 (new file)

    var response = await http.post(url, headers: {
      'Authorization': 'Bearer $token ',
      'Accept': 'application/json'
    }, body: {
      'cost': foodParcel.cost,
      'count': foodParcel.count,
      'image': foodParcel.image,
      'content':foodParcel.content,
      'size':foodParcel.size
    });

    String getResponse = response.body;
    print(getResponse);
    if (getResponse.contains('{"message":"updated successfully"')) {
      return 'OK';
    } else {
      return 'NO';
    }
  }

  Future<String> deleteFoodParcel(int id) async {
    String? token = await storage.read(key: 'token');
    print(token);

    var url = Uri.parse(
// <<<<<<< HEAD
        ServerConfig.domainNameServer + ServerConfig.deleteFoodParcel + '$id');
// =======
//         ServerConfig.domainNameServer + ServerConfig.deleteFoodParcel + '$id');
// >>>>>>> 2c6a0d2 (new file)

    var response = await http.delete(url, headers: {
      'Authorization': 'Bearer $token ',
      'Accept': 'application/json'
    });

    String getResponse = response.body;
    print(getResponse);
    if (getResponse.contains('{"message":"food parcel record get deleted "')) {
      return 'OK';
    } else {
      return 'NO';
    }
  }
}

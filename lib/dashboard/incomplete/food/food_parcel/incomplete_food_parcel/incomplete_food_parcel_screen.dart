import 'dart:async';
import 'dart:io';
import 'dart:math';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:get/get.dart';
import 'package:get/get_core/src/get_main.dart';
import 'package:image_picker/image_picker.dart';
import 'package:percent_indicator/linear_percent_indicator.dart';
import '../../../../../configrations/functions_status/functions_status.dart';
import '../../../../../configrations/the_helper_widget/the_helper_widget.dart';
import '../../../Education/in_complete_stationery/consts.dart';
import '../../food_parcel_model.dart';
import '../add_food_parcel/add_food_parcel_consts.dart';
import 'incomplete_food_parcel_controller.dart';
import 'incomplete_food_parcel_services.dart';

class InCompleteFoodParcel extends StatelessWidget {
  InCompleteFoodParcel({Key? key}) : super(key: key);

  InCompleteFoodParcelController controller =
  Get.put(InCompleteFoodParcelController());
  Rx<DateTime> currentDate = DateTime.now().obs;

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;

    return Obx(
          () => controller.isloading.value
          ? const SafeArea(
          child: Scaffold(body: Center(child: CircularProgressIndicator())))
          : Stack(
        children: [
          Scaffold(
            backgroundColor: kprimary,
            body: SafeArea(
              child: Obx(
                    () => Column(
                  children: [
                    Stack(children: [
                      Container(
                        width: double.infinity,
                        height: MediaQuery.of(context).size.height / 4,
                        decoration: const BoxDecoration(
                            color: kblue,
                            borderRadius: BorderRadius.only(
                                bottomLeft: Radius.circular(30),
                                bottomRight: Radius.circular(30))),
                      ),
                      Column(
                        children: [
                          Row(
                            mainAxisAlignment:
                            MainAxisAlignment.spaceBetween,
                            children: [
                              Padding(
                                padding: const EdgeInsets.only(left: 15),
                                child: IconButton(
                                  icon: const Icon(
                                    FontAwesomeIcons.arrowLeft,
                                    color: kprimary,
                                  ),
                                  // Icons.arrow_back,
                                  onPressed: () {
                                    Get.back();
                                  },
                                ),
                              ),
                            ],
                          ),
                          SizedBox(
                            height:
                            MediaQuery.of(context).size.height / 15,
                          ),
                          const Padding(
                            padding:
                            EdgeInsets.only(bottom: 10, left: 40),
                            child: Align(
                              alignment: Alignment.bottomLeft,
                              child: Text(
                                'Food Parcel . . .',
                                style: TextStyle(
                                    color: kprimary,
                                    fontSize: 30,
                                    fontStyle: FontStyle.italic,
                                    fontWeight: FontWeight.bold),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ]),
                    Expanded(child: _buildProductsList(size, context)),
                  ],
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget _buildProductsList(Size size, BuildContext context) {
    Random rnd;
    double min = 0;
    rnd = Random();
    double r = min + rnd.nextDouble();
    return ListView.builder(
      shrinkWrap: true,
      itemCount: controller.foodParcelsList.length,
      padding: const EdgeInsets.only(top: 16),
      itemBuilder: (context, index) => Padding(
        padding: const EdgeInsets.symmetric(vertical: 10),
        child: Container(
          height: MediaQuery.of(context).size.height / 6,
          decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(15),
              boxShadow: [kDefaultShadow]),
          child: Padding(
            padding: const EdgeInsets.symmetric(vertical: 8.0),
            child: SingleChildScrollView(
              child: ExpansionTile(
                  leading: const CircleAvatar(
                    radius: 30,
                    backgroundColor: Colors.black45,
                  ),
                  title: Padding(
                    padding: const EdgeInsets.only(top: 10),
                    child: Text(
                      "food parcel ${controller.foodParcelsList[index].id}",
                      style: const TextStyle(
                        color: Colors.black,
                      ),
                    ),
                  ),
                  subtitle: SingleChildScrollView(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Padding(
                          padding: const EdgeInsets.only(top: 5, bottom: 10),
                          child: Text(
                              '${controller.foodParcelsList[index].cost}',
                              style: const TextStyle(color: Colors.black)),
                        ),
                        LinearPercentIndicator(
                          barRadius: const Radius.circular(040),
                          width: MediaQuery.of(context).size.width / 2,
                          animation: true,
                          animationDuration: 800,
                          lineHeight: 10,
                          percent: r,
                          progressColor: kblue,
                          backgroundColor: kbblue,
                        ),
                        SizedBox(
                          height: MediaQuery.of(context).size.height / 100,
                        ),
                        Text(
                          '${controller.foodParcelsList[index].donate}\$/${controller.foodParcelsList[index].cost}',
                          style: const TextStyle(color: kstorm),
                        ),
                      ],
                    ),
                  ),
                  trailing: updatButtonEffect(index, context, size),
                  children: [
                    Stack(
                      children: [
                        Align(
                          alignment: const Alignment(0, 0),
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Padding(
                                padding: const EdgeInsets.all(12.0),
                                child: SizedBox(
                                  width: size.width / 2 + size.width / 20,
                                  child: Column(
                                    crossAxisAlignment:
                                    CrossAxisAlignment.start,
                                    children: [
                                      Text(
                                        controller
                                            .foodParcelsList[index].content,
                                        style: const TextStyle(color: kstorm),
                                      ),
                                      Text(
                                        '${controller.foodParcelsList[index].size}/count : ${controller.foodParcelsList[index].count}',
                                        style: const TextStyle(color: kstorm),
                                      ),
                                      const SizedBox(
                                        height: 10,
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                        Align(
                          alignment: const Alignment(-0.9, 0),
                          child: deleteButtonEffect(index, context, size),
                        ),
                      ],
                    ),
                  ]),
            ),
          ),
        ),
      ),
    );
  }

  Container updatButtonEffect(int index, BuildContext context, Size size) {
    return Container(
      height: 50,
      width: 75,
      decoration: BoxDecoration(
        color: kstorm,
        borderRadius: BorderRadius.circular(10),
      ),
      child: MaterialButton(
        onPressed: () {
          var id = controller.foodParcelsList[index].id;
          showModalBottomSheet(
            context: context,
            builder: (BuildContext context) {
              return Container(
                  height: MediaQuery.of(context).size.height / 2,
                  decoration: const BoxDecoration(
                    color: kprimary,
                    borderRadius: BorderRadius.only(
                      topRight: Radius.circular(25),
                      topLeft: Radius.circular(25),
                    ),
                  ),
                  child: SingleChildScrollView(
                    child: Column(
                      children: [
                        Container(
                          height: MediaQuery.of(context).size.height / 5,
                          width: MediaQuery.of(context).size.width,
                          //color: Colors.purple,
                          alignment: const Alignment(-0.6, 0.4),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                            children: [
                              Stack(
                                clipBehavior: Clip.none,
                                children: [
                                  Obx(
                                        () => CircleAvatar(
                                      radius: 40,
                                      backgroundColor: Colors.black45,
                                      backgroundImage: image.value != ''
                                          ? FileImage(
                                        File(image.value),
                                      )
                                          : null,
                                    ),
                                  ),
                                  Positioned(
                                    top: 45,
                                    right: 0,
                                    child: GestureDetector(
                                      onTap: () => {
                                        Timer(const Duration(seconds: 1),
                                                () async {
                                              await takeImage(ImageSource.gallery);
                                            })
                                      },
                                      child: CircleAvatar(
                                        backgroundColor:
                                        Colors.black54.withOpacity(.5),
                                        child: const Icon(
                                          Icons.camera_alt,
                                          size: 30,
                                          color: Colors.white,
                                        ),
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                              //const Spacer(),
                              //const Spacer(),
                            ],
                          ),
                        ),
                        Form(
                          key: foodParcelFormKey[0],
                          autovalidateMode: AutovalidateMode.onUserInteraction,
                          child:
                          getInput(0, size, foodParcelFormKey[0]),
                        ),
                        Form(
                          key: foodParcelFormKey[1],
                          autovalidateMode: AutovalidateMode.onUserInteraction,
                          child:
                          getInput(1, size, foodParcelFormKey[1]),
                        ),
                        Form(
                          key: foodParcelFormKey[2],
                          autovalidateMode: AutovalidateMode.onUserInteraction,
                          child:
                          getInput(2, size, foodParcelFormKey[2]),
                        ),
                        BuildStationerySizes(
                            size: size, controller: controller),
                        const SizedBox(height: 20),
                        GestureDetector(
                          onTap: () => {
                            if (checking())
                              {
                                Timer(const Duration(seconds: 0), () async {
                                  String save =
                                  await InCompleteFoodPacrelServices()
                                      .updateFoodParcel(
                                    FoodParcelModel(
                                        foodParcelEditors[0].text != ''
                                            ? foodParcelEditors[0].text
                                            : controller
                                            .foodParcelsList[index].cost
                                            .toString(),
                                        foodParcelEditors[1].text != ''
                                            ? foodParcelEditors[1].text
                                            : controller
                                            .foodParcelsList[index].count
                                            .toString(),
                                        controller.size.value != 'small'
                                            ? controller.size.value
                                            : controller
                                            .foodParcelsList[index].size,
                                        image.value,
                                        foodParcelEditors[2].text != ''
                                            ? foodParcelEditors[2].text
                                            : controller
                                            .foodParcelsList[index].content
                                     ),
                                    id,
                                  );
                                  Get.back();

                                  if (save == 'OK') {
                                    Timer(const Duration(seconds: 1), () {
                                      Get.to(FunctionStatus(
                                        controller: controller,
                                        title: "Food Parcel",
                                        function: "updated",
                                      ));
                                      controller.isTrue.value = true;
                                      controller.showFoodParcels();
                                    });
                                  } else {
                                    controller.isTrue.value = false;
                                    Get.to(
                                        FunctionStatus(
                                      controller: controller,
                                      title: "Food Parcel",
                                      function: "updated",
                                    ));
                                  }
                                }),
                              }
                          },
                          child: Container(
                            width: MediaQuery.of(context).size.width / 3,
                            height: MediaQuery.of(context).size.width / 9,
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(10),
                              color: kblue,
                            ),
                            alignment: Alignment.center,
                            child: const Text(
                              "Update",
                              style: TextStyle(
                                  color: Colors.white,
                                  fontWeight: FontWeight.bold,
                                  fontSize: 19),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ));
            },
          );
        },
        child: const Center(
          child: Text(
            'Update',
            style: TextStyle(color: kprimary, fontSize: 12.2),
          ),
        ),
      ),
    );
  }

  Container deleteButtonEffect(int index, BuildContext context, Size size) {
    return Container(
      height: 50,
      width: 75,
      decoration: BoxDecoration(
        color: kstorm,
        borderRadius: BorderRadius.circular(10),
      ),
      child: MaterialButton(
        onPressed: () {
          var id = controller.foodParcelsList[index].id;
          showModalBottomSheet(
            constraints: const BoxConstraints(maxHeight: 200),
            context: context,
            builder: (BuildContext context) {
              return Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  const Text(
                    "Would yo like to delete this element ? ",
                    style: TextStyle(
                        color: Colors.black54, fontWeight: FontWeight.bold),
                    //textAlign: TextAlign.center,
                  ),
                  const SizedBox(
                    height: 15,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      GestureDetector(
                        onTap: () => {
                          Timer(const Duration(seconds: 0), () async {
                            String save = await controller.deleteFoodParcel(id);
                            if (save == 'OK') {
                              controller.isTrue.value = true;
                              Get.back();
                              Get.to(FunctionStatus(
                                controller: controller,
                                title: "Food Parcels",
                                function: "deleted",
                              ));
                              controller.showFoodParcels();
                            } else {
                              print('error');
                              controller.isTrue.value = false;
                              Get.to(FunctionStatus(
                                controller: controller,
                                title: "Food Parcels",
                                function: "deleted",
                              ));
                            }
                          })
                        },
                        child: Container(
                          alignment: Alignment.center,
                          width: 80,
                          height: 50,
                          decoration: BoxDecoration(
                            color: kstorm,
                            borderRadius: BorderRadius.circular(10),
                          ),
                          child: const Text(
                            "Yes",
                            style: TextStyle(
                                color: Colors.white,
                                fontWeight: FontWeight.bold,
                                fontSize: 22),
                          ),
                        ),
                      ),
                      const SizedBox(
                        width: 15,
                      ),
                      GestureDetector(
                        onTap: () => Get.back(),
                        child: Container(
                          width: 80,
                          height: 50,
                          alignment: Alignment.center,
                          decoration: BoxDecoration(
                            color: kprimary,
                            borderRadius: BorderRadius.circular(10),
                          ),
                          child: const Text(
                            "No",
                            style: TextStyle(
                                color: Colors.white,
                                fontWeight: FontWeight.bold,
                                fontSize: 22),
                          ),
                        ),
                      ),
                    ],
                  ),
                ],
              );
            },
          );
        },
        child: const Center(
          child: Text(
            'Delete',
            style: TextStyle(color: kprimary, fontSize: 12.2),
          ),
        ),
      ),
    );
  }

  Future<void> openCylinder(BuildContext context) async {
    final DateTime? pickedDate = await showDatePicker(
        context: context,
        initialDate: currentDate.value,
        firstDate: currentDate.value,
        lastDate: DateTime(2050));
    if (pickedDate != null && pickedDate != currentDate) {
      currentDate.value = pickedDate;
    }
  }

  Container CameraStudio(IconData icon, Size size) {
    return Container(
      margin: const EdgeInsets.symmetric(horizontal: 5, vertical: 10),
      child: Icon(
        icon,
        size: 30,
        color: kprimary,
      ),
      width: size.width * .3,
      height: 120,
      decoration: BoxDecoration(
        color: Colors.white.withOpacity(.2),
        border: Border.all(
          width: 2,
          color: kprimary,
        ),
        borderRadius: BorderRadius.circular(20),
      ),
    );
  }

  Widget getInput(int index, Size size, formKey) {
    return Container(
      alignment: Alignment.center,
      width: size.width * .8,
      height: 78,
      margin: const EdgeInsets.symmetric(horizontal: 20, vertical: 15),
      padding: const EdgeInsets.symmetric(horizontal: 10),
      decoration: BoxDecoration(
          color: Colors.grey.withOpacity(.2),
          borderRadius: BorderRadius.circular(10)),
      child: TextFormField(
        //key: formKey,
        controller: foodParcelEditors[index],
        decoration: InputDecoration(
          labelText: foodParcelAttributeName[index],
          border: InputBorder.none,
          icon: foodParcelIcons[index],
          iconColor: Colors.red,
          labelStyle: TextStyle(
              fontSize: 16, fontWeight: FontWeight.w600, color: kprimary),
        ),
        maxLines: index == 3 ? 4 : 1,
        keyboardType: foodParcelTypes[index],
        validator: (value) {
          if (index == 0 && value != null  &&  value != '' && int.parse(value) <= 1000) {
            return foodParcelErrorText[0];
          } else if (index == 1 && value != null  &&  value != '' && int.parse(value) == 0) {
            return foodParcelErrorText[1];
          } else if (index == 2 && value != null && value.length < 7) {
            return foodParcelErrorText[2];
          }
        },
      ),
    );
  }

  bool check() {
    bool isVlidate = true;
    for (int i = 0; i <= 2; i++) {
      if (isVlidate) {
        isVlidate = foodParcelFormKey[i].currentState!.validate();
      }
      if (!isVlidate) {
        foodParcelFormKey[i].currentState!.validate();
      }
    }
    return isVlidate;
  }
}


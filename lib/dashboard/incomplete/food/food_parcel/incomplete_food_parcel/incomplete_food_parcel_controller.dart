import 'package:get/get.dart';
import '../add_food_parcel/add_food_parcel_consts.dart';
import 'incomplete_food_parcel_model.dart';
import 'incomplete_food_parcel_services.dart';


class InCompleteFoodParcelController extends GetxController
{
  var foodParcelsList =<InCompleteFoodParcelModel>[].obs;
  var foodParcels=<InCompleteFoodParcelModel>[];
  RxBool isTrue = false.obs;
  RxString image = ''.obs;
  RxInt currentItem = 0.obs;
  RxString size = 'small'.obs;

  var id=0.obs;

  InCompleteFoodPacrelServices service = InCompleteFoodPacrelServices();
  var isloading=true.obs;

  @override
  void onInit() {
    showFoodParcels();
    //loadProductsFromRepo(id.value);
    super.onInit();
  }

  void showFoodParcels() async {
    try {
      isloading(true);
      foodParcels = await service.getFoodParcels();
      if (foodParcels != null) {
        foodParcelsList.value = foodParcels;
      }
    } finally {
      isloading(false);
    }
  }

  Future<String> deleteFoodParcel(int id) async {

    String save;

    try {
      isloading(true);
      save = await service.deleteFoodParcel(id);
    }finally {
      isloading(false);
    }
    return save;
  }

  void changeItem(int index){
    print(index);

    currentItem.value = index;
    size.value = foodParcelSizes[index];

  }

}
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:get/get.dart';
import 'package:image_picker/image_picker.dart';

import 'add_food_parcel_consts.dart';

class FoodParcelController extends GetxController{

  RxString image = ''.obs;
  RxBool isGet = false.obs;//for ensure if the image get or not
  RxString? langStr;
  RxBool isTrue = false.obs;
  RxBool isFalse = false.obs;
  RxString size = 'small'.obs;
  RxInt currentItem = 0.obs;

  FlutterSecureStorage storage = const FlutterSecureStorage();

  Future<void> takeImage(ImageSource source) async {
    final picker = ImagePicker();
    final getImage = await picker.pickImage(source: source);
    if (getImage != null) {
      image.value = getImage.path;
      isGet.value = true;
    }
  }

  void changeItem(int index){
    print(index);

    currentItem.value = index;
    size.value = foodParcelSizes[index];

  }

}
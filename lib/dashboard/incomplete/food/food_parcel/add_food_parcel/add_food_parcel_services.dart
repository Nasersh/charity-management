import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:http/http.dart' as http;

import '../../../../../config/config.dart';
import '../../food_parcel_model.dart';
// <<<<<<< HEAD
class FoodParcelServices {
  var url = Uri.parse(ServerConfig.domainNameServer + ServerConfig.addFoodParcel);
// >>>>>>> 2c6a0d2 (new file)

  Future<String> addStationery(FoodParcelModel foodParcel) async {

    FlutterSecureStorage storage = const FlutterSecureStorage();
    String? token = await storage.read(key: 'token');
    print(token);


    print(foodParcel.cost);
    print(foodParcel.count);
    print(foodParcel.size);
    print(foodParcel.image);
    print(foodParcel.content);


    var response = await http.post(url, headers: {
      'Authorization': 'Bearer $token',
      'Accept': 'application/json'
    }, body: {
      'count':foodParcel.count,
      'cost': foodParcel.cost,
      'size': foodParcel.size,
      'image': foodParcel.image,
      'content':foodParcel.content
    });

    String getResponse = response.body;
    print(getResponse);
    if(getResponse.contains('{"message":"food parcel added successfully"}')){
      return 'OK';
    }else{
      return 'NO';
    }

  }

}

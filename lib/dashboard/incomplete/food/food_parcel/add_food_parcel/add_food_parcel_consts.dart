import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import '../../../../../configrations/consts.dart';

List<GlobalKey<FormState>> foodParcelFormKey = [
  GlobalKey<FormState>(),
  GlobalKey<FormState>(),
  GlobalKey<FormState>(),
];

List<String> foodParcelAttributeName = [
  "cost",
  "count",
  "content"
];

List<Icon> foodParcelIcons = [
  Icon(
    Icons.paypal,
    color: mainColor,
  ),
  Icon(
    Icons.numbers,
    color: mainColor,
  ),
  Icon(
    Icons.comment,
    color: mainColor,
  ),
];

List<String> foodParcelErrorText = [
  "your cost is less than 1000",
  "your size must be medium or big or small",
  "your content must be more than 7",
];

List<TextInputType> foodParcelTypes = [
  TextInputType.number,
  TextInputType.number,
  TextInputType.name,
];

List<TextEditingController> foodParcelEditors = [
  TextEditingController(),
  TextEditingController(),
  TextEditingController(),
];

List<String> foodParcelSizes = [
  'Small',
  'Medium',
  'Big'
];
import 'dart:convert';
// <<<<<<< HEAD
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:http/http.dart' as http;
// =======
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:http/http.dart' as http;
// >>>>>>> 2c6a0d2 (new file)
import '../../../../../config/config.dart';
import '../../food_section_model.dart';
import 'incomplete_food_section_model.dart';


class InCompleteFoodSectionServices {
  var message;
  var url = Uri.parse(
// <<<<<<< HEAD
      ServerConfig.domainNameServer + ServerConfig.inCompleteFoodSections);
// =======
//       ServerConfig.domainNameServer + ServerConfig.inCompleteFoodSections);
// >>>>>>> 2c6a0d2 (new file)
  FlutterSecureStorage storage = const FlutterSecureStorage();

  Future<List<InCompleteFoodSectionModel>> getFoodItems() async {

    String? token = await storage.read(key: 'token');
    print(token);
    var response = await http.get(
      url,
      headers: {'Accept': 'application/json', 'Authorization': 'Bearer $token'},
    );

    print(response.body);
    if (response.statusCode == 200) {
      List<InCompleteFoodSectionModel> inComFoodItems = combineFoodItems(json.decode(response.body));
      print(inComFoodItems);
      return inComFoodItems;
    } else {
      return [];
    }
  }

  List<InCompleteFoodSectionModel> combineFoodItems(List<dynamic> response) {
    List<InCompleteFoodSectionModel> inComFoodItems = [];

    for (int i = 0; i < response.length; i++) {

      print(i);
      inComFoodItems.add(InCompleteFoodSectionModel(
        response[i]['name'],
        response[i]['cost'],
        response[i]['count'],
        response[i]['image'],
        response[i]['donate'],
        response[i]['id'],
      ));

    }
    return inComFoodItems;
  }

  Future<String> updateFoodItem(FoodSectionModel foodItem, int id) async {

    String? token = await storage.read(key: 'token');
    print(token);

    var url = Uri.parse(
// <<<<<<< HEAD
        ServerConfig.domainNameServer + ServerConfig.updateFoodSection + '$id');
// =======
//         ServerConfig.domainNameServer + ServerConfig.updateFoodSection + '$id');
// >>>>>>> 2c6a0d2 (new file)

    print(foodItem.name);
    print(foodItem.count);print(foodItem.cost);print(foodItem.image);


    var response = await http.post(url, headers: {
      'Authorization': 'Bearer $token ',
      'Accept': 'application/json'
    }, body: {
      'name': foodItem.name,
      'cost': foodItem.cost,
      'count': foodItem.count,
      'image': foodItem.image,
    });

    String getResponse = response.body;
    print(getResponse);
    if (getResponse.contains('{"message":"updated successfully"')) {
      return 'OK';
    } else {
      return 'NO';
    }
  }


  Future<String> deleteFoodItem(int id) async {
    String? token = await storage.read(key: 'token');
    print(token);

    var url = Uri.parse(
// <<<<<<< HEAD
        ServerConfig.domainNameServer + ServerConfig.deleteFoodSection + '$id');
// =======
//         ServerConfig.domainNameServer + ServerConfig.deleteFoodSection + '$id');
// >>>>>>> 2c6a0d2 (new file)

    print('id : $id');

    var response = await http.delete(url, headers: {
      'Authorization': 'Bearer $token ',
      'Accept': 'application/json'
    });

    String getResponse = response.body;
    print(getResponse);
    if (response.statusCode == 200) {
      return 'OK';
    } else {
      return 'NO';
    }
  }


}

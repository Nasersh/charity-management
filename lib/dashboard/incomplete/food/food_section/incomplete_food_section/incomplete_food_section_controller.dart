import 'package:get/get.dart';
import 'incomplete_food_section_model.dart';
import 'incomplete_food_section_services.dart';

class InCompleteFoodSectionController extends GetxController
{
  var foodItemsList =<InCompleteFoodSectionModel>[].obs;
  var foodItems=<InCompleteFoodSectionModel>[];
  RxBool isTrue = false.obs;

  var id=0.obs;

  InCompleteFoodSectionServices service = InCompleteFoodSectionServices();
  var isloading=true.obs;

  @override
  void onInit() {
    showFoodItems();
    //loadProductsFromRepo(id.value);
    super.onInit();
  }

  void showFoodItems() async {
    try {
      isloading(true);
      foodItems = await service.getFoodItems();
      foodItems.forEach((element) {print(element.donate);});
      if (foodItems != null) {
        foodItemsList.value = foodItems;
      }
    } finally {
      isloading(false);
    }
  }


  Future<String> deleteFoodItem(int id) async {

    String save;

    try {
      isloading(true);
      save = await service.deleteFoodItem(id);
    }finally {
      isloading(false);
    }
    return save;
  }


}
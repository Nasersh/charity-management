import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import '../../../../../configrations/consts.dart';



List<GlobalKey<FormState>> foodSectionFormKey = [
  GlobalKey<FormState>(),
  GlobalKey<FormState>(),
  GlobalKey<FormState>(),
];

List<String> foodSectionAttributeName = [
  "name",
  "cost",
  "count",
];

List<Icon> foodSectionIcons = [
  Icon(
    Icons.person,
    color: mainColor,
  ),
  Icon(
    Icons.paypal,
    color: mainColor,
  ),
  Icon(
    Icons.numbers,
    color: mainColor,
  ),
];

List<String> foodSectionErrorText = [
  "your Name is less than 7",
  "your cost is less than 1000",
  "your count must be more than 0",
];

List<TextInputType> foodSectionTypes = [
  TextInputType.name,
  TextInputType.number,
  TextInputType.number,
];

List<TextEditingController> foodSectionEditors = [
  TextEditingController(),
  TextEditingController(),
  TextEditingController(),
];

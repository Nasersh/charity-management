import 'dart:async';
import 'dart:io';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:get/get.dart';
import 'package:image_picker/image_picker.dart';
import 'package:lottie/lottie.dart';
import '../../../../../configrations/const/const.dart';
import '../../../../../configrations/functions_status/functions_status.dart';
import '../../../../../configrations/the_helper_widget/the_hepler_services.dart';
import '../../food_section_model.dart';
import 'add_food_section_consts.dart';
import 'add_food_section_controller.dart';
import 'add_food_section_services.dart';

class AddFoodSection extends StatefulWidget {
  const AddFoodSection({Key? key}) : super(key: key);

  @override
  _AddFoodSectionState createState() => _AddFoodSectionState();
}

class _AddFoodSectionState extends State<AddFoodSection> {
  Rx<DateTime> currentDate = DateTime.now().obs;

  @override
  Widget build(BuildContext context) {
    FoodSectionController controller =
        Get.put<FoodSectionController>(FoodSectionController());

    Size size = MediaQuery.of(context).size;
    return Scaffold(
      body: SafeArea(
        child: Scaffold(
          body: Container(
            decoration: BoxDecoration(
                gradient: LinearGradient(
                    colors: [kprimary.withOpacity(.3), kprimary],
                    begin: Alignment.topLeft,
                    end: Alignment.bottomRight)),
            child: Stack(
              children: [
                Positioned(
                  top: size.height / 6.6,
                  left: size.width / 1.9,
                  child: SizedBox(
                    width: 150,
                    child: Lottie.asset('assets/food/24703-food-animation.json',
                        fit: BoxFit.fitWidth),
                  ),
                ),
                // Positioned(
                //   top: size.height / 1.4,
                //   left: size.width / 100,
                //   child: Container(
                //     width: 200,
                //     height: 200,
                //     decoration: BoxDecoration(
                //       shape: BoxShape.circle,
                //       color: Colors.white.withOpacity(.2),
                //     ),
                //   ),
                // ),
                // Positioned(
                //   top: size.height / 2.2,
                //   left: size.width / 2,
                //   child: Container(
                //     width: 200,
                //     height: 200,
                //     decoration: BoxDecoration(
                //       shape: BoxShape.circle,
                //       color: Colors.white.withOpacity(.2),
                //     ),
                //   ),
                // ),
                Positioned(
                  top: (size.height / 12 )+40,
                  left:size.width/9.4,
                  child: Container(
                    width: size.width * .8,
                    height: size.height * .7,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(30),
                      color: Colors.white.withOpacity(.2),
                    ),
                    //color: Colors.red,
                  ),
                ),
                Align(
                  alignment: Alignment.topCenter,
                  child: Container(
                    width: size.width * .8,
                    height: size.height * .7 / 1.5,
                    margin: EdgeInsets.only(
                      top: (size.height / 2) - (size.height * .7 / 5),
                    ),
                    decoration: BoxDecoration(
                      color: Colors.white.withOpacity(.2),
                      borderRadius: BorderRadius.circular(30),
                    ),
                    child: SingleChildScrollView(
                      child: Column(
                        children: [
                          Form(
                            key: foodSectionFormKey[0],
                            autovalidateMode:
                                AutovalidateMode.onUserInteraction,
                            child: getInput(0, size, foodSectionFormKey[0]),
                          ),
                          Form(
                            key: foodSectionFormKey[1],
                            autovalidateMode:
                                AutovalidateMode.onUserInteraction,
                            child: getInput(1, size, foodSectionFormKey[1]),
                          ),
                          Form(
                            key: foodSectionFormKey[2],
                            autovalidateMode:
                                AutovalidateMode.onUserInteraction,
                            child: getInput(2, size, foodSectionFormKey[2]),
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceAround,
                            children: [
                              GestureDetector(
                                child: CameraStudio(Icons.camera, size),
                                onTap: () =>
                                    controller.takeImage(ImageSource.gallery),
                              ),
                              GestureDetector(
                                child: CameraStudio(Icons.camera_alt, size),
                                onTap: () =>
                                    controller.takeImage(ImageSource.camera),
                              ),
                            ],
                          ),
                          Obx(
                            () => Container(
                              margin: const EdgeInsets.all(10),
                              child: (controller.isGet.value)
                                  ? ClipRRect(
                                      child: Image.file(
                                        File(controller.image.value),
                                        fit: BoxFit.cover,
                                      ),
                                      borderRadius: BorderRadius.circular(20),
                                    )
                                  : Icon(
                                      Icons.now_wallpaper_outlined,
                                      size: 30,
                                      color: kprimary,
                                    ),
                              width: size.width * .8 / 1.1,
                              height: 120,
                              decoration: BoxDecoration(
                                color: Colors.white.withOpacity(.2),
                                border: Border.all(
                                  width: 2,
                                  color: kprimary,
                                ),
                                borderRadius: BorderRadius.circular(20),
                              ),
                            ),
                          ),
                          GestureDetector(
                            onTap: () => {
                              check(),
                              if (controller.image.value != '' && check())
                                {
                                  Timer(const Duration(milliseconds: 0),
                                      () async {
                                    sendNoti(
                                        'Orphans app',
                                        "a new food item need your donate",
                                        DateTime.now().toString());
                                    notificationEnsure(context);
                                    String save = await FoodSectionServices()
                                        .addStationery(
                                      FoodSectionModel(
                                        foodSectionEditors[0].text,
                                        foodSectionEditors[1].text,
                                        foodSectionEditors[2].text,
                                        controller.image.value,
                                      ),
                                    );
                                    if (save == 'OK') {
                                      controller.isTrue.value = true;
                                      Get.to(FunctionStatus(
                                        controller: controller,
                                        title: "Food Item",
                                        function: "added",
                                      ));
                                      controller.isGet.value = false;
                                      controller.image.value = '';
                                    } else {
                                      print('error');
                                      controller.isFalse.value = true;
                                      Get.to(FunctionStatus(
                                        controller: controller,
                                        title: "Food Item",
                                        function: "added",
                                      ));
                                    }
                                  })
                                }
                              else
                                {
                                  print("enter the images"),
                                }
                            },
                            child: Container(
                              alignment: Alignment.center,
                              margin: const EdgeInsets.all(15),
                              width: size.width / 4,
                              height: 50,
                              decoration: BoxDecoration(
                                  color: Colors.white.withOpacity(.4),
                                  borderRadius: BorderRadius.circular(10)),
                              child: Text(
                                'Submit',
                                style: TextStyle(
                                    color: kprimary,
                                    fontSize: 16,
                                    fontWeight: FontWeight.bold),
                                textAlign: TextAlign.center,
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Container CameraStudio(IconData icon, Size size) {
    return Container(
      margin: const EdgeInsets.symmetric(horizontal: 5, vertical: 10),
      child: Icon(
        icon,
        size: 30,
        color: kprimary,
      ),
      width: size.width * .32,
      height: 120,
      decoration: BoxDecoration(
        color: Colors.white.withOpacity(.2),
        border: Border.all(
          width: 2,
          color: kprimary,
        ),
        borderRadius: BorderRadius.circular(20),
      ),
    );
  }

  Widget getInput(int index, Size size, formKey) {
    return Container(
      alignment: Alignment.center,
      width: size.width * .8,
      height: 78,
      margin: const EdgeInsets.symmetric(horizontal: 20, vertical: 15),
      padding: const EdgeInsets.symmetric(horizontal: 10),
      decoration: BoxDecoration(
          color: Colors.white.withOpacity(.2),
          borderRadius: BorderRadius.circular(10)),
      child: TextFormField(
        //key: formKey,
        controller: foodSectionEditors[index],
        decoration: InputDecoration(
          labelText: foodSectionAttributeName[index],
          border: InputBorder.none,
          icon: foodSectionIcons[index],
          iconColor: Colors.red,
          labelStyle: TextStyle(
              fontSize: 16, fontWeight: FontWeight.w600, color: kprimary),
        ),
        maxLines: index == 3 ? 4 : 1,
        keyboardType: foodSectionTypes[index],
        validator: (value) {
          if (index == 0 && value != null && value.length < 7) {
            return foodSectionErrorText[0];
          } else if (index == 1 &&
              value != null &&
              value != '' &&
              int.parse(value) <= 1000) {
            return foodSectionErrorText[1];
          } else if (index == 2 &&
              value != null &&
              value != '' &&
              int.parse(value) <= 0) {
            return foodSectionErrorText[2];
          }
        },
      ),
    );
  }

  bool check() {
    bool isVlidate = true;
    for (int i = 0; i <= 2; i++) {
      if (isVlidate) {
        isVlidate = foodSectionFormKey[i].currentState!.validate();
      }
      if (!isVlidate) {
        foodSectionFormKey[i].currentState!.validate();
      }
    }
    return isVlidate;
  }
}

import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:http/http.dart' as http;
// <<<<<<< HEAD
import '../../../../../config/config.dart';
import '../../food_section_model.dart';

class FoodSectionServices {
  var url = Uri.parse(ServerConfig.domainNameServer + ServerConfig.addFoodSection);
// >>>>>>> 2c6a0d2 (new file)

  Future<String> addStationery(FoodSectionModel foodSection) async {

    FlutterSecureStorage storage = const FlutterSecureStorage();
    String? token = await storage.read(key: 'token');
    print(token);

    print(foodSection.name);
    print(foodSection.cost);
    print(foodSection.count);
    print(foodSection.image);


    var response = await http.post(url, headers: {
      'Authorization': 'Bearer $token',
      'Accept': 'application/json'
    }, body: {
      'name': foodSection.name,
      'count':foodSection.count,
      'cost': foodSection.cost,
      'image': foodSection.image,
    });

    String getResponse = response.body;
    print(getResponse);
    if(getResponse.contains('{"message":"FoodSection added successfully"}')){
      return 'OK';
    }else{
      return 'NO';
    }

  }

}

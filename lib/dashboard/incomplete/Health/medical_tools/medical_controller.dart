import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:get/get.dart';
import 'package:image_picker/image_picker.dart';

class MedicalController extends GetxController{

  RxString image = ''.obs;
  RxBool isGet = false.obs;
  RxString? langStr;
  RxBool isTrue = false.obs;
  RxBool isFalse = false.obs;

  FlutterSecureStorage storage = const FlutterSecureStorage();

  Future<void> takeImage(ImageSource source) async {
    final picker = ImagePicker();
    final getImage = await picker.pickImage(source: source);
    if (getImage != null) {
      image.value = getImage.path;
      isGet.value = true;
    }
  }

}
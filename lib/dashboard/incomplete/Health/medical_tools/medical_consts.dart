import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import '../../../../configrations/consts.dart';

List<GlobalKey<FormState>> toolsFormKey = [
  GlobalKey<FormState>(),
  GlobalKey<FormState>(),
  GlobalKey<FormState>(),
];

List<String> toolsAttributeName = [
  "name",
  "cost",
  "count",
];

List<String> hinttext = [
  "enter the name ",
  "how is the cost",
  "number of items",
];


List<Icon> toolIcons = [
  Icon(
    Icons.person,
    color: mainColor,
  ),
  Icon(
    Icons.paypal,
    color: mainColor,
  ),
  Icon(
    Icons.numbers,
    color: mainColor,
  ),
];

List<String> toolsErrorText = [
  "your Name is less than 2 letters",
  "your cost is less than 100",
  "your count can not be 0",
];

List<TextInputType> toolTypes = [
  TextInputType.name,
  TextInputType.number,
  TextInputType.number,
];

List<TextEditingController> toolEditors = [
  TextEditingController(),
  TextEditingController(),
  TextEditingController(),
];
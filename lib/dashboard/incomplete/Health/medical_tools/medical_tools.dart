import 'dart:async';
import 'dart:io';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:image_picker/image_picker.dart';
import 'package:lottie/lottie.dart';
import '../../../../configrations/const/const.dart';
import '../medical_tool_model.dart';
import 'medical_consts.dart';
import 'medical_controller.dart';
import 'medical_services.dart';

class AddMedicalTool extends StatefulWidget {
  const AddMedicalTool({Key? key}) : super(key: key);

  @override
  _AddMedicalToolState createState() => _AddMedicalToolState();
}

class _AddMedicalToolState extends State<AddMedicalTool> {
  Rx<DateTime> currentDate = DateTime.now().obs;

  @override
  Widget build(BuildContext context) {
    MedicalController controller =
        Get.put<MedicalController>(MedicalController());

    Size size = MediaQuery.of(context).size;
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: const Text(
          'Add Medical tool',style: TextStyle(
          color: kprimary,
        ),
        ),
      ),
      body: SafeArea(
        child: Scaffold(
          backgroundColor: kprimary,
          body: Stack(
            children: [
              SingleChildScrollView(
                child: Column(
                  children: [
                    GestureDetector(
                      onTap: () => {
                        controller.takeImage(ImageSource.gallery),
                      },
                      child: Padding(
                        padding: const EdgeInsets.all(15.0),
                        child: Container(
                          height: MediaQuery.of(context).size.height/3,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(20),
                          color: kstorm.withOpacity(.5),
                        ),

                          child: Center(
                            child: Icon(
                              Icons.add_a_photo,
                              size: 30,
                              color: Colors.white.withOpacity(.5),
                            )
                          ),
                        ),
                      ),
                    ),
                    Form(
                      key: toolsFormKey[0],
                      autovalidateMode:
                          AutovalidateMode.onUserInteraction,
                      child: getInput(0, size, toolsFormKey[0]),
                    ),
                    Form(
                      key: toolsFormKey[1],
                      autovalidateMode:
                          AutovalidateMode.onUserInteraction,
                      child: getInput(1, size, toolsFormKey[1]),
                    ),
                    Form(
                      key: toolsFormKey[2],
                      autovalidateMode:
                          AutovalidateMode.onUserInteraction,
                      child: getInput(2, size, toolsFormKey[2]),
                    ),
                    SizedBox(
                      height:MediaQuery.of(context).size.height/20 ,
                    ),
                    MaterialButton(
                      onPressed: ()  async{
                      await MedicalServices().addMedical(
                      MedicalModel(
                      toolEditors[0].text,
                      toolEditors[1].text,
                      toolEditors[2].text,
                      controller.image.value,
                      ),
                      );
                        // check(),
                        // if (controller.image.value != '' && check())
                        //   {
                        //     Timer(const Duration(milliseconds: 0),
                        //         () async {
                        //       sendNoti(
                        //           'Orphans app',
                        //           "a new medical tool need your donate",
                        //           DateTime.now().toString());
                        //       notificationEnsure(context);
                        //       String save =
                        //           await MedicalServices().addMedical(
                        //         MedicalModel(
                        //           toolEditors[0].text,
                        //           toolEditors[1].text,
                        //           toolEditors[2].text,
                        //           controller.image.value,
                        //         ),
                        //       );
                        //       if (save == 'OK') {
                        //         controller.isTrue.value = true;
                        //         Get.to(FunctionStatus(
                        //           controller: controller,
                        //           title: "Medical Tool",
                        //           function: "added",
                        //         ));
                        //         controller.isGet.value = false;
                        //         controller.image.value = '';
                        //       } else {
                        //         print('error');
                        //         controller.isFalse.value = true;
                        //         Get.to(FunctionStatus(
                        //           controller: controller,
                        //           title: "Medical Tool",
                        //           function: "added",
                        //         ));
                        //       }
                        //     })
                        //   }
                        // else
                        //   {
                        //     print("enter the images"),
                        //   }
                      },
                      child: Container(
                        alignment: Alignment.center,
                        margin: const EdgeInsets.all(15),
                        width: size.width / 4,
                        height: 50,
                        decoration: BoxDecoration(
                            color:kstorm,
                            borderRadius: BorderRadius.circular(10)),
                        child: const Text(
                          'Submit',
                          style: TextStyle(
                              color: kprimary,
                              fontSize: 16,
                              fontWeight: FontWeight.bold),
                          textAlign: TextAlign.center,
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Container CameraStudio(IconData icon, Size size) {
    return Container(
      margin: const EdgeInsets.symmetric(horizontal: 5, vertical: 10),
      child: Icon(
        icon,
        size: 30,
        color: kprimary,
      ),
      width: size.width * .32,
      height: 120,
      decoration: BoxDecoration(
        color: Colors.white.withOpacity(.2),
        border: Border.all(
          width: 2,
          color: kprimary,
        ),
        borderRadius: BorderRadius.circular(20),
      ),
    );
  }

  Widget getInput(int index, Size size, formKey) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 20,vertical: 10),
      child: TextFormField(
        controller: toolEditors[index],
        decoration: InputDecoration(
          hintText: hinttext[index],
          labelText: toolsAttributeName[index],
          enabledBorder: UnderlineInputBorder(
            borderSide: BorderSide(
              width: 2,color: kblue.withOpacity(.2)
            )
          ),
          icon: toolIcons[index],
          iconColor: Colors.red,
          labelStyle: TextStyle(
              fontSize: 16,
              fontWeight: FontWeight.w600,
              color: kblue.withOpacity(.5)),
        ),
        keyboardType: toolTypes[index],
        validator: (value) {
          if (index == 0 && value != null && value.length < 2) {
            return toolsErrorText[0];
          } else if (index == 1 &&
              value != null &&
              value != '' &&
              int.parse(value) <= 1000) {
            return toolsErrorText[1];
          } else if (index == 2 &&
              value != null &&
              value != '' &&
              int.parse(value) <= 0) {
            return toolsErrorText[2];
          }
        },
      ),
    );
  }

  bool check() {
    bool isVlidate = true;
    for (int i = 0; i <= 2; i++) {
      if (isVlidate) {
        isVlidate = toolsFormKey[i].currentState!.validate();
      }
      if (!isVlidate) {
        toolsFormKey[i].currentState!.validate();
      }
    }
    return isVlidate;
  }

}

import 'dart:convert';
// <<<<<<< HEAD
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:http/http.dart' as http;
// =======
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:http/http.dart' as http;
// >>>>>>> 2c6a0d2 (new file)
import '../../../../config/config.dart';
import '../medicine_model.dart';
import 'medicine_model.dart';

class MedicinesServices {
  var message;
  var url = Uri.parse(
// <<<<<<< HEAD
      ServerConfig.domainNameServer + ServerConfig.inCompleteMedicines);
// =======
//
//       ServerConfig.domainNameServer + ServerConfig.inCompleteMedicines);
// >>>>>>> 2c6a0d2 (new file)
  FlutterSecureStorage storage = const FlutterSecureStorage();

  Future<List<InMedicineModel>> getMedicines() async {
    String? token = await storage.read(key: 'token');
    print(token);
    var response = await http.get(
      url,
      headers: {'Accept': 'application/json',
        'Authorization': 'Bearer $token'},

    );

    print(response.body);
    if (response.statusCode == 200) {
       List<InMedicineModel> inComMedicines = combineMedicines(json.decode(response.body));
       print(inComMedicines);
       return inComMedicines;
    } else {
      return [];
    }
  }

  List<InMedicineModel> combineMedicines(List<dynamic> response) {
    List<InMedicineModel> inComMedicines = [];

    for (int i = 0; i < response.length; i++) {

      print(i);
        inComMedicines.add(InMedicineModel(
          response[i]['name'],
          response[i]['cost'],
          response[i]['count'],
          response[i]['image'],
          response[i]['donate'],
          response[i]['id'],
        ));

      }
    return inComMedicines;
  }

  Future<String> updateMedicine(MedicineModel medicine, int id) async {

    String? token = await storage.read(key: 'token');
    print(token);

    var url = Uri.parse(
// <<<<<<< HEAD
        ServerConfig.domainNameServer + ServerConfig.updateMedicine + '$id');
// =======
//         ServerConfig.domainNameServer + ServerConfig.updateMedicine + '$id');
// >>>>>>> 2c6a0d2 (new file)

    var response = await http.post(url, headers: {
      'Authorization': 'Bearer $token ',
      'Accept': 'application/json'
    }, body: {
      'name': medicine.name,
      'cost': medicine.cost,
      'count': medicine.count,
      'image': medicine.image,
    });

    String getResponse = response.body;
    print(getResponse);
    if (getResponse.contains('{"message":"updated successfully"')) {
      return 'OK';
    } else {
      return 'NO';
    }
  }


  Future<String> deleteMedicine(int id) async {
    String? token = await storage.read(key: 'token');
    print(token);

    var url = Uri.parse(
// <<<<<<< HEAD
        ServerConfig.domainNameServer + ServerConfig.deleteMedicine + '$id');
// =======
//         ServerConfig.domainNameServer + ServerConfig.deleteMedicine + '$id');
// >>>>>>> 2c6a0d2 (new file)

    var response = await http.delete(url, headers: {
      'Authorization': 'Bearer $token ',
      'Accept': 'application/json'
    });

    String getResponse = response.body;
    print(getResponse);
    if (getResponse.contains('{"message":"medicine record get deleted "')) {
      return 'OK';
    } else {
      return 'NO';
    }
  }


}

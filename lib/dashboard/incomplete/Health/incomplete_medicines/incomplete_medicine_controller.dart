import 'package:charitysys/dashboard/incomplete/Health/incomplete_medicines/incomplete_medicines_services.dart';
import 'package:get/get.dart';
import 'medicine_model.dart';

class MedicinesController extends GetxController
{
  var medicinesList =<InMedicineModel>[].obs;
  var medicines=<InMedicineModel>[];
  RxBool isTrue = false.obs;

  var id=0.obs;

  MedicinesServices service = MedicinesServices();
  var isloading=true.obs;

  @override
  void onInit() {
    showMedicines();
    //loadProductsFromRepo(id.value);
    super.onInit();
  }

  void showMedicines() async {
    try {
      isloading(true);
      medicines = await service.getMedicines();
      medicines.forEach((element) {print(element.donate);});
      if (medicines != null) {
        medicinesList.value = medicines;
      }
    } finally {
      isloading(false);
    }
  }


  Future<String> deleteMedicine(int id) async {

    String save;

    try {
      isloading(true);
      save = await service.deleteMedicine(id);
    }finally {
      isloading(false);
    }
    return save;
  }


}
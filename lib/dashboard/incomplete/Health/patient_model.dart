import 'package:flutter/foundation.dart';

class PatientModel {
  int id;
  late String name;
  late String description;
  late String medicalCondition;
  late String cost;
  late String gender;
  late String date;
  late String image;

  PatientModel(this.name, this.date, this.medicalCondition, this.gender,
      this.cost, this.image,this.description,
      {this.id = 0});
}

// <<<<<<< HEAD
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:http/http.dart' as http;
import '../../../../config/config.dart';
import '../medicine_model.dart';

class MedicinesServices {
  var url = Uri.parse(ServerConfig.domainNameServer + ServerConfig.addMedicine);
// >>>>>>> 2c6a0d2 (new file)

  Future<String> addMedicine(MedicineModel medicine) async {

    FlutterSecureStorage storage = const FlutterSecureStorage();
    String? token = await storage.read(key: 'token');
    print(token);


    var response = await http.post(url, headers: {
      'Authorization': 'Bearer $token',
      'Accept': 'application/json'
    }, body: {
      'category_id':'1',
      'name': medicine.name,
      'count':medicine.count,
      'cost': medicine.cost,
      'image': medicine.image
    });

    String getResponse = response.body;
    print(getResponse);
    if(getResponse.contains('{"message":"medicine added successfully"}')){
      return 'OK';
    }else{
      return 'NO';
    }

  }

}

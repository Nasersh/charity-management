import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import '../../../../configrations/consts.dart';


List<GlobalKey<FormState>> medFormKey = [
  GlobalKey<FormState>(),
  GlobalKey<FormState>(),
  GlobalKey<FormState>(),
];

List<String> medAttributeName = [
  "name",
  "cost",
  "count",
];

List<Icon> medicineIcons = [
  Icon(
    Icons.person,
    color: mainColor,
  ),
  Icon(
    Icons.paypal,
    color: mainColor,
  ),
  Icon(
    Icons.numbers,
    color: mainColor,
  ),
];

List<String> medErrorText = [
  "your Name is less than 7",
  "your cost is less than 1000",
  "your count can not be 0",
];

List<TextInputType> medTypes = [
  TextInputType.name,
  TextInputType.number,
  TextInputType.number,
];

List<TextEditingController> medEditors = [
  TextEditingController(),
  TextEditingController(),
  TextEditingController(),
];
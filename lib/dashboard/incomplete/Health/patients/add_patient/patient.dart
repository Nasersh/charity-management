import 'dart:async';
import 'dart:io';
import 'package:charitysys/dashboard/incomplete/Health/patient_model.dart';
import 'package:charitysys/dashboard/incomplete/Health/patients/add_patient/patient_service.dart';
import 'package:charitysys/dashboard/incomplete/Health/patients/add_patient/patients_controller.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:image_picker/image_picker.dart';
import 'package:lottie/lottie.dart';
import '../../../../../configrations/const/const.dart';
import '../../../../../configrations/functions_status/functions_status.dart';
import '../../../../../configrations/the_helper_widget/the_hepler_services.dart';
import 'patient_consts.dart';

class AddPatients extends StatefulWidget {
  const AddPatients({Key? key}) : super(key: key);

  @override
  _AddPatientsState createState() => _AddPatientsState();
}

class _AddPatientsState extends State<AddPatients> {
  Rx<DateTime> currentDate = DateTime.now().obs;

  @override
  Widget build(BuildContext context) {
    PatientsController controller =
        Get.put<PatientsController>(PatientsController());

    Size size = MediaQuery.of(context).size;
    return Scaffold(
      body: SafeArea(
        child: Scaffold(
          body: Container(
            decoration: BoxDecoration(
                gradient: LinearGradient(
                    colors: [kprimary.withOpacity(.3), kprimary],
                    begin: Alignment.topLeft,
                    end: Alignment.bottomRight)),
            child: Stack(
              children: [
                Positioned(
                  top: (size.height / 12 )+40,
                  left:size.width/9.4,
                  child: Container(
                    width: size.width * .8,
                    height: size.height * .7,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(30),
                      color: Colors.white.withOpacity(.2),
                    ),
                    //color: Colors.red,
                  ),
                ),
                Positioned(
                  top: size.height / 12,
                  left: size.width / 3.6,
                  child: SizedBox(
                    width: 300,
                    child: Lottie.asset(
                      'assets/patients/88284-doctor-prescription (1).json',
                      fit: BoxFit.fitWidth,
                    ),
                  ),
                ),
                // Positioned(
                //   top: size.height / 1.4,
                //   left: size.width / 100,
                //   child: Container(
                //     width: 200,
                //     height: 200,
                //     decoration: BoxDecoration(
                //       shape: BoxShape.circle,
                //       color: Colors.white.withOpacity(.2),
                //     ),
                //   ),
                // ),
                // Positioned(
                //   top: size.height / 2.2,
                //   left: size.width / 2,
                //   child: Container(
                //     width: 200,
                //     height: 200,
                //     decoration: BoxDecoration(
                //       shape: BoxShape.circle,
                //       color: Colors.white.withOpacity(.2),
                //     ),
                //   ),
                // ),
                Align(
                  alignment: Alignment.topCenter,
                  child: Container(
                    width: size.width * .8,
                    height: size.height * .7 / 1.5,
                    margin: EdgeInsets.only(
                        top: (size.height / 2) - (size.height * .7 / 5)),
                    decoration: BoxDecoration(
                      color: Colors.white.withOpacity(.2),
                      borderRadius: BorderRadius.circular(30),
                    ),
                    child: SingleChildScrollView(
                      child: Column(
                        children: [
                          Form(
                            key: formKey[0],
                            autovalidateMode:
                                AutovalidateMode.onUserInteraction,
                            child: getInput(0, size, formKey[0]),
                          ),
                          Form(
                            key: formKey[1],
                            autovalidateMode:
                                AutovalidateMode.onUserInteraction,
                            child: getInput(1, size, formKey[1]),
                          ),
                          Form(
                            key: formKey[2],
                            autovalidateMode:
                                AutovalidateMode.onUserInteraction,
                            child: getInput(2, size, formKey[2]),
                          ),
                          Form(
                            key: formKey[3],
                            autovalidateMode:
                                AutovalidateMode.onUserInteraction,
                            child: getInput(3, size, formKey[3]),
                          ),
                          Form(
                            key: formKey[4],
                            autovalidateMode:
                                AutovalidateMode.onUserInteraction,
                            child: getInput(4, size, formKey[4]),
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceAround,
                            children: [
                              GestureDetector(
                                child: CameraStudio(Icons.date_range, size),
                                onTap: () => openCylinder(),
                              ),
                              GestureDetector(
                                child: CameraStudio(Icons.camera, size),
                                onTap: () =>
                                    controller.takeImage(ImageSource.gallery),
                              ),
                              GestureDetector(
                                child: CameraStudio(Icons.camera_alt, size),
                                onTap: () =>
                                    controller.takeImage(ImageSource.camera),
                              ),
                            ],
                          ),
                          Obx(
                            () => Container(
                              margin: const EdgeInsets.all(10),
                              child: (controller.isGet.value)
                                  ? ClipRRect(
                                      child: Image.file(
                                        File(controller.image.value),
                                        fit: BoxFit.cover,
                                      ),
                                      borderRadius: BorderRadius.circular(20),
                                    )
                                  : Icon(
                                      Icons.now_wallpaper_outlined,
                                      size: 30,
                                      color: kprimary,
                                    ),
                              width: size.width * .8 / 1.1,
                              height: 120,
                              decoration: BoxDecoration(
                                color: Colors.white.withOpacity(.2),
                                border: Border.all(
                                  width: 2,
                                  color: kprimary,
                                ),
                                borderRadius: BorderRadius.circular(20),
                              ),
                            ),
                          ),
                          GestureDetector(
                            onTap: () => {
                              check(),
                              if (controller.image.value != '' && check())
                                {
                                  Timer(const Duration(milliseconds: 0),
                                      () async {
                                    sendNoti(
                                        'Orphans app',
                                        "a new patient need your donate",
                                        DateTime.now().toString());
                                    notificationEnsure(context);
                                    String save =
                                        await PatientServices().addPatient(
                                      PatientModel(
                                        editors[0].text,
                                        currentDate.toString(),
                                        editors[2].text,
                                        editors[4].text,
                                        editors[3].text,
                                        controller.image.value,
                                        editors[1].text,
                                      ),
                                    );
                                    if (save == 'OK') {
                                      controller.isTrue.value = true;
                                      Get.to(FunctionStatus(
                                        controller: controller,
                                        title: "Patient",
                                        function: "added",
                                      ));
                                      controller.isGet.value = false;
                                      controller.image.value = '';
                                    } else {
                                      print('error');
                                      controller.isFalse.value = true;
                                      Get.to(FunctionStatus(
                                        controller: controller,
                                        title: "Patient",
                                        function: "added",
                                      ));
                                    }
                                  })
                                }
                              else
                                {
                                  print("enter the images"),
                                }
                            },
                            child: Container(
                              alignment: Alignment.center,
                              margin: const EdgeInsets.all(15),
                              width: size.width / 4,
                              height: 50,
                              decoration: BoxDecoration(
                                  color: Colors.white.withOpacity(.4),
                                  borderRadius: BorderRadius.circular(10)),
                              child: Text(
                                'Submit',
                                style: TextStyle(
                                    color: kprimary,
                                    fontSize: 16,
                                    fontWeight: FontWeight.bold),
                                textAlign: TextAlign.center,
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Container CameraStudio(IconData icon, Size size) {
    return Container(
      margin: const EdgeInsets.symmetric(horizontal: 5, vertical: 10),
      child: Icon(
        icon,
        size: 30,
        color: kprimary,
      ),
      width: size.width * .8 / 3.8,
      height: 120,
      decoration: BoxDecoration(
        color: Colors.white.withOpacity(.2),
        border: Border.all(
          width: 2,
          color: kprimary,
        ),
        borderRadius: BorderRadius.circular(20),
      ),
    );
  }

  Widget getInput(int index, Size size, formKey) {
    return Container(
      alignment: Alignment.center,
      width: size.width * .8,
      height: 78,
      margin: const EdgeInsets.symmetric(horizontal: 20, vertical: 15),
      padding: const EdgeInsets.symmetric(horizontal: 10),
      decoration: BoxDecoration(
          color: Colors.white.withOpacity(.2),
          borderRadius: BorderRadius.circular(10)),
      child: TextFormField(
        //key: formKey,
        controller: editors[index],
        decoration: InputDecoration(
          labelText: attributeName[index],
          border: InputBorder.none,
          icon: icons[index],
          iconColor: Colors.red,
          labelStyle: TextStyle(
              fontSize: 16, fontWeight: FontWeight.w600, color: kprimary),
        ),
        keyboardType: types[index],
        validator: (value) {
          if (index == 0 && value != null && value.length < 7) {
            return errorText[0];
          } else if (index == 1 && value != null && value.length < 10) {
            return errorText[1];
          } else if (index == 2 && value != null && value.length < 10)
            return errorText[2];
          else if (index == 3 &&
              value != null &&
              value.isNum &&
              value != '' &&
              int.parse(value) < 1000) {
            return errorText[3];
          } else if (index == 4 &&
              value != null &&
              (value != 'male' && value != 'women')) {
            return errorText[4];
          }
        },
      ),
    );
  }

  bool check() {
    bool isVlidate = true;
    for (int i = 0; i <= 4; i++) {
      if (isVlidate) {
        isVlidate = formKey[i].currentState!.validate();
      }
      if (!isVlidate) {
        formKey[i].currentState!.validate();
      }
    }
    return isVlidate;
  }

  Future<void> openCylinder() async {
    final DateTime? pickedDate = await showDatePicker(
        context: context,
        initialDate: currentDate.value,
        firstDate: currentDate.value,
        lastDate: DateTime(2050));
    if (pickedDate != null && pickedDate != currentDate) {
      currentDate.value = pickedDate;
    }
  }
}

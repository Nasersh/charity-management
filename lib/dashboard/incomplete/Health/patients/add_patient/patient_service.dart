import 'package:charitysys/dashboard/incomplete/Health/patient_model.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:http/http.dart' as http;

import '../../../../../config/config.dart';
// <<<<<<< HEAD
class PatientServices {
  var url = Uri.parse(
      ServerConfig.domainNameServer + ServerConfig.addPatient);
// >>>>>>> 2c6a0d2 (new file)
  FlutterSecureStorage storage = const FlutterSecureStorage();

  Future<String> addPatient(PatientModel patient) async {
    String? token = await storage.read(key: 'token');
    print(token);


    var response = await http.post(url, headers: {
      'Authorization': 'Bearer $token',
      'Accept': 'application/json'
    }, body: {
      'name': patient.name,
      'description':patient.description,
      'birth_date': patient.date,
      'medical_condition': patient.medicalCondition,
      'cost': patient.cost,
      'gender': patient.gender,
      'image': patient.image,
    });

    String getResponse = response.body;
    print(getResponse);
    if(getResponse.contains('{"message":"patient added successfully"}')){
      return 'OK';
    }else{
      return 'NO';
    }

  }

  inCompletePatient() async {
    String? token = await storage.read(key: 'token');
    print(token);

    var url = Uri.parse(
// <<<<<<< HEAD
      ServerConfig.domainNameServer + ServerConfig.inCompletePatient,
// =======
//       ServerConfig.domainNameServer + ServerConfig.inCompletePatient,
// >>>>>>> 2c6a0d2 (new file)
    );

    var response = await http.get(
      url,
      headers:{
        'Authorization': 'Bearer ${token}',
        'Accept': 'application/json'
      },
    );

    print(response.body);
  }
}

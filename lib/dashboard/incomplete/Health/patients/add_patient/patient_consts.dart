import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import '../../../../../configrations/consts.dart';


List<GlobalKey<FormState>> formKey = [
  GlobalKey<FormState>(),
  GlobalKey<FormState>(),
  GlobalKey<FormState>(),
  GlobalKey<FormState>(),
  GlobalKey<FormState>(),
  GlobalKey<FormState>(),
];

List<String> attributeName = [
  "name",
  "description",
  "medical_condition",
  "cost",
  "gender",
  "donate",
];

List<Icon> icons = [
  Icon(
    Icons.person,
    color: mainColor,
  ),
  Icon(
    Icons.description,
    color: mainColor,
  ),
  Icon(
    Icons.comment,
    color: mainColor,
  ),
  Icon(
    Icons.paypal,
    color: mainColor,
  ),
  Icon(
    Icons.boy,
    color: mainColor,
  ),
  Icon(
    Icons.sell,
    color: mainColor,
  ),
];

List<String> errorText = [
  "your Name is less than 7",
  "your description is less than 15",
  "your medical_condition is less than 10",
  "your cost is less than 1000",
  "your selection is not male or women",
  "your do not have enough money in your wallet",
];

List<TextInputType> types = [
  TextInputType.name,
  TextInputType.name,
  TextInputType.name,
  TextInputType.number,
  TextInputType.name,
  TextInputType.number,
];

List<TextEditingController> editors = [
  TextEditingController(),
  TextEditingController(),
  TextEditingController(),
  TextEditingController(),
  TextEditingController(),
  TextEditingController(),
];
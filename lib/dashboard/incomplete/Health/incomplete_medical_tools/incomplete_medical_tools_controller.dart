import 'package:get/get.dart';
import '../../../../user/model/equipment_model.dart';
import 'incomplete_medical_tools_model.dart';
import 'incomplete_medical_tools_services.dart';


class MedicalToolsController extends GetxController
{
  var medicalList =<EquipModel>[].obs;
  var medicals=<EquipModel>[];
  RxBool isTrue = false.obs;

  var id=0.obs;

  MedicalToolsServices service = MedicalToolsServices();
  var isloading=true.obs;

  @override
  void onInit() {
    showMedical();
    //loadProductsFromRepo(id.value);
    super.onInit();
  }

  void showMedical() async {
    try {
      isloading(true);
      medicals = await service.getMedicalTools();
      if (medicals != null) {
        medicalList.value = medicals;
      }
    } finally {
      isloading(false);
    }
  }


  Future<String> deleteMedicalTool(int id) async {

    String save;

    try {
      isloading(true);
      save = await service.deleteMedicalTool(id);
    }finally {
      isloading(false);
    }
    return save;
  }


}
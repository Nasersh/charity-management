import 'dart:convert';
// <<<<<<< HEAD
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:http/http.dart' as http;

// =======
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:http/http.dart' as http;
import '../../../../config/config.dart';
import '../../../../user/model/equipment_model.dart';
import '../medical_tool_model.dart';
import '../medicine_model.dart';
import 'incomplete_medical_tools_model.dart';

class MedicalToolsServices {
  var message;
  var url = Uri.parse(
// <<<<<<< HEAD
      ServerConfig.domainNameServer + ServerConfig.inCompleteMedicalTools);
// =======
//       ServerConfig.domainNameServer + ServerConfig.inCompleteMedicalTools);
// >>>>>>> 2c6a0d2 (new file)
  FlutterSecureStorage storage = const FlutterSecureStorage();

  Future<List<EquipModel>> getMedicalTools() async {

    String? token = await storage.read(key: 'token');
    print(token);
    var response = await http.get(
      url,
      headers: {'Accept': 'application/json', 'Authorization': 'Bearer $token'},
    );

    print(response.body);
    if (response.statusCode == 200) {
      List<EquipModel> inComMedicines = equipModelFromJson(response.body);
      print(inComMedicines);
      return inComMedicines;
    } else {
      return [];
    }
  }

  List<InMedicalTollsModel> combineMedicines(List<dynamic> response) {
    List<InMedicalTollsModel> inComMedicines = [];

    for (int i = 0; i < response.length; i++) {

      print(i);
      inComMedicines.add(InMedicalTollsModel(
        response[i]['name'],
        response[i]['cost'],
        response[i]['count'],
        response[i]['image'],
        response[i]['donate'],
        response[i]['id'],
      ));

    }
    return inComMedicines;
  }

  Future<String> updateMedical(MedicalModel medicine, int id) async {

    String? token = await storage.read(key: 'token');
    print(token);

    var url = Uri.parse(
// <<<<<<< HEAD
        '${ServerConfig.domainNameServer}${ServerConfig.updateMedicalTools}$id');
// =======
//         '${ServerConfig.domainNameServer}${ServerConfig.updateMedicalTools}$id');
// >>>>>>> 2c6a0d2 (new file)

    var response = await http.post(url, headers: {
      'Authorization': 'Bearer $token ',
      'Accept': 'application/json'
    }, body: {
      'name': medicine.name,
      'cost': medicine.cost,
      'count': medicine.count,
      'image': medicine.image,
    });

    String getResponse = response.body;
    print(getResponse);
    if (getResponse.contains('{"message":"updated successfully"')) {
      return 'OK';
    } else {
      return 'NO';
    }
  }


  Future<String> deleteMedicalTool(int id) async {

    String? token = await storage.read(key: 'token');
    print(token);

    var url = Uri.parse(
// <<<<<<< HEAD
        ServerConfig.domainNameServer + ServerConfig.deleteMedicalTool + '$id');
// =======
//         ServerConfig.domainNameServer + ServerConfig.deleteMedicalTool + '$id');
// >>>>>>> 2c6a0d2 (new file)

    var response = await http.delete(url, headers: {
      'Authorization': 'Bearer $token ',
      'Accept': 'application/json'
    });

    String getResponse = response.body;
    print(getResponse);
    if (getResponse.contains('{"message":"medical record get deleted "')) {
      return 'OK';
    } else {
      return 'NO';
    }
  }


}

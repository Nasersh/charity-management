class MedicalModel {
  late String name;
  late String cost;
  late String count;
  late String image;

  MedicalModel(
    this.name,
    this.cost,
    this.count,
    this.image,
  );
}

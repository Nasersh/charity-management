import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:lottie/lottie.dart';

import '../../../../configrations/const/const.dart';
import '../../../../configrations/consts.dart';

class InCompleteCategory extends StatelessWidget {
  InCompleteCategory({
    Key? key,
    this.title = '',
    this.routeName,
    this.categoriesName,
    this.categoriesLottie,
  }) : super(key: key);

  String title;
  var routeName;
  var categoriesName;
  var categoriesLottie;

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;

    return SafeArea(
      child: Scaffold(
        backgroundColor: kprimary,
        body: SingleChildScrollView(
          child: Column(
            children: [
              Container(
                width: size.width,
                height: (size.height) / 5.5,
                alignment: const Alignment(0, 0),
                decoration: BoxDecoration(
                    color: mainColor,
                    borderRadius: const BorderRadius.only(
                      bottomLeft: Radius.circular(50),
                      bottomRight: Radius.circular(50),
                    )),
                child: Text(
                  title, //Sections
                  style: whiteWith32,
                ),
              ),
              const SizedBox(height: 15),
              SizedBox(
                height: (size.height) - ((size.height) / 5.5) - 60,
                child: ListView.builder(
                  itemBuilder: (_, index) => GestureDetector(
                    onTap: () => Get.toNamed(routeName[index]),
                    child: Column(
                      children: [
                        //const SizedBox(height: 50),
                        Padding(
                          padding: const EdgeInsets.all(12.0),
                          child: Container(
                            decoration: BoxDecoration(
                              color: mainColor,
                              borderRadius: BorderRadius.circular(40),
                            ),
                            width: size.width,
                            height: size.height / 4,
                            child: Row(
                              children: [
                                ClipRRect(
                                  borderRadius: BorderRadius.circular(40),
                                  child: Lottie.asset(
                                    categoriesLottie[index],
                                    width: size.width / 2.2,
                                    height: size.height / 4,
                                  ),
                                ),
                                Container(
                                  width: size.width - (size.width / 2.2) - 24,
                                  decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(40)
                                  ),
                                  padding: const EdgeInsets.all(28),
                                  child: Column(
                                    mainAxisSize: MainAxisSize.min,
                                    children: [
                                      FittedBox(
                                        child: Text(
                                          categoriesName[index],
                                          style: const TextStyle(
                                            color: Colors.white,
                                            fontWeight: FontWeight.bold,
                                            fontSize: 25,
                                          ),
                                          textAlign: TextAlign.center,
                                        ),
                                      ),
                                      //const SizedBox(height:20),
                                      Container(
                                        alignment: const Alignment(0,0.7),
                                        height: size.height / 10,
                                        child: const FittedBox(
                                          child: Text(
                                            "Tap to see details \n all items",
                                            style: TextStyle(
                                              color: Colors.white,
                                              // fontWeight: FontWeight.w600,
                                              fontSize: 18,
                                            ),
                                            textAlign: TextAlign.center,
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                )
                              ],
                            ),
                          ),
                        )
                      ],
                    ),
                  ),
                  itemCount: routeName.length,
                ),
              ),
              const SizedBox(height: 50),
            ],
          ),
        ),
      ),
    );
  }
}

//Container(
//               width: size.width,
//               height: (size.height * 3) / 3.85,
//               alignment: Alignment.center,
//               child: Padding(
//                 padding: EdgeInsets.symmetric(
//                     horizontal: size.width / 10, vertical: (size.height) / 10),
//                 child: Swiper(
//                   itemCount: routeName.length,
//                   itemWidth: double.maxFinite,
//                   itemHeight: double.maxFinite,
//                   pagination: const SwiperPagination(
//                       builder: DotSwiperPaginationBuilder()),
//                   layout: SwiperLayout.TINDER,
//                   itemBuilder: (_, index) => Stack(
//                     clipBehavior: Clip.none,
//                     children: [
//                       GestureDetector(
//                         onTap: () => Get.toNamed(routeName[index]),
//                         child: Container(
//                           decoration: BoxDecoration(
//                             color: mainColor,
//                             borderRadius: BorderRadius.circular(50),
//                           ),
//                           child: Center(
//                             child: Text(
//                               categoriesName[index],
//                               style: kHeading2,
//                             ),
//                           ),
//                         ),
//                       ),
//                       Positioned(
//                         top: -60,
//                         child: Lottie.asset(
//                           categoriesLottie[index],
//                           width: 200,
//                           height: 200,
//                         ),
//                       )
//                     ],
//                   ),
//                 ),
//               ),
//             )

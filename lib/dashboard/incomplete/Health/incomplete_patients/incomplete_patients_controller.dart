import 'package:get/get.dart';
import 'incomplete_patients_services.dart';

class PatientsController extends GetxController
{
  var patientslist =<dynamic>[].obs;
  var patients=<dynamic>[];
  RxBool isTrue = false.obs;

  var id=0.obs;

  PatientsService service= PatientsService();
  var isloading=true.obs;

  @override
  void onInit() {
    showPatients();
    //loadProductsFromRepo(id.value);
    super.onInit();
  }

  void showPatients() async {
    try {
      isloading(true);
      patients = await service.getPatient();
      if (patients != null) {
        patientslist.value = patients;
      }
    } finally {
      isloading(false);
    }
  }


  Future<String> deletePatient(int id) async {

    String save;

    try {
      isloading(true);
      save = await service.deletePatient(id);
    }finally {
      isloading(false);
    }
    return save;
  }


}
// <<<<<<< HEAD
// =======
// >>>>>>> 2c6a0d2 (new file)
import 'package:charitysys/dashboard/incomplete/Health/patient_model.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:http/http.dart' as http;

import '../../../../config/config.dart';
import '../../../../user/model/Patient.dart';

// <<<<<<< HEAD
// =======

// >>>>>>> 2c6a0d2 (new file)

class PatientsService {
  var message;
  var url = Uri.parse(
// <<<<<<< HEAD
      ServerConfig.domainNameServer + ServerConfig.inCompletePatient);
// =======
//       ServerConfig.domainNameServer + ServerConfig.inCompletePatient);
// >>>>>>> 2c6a0d2 (new file)
  FlutterSecureStorage storage = const FlutterSecureStorage();

  Future<List<Patient>> getPatient() async {
    storage.write(key: 'token', value: '34|J5KbwKnvFWY0HFO3ocro3XPJRMqDWnfULoDaV5Lj');
    String? token = await storage.read(key: 'token');
    print(token);
    var response = await http.get(url, headers: {
      'Accept': 'application/json',
      'Authorization': 'Bearer $token'
    },);
    print(response.statusCode);

    if (response.statusCode == 200) {
      var patients = patientFromJson(response.body);
      return patients.patients;
    }
    else {
      return [];
    }
  }



    Future<String> updatePatient(PatientModel patient,int id) async {

    String? token = await storage.read(key: 'token');
      print(token);

      var url = Uri.parse(
// <<<<<<< HEAD
          ServerConfig.domainNameServer + ServerConfig.updatePatient + '$id');
// =======
//           ServerConfig.domainNameServer + ServerConfig.updatePatient + '$id');
// >>>>>>> 2c6a0d2 (new file)


      var response = await http.post(url, headers: {
        'Authorization': 'Bearer $token ',
        'Accept': 'application/json'
      }, body: {
        'name': patient.name,
        'birth_date': patient.date,
        'medical_condition': patient.medicalCondition,
        'cost': patient.cost,
        'gender': patient.gender,
        'image': patient.image,
        'description':patient.description
      });

      String getResponse = response.body;
      print(getResponse);
      if(getResponse.contains('{"message":"updated successfully"')){
        return 'OK';
      }else{
        return 'NO';
      }

  }


  Future<String> deletePatient(int id) async {
    String? token = await storage.read(key: 'token');
    print(token);

    var url = Uri.parse(
// <<<<<<< HEAD
        ServerConfig.domainNameServer + ServerConfig.deletePatient + '$id');
// =======
//         ServerConfig.domainNameServer + ServerConfig.deletePatient + '$id');
// >>>>>>> 2c6a0d2 (new file)

    var response = await http.delete(url, headers: {
      'Authorization': 'Bearer $token ',
      'Accept': 'application/json'
    });

    String getResponse = response.body;
    print(getResponse);
    if (getResponse.contains('{"message":"stationery record get deleted "')) {
      return 'OK';
    } else {
      return 'NO';
    }
  }


}

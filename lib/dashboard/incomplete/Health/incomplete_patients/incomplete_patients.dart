import 'dart:async';
import 'dart:io';
import 'dart:math';
import 'package:charitysys/dashboard/incomplete/Health/patient_model.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:get/get.dart';
import 'package:get/get_core/src/get_main.dart';
import 'package:image_picker/image_picker.dart';
import 'package:percent_indicator/linear_percent_indicator.dart';
import '../../../../configrations/functions_status/functions_status.dart';
import '../../../../configrations/the_helper_widget/the_helper_widget.dart';
import '../patients/add_patient/patient_consts.dart';
import 'consts.dart';
import 'incomplete_patients_controller.dart';
import 'incomplete_patients_services.dart';

class IncompletePatients extends StatelessWidget {
  IncompletePatients({Key? key}) : super(key: key);

  PatientsController controller = Get.put(PatientsController());
  Rx<DateTime> currentDate = DateTime.now().obs;

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;

    return Obx(
      () => controller.isloading.value
          ? const SafeArea(
              child: Scaffold(body: Center(child: CircularProgressIndicator())))
          : Stack(
              children: [
                Scaffold(
                  backgroundColor: kprimary,
                  body: SafeArea(
                    child: Obx(
                      () => Column(
                        children: [
                          Stack(children: [
                            Container(
                              width: double.infinity,
                              height: MediaQuery.of(context).size.height / 4,
                              decoration: const BoxDecoration(
                                  color: kblue,
                                  borderRadius: BorderRadius.only(
                                      bottomLeft: Radius.circular(30),
                                      bottomRight: Radius.circular(30))),
                            ),
                            Column(
                              children: [
                                Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    Padding(
                                      padding: const EdgeInsets.only(left: 15),
                                      child: IconButton(
                                        icon: const Icon(
                                          FontAwesomeIcons.arrowLeft,
                                          color: kprimary,
                                        ),
                                        // Icons.arrow_back,
                                        onPressed: () {
                                          Get.back();
                                        },
                                      ),
                                    ),
                                  ],
                                ),
                                SizedBox(
                                  height:
                                      MediaQuery.of(context).size.height / 15,
                                ),
                                const Padding(
                                  padding:
                                      EdgeInsets.only(bottom: 10, left: 40),
                                  child: Align(
                                    alignment: Alignment.bottomLeft,
                                    child: Text(
                                      'Patients . . .',
                                      style: TextStyle(
                                          color: kprimary,
                                          fontSize: 30,
                                          fontStyle: FontStyle.italic,
                                          fontWeight: FontWeight.bold),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ]),
                          Expanded(child: _buildProductsList(size, context)),
                        ],
                      ),
                    ),
                  ),
                ),
              ],
            ),
    );
  }

  Widget _buildProductsList(Size size, BuildContext context) {
    Random rnd;
    double min = 0;
    rnd = Random();
    double r = min + rnd.nextDouble();
    return ListView.builder(
      shrinkWrap: true,
      itemCount: controller.patientslist.length,
      padding: const EdgeInsets.only(top: 16),
      itemBuilder: (context, index) => Padding(
        padding: const EdgeInsets.symmetric(vertical: 10),
        child: Container(
          height: MediaQuery.of(context).size.height / 5.5,
          decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(15),
              boxShadow: const [kDefaultShadow]),
          child: Padding(
            padding: const EdgeInsets.symmetric(vertical: 8.0),
            child: SingleChildScrollView(
              child: ExpansionTile(
                children: [
                  Stack(
                    children: [
                      Align(
                        alignment: const Alignment(0, 0),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Padding(
                              padding: const EdgeInsets.all(12.0),
                              child: SizedBox(
                                width: size.width / 2 + size.width / 20,
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Text(
                                      '${controller.patientslist[index].birthDate}',
                                      style: const TextStyle(color: kstorm),
                                    ),
                                    Text(
                                      '${controller.patientslist[index].description} /gender : ${controller.patientslist[index].gender}',
                                      style: const TextStyle(color: kstorm),
                                    ),
                                    const SizedBox(
                                      height: 10,
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                      Align(
                        alignment: const Alignment(-0.9, 0),
                        child: deleteButtonEffect(index, context, size),
                      ),
                    ],
                  ),
                ],
                leading: const CircleAvatar(
                  radius: 30,
                  backgroundColor: Colors.black45,
                ),
                title: Padding(
                  padding: const EdgeInsets.only(top: 10),
                  child: Text(
                    controller.patientslist[index].name,
                    style: const TextStyle(
                      color: Colors.black,
                    ),
                  ),
                ),
                subtitle: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Padding(
                      padding: const EdgeInsets.only(top: 5, bottom: 10),
                      child: Text(
                        controller.patientslist[index].medicalCondition,
                        style: const TextStyle(
                          color: Colors.black,
                        ),
                      ),
                    ),
                    LinearPercentIndicator(
                      barRadius: const Radius.circular(040),
                      width: MediaQuery.of(context).size.width / 2,
                      animation: true,
                      animationDuration: 800,
                      lineHeight: 10,
                      percent: r,
                      progressColor: kprimary,
                      backgroundColor: kprimary,
                    ),
                    SizedBox(
                      height: MediaQuery.of(context).size.height / 100,
                    ),
                    Text(
                      '${controller.patientslist[index].donate}\$/${controller.patientslist[index].cost}',
                      style: const TextStyle(color: kstorm),
                    ),
                  ],
                ),
                trailing: updatePatient(index, context, size),
              ),
            ),
          ),
        ),
      ),
    );
  }

  Padding updatePatient(int index, BuildContext context, Size size) {
    return Padding(
      padding: const EdgeInsets.only(top: 8.0, right: 5),
      child: Container(
        height: 50,
        width: 75,
        decoration: BoxDecoration(
          color: kstorm,
          borderRadius: BorderRadius.circular(10),
        ),
        child: MaterialButton(
          onPressed: () {
            var id = controller.patientslist[index].id;
            showModalBottomSheet(
              context: context,
              builder: (BuildContext context) {
                return Container(
                  height: MediaQuery.of(context).size.height / 2,
                  decoration: const BoxDecoration(
                    color: kprimary,
                    borderRadius: BorderRadius.only(
                      topRight: Radius.circular(25),
                      topLeft: Radius.circular(25),
                    ),
                  ),
                  child: SingleChildScrollView(
                    child: Column(
                      children: [
                        Container(
                          height: MediaQuery.of(context).size.height / 5,
                          width: MediaQuery.of(context).size.width,
                          //color: Colors.purple,
                          alignment: const Alignment(-0.6, 0.4),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                            children: [
                              Stack(
                                clipBehavior: Clip.none,
                                children: [
                                  Obx(
                                    () => CircleAvatar(
                                      radius: 40,
                                      backgroundColor: Colors.black45,
                                      backgroundImage: image.value != ''
                                          ? FileImage(
                                              File(image.value),
                                            )
                                          : null,
                                    ),
                                  ),
                                  Positioned(
                                    top: 45,
                                    right: 0,
                                    child: GestureDetector(
                                      onTap: () => {
                                        Timer(const Duration(seconds: 1),
                                            () async {
                                          await takeImage(ImageSource.gallery);
                                        })
                                      },
                                      child: CircleAvatar(
                                        backgroundColor:
                                            Colors.black54.withOpacity(.5),
                                        child: const Icon(
                                          Icons.camera_alt,
                                          size: 30,
                                          color: Colors.white,
                                        ),
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                              //const Spacer(),
                              //const Spacer(),
                            ],
                          ),
                        ),
                        Form(
                          key: formKey[0],
                          autovalidateMode: AutovalidateMode.onUserInteraction,
                          child: getInput(0, size, formKey[0]),
                        ),
                        Form(
                          key: formKey[1],
                          autovalidateMode: AutovalidateMode.onUserInteraction,
                          child: getInput(1, size, formKey[1]),
                        ),
                        Form(
                          key: formKey[2],
                          autovalidateMode: AutovalidateMode.onUserInteraction,
                          child: getInput(2, size, formKey[2]),
                        ),
                        Form(
                          key: formKey[3],
                          autovalidateMode: AutovalidateMode.onUserInteraction,
                          child: getInput(3, size, formKey[3]),
                        ),
                        Form(
                          key: formKey[4],
                          autovalidateMode: AutovalidateMode.onUserInteraction,
                          child: getInput(4, size, formKey[4]),
                        ),
                        GestureDetector(
                          child: CameraStudio(Icons.date_range, size),
                          onTap: () => openCylinder(context),
                        ),
                        const SizedBox(height: 20),
                        GestureDetector(
                          onTap: () => {
                            print(editors[0].text),
                            print(editors[1].text),
                            print(editors[2].text),
                            print(editors[3].text),
                            print(editors[4].text),
                            print(editors[5].text),
                            if (checking())
                              {
                                Timer(const Duration(seconds: 0), () async {
                                  String save =
                                      await PatientsService().updatePatient(
                                    PatientModel(
                                      editors[0].text != ''
                                          ? editors[0].text
                                          : controller.patientslist[index].name,
                                      currentDate.value.toString() != ''
                                          ? currentDate.value.toString()
                                          : controller
                                              .patientslist[index].birthDate
                                              .toString(),
                                      editors[2].text != ''
                                          ? editors[2].text
                                          : controller.patientslist[index]
                                              .medicalCondition,
                                      editors[4].text != ''
                                          ? editors[4].text
                                          : controller
                                              .patientslist[index].gender,
                                      editors[3].text != ''
                                          ? editors[3].text
                                          : controller.patientslist[index].cost
                                              .toString(),
                                      image.value != ''
                                          ? image.value
                                          : controller
                                              .patientslist[index].image,
                                      editors[1].text != ''
                                          ? editors[1].text
                                          : controller
                                              .patientslist[index].description
                                              .toString(),
                                    ),
                                    id,
                                  );
                                  Get.back();
                                  if (save == 'OK') {
                                    Timer(const Duration(seconds: 1), () {
                                      Get.to(FunctionStatus(
                                        controller: controller,
                                        title: "Patient",
                                        function: "updated",
                                      ));
                                      controller.isTrue.value = true;
                                      controller.showPatients();
                                    });
                                  } else {
                                    controller.isTrue.value = false;
                                    Get.to(FunctionStatus(
                                      controller: controller,
                                      title: "Patient",
                                      function: "updated",
                                    ));
                                  }
                                })
                              }
                          },
                          child: Container(
                            width: MediaQuery.of(context).size.width / 3,
                            height: MediaQuery.of(context).size.width / 9,
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(10),
                              color: kprimary,
                            ),
                            alignment: Alignment.center,
                            child: const Text(
                              "Update",
                              style: TextStyle(
                                  color: Colors.white,
                                  fontWeight: FontWeight.bold,
                                  fontSize: 19),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                );
              },
            );
          },
          child: const Center(
            child: Text(
              'Update',
              style: TextStyle(color: kprimary, fontSize: 12.2),
            ),
          ),
        ),
      ),
    );
  }

  Container deleteButtonEffect(int index, BuildContext context, Size size) {
    return Container(
      height: 50,
      width: 75,
      decoration: BoxDecoration(
        color: kstorm,
        borderRadius: BorderRadius.circular(10),
      ),
      child: MaterialButton(
        onPressed: () {
          var id = controller.patientslist[index].id;
          showModalBottomSheet(
            constraints: const BoxConstraints(maxHeight: 200),
            context: context,
            builder: (BuildContext context) {
              return Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  const Text(
                    "Would yo like to delete this element ? ",
                    style: TextStyle(
                        color: Colors.black54, fontWeight: FontWeight.bold),
                    //textAlign: TextAlign.center,
                  ),
                  const SizedBox(
                    height: 15,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      GestureDetector(
                        onTap: () => {
                          Timer(const Duration(seconds: 0), () async {
                            String save = await controller.deletePatient(id);
                            if (save == 'OK') {
                              controller.isTrue.value = true;
                              Get.back();
                              Get.to(FunctionStatus(
                                controller: controller,
                                title: "Stationery",
                                function: "deleted",
                              ));
                              controller.showPatients();
                            } else {
                              print('error');
                              controller.isTrue.value = false;
                              Get.to(FunctionStatus(
                                controller: controller,
                                title: "Stationery",
                                function: "deleted",
                              ));
                            }
                          })
                        },
                        child: Container(
                          alignment: Alignment.center,
                          width: 80,
                          height: 50,
                          decoration: BoxDecoration(
                            color: kstorm,
                            borderRadius: BorderRadius.circular(10),
                          ),
                          child: const Text(
                            "Yes",
                            style: TextStyle(
                                color: Colors.white,
                                fontWeight: FontWeight.bold,
                                fontSize: 22),
                          ),
                        ),
                      ),
                      const SizedBox(
                        width: 15,
                      ),
                      GestureDetector(
                        onTap: () => Get.back(),
                        child: Container(
                          width: 80,
                          height: 50,
                          alignment: Alignment.center,
                          decoration: BoxDecoration(
                            color: kprimary,
                            borderRadius: BorderRadius.circular(10),
                          ),
                          child: const Text(
                            "No",
                            style: TextStyle(
                                color: Colors.white,
                                fontWeight: FontWeight.bold,
                                fontSize: 22),
                          ),
                        ),
                      ),
                    ],
                  ),
                ],
              );
            },
          );
        },
        child: const Center(
          child: Text(
            'Delete',
            style: TextStyle(color: kprimary, fontSize: 12.2),
          ),
        ),
      ),
    );
  }

  Future<void> openCylinder(BuildContext context) async {
    final DateTime? pickedDate = await showDatePicker(
        context: context,
        initialDate: currentDate.value,
        firstDate: currentDate.value,
        lastDate: DateTime(2050));
    if (pickedDate != null && pickedDate != currentDate) {
      currentDate.value = pickedDate;
    }
  }

  Container CameraStudio(IconData icon, Size size) {
    return Container(
      margin: const EdgeInsets.symmetric(horizontal: 5, vertical: 10),
      child: Icon(
        icon,
        size: 30,
        color: kprimary,
      ),
      width: size.width * .3,
      height: 120,
      decoration: BoxDecoration(
        color: Colors.white.withOpacity(.2),
        border: Border.all(
          width: 2,
          color: kprimary,
        ),
        borderRadius: BorderRadius.circular(20),
      ),
    );
  }
}

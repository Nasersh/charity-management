class MedicineModel {
  late String name;
  late String cost;
  late String count;
  late String image;

  MedicineModel(
    this.name,
    this.cost,
    this.count,
    this.image,
  );
}

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import '../../../../configrations/consts.dart';

List<GlobalKey<FormState>> guaranteeFormKey = [
  GlobalKey<FormState>(),
  GlobalKey<FormState>(),
];

List<String> guaranteeAttributeName = [
  "name",
  "cost",
];

List<Icon> guaranteeIcons = [
  Icon(
    Icons.person,
    color: mainColor,
  ),
  Icon(
    Icons.paypal,
    color: mainColor,
  ),
];

List<String> guaranteeErrorText = [
  "your Name is less than 7",
  "your cost is less than 1000",
];

List<TextInputType> guaranteeTypes = [
  TextInputType.name,
  TextInputType.number,
];

List<TextEditingController> guaranteeEditors = [
  TextEditingController(),
  TextEditingController(),
];


List<String> academicYears = [
  "first",
  "second",
  "third",
  "fourth",
  "fifth",
  "sixth",
  "seventh",
  "eight",
  "ninth",
  "tenth",
  "eleventh",
  "twelve"
];

List<String> sortTypes = [
  "name",
  "cost",
  "birth_date",
  "academic_year"
];
import 'dart:async';
import 'dart:io';
import 'package:charitysys/dashboard/guarantee_model.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:get/get.dart';
import 'package:image_picker/image_picker.dart';
import 'package:lottie/lottie.dart';
import '../../../../configrations/const/const.dart';
import '../../../../configrations/functions_status/functions_status.dart';
import '../../../../configrations/the_helper_widget/the_hepler_services.dart';
import 'guarantee_consts.dart';
import 'guarantee_controller.dart';

class AddGuarantee extends StatefulWidget {
  const AddGuarantee({Key? key}) : super(key: key);

  @override
  _AddGuaranteeState createState() => _AddGuaranteeState();
}

class _AddGuaranteeState extends State<AddGuarantee> {
  Rx<DateTime> currentDate = DateTime.now().obs;

  @override
  Widget build(BuildContext context) {
    GuaranteeController controller =
        Get.put<GuaranteeController>(GuaranteeController());

    Size size = MediaQuery.of(context).size;
    return Scaffold(
      backgroundColor: kprimary,
      body: SafeArea(
        child: Scaffold(
          body: Container(
            decoration: BoxDecoration(
              gradient: LinearGradient(
                  colors: [kprimary.withOpacity(.3), kprimary],
                  begin: Alignment.topLeft,
                  end: Alignment.bottomRight),
            ),
            child: Stack(
              children: [
                Positioned(
                  top: size.height / 6.2,
                  left: size.width / 1.8,
                  child: SizedBox(
                    width: 150,
                    child: Lottie.asset(
                        'assets/edu/guarantee/80164-reject-document-files.json',
                        fit: BoxFit.fitWidth),
                  ),
                ),
                // Positioned(
                //   top: size.height / 1.4,
                //   left: size.width / 100,
                //   child: Container(
                //     width: 200,
                //     height: 200,
                //     decoration: BoxDecoration(
                //       shape: BoxShape.circle,
                //       color: Colors.white.withOpacity(.2),
                //     ),
                //   ),
                // ),
                // Positioned(
                //   top: size.height / 2.2,
                //   left: size.width / 2,
                //   child: Container(
                //     width: 200,
                //     height: 200,
                //     decoration: BoxDecoration(
                //       shape: BoxShape.circle,
                //       color: Colors.white.withOpacity(.2),
                //     ),
                //   ),
                // ),
                Positioned(
                  top: (size.height / 12 )+40,
                  left:size.width/9.4,
                  child: Container(
                    width: size.width * .8,
                    height: size.height * .7,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(30),
                      color: Colors.white.withOpacity(.2),
                    ),
                    //color: Colors.red,
                  ),
                ),
                Align(
                  alignment: Alignment.topCenter,
                  child: Container(
                    width: size.width * .8,
                    height: size.height * .7 / 1.5,
                    margin: EdgeInsets.only(
                      top: (size.height / 2) - (size.height * .7 / 5),
                    ),
                    decoration: BoxDecoration(
                      color: Colors.white.withOpacity(.2),
                      borderRadius: BorderRadius.circular(30),
                    ),
                    child: SingleChildScrollView(
                      child: Column(
                        children: [
                          Form(
                            key: guaranteeFormKey[0],
                            autovalidateMode:
                                AutovalidateMode.onUserInteraction,
                            child: getInput(0, size, guaranteeFormKey[0]),
                          ),
                          Form(
                            key: guaranteeFormKey[1],
                            autovalidateMode:
                                AutovalidateMode.onUserInteraction,
                            child: getInput(1, size, guaranteeFormKey[1]),
                          ),
                          Obx(
                            () => Container(
                              alignment: controller.isExpand.value
                                  ? Alignment.topCenter
                                  : Alignment.center,
                              width: size.width * .8,
                              height: controller.isExpand.value ? 220 : 78,
                              margin: const EdgeInsets.symmetric(
                                  horizontal: 20, vertical: 15),
                              padding:
                                  const EdgeInsets.symmetric(horizontal: 10),
                              decoration: BoxDecoration(
                                color: Colors.white.withOpacity(.2),
                                borderRadius: BorderRadius.circular(10),
                              ),
                              child: SingleChildScrollView(
                                child: ExpansionTile(
                                  children: [
                                    Container(
                                      height: 150,
                                      width: double.maxFinite,
                                      child: GridView.builder(
                                        gridDelegate:
                                            const SliverGridDelegateWithFixedCrossAxisCount(
                                          crossAxisCount: 3,
                                          mainAxisSpacing: 30,
                                          crossAxisSpacing: 30,
                                        ),
                                        itemCount: 12,
                                        itemBuilder:
                                            (BuildContext context, int index) {
                                          return GestureDetector(
                                            onTap: () =>
                                                controller.changeItem(index),
                                            child: Obx(
                                              () => Container(
                                                decoration: BoxDecoration(
                                                  border: Border.all(
                                                      width: 2,
                                                      color: kprimary),
                                                  borderRadius:
                                                      BorderRadius.circular(15),
                                                  color: controller.currentItem
                                                              .value ==
                                                          index
                                                      ? kprimary
                                                      : Colors.white
                                                          .withOpacity(.2),
                                                ),
                                                child: Center(
                                                  child: Text(
                                                    academicYears[index],
                                                    style: TextStyle(
                                                      color: controller
                                                                  .currentItem
                                                                  .value ==
                                                              index
                                                          ? Colors.white
                                                          : kprimary,
                                                    ),
                                                  ),
                                                ),
                                              ),
                                            ),
                                          );
                                        },
                                      ),
                                    )
                                  ],
                                  title: FittedBox(
                                    child: Text(
                                      "Select child academic year",
                                      style: TextStyle(
                                        fontSize: 16,
                                        fontWeight: FontWeight.w600,
                                        color: kprimary,
                                      ),
                                    ),
                                  ),
                                  onExpansionChanged: (value) {
                                    controller
                                        .isExpand(!controller.isExpand.value);
                                  },
                                ),
                              ),
                            ),
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceAround,
                            children: [
                              GestureDetector(
                                child: CameraStudio(Icons.date_range, size),
                                onTap: () => openCylinder(),
                              ),
                              GestureDetector(
                                child: CameraStudio(Icons.camera, size),
                                onTap: () =>
                                    controller.takeImage(ImageSource.gallery),
                              ),
                              GestureDetector(
                                child: CameraStudio(Icons.camera_alt, size),
                                onTap: () =>
                                    controller.takeImage(ImageSource.camera),
                              ),
                            ],
                          ),
                          Obx(
                            () => Container(
                              margin: const EdgeInsets.all(10),
                              child: (controller.isGet.value)
                                  ? ClipRRect(
                                      child: Image.file(
                                        File(controller.image.value),
                                        fit: BoxFit.cover,
                                      ),
                                      borderRadius: BorderRadius.circular(20),
                                    )
                                  : Icon(
                                      Icons.now_wallpaper_outlined,
                                      size: 30,
                                      color: kprimary,
                                    ),
                              width: size.width * .8 / 1.1,
                              height: 120,
                              decoration: BoxDecoration(
                                color: Colors.white.withOpacity(.2),
                                border: Border.all(
                                  width: 2,
                                  color: kprimary,
                                ),
                                borderRadius: BorderRadius.circular(20),
                              ),
                            ),
                          ),
                          GestureDetector(
                            onTap: () => {
                              check(),
                              if (controller.image.value != '' && check())
                                {
                                  Timer(const Duration(milliseconds: 0),
                                      () async {
                                    sendNoti(
                                        'Orphans app',
                                        "a new student need your guarantee donate",
                                        DateTime.now().toString());
                                    notificationEnsure(context);
                                    String save = await controller
                                        .addGuarantee(GuaranteeModel(
                                      guaranteeEditors[0].text,
                                      guaranteeEditors[1].text,
                                      currentDate.value.toString(),
                                      controller.academicYear.value,
                                      controller.image.value,
                                    ));
                                    if (save == 'OK') {
                                      controller.isTrue.value = true;
                                      Get.to(
                                        FunctionStatus(
                                          controller: controller,
                                          title: "Guarantee",
                                          function: "added",
                                        ),
                                      );
                                      controller.isGet.value = false;
                                      controller.image.value = '';
                                    } else {
                                      print('error');
                                      controller.isFalse.value = true;
                                      Get.to(FunctionStatus(
                                        controller: controller,
                                        title: "Guarantee",
                                        function: "added",
                                      ));
                                    }
                                  })
                                }
                              else
                                {
                                  print("enter the images"),
                                }
                            },
                            child: Container(
                              alignment: Alignment.center,
                              margin: const EdgeInsets.all(15),
                              width: size.width / 4,
                              height: 50,
                              decoration: BoxDecoration(
                                  color: Colors.white.withOpacity(.4),
                                  borderRadius: BorderRadius.circular(10)),
                              child: Text(
                                'Submit',
                                style: TextStyle(
                                    color: kprimary,
                                    fontSize: 16,
                                    fontWeight: FontWeight.bold),
                                textAlign: TextAlign.center,
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Container CameraStudio(IconData icon, Size size) {
    return Container(
      margin: const EdgeInsets.symmetric(horizontal: 5, vertical: 10),
      child: Icon(
        icon,
        size: 30,
        color: kprimary,
      ),
      width: size.width * .22,
      height: 120,
      decoration: BoxDecoration(
        color: Colors.white.withOpacity(.2),
        border: Border.all(
          width: 2,
          color: kprimary,
        ),
        borderRadius: BorderRadius.circular(20),
      ),
    );
  }

  Widget getInput(int index, Size size, formKey) {
    return Container(
      alignment: Alignment.center,
      width: size.width * .8,
      height: 78,
      margin: const EdgeInsets.symmetric(horizontal: 20, vertical: 15),
      padding: const EdgeInsets.symmetric(horizontal: 10),
      decoration: BoxDecoration(
          color: Colors.white.withOpacity(.2),
          borderRadius: BorderRadius.circular(10)),
      child: TextFormField(
        //key: formKey,
        controller: guaranteeEditors[index],
        decoration: InputDecoration(
          labelText: guaranteeAttributeName[index],
          border: InputBorder.none,
          icon: guaranteeIcons[index],
          iconColor: Colors.red,
          labelStyle: TextStyle(
              fontSize: 16, fontWeight: FontWeight.w600, color: kprimary),
        ),
        maxLines: index == 3 ? 4 : 1,
        keyboardType: guaranteeTypes[index],
        validator: (value) {
          if (index == 0 && value != null && value.length < 7) {
            return guaranteeErrorText[0];
          } else if ((index == 1 && value != null && int.parse(value) <= 100)) {
            return guaranteeErrorText[1];
          }
        },
      ),
    );
  }

  bool check() {
    bool isVlidate = true;
    for (int i = 0; i <= 1; i++) {
      if (isVlidate) {
        isVlidate = guaranteeFormKey[i].currentState!.validate();
      }
      if (!isVlidate) {
        guaranteeFormKey[i].currentState!.validate();
      }
    }
    return isVlidate;
  }

  Future<void> openCylinder() async {
    final DateTime? pickedDate = await showDatePicker(
        context: context,
        initialDate: currentDate.value,
        firstDate: currentDate.value,
        lastDate: DateTime(2050));
    if (pickedDate != null && pickedDate != currentDate.value) {
      currentDate.value = pickedDate;
    }
  }
}

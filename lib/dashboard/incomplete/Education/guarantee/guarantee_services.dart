import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:http/http.dart' as http;

import '../../../../config/config.dart';
import '../../../guarantee_model.dart';
// <<<<<<< HEAD
class GuaranteeServices {
  var url = Uri.parse(ServerConfig.domainNameServer + ServerConfig.addGuarantee);
// >>>>>>> 2c6a0d2 (new file)

  Future<String> addGuarantee(GuaranteeModel guarantee) async {

    FlutterSecureStorage storage = const FlutterSecureStorage();
    String? token = await storage.read(key: 'token');
    print(token);

    print(guarantee.name);
    print(guarantee.cost);
    print(guarantee.birthDate);
    print(guarantee.academicYear);
    print(guarantee.image);


    var response = await http.post(url, headers: {
      'Authorization': 'Bearer $token',
      'Accept': 'application/json'
    }, body: {
      'name': guarantee.name,
      'cost':guarantee.cost,
      'birth_date': guarantee.birthDate,
      'academic_year': guarantee.academicYear,
      'image': guarantee.image,
    });

    String getResponse = response.body;
    print(getResponse);
    if(getResponse.contains('{"message":"A student added successfully"}')){
      return 'OK';
    }else{
      return 'NO';
    }

  }

}

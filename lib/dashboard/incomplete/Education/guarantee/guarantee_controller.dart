import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:get/get.dart';
import 'package:image_picker/image_picker.dart';
import '../../../guarantee_model.dart';
import 'guarantee_consts.dart';
import 'guarantee_services.dart';

class GuaranteeController extends GetxController{

  RxString image = ''.obs;
  RxBool isGet = false.obs;
  RxString? langStr;
  RxBool isTrue = false.obs;
  RxBool isFalse = false.obs;
  RxString size = 'small'.obs;
  RxInt currentItem = 0.obs;
  RxBool isExpand = false.obs;
  RxString academicYear = 'first'.obs;
  GuaranteeServices service = GuaranteeServices();

  FlutterSecureStorage storage = const FlutterSecureStorage();

  Future<void> takeImage(ImageSource source) async {
    final picker = ImagePicker();
    final getImage = await picker.pickImage(source: source);
    if (getImage != null) {
      image.value = getImage.path;
      isGet.value = true;
    }
  }

  void changeItem(int index){

    currentItem.value = index;
    academicYear.value = academicYears[index];

  }

  Future<String> addGuarantee(GuaranteeModel model) async {

    String save;
    save = await service.addGuarantee(model);
    return save;

  }

}
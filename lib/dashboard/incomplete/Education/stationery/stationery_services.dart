import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:http/http.dart' as http;

import '../../../../config/config.dart';
import '../../../stationery_model.dart';
// <<<<<<< HEAD
class StationeryServices {
  var url = Uri.parse(ServerConfig.domainNameServer + ServerConfig.addStationery);
  Future<String> addStationery(StationeryModel stationery) async {

    FlutterSecureStorage storage = const FlutterSecureStorage();
    await storage.write(key: 'token',value: 'Bearer 2|0DKTqepKeX4ZqKRYIhtFsoyExXKj4SnswfozryES');
    String? token = await storage.read(key: 'token');
    print(token);

    print(stationery.name);
    print(stationery.size);
    print(stationery.cost);
    print(stationery.image);


    var response = await http.post(url, headers: {
      'Authorization': 'Bearer $token',
      'Accept': 'application/json'
    }, body: {
      'name': stationery.name,
      'count':stationery.count,
      'cost': stationery.cost,
      'size': stationery.size,
      'image': stationery.image,
      'content':stationery.content
    });

    String getResponse = response.body;
    print(getResponse);
    if(getResponse.contains('{"message":"Stationery added successfully"}')){
      return 'OK';
    }else{
      return 'NO';
    }

  }

}

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import '../../../../configrations/consts.dart';

List<GlobalKey<FormState>> stationeryFormKey = [
  GlobalKey<FormState>(),
  GlobalKey<FormState>(),
  GlobalKey<FormState>(),
  GlobalKey<FormState>(),
];

List<String> stationeryAttributeName = [
  "name",
  "cost",
  "count",
  "content"
];

List<Icon> stationeryIcons = [
  Icon(
    Icons.person,
    color: mainColor,
  ),
  Icon(
    Icons.paypal,
    color: mainColor,
  ),
  Icon(
    Icons.numbers,
    color: mainColor,
  ),
  Icon(
    Icons.comment,
    color: mainColor,
  ),
];

List<String> stationeryErrorText = [
  "your Name is less than 7",
  "your cost is less than 1000",
  "your size must be medium or big or small",
  "your content must be more than 7",
];

List<TextInputType> stationeryTypes = [
  TextInputType.name,
  TextInputType.number,
  TextInputType.number,
  TextInputType.name,
];

List<TextEditingController> stationeryEditors = [
  TextEditingController(),
  TextEditingController(),
  TextEditingController(),
  TextEditingController(),
];

List<String> stationerySizes = [
  'Small',
  'Medium',
  'Big'
];
class InCompleteStationery {
  late String name;
  late int cost;
  late int count;
  late String size;
  late String image;
  late String content;
  late int donate;
  late int id;

  InCompleteStationery(
    this.name,
    this.cost,
    this.count,
    this.size,
    this.image,
    this.content,
    this.donate,
    this.id,
  );
}

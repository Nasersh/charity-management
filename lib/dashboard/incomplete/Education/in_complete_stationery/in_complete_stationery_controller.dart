import 'package:get/get.dart';

import '../stationery/stationery_consts.dart';
import 'incomplete_stationery_model.dart';
import 'incomplete_stationery_services.dart';

class InCompleteStationeryController extends GetxController
{
  var stationersList =<InCompleteStationery>[].obs;
  var stationers=<InCompleteStationery>[];
  RxBool isTrue = false.obs;
  RxString image = ''.obs;
  RxInt currentItem = 0.obs;
  RxString size = 'small'.obs;

  var id=0.obs;

  InCompleteStationeryServices service = InCompleteStationeryServices();
  var isloading=true.obs;

  @override
  void onInit() {
    showStationers();
    //loadProductsFromRepo(id.value);
    super.onInit();
  }

  void showStationers() async {
    try {
      isloading(true);
      stationers = await service.getStationers();
      stationers.forEach((element) {print(element.donate);});
      if (stationers != null) {
        stationersList.value = stationers;
      }
    } finally {
      isloading(false);
    }
  }

  Future<String> deleteStationery(int id) async {

    String save;

    try {
      isloading(true);
      save = await service.deleteStationery(id);
    }finally {
      isloading(false);
    }
    return save;
  }

  void changeItem(int index){
    print(index);

    currentItem.value = index;
    size.value = stationerySizes[index];

  }

}
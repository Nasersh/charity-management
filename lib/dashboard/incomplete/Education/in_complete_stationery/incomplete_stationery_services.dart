import 'dart:convert';

import 'package:charitysys/dashboard/stationery_model.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:http/http.dart' as http;
// <<<<<<< HEAD
// =======
// >>>>>>> 2c6a0d2
import '../../../../config/config.dart';
import 'incomplete_stationery_model.dart';


class InCompleteStationeryServices {
  var message;
  var url = Uri.parse(
// <<<<<<< HEAD
      ServerConfig.domainNameServer + ServerConfig.inCompleteStationery);
// =======
//       ServerConfig.domainNameServer + ServerConfig.inCompleteStationery);
// >>>>>>> 2c6a0d2 (new file)
  FlutterSecureStorage storage = const FlutterSecureStorage();

  Future<List<InCompleteStationery>> getStationers() async {

    String? token = await storage.read(key: 'token');
    print(token);
    var response = await http.get(
      url,
      headers: {'Accept': 'application/json', 'Authorization': 'Bearer $token'},
    );

    print(response.body);
    if (response.statusCode == 200) {
      List<InCompleteStationery> inComMedicines = combineStationery(json.decode(response.body));
      print(inComMedicines);
      return inComMedicines;
    } else {
      return [];
    }
  }

  List<InCompleteStationery> combineStationery(var response) {
    List<InCompleteStationery> inComMedicines = [];

    for (int i = 0; i < response['stationery'].length; i++) {

      inComMedicines.add(InCompleteStationery(
        response['stationery'][i]['name'],
        response['stationery'][i]['cost'],
        response['stationery'][i]['count'],
        response['stationery'][i]['size'],
        response['stationery'][i]['image'],
        response['stationery'][i]['content'],
        response['stationery'][i]['donate'],
        response['stationery'][i]['id'],

      ));

    }
    return inComMedicines;
  }

  Future<String> updateStationery(StationeryModel stationery, int id) async {
    String? token = await storage.read(key: 'token');
    print(token);

    var url = Uri.parse(
// <<<<<<< HEAD
        ServerConfig.domainNameServer + ServerConfig.updateStationery + '$id');
// =======
//         ServerConfig.domainNameServer + ServerConfig.updateStationery + '$id');
// >>>>>>> 2c6a0d2 (new file)

    var response = await http.post(url, headers: {
      'Authorization': 'Bearer $token ',
      'Accept': 'application/json'
    }, body: {
      'name': stationery.name,
      'cost': stationery.cost,
      'count': stationery.count,
      'image': stationery.image,
      'content':stationery.content,
      'size':stationery.size
    });

    String getResponse = response.body;
    print(getResponse);
    if (getResponse.contains('{"message":"updated successfully"')) {
      return 'OK';
    } else {
      return 'NO';
    }
  }

  Future<String> deleteStationery(int id) async {
    String? token = await storage.read(key: 'token');
    print(token);

    var url = Uri.parse(
// <<<<<<< HEAD
        ServerConfig.domainNameServer + ServerConfig.deleteStationery + '$id');
// =======
//         ServerConfig.domainNameServer + ServerConfig.deleteStationery + '$id');
// >>>>>>> 2c6a0d2 (new file)

    var response = await http.delete(url, headers: {
      'Authorization': 'Bearer $token ',
      'Accept': 'application/json'
    });

    String getResponse = response.body;
    print(getResponse);
    if (getResponse.contains('{"message":"stationery record get deleted "')) {
      return 'OK';
    } else {
      return 'NO';
    }
  }
}

import 'dart:async';
import 'dart:io';
import 'dart:math';
import 'package:charitysys/dashboard/stationery_model.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:get/get.dart';
import 'package:get/get_core/src/get_main.dart';
import 'package:image_picker/image_picker.dart';
import 'package:percent_indicator/linear_percent_indicator.dart';
import '../../../../configrations/functions_status/functions_status.dart';
import '../../../../configrations/the_helper_widget/the_helper_widget.dart';
import '../stationery/stationery_consts.dart';
import 'consts.dart';
import 'in_complete_stationery_controller.dart';
import 'incomplete_stationery_services.dart';

class InCompleteStationery extends StatelessWidget {
  InCompleteStationery({Key? key}) : super(key: key);

  InCompleteStationeryController controller =
      Get.put(InCompleteStationeryController());
  Rx<DateTime> currentDate = DateTime.now().obs;

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;

    return Obx(
      () => controller.isloading.value
          ? const SafeArea(
              child: Scaffold(
                  backgroundColor: kprimary,
                  body: Center(child: CircularProgressIndicator())))
          : Stack(
              children: [
                Scaffold(
                  backgroundColor: kprimary,
                  body: SafeArea(
                    child: Obx(
                      () => Column(
                        children: [
                          Stack(children: [
                            Container(
                              width: double.infinity,
                              height: MediaQuery.of(context).size.height / 4,
                              decoration: const BoxDecoration(
                                  color: kblue,
                                  borderRadius: BorderRadius.only(
                                      bottomLeft: Radius.circular(30),
                                      bottomRight: Radius.circular(30))),
                            ),
                            Column(
                              children: [
                                Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    Padding(
                                      padding: const EdgeInsets.only(left: 15),
                                      child: IconButton(
                                        icon: const Icon(
                                          FontAwesomeIcons.arrowLeft,
                                          color: kprimary,
                                        ),
                                        // Icons.arrow_back,
                                        onPressed: () {
                                          Get.back();
                                        },
                                      ),
                                    ),
                                  ],
                                ),
                                SizedBox(
                                  height:
                                      MediaQuery.of(context).size.height / 15,
                                ),
                                const Padding(
                                  padding:
                                      EdgeInsets.only(bottom: 10, left: 40),
                                  child: Align(
                                    alignment: Alignment.bottomLeft,
                                    child: Text(
                                      'Stationers . . .',
                                      style: TextStyle(
                                          color: kprimary,
                                          fontSize: 30,
                                          fontStyle: FontStyle.italic,
                                          fontWeight: FontWeight.bold),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ]),
                          Expanded(child: _buildProductsList(size, context)),
                        ],
                      ),
                    ),
                  ),
                ),
              ],
            ),
    );
  }

  Widget _buildProductsList(Size size, BuildContext context) {
    Random rnd;
    double min = 0;
    rnd = Random();
    double r = min + rnd.nextDouble();
    return ListView.builder(
      shrinkWrap: true,
      itemCount: controller.stationersList.length,
      padding: const EdgeInsets.only(top: 16),
      itemBuilder: (context, index) => Padding(
        padding: const EdgeInsets.symmetric(vertical: 10),
        child: Container(
          height: MediaQuery.of(context).size.height / 6,
          decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(15),
              boxShadow: [kDefaultShadow]),
          child: Padding(
            padding: const EdgeInsets.symmetric(vertical: 8.0),
            child: SingleChildScrollView(
              child: ExpansionTile(
                  children: [
                    Stack(
                      children: [
                        Align(
                          alignment: const Alignment(0, 0),
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Padding(
                                padding: const EdgeInsets.all(12.0),
                                child: SizedBox(
                                  width: size.width / 2 + size.width / 20,
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Text(
                                        controller
                                            .stationersList[index].content,
                                        style: const TextStyle(color: kstorm),
                                      ),
                                      Text(
                                        '${controller.stationersList[index].size}/count : ${controller.stationersList[index].count}',
                                        style: const TextStyle(color: kstorm),
                                      ),
                                      const SizedBox(
                                        height: 10,
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                        Align(
                          alignment: const Alignment(-0.9, 0),
                          child: deleteButtonEffect(index, context, size),
                        ),
                      ],
                    ),
                  ],
                  leading: const CircleAvatar(
                    radius: 30,
                    backgroundColor: Colors.black45,
                  ),
                  title: Padding(
                    padding: const EdgeInsets.only(top: 10),
                    child: Text(
                      controller.stationersList[index].name,
                      style: const TextStyle(
                        color: Colors.black,
                      ),
                    ),
                  ),
                  subtitle: SingleChildScrollView(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Padding(
                          padding: const EdgeInsets.only(top: 5, bottom: 10),
                          child: Text(
                              '${controller.stationersList[index].cost}',
                              style: const TextStyle(color: Colors.black)),
                        ),
                        LinearPercentIndicator(
                          barRadius: const Radius.circular(040),
                          width: MediaQuery.of(context).size.width / 2,
                          animation: true,
                          animationDuration: 800,
                          lineHeight: 10,
                          percent: r,
                          progressColor: kblue,
                          backgroundColor: kbblue,
                        ),
                        SizedBox(
                          height: MediaQuery.of(context).size.height / 100,
                        ),
                        Text(
                          '${controller.stationersList[index].donate}\$/${controller.stationersList[index].cost}',
                          style: const TextStyle(color: kstorm),
                        ),
                      ],
                    ),
                  ),
                  trailing: updatButtonEffect(index, context, size)),
            ),
          ),
        ),
      ),
    );
  }

  Container updatButtonEffect(int index, BuildContext context, Size size) {
    return Container(
      height: 50,
      width: 75,
      decoration: BoxDecoration(
        color: kstorm,
        borderRadius: BorderRadius.circular(10),
      ),
      child: MaterialButton(
        onPressed: () {
          var id = controller.stationersList[index].id;
          showModalBottomSheet(
            context: context,
            builder: (BuildContext context) {
              return Container(
                  height: MediaQuery.of(context).size.height / 2,
                  decoration: const BoxDecoration(
                    color: kprimary,
                    borderRadius: BorderRadius.only(
                      topRight: Radius.circular(25),
                      topLeft: Radius.circular(25),
                    ),
                  ),
                  child: SingleChildScrollView(
                    child: Column(
                      children: [
                        Container(
                          height: MediaQuery.of(context).size.height / 5,
                          width: MediaQuery.of(context).size.width,
                          //color: Colors.purple,
                          alignment: const Alignment(-0.6, 0.4),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                            children: [
                              Stack(
                                clipBehavior: Clip.none,
                                children: [
                                  Obx(
                                    () => CircleAvatar(
                                      radius: 40,
                                      backgroundColor: Colors.black45,
                                      backgroundImage: image.value != ''
                                          ? FileImage(
                                              File(image.value),
                                            )
                                          : null,
                                    ),
                                  ),
                                  Positioned(
                                    top: 45,
                                    right: 0,
                                    child: GestureDetector(
                                      onTap: () => {
                                        Timer(const Duration(seconds: 1),
                                            () async {
                                          await takeImage(ImageSource.gallery);
                                        })
                                      },
                                      child: CircleAvatar(
                                        backgroundColor:
                                            Colors.black54.withOpacity(.5),
                                        child: const Icon(
                                          Icons.camera_alt,
                                          size: 30,
                                          color: Colors.white,
                                        ),
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                              //const Spacer(),
                              //const Spacer(),
                            ],
                          ),
                        ),
                        Form(
                          key: stationeryFormKey[0],
                          autovalidateMode: AutovalidateMode.onUserInteraction,
                          child:
                              getStationersInput(0, size, stationeryFormKey[0]),
                        ),
                        Form(
                          key: stationeryFormKey[1],
                          autovalidateMode: AutovalidateMode.onUserInteraction,
                          child:
                              getStationersInput(1, size, stationeryFormKey[1]),
                        ),
                        Form(
                          key: stationeryFormKey[2],
                          autovalidateMode: AutovalidateMode.onUserInteraction,
                          child:
                              getStationersInput(2, size, stationeryFormKey[2]),
                        ),
                        Form(
                          key: stationeryFormKey[3],
                          autovalidateMode: AutovalidateMode.onUserInteraction,
                          child:
                              getStationersInput(3, size, stationeryFormKey[3]),
                        ),
                        BuildStationerySizes(
                            size: size, controller: controller),
                        const SizedBox(height: 20),
                        GestureDetector(
                          onTap: () => {
                            if (checking())
                              {
                                Timer(const Duration(seconds: 0), () async {
                                  String save =
                                      await InCompleteStationeryServices()
                                          .updateStationery(
                                    StationeryModel(
                                        stationeryEditors[0].text != ''
                                            ? stationeryEditors[0].text
                                            : controller
                                                .stationersList[index].name,
                                        stationeryEditors[1].text != ''
                                            ? stationeryEditors[1].text
                                            : controller
                                                .stationersList[index].cost
                                                .toString(),
                                        stationeryEditors[2].text != ''
                                            ? stationeryEditors[2].text
                                            : controller
                                                .stationersList[index].count
                                                .toString(),
                                        controller.size.value != 'small'
                                            ? controller.size.value
                                            : controller
                                                .stationersList[index].size,
                                        image.value != ''
                                            ? image.value
                                            : controller
                                                .stationersList[index].image,
                                        stationeryEditors[3].text != ''
                                            ? stationeryEditors[3].text
                                            : controller
                                                .stationersList[index].content),
                                    id,
                                  );
                                  Get.back();

                                  if (save == 'OK') {
                                    Timer(const Duration(seconds: 1), () {
                                      Get.to(FunctionStatus(
                                        controller: controller,
                                        title: "Stationery",
                                        function: "updated",
                                      ));
                                      controller.isTrue.value = true;
                                      controller.showStationers();
                                    });
                                  } else {
                                    controller.isTrue.value = false;
                                    Get.to(FunctionStatus(
                                      controller: controller,
                                      title: "Stationery",
                                      function: "updated",
                                    ));
                                  }
                                }),
                              }
                          },
                          child: Container(
                            width: MediaQuery.of(context).size.width / 3,
                            height: MediaQuery.of(context).size.width / 9,
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(10),
                              color: kblue,
                            ),
                            alignment: Alignment.center,
                            child: const Text(
                              "Update",
                              style: TextStyle(
                                  color: Colors.white,
                                  fontWeight: FontWeight.bold,
                                  fontSize: 19),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ));
            },
          );
        },
        child: const Center(
          child: Text(
            'Update',
            style: TextStyle(color: kprimary, fontSize: 12.2),
          ),
        ),
      ),
    );
  }

  Container deleteButtonEffect(int index, BuildContext context, Size size) {
    return Container(
      height: 50,
      width: 75,
      decoration: BoxDecoration(
        color: kstorm,
        borderRadius: BorderRadius.circular(10),
      ),
      child: MaterialButton(
        onPressed: () {
          var id = controller.stationersList[index].id;
          showModalBottomSheet(
            constraints: const BoxConstraints(maxHeight: 200),
            context: context,
            builder: (BuildContext context) {
              return Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  const Text(
                    "Would yo like to delete this element ? ",
                    style: TextStyle(
                        color: Colors.black54, fontWeight: FontWeight.bold),
                    //textAlign: TextAlign.center,
                  ),
                  const SizedBox(
                    height: 15,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      GestureDetector(
                        onTap: () => {
                          Timer(const Duration(seconds: 0), () async {
                            String save = await controller.deleteStationery(id);
                            if (save == 'OK') {
                              controller.isTrue.value = true;
                              Get.back();
                              Get.to(FunctionStatus(
                                controller: controller,
                                title: "Stationery",
                                function: "deleted",
                              ));
                              controller.showStationers();
                            } else {
                              print('error');
                              controller.isTrue.value = false;
                              Get.to(FunctionStatus(
                                controller: controller,
                                title: "Stationery",
                                function: "deleted",
                              ));
                            }
                          })
                        },
                        child: Container(
                          alignment: Alignment.center,
                          width: 80,
                          height: 50,
                          decoration: BoxDecoration(
                            color: kstorm,
                            borderRadius: BorderRadius.circular(10),
                          ),
                          child: const Text(
                            "Yes",
                            style: TextStyle(
                                color: Colors.white,
                                fontWeight: FontWeight.bold,
                                fontSize: 22),
                          ),
                        ),
                      ),
                      const SizedBox(
                        width: 15,
                      ),
                      GestureDetector(
                        onTap: () => Get.back(),
                        child: Container(
                          width: 80,
                          height: 50,
                          alignment: Alignment.center,
                          decoration: BoxDecoration(
                            color: kprimary,
                            borderRadius: BorderRadius.circular(10),
                          ),
                          child: const Text(
                            "No",
                            style: TextStyle(
                                color: Colors.white,
                                fontWeight: FontWeight.bold,
                                fontSize: 22),
                          ),
                        ),
                      ),
                    ],
                  ),
                ],
              );
            },
          );
        },
        child: const Center(
          child: Text(
            'Delete',
            style: TextStyle(color: kprimary, fontSize: 12.2),
          ),
        ),
      ),
    );
  }

  Future<void> openCylinder(BuildContext context) async {
    final DateTime? pickedDate = await showDatePicker(
        context: context,
        initialDate: currentDate.value,
        firstDate: currentDate.value,
        lastDate: DateTime(2050));
    if (pickedDate != null && pickedDate != currentDate) {
      currentDate.value = pickedDate;
    }
  }

  Container CameraStudio(IconData icon, Size size) {
    return Container(
      margin: const EdgeInsets.symmetric(horizontal: 5, vertical: 10),
      child: Icon(
        icon,
        size: 30,
        color: kprimary,
      ),
      width: size.width * .3,
      height: 120,
      decoration: BoxDecoration(
        color: Colors.white.withOpacity(.2),
        border: Border.all(
          width: 2,
          color: kprimary,
        ),
        borderRadius: BorderRadius.circular(20),
      ),
    );
  }
}

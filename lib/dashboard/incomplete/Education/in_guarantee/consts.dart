import 'dart:ui';

import 'package:flutter/material.dart';

const kprimary = Color(0xfff6f7f9);
const kbrowm = Color(0xFFb5c2c7);
const kblue = Color(0xff093d65);
const kbblue = Color(0xffc5c6d0);
const kstorm = Color(0xFFF98D53);
const knude = Color(0xFFae9789);
const kDefaultPadding = 20.0;


var kongreen =Colors.white.withOpacity(.7);


const kDefaultShadow = BoxShadow(
  offset: Offset(0, 10),
  blurRadius: 5,
  color: Colors.black12, // Black color with 12% opacity
);


const TextStyle kHeading = TextStyle(
  fontSize: 55,
  fontWeight: FontWeight.bold,
  color: Colors.white,
);

const TextStyle kBodyText = TextStyle(
  fontSize: 22,
  color: Colors.white,
);


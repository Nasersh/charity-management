class InCompleteGuarantee {
  late String name;
  late int cost;
  late String birthDate;
  late String academicYear;
  late String image;
  late int id;

  InCompleteGuarantee(
      this.name,
      this.cost,
      this.birthDate,
      this.academicYear,
      this.image,
      this.id,
      );
}

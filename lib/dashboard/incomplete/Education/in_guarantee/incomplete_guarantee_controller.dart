import 'package:get/get.dart';
import '../../../guarantee_model.dart';
import '../guarantee/guarantee_consts.dart';
import 'incomplete_guarantee_model.dart';
import 'incomplete_guarantee_services.dart';

  class InCompleteGuaranteeController extends GetxController
{
  var guaranteeList =<InCompleteGuarantee>[].obs;
  var guarantees=<InCompleteGuarantee>[];
  RxBool isTrue = false.obs;
  RxString image = ''.obs;
  RxInt currentItem = 0.obs;
  RxInt currentSortItem = 0.obs;
  RxBool isExpand = false.obs;
  RxString academicYear = 'first'.obs;
  RxString sortSelected = 'id'.obs;
  RxString searchValue = ''.obs;

  var id=0.obs;

  InCompleteGuaranteeServices service = InCompleteGuaranteeServices();
  var isloading=true.obs;

  @override
  void onInit() {
    showStationers();
    //loadProductsFromRepo(id.value);
    super.onInit();
  }

  void showStationers() async {
    try {
      isloading(true);
      guarantees = await service.getGuarantees();
      if (guarantees != null) {
        guaranteeList.value = guarantees;
      }
    } finally {
      isloading(false);
    }
  }

  Future<String> deleteGuarantee(int id) async {

    String save;

    try {
      isloading(true);
      save = await service.deleteGuarantee(id);
    }finally {
      isloading(false);
    }
    return save;
  }

  void changeItem(int index){
    print(index);

    currentItem.value = index;
    academicYear.value = academicYears[index];

  }


  void sortItem(int index) async {
    print(index);

    currentSortItem.value = index;
    sortSelected.value = sortTypes[index];

    try{
      isloading(true);
      guarantees = await service.sortGuarantee(sortSelected.value);
      if (guarantees != null) {
        guaranteeList.value = guarantees;
      }
    } finally {
      isloading(false);
    }


  }

  Future<String> updateGuarantee(GuaranteeModel model,int id) async {

    String save;
    save = await service.updateGuarantee(model,id);
    return save;

  }

  Future<void> searchGuarantee(String name) async {

    try{
      isloading(true);
      guarantees = await service.searchGuarantee(name);
      if (guarantees != null) {
        guaranteeList.value = guarantees;
      }
    } finally {
    isloading(false);
    }
  }

}
import 'dart:convert';

import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:http/http.dart' as http;
// <<<<<<< HEAD
// =======
// >>>>>>> 2c6a0d2 (new file)
import '../../../../config/config.dart';
import '../../../guarantee_model.dart';
import 'incomplete_guarantee_model.dart';

class InCompleteGuaranteeServices {
  var message;
  var url = Uri.parse(
// <<<<<<< HEAD
      ServerConfig.domainNameServer + ServerConfig.inCompleteGuarantees);
// =======
//       ServerConfig.domainNameServer + ServerConfig.inCompleteGuarantees);
// >>>>>>> 2c6a0d2 (new file)
  FlutterSecureStorage storage = const FlutterSecureStorage();

  Future<List<InCompleteGuarantee>> getGuarantees() async {
    String? token = await storage.read(key: 'token');
    print(token);
    var response = await http.get(
      url,
      headers: {'Accept': 'application/json', 'Authorization': 'Bearer $token'},
    );

    print(response.body);
    if (response.statusCode == 200) {
      List<InCompleteGuarantee> inComGuarantee =
          combineGuarantee(json.decode(response.body));
      print(inComGuarantee);
      return inComGuarantee;
    } else {
      return [];
    }
  }

  List<InCompleteGuarantee> combineGuarantee(var response) {
    List<InCompleteGuarantee> inComGuarantees = [];

    for (int i = 0; i < response.length; i++) {
      inComGuarantees.add(InCompleteGuarantee(
        response[i]['name'],
        response[i]['cost'],
        response[i]['birth_date'],
        response[i]['academic_year'],
        response[i]['image'],
        response[i]['id'],
      ));
    }
    return inComGuarantees;
  }

  Future<String> updateGuarantee(GuaranteeModel guarantee, int id) async {

    String? token = await storage.read(key: 'token');
    print(token);

    var url = Uri.parse(
// <<<<<<< HEAD
        ServerConfig.domainNameServer + ServerConfig.updateGuarantee + '$id');
// =======
//         ServerConfig.domainNameServer + ServerConfig.updateGuarantee + '$id');
// >>>>>>> 2c6a0d2 (new file)

    var response = await http.post(url, headers: {
      'Authorization': 'Bearer $token ',
      'Accept': 'application/json'
    }, body: {
      'name': guarantee.name,
      'cost': guarantee.cost,
      'birth_date': guarantee.birthDate,
      'academic_year': guarantee.academicYear,
      'image': guarantee.image,
    });

    String getResponse = response.body;
    print(getResponse);
    if (getResponse.contains('{"message":"student update successfully"')) {
      return 'OK';
    } else {
      return 'NO';
    }
  }

  Future<String> deleteGuarantee(int id) async {

    String? token = await storage.read(key: 'token');
    print(token);

    var url = Uri.parse(
// <<<<<<< HEAD
        ServerConfig.domainNameServer + ServerConfig.deleteGuarantee + '$id');
// =======
//         ServerConfig.domainNameServer + ServerConfig.deleteGuarantee + '$id');
// >>>>>>> 2c6a0d2 (new file)

    var response = await http.delete(url, headers: {
      'Authorization': 'Bearer $token ',
      'Accept': 'application/json'
    });

    String getResponse = response.body;
    print(getResponse);
    if (getResponse.contains('{"message":"student deleted successfully"')) {
      return 'OK';
    } else {
      return 'NO';
    }
  }

  Future<List<InCompleteGuarantee>> searchGuarantee(String searchValue) async {
    String? token = await storage.read(key: 'token');
    print(token);
    print(searchValue);
    var url = Uri.parse(
// <<<<<<< HEAD
        ServerConfig.domainNameServer + ServerConfig.searchGuarantee);
// =======
//         ServerConfig.domainNameServer + ServerConfig.searchGuarantee);
// >>>>>>> 2c6a0d2 (new file)

    var response = await http.post(url, headers: {
      'Authorization': 'Bearer $token ',
      'Accept': 'application/json'
    }, body: {
      'name': searchValue,
      'birth_date': searchValue,
      'academic_year': searchValue,
    });

    String getResponse = response.body;

    print(getResponse);
    if (response.statusCode == 200) {
      List<InCompleteGuarantee> searchGuarantees =
          combineSearchGuarantee(json.decode(response.body));
      print(searchGuarantees);
      return searchGuarantees;
    } else {
      return [];
    }
  }

  List<InCompleteGuarantee> combineSearchGuarantee(var response) {
    List<InCompleteGuarantee> inComGuarantees = [];

    //print('this is res ${response.toString()}');

    if (response['Students'].toString() != '[]') {
      for (int i = 0; i < response.length; i++) {
        inComGuarantees.add(InCompleteGuarantee(
          response['Students'][i]['name'],
          response['Students'][i]['cost'],
          response['Students'][i]['birth_date'],
          response['Students'][i]['academic_year'],
          response['Students'][i]['image'],
          response['Students'][i]['id'],
        ));
      }
    }
    return inComGuarantees;
  }

  List<InCompleteGuarantee> combineSortGuarantee(var response, bool isBirth) {
    List<InCompleteGuarantee> inComGuarantees = [];

    //print('this is res ${response.toString()}');

    if(isBirth == true){
      for (int i = 0; i < response.length; i++) {
        inComGuarantees.add(InCompleteGuarantee(
          response[i]['name'],
          response[i]['cost'],
          response[i]['birth_date'],
          response[i]['academic_year'],
          response[i]['image'],
          response[i]['id'],
        ));
      }
    }else{
      print('pleaseeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee${response['0']['name']}');
      for (int i = 0; i < response.length; i++) {
        inComGuarantees.add(InCompleteGuarantee(
          response['$i']['name'],
          response['$i']['cost'],
          response['$i']['birth_date'],
          response['$i']['academic_year'],
          response['$i']['image'],
          response['$i']['id'],
        ));
      }
    }
    return inComGuarantees;
  }

  Future<List<InCompleteGuarantee>> sortGuarantee(String sortValue) async {

    String? token = await storage.read(key: 'token');
    print(token);
    print(sortValue);

    var url = Uri.parse(ServerConfig.domainNameServer +
        ServerConfig.sortGuarantee +
// >>>>>>> 2c6a0d2 (new file)
        sortValue);

    var response = await http.get(url, headers: {
      'Authorization': 'Bearer $token ',
      'Accept': 'application/json'
    });

    String getResponse = response.body;

    print(getResponse);
    if (response.statusCode == 200) {
      List<InCompleteGuarantee> sortGuarantees = combineSortGuarantee(
        json.decode(response.body),
          sortValue == "birth_date" ? true : false
      );
      print(sortGuarantees);
      return sortGuarantees;
    } else {
      return [];
    }
  }
}

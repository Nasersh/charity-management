import 'dart:async';
import 'dart:io';
import 'dart:math';
import 'package:charitysys/dashboard/guarantee_model.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:get/get.dart';
import 'package:get/get_core/src/get_main.dart';
import 'package:image_picker/image_picker.dart';
import 'package:percent_indicator/linear_percent_indicator.dart';
import '../../../../configrations/functions_status/functions_status.dart';
import '../../../../configrations/the_helper_widget/the_helper_widget.dart';
import '../guarantee/guarantee_consts.dart';
import 'consts.dart';
import 'incomplete_guarantee_controller.dart';

class InCompleteGuarantee extends StatelessWidget {
  InCompleteGuarantee({Key? key}) : super(key: key);

  InCompleteGuaranteeController controller =
      Get.put(InCompleteGuaranteeController());
  Rx<DateTime> currentDate = DateTime.now().obs;

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;

    return Obx(
      () => controller.isloading.value
          ? const SafeArea(
              child: Scaffold(body: Center(child: CircularProgressIndicator())))
          : Scaffold(
              backgroundColor: kprimary,
              body: SafeArea(
                child: Obx(
                  () => Column(
                    children: [
                      Stack(children: [
                        Container(
                          width: double.infinity,
                          height: MediaQuery.of(context).size.height / 4,
                          decoration: const BoxDecoration(
                              color: kblue,
                              borderRadius: BorderRadius.only(
                                  bottomLeft: Radius.circular(30),
                                  bottomRight: Radius.circular(30))),
                        ),
                        Column(
                          children: [
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Padding(
                                  padding: const EdgeInsets.only(left: 15),
                                  child: IconButton(
                                    icon: const Icon(
                                      FontAwesomeIcons.arrowLeft,
                                      color: kprimary,
                                    ),
                                    // Icons.arrow_back,
                                    onPressed: () {
                                      Get.back();
                                    },
                                  ),
                                ),
                              ],
                            ),
                            SizedBox(
                              height: MediaQuery.of(context).size.height / 90,
                            ),
                            const Padding(
                              padding: EdgeInsets.only(bottom: 10, left: 40),
                              child: Align(
                                alignment: Alignment.bottomLeft,
                                child: Text(
                                  'Guarantees . . .',
                                  style: TextStyle(
                                    color: kprimary,
                                    fontSize: 30,
                                    fontStyle: FontStyle.italic,
                                    fontWeight: FontWeight.bold,
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                        Container(
                          alignment: Alignment.center,
                          width: size.width,
                          height: (MediaQuery.of(context).size.height / 4),
                          child: Stack(
                            children: [
                              Positioned(
                                top: (MediaQuery.of(context).size.height / 4) -
                                    90,
                                left: size.width / 1.3 / 6.5,
                                child: Container(
                                  width: size.width / 1.3,
                                  height: 50,
                                  decoration: BoxDecoration(
                                      color: kprimary.withOpacity(.9)),
                                  child: TextFormField(
                                    decoration: InputDecoration(
                                      focusColor: Colors.white,
                                      hoverColor: Colors.white,
                                      suffixIcon: GestureDetector(
                                        onTap: () => {
                                          Timer(const Duration(seconds: 0),
                                              () async {
                                            await controller.searchGuarantee(
                                                controller.searchValue.value);
                                          })
                                        },
                                        child: Container(
                                          color: Colors.white.withOpacity(.2),
                                          child: Icon(
                                            Icons.search,
                                            color: Colors.white.withOpacity(.6),
                                            size: 24,
                                          ),
                                        ),
                                      ),
                                      hintText: "Search for guarantees",
                                      contentPadding: const EdgeInsets.all(10),
                                      hintStyle: TextStyle(
                                        color: Colors.white.withOpacity(.6),
                                        fontSize: 15,
                                      ),
                                    ),
                                    style: const TextStyle(color: Colors.white),
                                    onChanged: (value) {
                                      controller.searchValue.value = value;
                                    },
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ]),
                      controller.guaranteeList.isEmpty
                          ? SizedBox(
                              height: 250,
                              child: Align(
                                alignment: Alignment.center,
                                child: Text(
                                  "No items",
                                  style: TextStyle(
                                      color: kprimary,
                                      fontSize: 24,
                                      fontWeight: FontWeight.bold),
                                ),
                              ),
                            )
                          : Expanded(
                              child: Column(
                              children: [
                                const SizedBox(height: 20),
                                Container(
                                  color: Colors.grey.withOpacity(.2),
                                  padding: const EdgeInsets.symmetric(
                                    horizontal: 20,
                                    vertical: 0,
                                  ),
                                  child: SingleChildScrollView(
                                    child: ExpansionTile(
                                      children: [
                                        Container(
                                          height: 120,
                                          margin:
                                              const EdgeInsets.only(top: 15),
                                          padding: const EdgeInsets.symmetric(
                                            horizontal: 20,
                                            vertical: 0,
                                          ),
                                          width: double.maxFinite,
                                          child: GridView.builder(
                                            gridDelegate:
                                                const SliverGridDelegateWithFixedCrossAxisCount(
                                              crossAxisCount: 2,
                                              mainAxisSpacing: 20,
                                              crossAxisSpacing: 50,
                                            ),
                                            itemCount: 4,
                                            itemBuilder: (BuildContext context,
                                                int index) {
                                              return GestureDetector(
                                                onTap: () =>
                                                    controller.sortItem(index),
                                                child: Obx(
                                                  () => Padding(
                                                    padding: const EdgeInsets
                                                        .symmetric(
                                                      horizontal: 12.0,
                                                      vertical: 25,
                                                    ),
                                                    child: Container(
                                                      decoration: BoxDecoration(
                                                        border: Border.all(
                                                          width: 2,
                                                          color: kprimary,
                                                        ),
                                                        borderRadius:
                                                            BorderRadius
                                                                .circular(15),
                                                        color: controller
                                                                    .currentSortItem
                                                                    .value ==
                                                                index
                                                            ? kprimary
                                                            : Colors.white
                                                                .withOpacity(
                                                                .2,
                                                              ),
                                                      ),
                                                      child: Center(
                                                        child: Text(
                                                          sortTypes[index],
                                                          style: TextStyle(
                                                            color: controller
                                                                        .currentSortItem
                                                                        .value ==
                                                                    index
                                                                ? Colors.white
                                                                : kprimary,
                                                          ),
                                                        ),
                                                      ),
                                                    ),
                                                  ),
                                                ),
                                              );
                                            },
                                          ),
                                        )
                                      ],
                                      title: Text(
                                        "Select type to sortBy",
                                        style: TextStyle(
                                          fontSize: 18,
                                          //fontWeight: FontWeight.w600,
                                          color: kprimary,
                                        ),
                                      ),
                                      onExpansionChanged: (value) {
                                        controller.isExpand(
                                            !controller.isExpand.value);
                                      },
                                    ),
                                  ),
                                ),
                                Expanded(
                                    flex: 8,
                                    child: _buildProductsList(size, context)),
                              ],
                            )),
                    ],
                  ),
                ),
              ),
            ),
    );
  }

  Widget _buildProductsList(Size size, BuildContext context) {
    Random rnd;
    double min = 0;
    rnd = Random();
    double r = min + rnd.nextDouble();
    return ListView.builder(
      shrinkWrap: true,
      itemCount: controller.guaranteeList.length,
      padding: const EdgeInsets.only(top: 16),
      itemBuilder: (context, index) => Padding(
        padding: const EdgeInsets.symmetric(vertical: 10),
        child: Container(
          height: MediaQuery.of(context).size.height / 6,
          decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(15),
              boxShadow: [kDefaultShadow]),
          child: Padding(
            padding: const EdgeInsets.symmetric(vertical: 8.0),
            child: SingleChildScrollView(
              child: ExpansionTile(
                  children: [
                    Stack(
                      children: [
                        Align(
                          alignment: const Alignment(0, 0),
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Padding(
                                padding: const EdgeInsets.all(12.0),
                                child: SizedBox(
                                  width: size.width / 2 + size.width / 20,
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Text(
                                        controller
                                            .guaranteeList[index].birthDate,
                                        style: const TextStyle(color: kstorm),
                                      ),
                                      Text(
                                        '${controller.guaranteeList[index].cost}/academic_year : ${controller.guaranteeList[index].academicYear}',
                                        style: const TextStyle(color: kstorm),
                                      ),
                                      const SizedBox(
                                        height: 10,
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                        Align(
                          alignment: const Alignment(-0.9, 0),
                          child: deleteButtonEffect(index, context, size),
                        ),
                      ],
                    ),
                  ],
                  leading: const CircleAvatar(
                    radius: 30,
                    backgroundColor: Colors.black45,
                  ),
                  title: Padding(
                    padding: const EdgeInsets.only(top: 10),
                    child: Text(
                      controller.guaranteeList[index].name,
                      style: const TextStyle(
                        color: Colors.black,
                      ),
                    ),
                  ),
                  subtitle: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Padding(
                        padding: const EdgeInsets.only(top: 5, bottom: 10),
                        child: Text('${controller.guaranteeList[index].cost}',
                            style: const TextStyle(color: Colors.black)),
                      ),
                      LinearPercentIndicator(
                        barRadius: const Radius.circular(040),
                        width: MediaQuery.of(context).size.width / 2,
                        animation: true,
                        animationDuration: 800,
                        lineHeight: 10,
                        percent: r,
                        progressColor: kblue,
                        backgroundColor: kbblue,
                      ),
                      SizedBox(
                        height: MediaQuery.of(context).size.height / 100,
                      ),
                      Text(
                        '${controller.guaranteeList[index].cost} \$',
                        style: const TextStyle(color: kstorm),
                      ),
                    ],
                  ),
                  trailing: updatButtonEffect(index, context, size)),
            ),
          ),
        ),
      ),
    );
  }

  Container updatButtonEffect(int index, BuildContext context, Size size) {
    return Container(
      height: 50,
      width: 75,
      decoration: BoxDecoration(
        color: kstorm,
        borderRadius: BorderRadius.circular(10),
      ),
      child: MaterialButton(
        onPressed: () {
          var id = controller.guaranteeList[index].id;
          showModalBottomSheet(
            context: context,
            builder: (BuildContext context) {
              return Container(
                  height: MediaQuery.of(context).size.height / 2,
                  decoration: const BoxDecoration(
                    color: kprimary,
                    borderRadius: BorderRadius.only(
                      topRight: Radius.circular(25),
                      topLeft: Radius.circular(25),
                    ),
                  ),
                  child: SingleChildScrollView(
                    child: Column(
                      children: [
                        Container(
                          height: MediaQuery.of(context).size.height / 5,
                          width: MediaQuery.of(context).size.width,
                          //color: Colors.purple,
                          alignment: const Alignment(-0.6, 0.4),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                            children: [
                              Stack(
                                clipBehavior: Clip.none,
                                children: [
                                  Obx(
                                    () => CircleAvatar(
                                      radius: 40,
                                      backgroundColor: Colors.black45,
                                      backgroundImage: image.value != ''
                                          ? FileImage(
                                              File(image.value),
                                            )
                                          : null,
                                    ),
                                  ),
                                  Positioned(
                                    top: 45,
                                    right: 0,
                                    child: GestureDetector(
                                      onTap: () => {
                                        Timer(const Duration(seconds: 1),
                                            () async {
                                          await takeImage(ImageSource.gallery);
                                        })
                                      },
                                      child: CircleAvatar(
                                        backgroundColor:
                                            Colors.black54.withOpacity(.5),
                                        child: const Icon(
                                          Icons.camera_alt,
                                          size: 30,
                                          color: Colors.white,
                                        ),
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                              //const Spacer(),
                              //const Spacer(),
                            ],
                          ),
                        ),
                        Form(
                          key: guaranteeFormKey[0],
                          autovalidateMode: AutovalidateMode.onUserInteraction,
                          child:
                              getGuaranteeInput(0, size, guaranteeFormKey[0]),
                        ),
                        Form(
                          key: guaranteeFormKey[1],
                          autovalidateMode: AutovalidateMode.onUserInteraction,
                          child:
                              getGuaranteeInput(1, size, guaranteeFormKey[1]),
                        ),
                        Obx(
                          () => Container(
                            alignment: controller.isExpand.value
                                ? Alignment.topCenter
                                : Alignment.center,
                            width: size.width * .8,
                            height: controller.isExpand.value ? 220 : 78,
                            margin: const EdgeInsets.symmetric(
                                horizontal: 20, vertical: 15),
                            padding: const EdgeInsets.symmetric(horizontal: 10),
                            decoration: BoxDecoration(
                              color: Colors.grey.withOpacity(.2),
                              borderRadius: BorderRadius.circular(10),
                            ),
                            child: SingleChildScrollView(
                              child: ExpansionTile(
                                children: [
                                  Container(
                                    height: 150,
                                    width: double.maxFinite,
                                    child: GridView.builder(
                                      gridDelegate:
                                          const SliverGridDelegateWithFixedCrossAxisCount(
                                        crossAxisCount: 3,
                                        mainAxisSpacing: 30,
                                        crossAxisSpacing: 30,
                                      ),
                                      itemCount: 12,
                                      itemBuilder:
                                          (BuildContext context, int index) {
                                        return GestureDetector(
                                          onTap: () =>
                                              controller.changeItem(index),
                                          child: Obx(
                                            () => Container(
                                              decoration: BoxDecoration(
                                                border: Border.all(
                                                    width: 2, color: kprimary),
                                                borderRadius:
                                                    BorderRadius.circular(15),
                                                color: controller.currentItem
                                                            .value ==
                                                        index
                                                    ? kprimary
                                                    : Colors.white
                                                        .withOpacity(.2),
                                              ),
                                              child: Center(
                                                child: Text(
                                                  academicYears[index],
                                                  style: TextStyle(
                                                    color: controller
                                                                .currentItem
                                                                .value ==
                                                            index
                                                        ? Colors.white
                                                        : kprimary,
                                                  ),
                                                ),
                                              ),
                                            ),
                                          ),
                                        );
                                      },
                                    ),
                                  )
                                ],
                                title: FittedBox(
                                  child: Text(
                                    "Select child academic year",
                                    style: TextStyle(
                                      fontSize: 16,
                                      fontWeight: FontWeight.w600,
                                      color: kprimary,
                                    ),
                                  ),
                                ),
                                onExpansionChanged: (value) {
                                  controller
                                      .isExpand(!controller.isExpand.value);
                                },
                              ),
                            ),
                          ),
                        ),
                        GestureDetector(
                          child: CameraStudio(Icons.date_range, size),
                          onTap: () => openCylinder(context),
                        ),
                        const SizedBox(height: 20),
                        GestureDetector(
                          onTap: () => {
                            if (checking())
                              {
                                Timer(const Duration(seconds: 0), () async {
                                  String save =
                                      await InCompleteGuaranteeController()
                                          .updateGuarantee(
                                    GuaranteeModel(
                                      guaranteeEditors[0].text != ''
                                          ? guaranteeEditors[0].text
                                          : controller
                                              .guaranteeList[index].name,
                                      guaranteeEditors[1].text != ''
                                          ? guaranteeEditors[1].text
                                          : controller.guaranteeList[index].cost
                                              .toString(),
                                      currentDate.value.toString() != ''
                                          ? currentDate.value.toString()
                                          : controller
                                              .guaranteeList[index].birthDate
                                              .toString(),
                                      controller.academicYear.value,
                                      image.value != ''
                                          ? image.value
                                          : controller
                                              .guaranteeList[index].image,
                                    ),
                                    id,
                                  );
                                  Get.back();

                                  if (save == 'OK') {
                                    Timer(const Duration(seconds: 1), () {
                                      Get.to(FunctionStatus(
                                        controller: controller,
                                        title: "Guarantee",
                                        function: "updated",
                                      ));
                                      controller.isTrue.value = true;
                                      controller.showStationers();
                                    });
                                  } else {
                                    controller.isTrue.value = false;
                                    Get.to(FunctionStatus(
                                      controller: controller,
                                      title: "Guarantee",
                                      function: "updated",
                                    ));
                                  }
                                }),
                              }
                          },
                          child: Container(
                            width: MediaQuery.of(context).size.width / 3,
                            height: MediaQuery.of(context).size.width / 9,
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(10),
                              color: kblue,
                            ),
                            alignment: Alignment.center,
                            child: const Text(
                              "Update",
                              style: TextStyle(
                                  color: Colors.white,
                                  fontWeight: FontWeight.bold,
                                  fontSize: 19),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ));
            },
          );
        },
        child: const Center(
          child: Text(
            'Update',
            style: TextStyle(color: kprimary, fontSize: 12.2),
          ),
        ),
      ),
    );
  }

  Container deleteButtonEffect(int index, BuildContext context, Size size) {
    return Container(
      height: 50,
      width: 75,
      decoration: BoxDecoration(
        color: kstorm,
        borderRadius: BorderRadius.circular(10),
      ),
      child: MaterialButton(
        onPressed: () {
          var id = controller.guaranteeList[index].id;
          showModalBottomSheet(
            constraints: const BoxConstraints(maxHeight: 200),
            context: context,
            builder: (BuildContext context) {
              return Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  const Text(
                    "Would yo like to delete this element ? ",
                    style: TextStyle(
                        color: Colors.black54, fontWeight: FontWeight.bold),
                    //textAlign: TextAlign.center,
                  ),
                  const SizedBox(
                    height: 15,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      GestureDetector(
                        onTap: () => {
                          Timer(const Duration(seconds: 0), () async {
                            String save = await controller.deleteGuarantee(id);
                            if (save == 'OK') {
                              controller.isTrue.value = true;
                              Get.back();
                              Get.to(FunctionStatus(
                                controller: controller,
                                title: "Guarantee",
                                function: "deleted",
                              ));
                              controller.showStationers();
                            } else {
                              print('error');
                              controller.isTrue.value = false;
                              Get.to(FunctionStatus(
                                controller: controller,
                                title: "Guarantee",
                                function: "deleted",
                              ));
                            }
                          })
                        },
                        child: Container(
                          alignment: Alignment.center,
                          width: 80,
                          height: 50,
                          decoration: BoxDecoration(
                            color: kstorm,
                            borderRadius: BorderRadius.circular(10),
                          ),
                          child: const Text(
                            "Yes",
                            style: TextStyle(
                                color: Colors.white,
                                fontWeight: FontWeight.bold,
                                fontSize: 22),
                          ),
                        ),
                      ),
                      const SizedBox(
                        width: 15,
                      ),
                      GestureDetector(
                        onTap: () => Get.back(),
                        child: Container(
                          width: 80,
                          height: 50,
                          alignment: Alignment.center,
                          decoration: BoxDecoration(
                            color: kprimary,
                            borderRadius: BorderRadius.circular(10),
                          ),
                          child: const Text(
                            "No",
                            style: TextStyle(
                                color: Colors.white,
                                fontWeight: FontWeight.bold,
                                fontSize: 22),
                          ),
                        ),
                      ),
                    ],
                  ),
                ],
              );
            },
          );
        },
        child: const Center(
          child: Text(
            'Delete',
            style: TextStyle(color: kprimary, fontSize: 12.2),
          ),
        ),
      ),
    );
  }

  Future<void> openCylinder(BuildContext context) async {
    final DateTime? pickedDate = await showDatePicker(
        context: context,
        initialDate: currentDate.value,
        firstDate: currentDate.value,
        lastDate: DateTime(2050));
    if (pickedDate != null && pickedDate != currentDate.value) {
      currentDate.value = pickedDate;
    }
  }

  Container CameraStudio(IconData icon, Size size) {
    return Container(
      margin: const EdgeInsets.symmetric(horizontal: 5, vertical: 10),
      child: Icon(
        icon,
        size: 30,
        color: kprimary,
      ),
      width: size.width * .3,
      height: 120,
      decoration: BoxDecoration(
        color: Colors.white.withOpacity(.2),
        border: Border.all(
          width: 2,
          color: kprimary,
        ),
        borderRadius: BorderRadius.circular(20),
      ),
    );
  }

  Widget getGuaranteeInput(int index, Size size, formKey) {
    return Container(
      alignment: Alignment.center,
      width: size.width * .8,
      height: 78,
      margin: const EdgeInsets.symmetric(horizontal: 20, vertical: 15),
      padding: const EdgeInsets.symmetric(horizontal: 10),
      decoration: BoxDecoration(
          color: Colors.grey.withOpacity(.2),
          borderRadius: BorderRadius.circular(10)),
      child: TextFormField(
        //key: formKey,
        controller: guaranteeEditors[index],
        decoration: InputDecoration(
          labelText: guaranteeAttributeName[index],
          border: InputBorder.none,
          icon: guaranteeIcons[index],
          iconColor: Colors.red,
          labelStyle: TextStyle(
              fontSize: 16, fontWeight: FontWeight.w600, color: kprimary),
        ),
        maxLines: index == 3 ? 4 : 1,
        keyboardType: guaranteeTypes[index],
        validator: (value) {
          if (index == 0 && value != null && value.length < 7) {
            return guaranteeErrorText[0];
          } else if (index == 1 && value != null && int.parse(value) <= 1000) {
            return guaranteeErrorText[1];
          }
        },
      ),
    );
  }
}

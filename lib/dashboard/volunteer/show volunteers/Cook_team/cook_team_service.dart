import 'dart:convert';
import 'package:http/http.dart' as http;

import '../../../../config/config.dart';
import '../../../../secureStorage.dart';
import '../../../../user/model/volunteer_team_model.dart';

class CookService
{
  var message;
  var name;
  var email;
  var url = Uri.parse(ServerConfig.domainNameServer + ServerConfig.ShowCookTeam);

  Future<List<dynamic>>getCook() async{
    var _storage=SecureStorage();
    String? token = await _storage.read('token');
    var response = await http.get(url,headers:  {
      'Accept' : 'application/json',
      'Authorization'  :  'Bearer $token'
    },);
    print(response.statusCode);
    if(response.statusCode == 200){
      var j=jsonDecode(response.body);
      var i=j["FOOD_TEAM"];
      print('${i}');
      var Cook = cookTeamFromJson(response.body);



      return Cook.cookTeam;
    }
    else {
      return [];
    }


  }

  Future<String> dismissvol(int id) async {
    var _storage=SecureStorage();
    String? token = await _storage.read('token');
    print(token);

    var url = Uri.parse(
        ServerConfig.domainNameServer + ServerConfig.dismiss + '$id');

    var response = await http.delete(url, headers: {
      'Authorization': 'Bearer $token ',
      'Accept': 'application/json'
    });

    String getResponse = response.body;
    print(getResponse);
    if (getResponse.contains('{"message":"stationery record get deleted "')) {
      return 'OK';
    } else {
      return 'NO';
    }
  }

//
// Future<List<Patient>> loadProductsFromApi(int index) async {
//   var response = await http.get(Uri.parse(ServerConfig.domainNameServer+ServerConfig.showCategoryProducts + '${index}'),
//     headers: {
//       'Accept': 'application/json',
//     },
//   );
//   print(response.statusCode);
//   //  print(response.body);
//
//   if(response.statusCode == 200){
//     var porductscat = productsFromJson(response.body);
//     return porductscat.products;
//   }
//   else {
//     return [];
//   }
}

//
// Future<List<Patient>> SearchProducts(String name) async {
//   var response = await http.get(Uri.parse(ServerConfig.domainNameServer+ServerConfig.SearchProducts + '${name}'),
//     headers: {
//       'Accept': 'application/json',
//     },
//   );
//   print(response.statusCode);
//   //  print(response.body);
//
//   if(response.statusCode == 200){
//     var porductsearch = productsFromJson(response.body);
//     return porductsearch.products;
//   }
//   else {
//     return [];
//   }
// }

import 'package:charitysys/user/volunteer/volunteer.dart';
import 'package:get/get.dart';

import 'cook_team_service.dart';



class CookVolController extends GetxController
{
  var Cookslist =<dynamic>[].obs;
  var Cooks=<dynamic>[];

  var id=0.obs;

  CookService _service= CookService();
  var isloading=true.obs;

  @override
  void onInit() {
    showCooks();
    super.onInit();
  }

  void showCooks() async {
    try {
      isloading(true);
      Cooks = await _service.getCook();
      if (Cooks != null) {
        print('${Cooks}');
        Cookslist.value = Cooks;
      }
    } finally {
      isloading(false);
    }
  }
  Future<String> dismiss(int id) async {

    String save;

    try {
      isloading(true);
      save = await _service.dismissvol(id);
    }finally {
      isloading(false);
    }
    return save;
  }



}
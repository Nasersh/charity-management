import 'package:charitysys/dashboard/volunteer/show%20volunteers/teachers_team/teachers_team_service.dart';
import 'package:get/get.dart';


class TeachersTeamController extends GetxController
{
  var Teacherslist =<dynamic>[].obs;
  var Teachers=<dynamic>[];

  var id=0.obs;

  TeachersService _service= TeachersService();
  var isloading=true.obs;

  @override
  void onInit() {
    showTeacherss();
    super.onInit();
  }

  void showTeacherss() async {

      Teachers = await _service.getTeachers();
      if (Teachers != null) {
        Teacherslist.value = Teachers;
      }
    }
  Future<String> dismiss(int id) async {

    String save;

    try {
      isloading(true);
      save = await _service.dismissvol(id);
    }finally {
      isloading(false);
    }
    return save;
  }

}
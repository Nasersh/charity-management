import 'package:http/http.dart' as http;

import '../../../../config/config.dart';
import '../../../../secureStorage.dart';
import '../../../../user/model/volunteer_team_model.dart';
class TeachersService {
  var message;
  var name;
  var email;
  var url =
      Uri.parse(ServerConfig.domainNameServer + ServerConfig.ShowEducationTeam);

  Future<List<dynamic>> getTeachers() async {
    var _storage = SecureStorage();
    String? token = await _storage.read('token');
    var response = await http.get(
      url,
      headers: {
        'Accept': 'application/json',
        'Authorization': '${token}'},
    );
    print(response.statusCode);

    if (response.statusCode == 200) {
      var Teachers = EducationTeamFromJson(response.body);
      return Teachers.educationTeam;
    } else {
      return [];
    }
  }


  Future<String> dismissvol(int id) async {
    var _storage=SecureStorage();
    String? token = await _storage.read('token');
    print(token);

    var url = Uri.parse(
        ServerConfig.domainNameServer + ServerConfig.dismiss + '$id');

    var response = await http.delete(url, headers: {
      'Authorization': 'Bearer $token ',
      'Accept': 'application/json'
    });

    String getResponse = response.body;
    print(getResponse);
    if (getResponse.contains('{"message":"stationery record get deleted "')) {
      return 'OK';
    } else {
      return 'NO';
    }
  }

}

import 'package:charitysys/user/volunteer/volunteer.dart';
import 'package:get/get.dart';

import 'Medical_service.dart';



class MedicalVolController extends GetxController
{
  var Medicalslist =<dynamic>[].obs;
  var Medicals=<dynamic>[];

  var id=0.obs;

  MedicalService _service= MedicalService();
  var isloading=true.obs;

  @override
  void onInit() {
    showMedicals();
    super.onInit();
  }

  void showMedicals() async {
      Medicals = await _service.getMedical();
      if (Medicals != null) {
        Medicalslist.value = Medicals;
      }
  }

  Future<void> dismiss(int id) async {


    try {
      isloading(true);
       await _service.dismissvol(id);
    }finally {
      isloading(false);
    }
  }



}
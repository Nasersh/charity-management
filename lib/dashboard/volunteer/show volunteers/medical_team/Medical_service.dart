import 'package:http/http.dart' as http;

import '../../../../config/config.dart';
import '../../../../secureStorage.dart';
import '../../../../user/model/volunteer_team_model.dart';
class MedicalService
{
  var message;
  var name;
  var email;
  var url = Uri.parse(ServerConfig.domainNameServer + ServerConfig.ShowMedicalTeam);

  Future<List<MedicalTeamElement>>getMedical() async{
    var _storage=SecureStorage();
    String? token = await _storage.read('token');
    print('here');
    var response = await http.get(url,headers:  {
      'Accept' : 'application/json',
      'Authorization'  :  'Bearer $token'
    },);
    // print(response.statusCode);

    if(response.statusCode == 200){
      print('200');
      var Medical = medicalTeamFromJson(response.body);
      return Medical.medicalTeam;
    }
    else {
      return [];
    }

  }
  Future<void> dismissvol(int id) async {
    var _storage=SecureStorage();
    String? token = await _storage.read('token');
    print(token);

    var url = Uri.parse(
        ServerConfig.domainNameServer + ServerConfig.dismiss + '$id');

    var response = await http.delete(url, headers: {
      // 'Authorization': 'Bearer $token ',
      'Authorization': 'Bearer 20|Fls21MOL3NYY6Ghxgb3qmFcCbtp0SEx9we5F8etk',
      'Accept': 'application/json'
    });

    String jsonresponse = response.body;
    print('${response.statusCode}');

    if(response.statusCode==200)
     {
       print('success');
     }
     }

}

//
// Future<List<Patient>> SearchProducts(String name) async {
//   var response = await http.get(Uri.parse(ServerConfig.domainNameServer+ServerConfig.SearchProducts + '${name}'),
//     headers: {
//       'Accept': 'application/json',
//     },
//   );
//   print(response.statusCode);
//   //  print(response.body);
//
//   if(response.statusCode == 200){
//     var porductsearch = productsFromJson(response.body);
//     return porductsearch.products;
//   }
//   else {
//     return [];
//   }
// }

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:get/get.dart';
import '../../../../configrations/const/const.dart';
import 'Medical_team_controller.dart';



class MedicalVol extends StatelessWidget {
  MedicalVol({
    Key? key,
  }) : super(key: key);
  MedicalVolController controller=Get.put(MedicalVolController());
  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;

    return SafeArea(
      child: Scaffold(
        appBar: AppBar(
          leading: IconButton(
            icon: const Icon(
                FontAwesomeIcons.arrowLeft
            ), onPressed: () {
            Get.back();
          },
          ),
          elevation: 0,
          backgroundColor: kblue,
        ),
        backgroundColor: kprimary,
        body: SingleChildScrollView(
          child: Center(
            child:
            Column(
              children: [
                Container(
                  width: size.width,
                  height: (size.height) / 7,
                  alignment: const Alignment(0, 0),
                  decoration: const BoxDecoration(
                      color: kblue,
                      borderRadius: BorderRadius.only(
                        bottomLeft: Radius.circular(50),
                        bottomRight: Radius.circular(50),
                      )),
                  child: const Text(
                    'Medical crew', //Sections
                    style: TextStyle(
                        color: kprimary,
                        fontSize: 30,
                        fontWeight: FontWeight.bold
                    ),
                  ),
                ),
                const SizedBox(height: 15),
                SizedBox(
                  height: (size.height) - ((size.height) / 5.5) - 60,
                  child:
                  // PageView.builder(
                  //   itemCount:controller.Medicalslist.value.length,
                  //   scrollDirection: Axis.horizontal,
                  //   itemBuilder: (BuildContext context, int index) {
                  //     return
                  //     Padding(
                  //       padding: const EdgeInsets.symmetric(vertical: 10),
                  //       child: Container(
                  //         height: MediaQuery.of(context).size.height / 5,
                  //         decoration: BoxDecoration(
                  //             color: Colors.white,
                  //             borderRadius: BorderRadius.circular(15),
                  //             boxShadow: [kDefaultShadow]),
                  //         child: Padding(
                  //           padding: const EdgeInsets.symmetric(vertical: 8.0),
                  //           child: ExpansionTile(
                  //             leading: const CircleAvatar(
                  //               radius: 30,
                  //               backgroundColor: Colors.black45,
                  //             ),
                  //             title:  Padding(
                  //               padding: EdgeInsets.only(top: 10),
                  //               child: Text('${controller.Medicalslist[index].user.name}',style: TextStyle(
                  //                   fontWeight:  FontWeight.bold,color: kblue
                  //               ),),
                  //             ),
                  //             subtitle: Column(
                  //               crossAxisAlignment: CrossAxisAlignment.start,
                  //               children: [
                  //                 Padding(
                  //                   padding: EdgeInsets.only(top: 5, bottom: 10),
                  //                   child:
                  //                   Text('${controller.Medicalslist[index].user.email}',style: TextStyle(
                  //                       fontWeight: FontWeight.w300
                  //                   ),),
                  //                 ),
                  //                 SizedBox(
                  //                   height: MediaQuery.of(context).size.height/1000,
                  //                 ),
                  //                 Text('Phone: ${controller.Medicalslist[index].phone}',style: TextStyle(
                  //                     fontWeight: FontWeight.w500
                  //                 ),),
                  //                 SizedBox(
                  //                   height: MediaQuery.of(context).size.height / 100,
                  //                 ),
                  //                 // Obx(()=>
                  //                 //     Text(
                  //                 //       '',
                  //                 //       style: TextStyle(color: kstorm),
                  //                 //     )
                  //                 // )
                  //               ],
                  //             ),
                  //             trailing: Padding(
                  //               padding: const EdgeInsets.only(top: 20.0),
                  //               child: Container(
                  //                 height: MediaQuery.of(context).size.height/20,
                  //                 width: MediaQuery.of(context).size.width/4.8,
                  //                 decoration: BoxDecoration(
                  //                     color: kstorm, borderRadius: BorderRadius.circular(10)),
                  //                 child: MaterialButton(
                  //                     onPressed: () {
                  //                       showAlertDialog(context,index);
                  //                     },
                  //                     child: const Center(
                  //                         child: Text(
                  //                           'Fire',
                  //                           style: TextStyle(color: kprimary, fontSize: 13.2),
                  //                         ))),
                  //               ),
                  //             ),
                  //             children:  [
                  //               Text(' job: ${controller.Medicalslist[index].job} ',style: TextStyle(
                  //                   color: kblue,
                  //                   fontWeight: FontWeight.bold
                  //               ),)
                  //             ],
                  //           ),
                  //         ),
                  //       ),
                  //     );
                  //   },
                  //
                  // ),
                  Obx(()=> controller.isloading.value?_buildProductsList():SizedBox())

                  ),
              ],
            ),
          ),
        ),
      ),
    );
  }
  Widget _buildProductsList() {
    print(controller.Medicalslist.length);
    // print('top');
    return ListView.builder(
      shrinkWrap: true,
      itemCount:controller.Medicalslist.value.length,
      padding: const EdgeInsets.only(top: 16),
      itemBuilder: (context, index) => Padding(
        padding: const EdgeInsets.symmetric(vertical: 10),
        child: Container(
          height: MediaQuery.of(context).size.height / 5,
          decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(15),
              boxShadow: [kDefaultShadow]),
          child: Padding(
            padding: const EdgeInsets.symmetric(vertical: 8.0),
            child: ExpansionTile(
              leading: const CircleAvatar(
                radius: 30,
                backgroundColor: Colors.black45,
              ),
              title:  Padding(
                padding: EdgeInsets.only(top: 10),
                child: Text('${controller.Medicalslist[index].user.name}',style: TextStyle(
                    fontWeight:  FontWeight.bold,color: kblue
                ),),
              ),
              subtitle: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                   Padding(
                    padding: EdgeInsets.only(top: 5, bottom: 10),
                    child:
                    Text('${controller.Medicalslist[index].user.email}',style: TextStyle(
                        fontWeight: FontWeight.w300
                    ),),
                  ),
                  SizedBox(
                    height: MediaQuery.of(context).size.height/1000,
                  ),
                  Text('Phone: ${controller.Medicalslist[index].phone}',style: TextStyle(
                      fontWeight: FontWeight.w500
                  ),),
                  SizedBox(
                    height: MediaQuery.of(context).size.height / 100,
                  ),
                  // Obx(()=>
                  //     Text(
                  //       '',
                  //       style: TextStyle(color: kstorm),
                  //     )
                  // )
                ],
              ),
              trailing: Padding(
                padding: const EdgeInsets.only(top: 20.0),
                child: Container(
                  height: MediaQuery.of(context).size.height/20,
                  width: MediaQuery.of(context).size.width/4.8,
                  decoration: BoxDecoration(
                      color: kstorm, borderRadius: BorderRadius.circular(10)),
                  child: MaterialButton(
                      onPressed: () {
                        showAlertDialog(context,index);
                      },
                      child: const Center(
                          child: Text(
                            'Fire',
                            style: TextStyle(color: kprimary, fontSize: 13.2),
                          ))),
                ),
              ),
              children:  [
                Text(' job: ${controller.Medicalslist[index].job} ',style: TextStyle(
                  color: kblue,
                  fontWeight: FontWeight.bold
                ),)
              ],
            ),
          ),
        ),
      ),
    );
  }
  showAlertDialog(BuildContext context, index) {
    // set up the buttons
    Widget cancelButton = ElevatedButton(
      child: Text("Cancel",style: TextStyle(color: kblue),),
      onPressed: () {
        Get.back();
      },
    );
    Widget continueButton = ElevatedButton(
      child: Text("Continue",style: TextStyle(color: kblue)),
      onPressed: () {

        var id= controller.Medicalslist[index].id;
        controller.dismiss(id);
        Get.back();
        }

          );

    // set up the AlertDialog
    AlertDialog alert = AlertDialog(
      title: Text("Guarantee",style: TextStyle(color: kstorm),),
      content: Text(
        "Would you like to dismiss this volunteer?",style: TextStyle(color: kblue),),
      actions: [
        cancelButton,
        continueButton,
      ],
    );

    // show the dialog
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }

}

//Container(
//               width: size.width,
//               height: (size.height * 3) / 3.85,
//               alignment: Alignment.center,
//               child: Padding(
//                 padding: EdgeInsets.symmetric(
//                     horizontal: size.width / 10, vertical: (size.height) / 10),
//                 child: Swiper(
//                   itemCount: routeName.length,
//                   itemWidth: double.maxFinite,
//                   itemHeight: double.maxFinite,
//                   pagination: const SwiperPagination(
//                       builder: DotSwiperPaginationBuilder()),
//                   layout: SwiperLayout.TINDER,
//                   itemBuilder: (_, index) => Stack(
//                     clipBehavior: Clip.none,
//                     children: [
//                       GestureDetector(
//                         onTap: () => Get.toNamed(routeName[index]),
//                         child: Container(
//                           decoration: BoxDecoration(
//                             color: mainColor,
//                             borderRadius: BorderRadius.circular(50),
//                           ),
//                           child: Center(
//                             child: Text(
//                               categoriesName[index],
//                               style: kHeading2,
//                             ),
//                           ),
//                         ),
//                       ),
//                       Positioned(
//                         top: -60,
//                         child: Lottie.asset(
//                           categoriesLottie[index],
//                           width: 200,
//                           height: 200,
//                         ),
//                       )
//                     ],
//                   ),
//                 ),
//               ),
//             )

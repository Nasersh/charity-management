
import 'package:charitysys/dashboard/volunteer/show%20volunteers/teachers_team/teachers_team.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:get/get.dart';
import '../../../configrations/const/const.dart';
import 'Cook_team/cook_team.dart';
import 'medical_team/medical_team.dart';

class ShowVolunteers extends StatelessWidget {
  ShowVolunteers({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;

    return SafeArea(
      child: Scaffold(
        appBar: AppBar(
          leading: IconButton(
            icon: Icon(
              FontAwesomeIcons.arrowLeft
            ), onPressed: () {
              Get.back();
          },
          ),
          elevation: 0,
          backgroundColor: kblue,
        ),
        backgroundColor: kprimary,
        body: SingleChildScrollView(
          child: Column(
            children: [
              Container(
                width: size.width,
                height: (size.height) / 7,
                alignment: const Alignment(0, 0),
                decoration: BoxDecoration(
                    color: kblue,
                    borderRadius: const BorderRadius.only(
                      bottomLeft: Radius.circular(50),
                      bottomRight: Radius.circular(50),
                    )),
                child: const Text(
                 'Volunteer crew', //Sections
                  style: TextStyle(
                    color: kprimary,
                    fontSize: 30,
                    fontWeight: FontWeight.bold
                  ),
                ),
              ),
              const SizedBox(height: 15),
              SizedBox(
                height: (size.height) - ((size.height) / 5.5) - 60,
                child: ListView(
                    children:[
                      ElevatedButton(
                        onPressed: () {
                          Get.to(MedicalVol());
                        },
                        child: Container(
                          height: size.height/5,
                         width: size.width-20,
                         decoration: BoxDecoration(
                           color: Colors.white,
                           boxShadow: const [
                             kDefaultShadow
                           ],
                           borderRadius: BorderRadius.circular(20)
                         ),
                          child:  Column(
                             children: [
                               Padding(
                                 padding:  EdgeInsets.symmetric(horizontal: size.width/4
                                 ,vertical:size.width/10 ),
                                 child: Column(
                                   children:  [
                                     Text('Medical Team',style: const TextStyle(
                                       color: kstorm,
                                       fontSize: 20,
                                       fontWeight: FontWeight.bold
                                     ),),
                                     SizedBox(
                                       height: MediaQuery.of(context).size.height/50,
                                     ),
                                     Text(
                                       'this category shows you the medical team',
                                       style: TextStyle(fontSize: 15,color: Colors.grey),
                                       maxLines: 2,
                                     )
                                   ],
                                 ),
                               )
                             ],
                            ),
                          ),
                      ),
                      SizedBox(
                        height: size.height/30,
                      ),
                      ElevatedButton(
                        onPressed: () {
                          Get.to(CookVol());

                        },
                        child: Container(
                          height: size.height/5,
                         width: size.width-20,
                         decoration: BoxDecoration(
                           color: Colors.white,
                           boxShadow: const [
                             kDefaultShadow
                           ],
                           borderRadius: BorderRadius.circular(20)
                         ),
                          child:  Column(
                             children: [
                               Padding(
                                 padding:  EdgeInsets.symmetric(horizontal: size.width/4
                                 ,vertical:size.width/10 ),
                                 child: Column(
                                   children: [
                                     const Text('Cook Team',style: TextStyle(
                                       color: kstorm,
                                       fontSize: 20,
                                       fontWeight: FontWeight.bold
                                     ),),
                                     SizedBox(
                                       height: MediaQuery.of(context).size.height/50,
                                     ),
                                     const Text(
                                       'this category shows you the  Cook team',
                                       style: TextStyle(fontSize: 15,color: Colors.grey),
                                       maxLines: 2,
                                     )
                                   ],
                                 ),
                               )
                             ],
                            ),
                          ),
                      ),
                      SizedBox(
                        height: size.height/30,
                      ),
                      ElevatedButton(
                        onPressed: () {
                          Get.to(TeachersVol());
                        },
                        child: Container(
                          height: size.height/5,
                          width: size.width-20,
                          decoration: BoxDecoration(
                              color: Colors.white,
                              boxShadow: const [
                                kDefaultShadow
                              ],
                              borderRadius: BorderRadius.circular(20)
                          ),
                          child:  Column(
                            children: [
                              Padding(
                                padding:  EdgeInsets.symmetric(horizontal: size.width/4
                                    ,vertical:size.width/10 ),
                                child: Column(
                                  children: [
                                    Text("Teachers Team",style: TextStyle(
                                        color: kstorm,
                                        fontSize: 20,
                                        fontWeight: FontWeight.bold
                                    ),),
                                    SizedBox(
                                      height: MediaQuery.of(context).size.height/50,
                                    ),
                                    Text(
                                      'this category shows you Teachers team',
                                      style: TextStyle(fontSize: 15,color: Colors.grey),

                                      maxLines: 2,
                                    )
                                  ],
                                ),
                              )
                            ],
                          ),
                        ),
                      ),

                                ]
                  ),
                ),
            ],
          ),
        ),
      ),
    );
  }
}

//Container(
//               width: size.width,
//               height: (size.height * 3) / 3.85,
//               alignment: Alignment.center,
//               child: Padding(
//                 padding: EdgeInsets.symmetric(
//                     horizontal: size.width / 10, vertical: (size.height) / 10),
//                 child: Swiper(
//                   itemCount: routeName.length,
//                   itemWidth: double.maxFinite,
//                   itemHeight: double.maxFinite,
//                   pagination: const SwiperPagination(
//                       builder: DotSwiperPaginationBuilder()),
//                   layout: SwiperLayout.TINDER,
//                   itemBuilder: (_, index) => Stack(
//                     clipBehavior: Clip.none,
//                     children: [
//                       GestureDetector(
//                         onTap: () => Get.toNamed(routeName[index]),
//                         child: Container(
//                           decoration: BoxDecoration(
//                             color: mainColor,
//                             borderRadius: BorderRadius.circular(50),
//                           ),
//                           child: Center(
//                             child: Text(
//                               categoriesName[index],
//                               style: kHeading2,
//                             ),
//                           ),
//                         ),
//                       ),
//                       Positioned(
//                         top: -60,
//                         child: Lottie.asset(
//                           categoriesLottie[index],
//                           width: 200,
//                           height: 200,
//                         ),
//                       )
//                     ],
//                   ),
//                 ),
//               ),
//             )

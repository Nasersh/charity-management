class ResignVolunteer{
  String id;
  String birth_date;
  String gender;
  String job;
  String phone;
  String email;
  String name;

  ResignVolunteer(
      this.id,
      this.birth_date,
      this.gender,
      this.job,
      this.phone,
      this.email,
      this.name
      );

}

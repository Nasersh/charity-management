import 'package:charitysys/dashboard/volunteer/resign_volunteer/resign_volunteer_services.dart';
import 'package:get/get.dart';


class ResignController extends GetxController{


  ResignVolunteerService service = ResignVolunteerService();
  // late List<VolunteerModel>
  RxList<dynamic> resignDemands = [].obs;
  RxString status = ''.obs;
  RxBool isTrue = false.obs;
  RxBool isFalse = false.obs;
  RxBool isLoaded = false.obs;
  @override
  void onInit() async {
    // TODO: implement onInit

    await allResignDemands();

    super.onInit();
  }

  Future<void> accResignVolunteer(String id) async {

    status.value = await service.accResignVolunteer(id);

  }

  Future<void> rejResignVolunteer(String id) async {

    status.value = await service.rejResignDemand(id);

  }

  Future<void> allResignDemands() async {
    //isLoaded.value = false;
    var vol =  await service.allResignDemands();
    resignDemands.value = vol;
    //isLoaded.value = true;
    isLoaded(true);
    print("yyyyyyyyyyyyyes ${resignDemands.length}");
  }

}
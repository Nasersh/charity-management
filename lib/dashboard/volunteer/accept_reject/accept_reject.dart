import 'dart:async';
import 'package:charitysys/dashboard/shwimmer.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:lottie/lottie.dart';
import '../../../configrations/consts.dart';
import '../../../configrations/functions_status/functions_status.dart';
import 'accept_reject_controller.dart';

class AcceptReject extends StatefulWidget {
  const AcceptReject({Key? key}) : super(key: key);

  @override
  _AcceptRejectState createState() => _AcceptRejectState();
}

class _AcceptRejectState extends State<AcceptReject> {
  @override
  Widget build(BuildContext context) {
    AcceptRejectController controller =
        Get.put<AcceptRejectController>(AcceptRejectController());
    Size size = MediaQuery.of(context).size;

    return SafeArea(
      child: Scaffold(
        body: Obx(
          () => Column(
            children: [
              Container(
                height: size.height / 4,
                decoration: BoxDecoration(
                  color: mainColor,
                  borderRadius: const BorderRadius.only(
                    bottomRight: Radius.circular(60),
                    bottomLeft: Radius.circular(60),
                  ),
                ),
                child: Center(
                  child: Text(
                    "Volunteer Demands".tr,
                    style: const TextStyle(color: Colors.white, fontSize: 30),
                  ),
                ),
              ),
              controller.isLoaded.value
                  ? Expanded(
                      flex: 3,
                      child: SizedBox(
                        child: ListView.builder(
                          itemBuilder: (_, index) => Column(
                            children: [
                              Container(
                                padding: const EdgeInsets.symmetric(
                                  horizontal: 15,
                                  vertical: 5,
                                ),
                                width: size.width,
                                height: size.height / 5,
                                decoration: BoxDecoration(
                                  gradient: LinearGradient(
                                    colors: [
                                      Colors.white,
                                      Colors.white.withOpacity(.2)
                                    ],
                                    begin: Alignment.topLeft,
                                    end: Alignment.bottomRight,
                                  ),
                                ),
                                child: Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    Container(
                                      alignment: Alignment.center,
                                      width: size.width / 5,
                                      height: size.height / 5,
                                      decoration: BoxDecoration(
                                          color: mainColor,
                                          shape: BoxShape.circle),
                                      child: Text(
                                        "${index + 1}",
                                        style: ksubTitle,
                                      ),
                                    ),
                                    Container(
                                      //color: Colors.red,
                                      padding: EdgeInsets.symmetric(
                                        vertical: size.width / 15,
                                      ),
                                      child: Column(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceBetween,
                                        children: [
                                          Text(
                                            controller.volunteers[index].job,
                                            style: const TextStyle(
                                                fontSize: 18,
                                                fontWeight: FontWeight.bold),
                                          ),
                                          Text(
                                            controller.volunteers[index].phone,
                                            textAlign: TextAlign.center,
                                          ),
                                          GestureDetector(
                                            onTap: () {
                                              Get.bottomSheet(
                                                Container(
                                                  decoration: BoxDecoration(
                                                    color: mainColor,
                                                    borderRadius:
                                                        const BorderRadius.only(
                                                      topRight:
                                                          Radius.circular(50),
                                                      topLeft:
                                                          Radius.circular(50),
                                                    ),
                                                  ),
                                                  height: 200,
                                                  child: Row(
                                                    mainAxisAlignment:
                                                        MainAxisAlignment
                                                            .spaceBetween,
                                                    children: [
                                                      SizedBox(
                                                        width: size.width / 3,
                                                        child: Image.asset(
                                                            'assets/stationery.png'
                                                            //controller.volunteers[index].image
                                                            ),
                                                      ),
                                                      SizedBox(
                                                        width: size.width / 1.5,
                                                        child: Column(
                                                          mainAxisAlignment:
                                                              MainAxisAlignment
                                                                  .center,
                                                          children: [
                                                            Text(
                                                              controller
                                                                  .volunteers[
                                                                      index]
                                                                  .job,
                                                              style: const TextStyle(
                                                                  color: Colors
                                                                      .white,
                                                                  fontSize: 18),
                                                            ),
                                                            Text(
                                                              controller
                                                                  .volunteers[
                                                                      index]
                                                                  .phone,
                                                              style: const TextStyle(
                                                                  color: Colors
                                                                      .white,
                                                                  fontSize: 18),
                                                            ),
                                                            Text(
                                                              controller
                                                                  .volunteers[
                                                                      index]
                                                                  .gender,
                                                              style: const TextStyle(
                                                                  color: Colors
                                                                      .white,
                                                                  fontSize: 18),
                                                            ),
                                                            Text(
                                                              controller
                                                                  .volunteers[
                                                                      index]
                                                                  .birth_date,
                                                              style: const TextStyle(
                                                                  color: Colors
                                                                      .white,
                                                                  fontSize: 18),
                                                            ),
                                                          ],
                                                        ),
                                                      )
                                                    ],
                                                  ),
                                                ),
                                              );
                                            },
                                            child: Container(
                                              padding:
                                                  const EdgeInsets.symmetric(
                                                      horizontal: 15,
                                                      vertical: 10),
                                              decoration: BoxDecoration(
                                                  color: mainColor,
                                                  borderRadius:
                                                      BorderRadius.circular(
                                                          16)),
                                              child: Text(
                                                "Details".tr,
                                                style: const TextStyle(
                                                    fontSize: 11,
                                                    color: Colors.white),
                                              ),
                                            ),
                                          )
                                        ],
                                      ),
                                    ),
                                    Column(
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      children: [
                                        Row(
                                          children: [
                                            GestureDetector(
                                              onTap: () {
                                                print('object');
                                                Timer(
                                                    const Duration(seconds: 0),
                                                    () async {
                                                  await controller.accVolunteer(
                                                      controller
                                                          .volunteers[index]
                                                          .id);
                                                  await controller
                                                      .allVolunteers();
                                                  if (controller.status.value ==
                                                      'OK') {
                                                    controller.isTrue.value =
                                                        true;
                                                    Get.to(FunctionStatus(
                                                      controller: controller,
                                                      title: "Guarantee".tr,
                                                      function: "accepted".tr,
                                                    ));
                                                  } else {
                                                    print('error');
                                                    controller.isFalse.value =
                                                        true;
                                                    Get.to(FunctionStatus(
                                                      controller: controller,
                                                      title: "Guarantee".tr,
                                                      function: "accepted".tr,
                                                    ));
                                                  }
                                                });
                                              },
                                              child: Container(
                                                width: 35,
                                                height: 35,
                                                decoration: BoxDecoration(
                                                    shape: BoxShape.circle,
                                                    color: mainColor),
                                                child: Lottie.asset(
                                                    'assets/ok_not/33886-check-okey-done.json'),
                                              ),
                                            ),
                                            const SizedBox(
                                              width: 6,
                                            ),
                                            GestureDetector(
                                              onTap: () {
                                                Timer(
                                                    const Duration(seconds: 0),
                                                    () async {
                                                  await controller.rejVolunteer(
                                                      controller
                                                          .volunteers[index]
                                                          .id);
                                                  await controller
                                                      .allVolunteers();
                                                  if (controller.status.value ==
                                                      'OK') {
                                                    controller.isTrue.value =
                                                        true;
                                                    Get.to(FunctionStatus(
                                                      controller: controller,
                                                      title: "Guarantee".tr,
                                                      function: "rejected".tr,
                                                    ));
                                                  } else {
                                                    print('error');
                                                    controller.isFalse.value =
                                                        true;
                                                    Get.to(FunctionStatus(
                                                      controller: controller,
                                                      title: "Guarantee",
                                                      function: "rejected",
                                                    ));
                                                  }
                                                });
                                              },
                                              child: Container(
                                                width: 35,
                                                height: 35,
                                                decoration: BoxDecoration(
                                                  shape: BoxShape.circle,
                                                  color: mainColor,
                                                ),
                                                child: Lottie.asset(
                                                    'assets/ok_not/74894-failure-feedback-rejected.json'),
                                              ),
                                            )
                                          ],
                                        )
                                      ],
                                    )
                                  ],
                                ),
                              ),
                              Divider(
                                thickness: 1,
                                color: mainColor,
                              ),
                            ],
                          ),
                          itemCount: controller.volunteers.length,
                        ),
                      ),
                    )
                  :  Expanded(
                      child: Align(
                        alignment: Alignment.center,
                        child: ShimmerWidget(),
                      ),
                    ),
            ],
          ),
        ),
      ),
    );
  }
}

//Container(
//                                           decoration: const BoxDecoration(
//                                             color: Colors.grey,
//                                             borderRadius: BorderRadius.only(
//                                               topRight: Radius.circular(50),
//                                               topLeft: Radius.circular(50)
//                                             )
//                                           ),
//                                           height: 200,
//                                           child: Row(
//                                             mainAxisAlignment: MainAxisAlignment.spaceBetween,
//                                             children: [
//                                               SizedBox(
//                                                 width: size.width / 3,
//                                                 child: Image.asset('assets/stationery.png'),
//                                               ),
//                                               SizedBox(
//                                                 width: size.width / 1.5,
//                                                 child: Column(
//                                                   mainAxisAlignment: MainAxisAlignment.center,
//                                                   children: const [
//                                                     Text(
//                                                       "job : teacher",
//                                                       style: TextStyle(
//                                                         color: Colors.white,
//                                                         fontSize: 18
//                                                       ),
//                                                     ),
//                                                     Text(
//                                                       "phone : 0934212063",
//                                                       style: TextStyle(
//                                                           color: Colors.white,
//                                                           fontSize: 18
//                                                       ),
//                                                     ),
//                                                     Text(
//                                                       "gender : male",
//                                                       style: TextStyle(
//                                                           color: Colors.white,
//                                                           fontSize: 18
//                                                       ),
//                                                     ),
//                                                     Text(
//                                                       "birth : 1/4/2001",
//                                                       style: TextStyle(
//                                                           color: Colors.white,
//                                                           fontSize: 18
//                                                       ),
//                                                     ),
//                                                   ],
//                                                 ),
//                                               )
//                                             ],
//                                           ),
//                                         )

import 'package:get/get.dart';
import 'accept_reject_service.dart';

class AcceptRejectController extends GetxController{


  AcceptRejectService service = AcceptRejectService();
  // late List<VolunteerModel>
  RxList<dynamic> volunteers = [].obs;
  RxString status = ''.obs;
  RxBool isTrue = false.obs;
  RxBool isFalse = false.obs;
  RxBool isLoaded = false.obs;
  @override
  void onInit() async {
    // TODO: implement onInit

    await allVolunteers();

    super.onInit();
  }

  Future<void> accVolunteer(int id) async {

    status.value = await service.accVolunteer(id);

  }

  Future<void> rejVolunteer(int id) async {

    status.value = await service.rejVolunteer(id);

  }

  Future<void> allVolunteers() async {
    //isLoaded.value = false;
    var vol =  await service.allVolunteer();
    volunteers.value = vol;
    //isLoaded.value = true;
    isLoaded(true);
    print("yyyyyyyyyyyyyes ${volunteers.length}");
  }

}
import 'dart:convert' show json;
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:http/http.dart' as http;
// <<<<<<< HEAD
// =======
import '../../../../config/config.dart';
// >>>>>>> 2c6a0d2 (new file)
import 'complete_guarantee_model.dart';

class CompleteGuaranteeServices {
  var message;
  var url = Uri.parse(
// <<<<<<< HEAD
      ServerConfig.domainNameServer + ServerConfig.completeGuarantee);
// =======
//       ServerConfig.domainNameServer + ServerConfig.completeGuarantee);
// >>>>>>> 2c6a0d2 (new file)
  FlutterSecureStorage storage = const FlutterSecureStorage();

  Future<List<CompleteGuarantee>> getGuarantees() async {
    String? token = await storage.read(key: 'token');
    print(token);
    var response = await http.get(
      url,
      headers: {'Accept': 'application/json', 'Authorization': 'Bearer $token'},
    );

    print(response.body);
    if (response.statusCode == 200) {
      List<CompleteGuarantee> inComGuarantee =
          combineGuarantee(json.decode(response.body));
      print(inComGuarantee);
      return inComGuarantee;
    } else {
      return [];
    }
  }

  List<CompleteGuarantee> combineGuarantee(var response) {
    List<CompleteGuarantee> inComGuarantees = [];

    for (int i = 0; i < response.length; i++) {
      inComGuarantees.add(CompleteGuarantee(
        response[i]['name'],
        response[i]['cost'],
        response[i]['birth_date'],
        response[i]['academic_year'],
        response[i]['image'],
        response[i]['id'],
      ));
    }
    return inComGuarantees;
  }

  Future<String> deleteGuarantee(int id) async {
    String? token = await storage.read(key: 'token');
    print(token);

    var url = Uri.parse(
// <<<<<<< HEAD
        ServerConfig.domainNameServer + ServerConfig.deleteGuarantee + '$id');
// =======
//         ServerConfig.domainNameServer + ServerConfig.deleteGuarantee + '$id');
// >>>>>>> 2c6a0d2 (new file)

    print(id);

    var response = await http.delete(url, headers: {
      'Authorization': 'Bearer $token ',
      'Accept': 'application/json'
    });

    String getResponse = response.body;
    print(getResponse);
    if (response.statusCode == 200) {
      return 'OK';
    } else {
      return 'NO';
    }
  }

}

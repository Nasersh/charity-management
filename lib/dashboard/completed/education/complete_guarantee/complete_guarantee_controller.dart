import 'package:get/get.dart';
import '../../../incomplete/Education/guarantee/guarantee_consts.dart';
import 'complete_guarantee_model.dart';
import 'complete_guarantee_services.dart';


  class CompleteGuaranteeController extends GetxController
{
  var guaranteeList =<CompleteGuarantee>[].obs;
  var guarantees=<CompleteGuarantee>[];
  RxBool isTrue = false.obs;
  RxString image = ''.obs;
  RxInt currentItem = 0.obs;
  RxInt currentSortItem = 0.obs;
  RxBool isExpand = false.obs;
  RxString academicYear = 'first'.obs;
  RxString sortSelected = 'id'.obs;
  RxString searchValue = ''.obs;

  var id=0.obs;

  CompleteGuaranteeServices service = CompleteGuaranteeServices();
  var isloading=true.obs;

  @override
  void onInit() {
    showStationers();
    //loadProductsFromRepo(id.value);
    super.onInit();
  }

  void showStationers() async {
    try {
      isloading(true);
      guarantees = await service.getGuarantees();
      if (guarantees != null) {
        guaranteeList.value = guarantees;
      }
    } finally {
      isloading(false);
    }
  }


  void changeItem(int index){
    print(index);

    currentItem.value = index;
    academicYear.value = academicYears[index];

  }

  Future<String> deleteGuarantee(int id) async {

    String save;

    try {
      isloading(true);
      save = await service.deleteGuarantee(id);
    }finally {
      isloading(false);
    }
    return save;
  }


}
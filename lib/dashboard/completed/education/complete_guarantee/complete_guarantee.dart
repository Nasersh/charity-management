import 'dart:async';
import 'dart:io';
import 'dart:math';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:get/get.dart';
import 'package:get/get_core/src/get_main.dart';
import 'package:percent_indicator/linear_percent_indicator.dart';
import '../../../../configrations/functions_status/functions_status.dart';
import '../../../incomplete/Education/guarantee/guarantee_consts.dart';
import 'complete_guarantee_controller.dart';
import 'consts.dart';

class CompleteGuarantee extends StatelessWidget {
  CompleteGuarantee({Key? key}) : super(key: key);

  CompleteGuaranteeController controller =
      Get.put(CompleteGuaranteeController());
  Rx<DateTime> currentDate = DateTime.now().obs;

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;

    return Obx(
      () => controller.isloading.value
          ? const SafeArea(
              child: Scaffold(body: Center(child: CircularProgressIndicator())))
          : Scaffold(
              backgroundColor: kprimary,
              body: SafeArea(
                child: Obx(
                  () => Column(
                    children: [
                      Stack(children: [
                        Container(
                          width: double.infinity,
                          height: MediaQuery.of(context).size.height / 4,
                          decoration: const BoxDecoration(
                              color: kblue,
                              borderRadius: BorderRadius.only(
                                  bottomLeft: Radius.circular(30),
                                  bottomRight: Radius.circular(30))),
                        ),
                        Column(
                          children: [
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Padding(
                                  padding: const EdgeInsets.only(left: 15),
                                  child: IconButton(
                                    icon: const Icon(
                                      FontAwesomeIcons.arrowLeft,
                                      color: kprimary,
                                    ),
                                    // Icons.arrow_back,
                                    onPressed: () {
                                      Get.back();
                                    },
                                  ),
                                ),
                              ],
                            ),
                            SizedBox(
                              height: MediaQuery.of(context).size.height / 90,
                            ),
                            const Padding(
                              padding: EdgeInsets.only(bottom: 10, left: 40),
                              child: Align(
                                alignment: Alignment.bottomLeft,
                                child: Text(
                                  'Guarantees . . .',
                                  style: TextStyle(
                                    color: kprimary,
                                    fontSize: 30,
                                    fontStyle: FontStyle.italic,
                                    fontWeight: FontWeight.bold,
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ]), Expanded(
                              child: Column(
                              children: [
                                const SizedBox(height: 20),
                                Expanded(
                                    flex: 8,
                                    child: _buildProductsList(size, context)),
                              ],
                            )),
                    ],
                  ),
                ),
              ),
            ),
    );
  }

  Widget _buildProductsList(Size size, BuildContext context) {
    // Random rnd;
    // double min = 0;
    // rnd = Random();
    // double r = min + rnd.nextDouble();
    return ListView.builder(
      shrinkWrap: true,
      itemCount: controller.guaranteeList.length,
      padding: const EdgeInsets.only(top: 16),
      itemBuilder: (context, index) => Padding(
        padding: const EdgeInsets.symmetric(vertical: 10),
        child: Container(
          height: MediaQuery.of(context).size.height / 6,
          decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(15),
              boxShadow: [kDefaultShadow]),
          child: Padding(
            padding: const EdgeInsets.symmetric(vertical: 8.0),
            child: SingleChildScrollView(
              child: ExpansionTile(
                  leading: const CircleAvatar(
                    radius: 30,
                    backgroundColor: Colors.black45,
                  ),
                  title: Padding(
                    padding: const EdgeInsets.only(top: 10),
                    child: Text(
                      controller.guaranteeList[index].name,
                      style: const TextStyle(
                        color: Colors.black,
                      ),
                    ),
                  ),
                  subtitle: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Padding(
                        padding: const EdgeInsets.only(top: 5, bottom: 10),
                        child: Text('${controller.guaranteeList[index].cost}',
                            style: const TextStyle(color: Colors.black)),
                      ),
                      // LinearPercentIndicator(
                      //   barRadius: const Radius.circular(040),
                      //   width: MediaQuery.of(context).size.width / 2,
                      //   animation: true,
                      //   animationDuration: 800,
                      //   lineHeight: 10,
                      //   percent: r,
                      //   progressColor: kblue,
                      //   backgroundColor: kbblue,
                      // ),
                      SizedBox(
                        height: MediaQuery.of(context).size.height / 100,
                      ),
                      Text(
                        '${controller.guaranteeList[index].cost} \$',
                        style: const TextStyle(color: kstorm),
                      ),
                    ],
                  ),
                  trailing: deleteButtonEffect(index, context, size),
                  children: [
                    Stack(
                      children: [
                        Align(
                          alignment: const Alignment(0, 0),
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Padding(
                                padding: const EdgeInsets.all(12.0),
                                child: SizedBox(
                                  width: size.width / 2 + size.width / 20,
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Text(
                                        controller
                                            .guaranteeList[index].birthDate,
                                        style: const TextStyle(color: kstorm),
                                      ),
                                      Text(
                                        '${controller.guaranteeList[index].cost}/academic_year : ${controller.guaranteeList[index].academicYear}',
                                        style: const TextStyle(color: kstorm),
                                      ),
                                      const SizedBox(
                                        height: 10,
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ]),
            ),
          ),
        ),
      ),
    );
  }

  Container deleteButtonEffect(int index, BuildContext context, Size size) {
    return Container(
      height: 50,
      width: 75,
      decoration: BoxDecoration(
        color: kstorm,
        borderRadius: BorderRadius.circular(10),
      ),
      child: MaterialButton(
        onPressed: () {
          var id = controller.guaranteeList[index].id;
          showModalBottomSheet(
            constraints: const BoxConstraints(maxHeight: 200),
            context: context,
            builder: (BuildContext context) {
              return Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  const Text(
                    "Would yo like to delete this element ? ",
                    style: TextStyle(
                        color: Colors.black54, fontWeight: FontWeight.bold),
                    //textAlign: TextAlign.center,
                  ),
                  const SizedBox(
                    height: 15,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      GestureDetector(
                        onTap: () => {
                          Timer(const Duration(seconds: 0), () async {
                            String save = await controller.deleteGuarantee(id);
                            if (save == 'OK') {
                              controller.isTrue.value = true;
                              Get.back();
                              Get.to(FunctionStatus(
                                controller: controller,
                                title: "Guarantee",
                                function: "deleted",
                              ));
                              controller.showStationers();
                            } else {
                              print('error');
                              controller.isTrue.value = false;
                              Get.to(FunctionStatus(
                                controller: controller,
                                title: "Guarantee",
                                function: "deleted",
                              ));
                            }
                          })
                        },
                        child: Container(
                          alignment: Alignment.center,
                          width: 80,
                          height: 50,
                          decoration: BoxDecoration(
                            color: kstorm,
                            borderRadius: BorderRadius.circular(10),
                          ),
                          child: const Text(
                            "Yes",
                            style: TextStyle(
                                color: Colors.white,
                                fontWeight: FontWeight.bold,
                                fontSize: 22),
                          ),
                        ),
                      ),
                      const SizedBox(
                        width: 15,
                      ),
                      GestureDetector(
                        onTap: () => Get.back(),
                        child: Container(
                          width: 80,
                          height: 50,
                          alignment: Alignment.center,
                          decoration: BoxDecoration(
                            color: kprimary,
                            borderRadius: BorderRadius.circular(10),
                          ),
                          child: const Text(
                            "No",
                            style: TextStyle(
                                color: Colors.white,
                                fontWeight: FontWeight.bold,
                                fontSize: 22),
                          ),
                        ),
                      ),
                    ],
                  ),
                ],
              );
            },
          );
        },
        child: const Center(
          child: Text(
            'Delete',
            style: TextStyle(color: kprimary, fontSize: 12.2),
          ),
        ),
      ),
    );
  }

  Future<void> openCylinder(BuildContext context) async {
    final DateTime? pickedDate = await showDatePicker(
        context: context,
        initialDate: currentDate.value,
        firstDate: currentDate.value,
        lastDate: DateTime(2050));
    if (pickedDate != null && pickedDate != currentDate.value) {
      currentDate.value = pickedDate;
    }
  }

  Container CameraStudio(IconData icon, Size size) {
    return Container(
      margin: const EdgeInsets.symmetric(horizontal: 5, vertical: 10),
      child: Icon(
        icon,
        size: 30,
        color: kprimary,
      ),
      width: size.width * .3,
      height: 120,
      decoration: BoxDecoration(
        color: Colors.white.withOpacity(.2),
        border: Border.all(
          width: 2,
          color: kprimary,
        ),
        borderRadius: BorderRadius.circular(20),
      ),
    );
  }

  Widget getGuaranteeInput(int index, Size size, formKey) {
    return Container(
      alignment: Alignment.center,
      width: size.width * .8,
      height: 78,
      margin: const EdgeInsets.symmetric(horizontal: 20, vertical: 15),
      padding: const EdgeInsets.symmetric(horizontal: 10),
      decoration: BoxDecoration(
          color: Colors.grey.withOpacity(.2),
          borderRadius: BorderRadius.circular(10)),
      child: TextFormField(
        //key: formKey,
        controller: guaranteeEditors[index],
        decoration: InputDecoration(
          labelText: guaranteeAttributeName[index],
          border: InputBorder.none,
          icon: guaranteeIcons[index],
          iconColor: Colors.red,
          labelStyle: const TextStyle(
              fontSize: 16, fontWeight: FontWeight.w600, color: kprimary),
        ),
        maxLines: index == 3 ? 4 : 1,
        keyboardType: guaranteeTypes[index],
        validator: (value) {
          if (index == 0 && value != null && value.length < 7) {
            return guaranteeErrorText[0];
          } else if (index == 1 && value != null && int.parse(value) <= 1000) {
            return guaranteeErrorText[1];
          }
        },
      ),
    );
  }
}

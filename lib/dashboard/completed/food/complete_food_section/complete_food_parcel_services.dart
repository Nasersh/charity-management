import 'dart:convert';
// <<<<<<< HEAD

import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:http/http.dart' as http;

// =======
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:http/http.dart' as http;
import '../../../../config/config.dart';
// >>>>>>> 2c6a0d2 (new file)
import 'complete_food_parcel_model.dart';


class CompleteFoodSectionServices {
  var message;
  var url = Uri.parse(
// <<<<<<< HEAD
      ServerConfig.domainNameServer + ServerConfig.completeFoodSection);
// =======
//       ServerConfig.domainNameServer + ServerConfig.completeFoodSection);
// >>>>>>> 2c6a0d2 (new file)
  FlutterSecureStorage storage = const FlutterSecureStorage();

  Future<List<CompleteFoodSectionModel>> getFoodItems() async {

    String? token = await storage.read(key: 'token');
    print(token);
    var response = await http.get(
      url,
      headers: {'Accept': 'application/json', 'Authorization': 'Bearer $token'},
    );

    print(response.body);
    if (response.statusCode == 200) {
      List<CompleteFoodSectionModel> inComFoodItems = combineFoodItems(json.decode(response.body));
      print(inComFoodItems);
      return inComFoodItems;
    } else {
      return [];
    }
  }

  List<CompleteFoodSectionModel> combineFoodItems(List<dynamic> response) {
    List<CompleteFoodSectionModel> inComFoodItems = [];

    for (int i = 0; i < response.length; i++) {

      print(i);
      inComFoodItems.add(CompleteFoodSectionModel(
        response[i]['name'],
        response[i]['cost'],
        response[i]['count'],
        response[i]['image'],
        response[i]['donate'],
        response[i]['id'],
      ));

    }
    return inComFoodItems;
  }


  Future<String> deleteFoodItem(int id) async {

    String? token = await storage.read(key: 'token');
    print(token);

    var url = Uri.parse(
// <<<<<<< HEAD
        ServerConfig.domainNameServer + ServerConfig.deleteFoodSection + '$id');
// =======
//         ServerConfig.domainNameServer + ServerConfig.deleteFoodSection + '$id');
// >>>>>>> 2c6a0d2 (new file)

    print('id : $id');

    var response = await http.delete(url, headers: {
      'Authorization': 'Bearer $token ',
      'Accept': 'application/json'
    });

    String getResponse = response.body;
    print(getResponse);
    if (response.statusCode == 200) {
      return 'OK';
    } else {
      return 'NO';
    }
  }


  Future<String> deleteAllFoodItem() async {

    String? token = await storage.read(key: 'token');
    print(token);

    var url = Uri.parse(
// <<<<<<< HEAD
        ServerConfig.domainNameServer + ServerConfig.deleteCompleteFoodSection);
// =======
//         ServerConfig.domainNameServer + ServerConfig.deleteCompleteFoodSection);
// >>>>>>> 2c6a0d2 (new file)

    //print('id : $id');

    var response = await http.delete(url, headers: {
      'Authorization': 'Bearer $token ',
      'Accept': 'application/json'
    });

    String getResponse = response.body;
    print(getResponse);
    if (response.statusCode == 200) {
      return 'OK';
    } else {
      return 'NO';
    }
  }


}

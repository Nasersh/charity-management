import 'package:get/get.dart';

import 'complete_food_parcel_model.dart';
import 'complete_food_parcel_services.dart';


class CompleteFoodSectionController extends GetxController
{
  var foodItemsList =<CompleteFoodSectionModel>[].obs;
  var foodItems=<CompleteFoodSectionModel>[];
  RxBool isTrue = false.obs;

  var id=0.obs;

  CompleteFoodSectionServices service = CompleteFoodSectionServices();
  var isloading=true.obs;

  @override
  void onInit() {
    showFoodItems();
    //loadProductsFromRepo(id.value);
    super.onInit();
  }

  void showFoodItems() async {
    try {
      isloading(true);
      foodItems = await service.getFoodItems();
      foodItems.forEach((element) {print(element.donate);});
      if (foodItems != null) {
        foodItemsList.value = foodItems;
      }
    } finally {
      isloading(false);
    }
  }


  Future<String> deleteFoodItem(int id) async {

    String save;

    try {
      isloading(true);
      save = await service.deleteFoodItem(id);
    }finally {
      isloading(false);
    }
    return save;
  }

  Future<String> deleteAllFoodItem() async {

    String save;

    try {
      isloading(true);
      save = await service.deleteAllFoodItem();
    }finally {
      isloading(false);
    }
    return save;
  }


}
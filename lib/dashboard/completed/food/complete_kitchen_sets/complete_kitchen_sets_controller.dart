import 'package:get/get.dart';

import 'complete_kitchen_sets_model.dart';
import 'complete_kitchen_sets_services.dart';


class CompleteKitchenSetController extends GetxController
{
  var kitchenList =<CompleteKitchenSetModel>[].obs;
  var kitchenItems=<CompleteKitchenSetModel>[];
  RxBool isTrue = false.obs;

  var id=0.obs;

  CompleteKitchenSetServices service = CompleteKitchenSetServices();
  var isloading=true.obs;

  @override
  void onInit() {
    showKitchenItem();
    //loadProductsFromRepo(id.value);
    super.onInit();
  }

  void showKitchenItem() async {
    try {
      isloading(true);
      kitchenItems = await service.getKitchenItems();
      kitchenItems.forEach((element) {print(element.donate);});
      if (kitchenItems != null) {
        kitchenList.value = kitchenItems;
      }
    } finally {
      isloading(false);
    }
  }


  Future<String> deleteKitchenItem(int id) async {

    String save;

    try {
      isloading(true);
      save = await service.deleteKitchenItem(id);
    }finally {
      isloading(false);
    }
    return save;
  }

  Future<String> deleteAllKitchenItem() async {

    String save;

    try {
      isloading(true);
      save = await service.deleteAllKitchenItem();
    }finally {
      isloading(false);
    }
    return save;
  }


}
class CompleteMedicalTollsModel {
  late String name;
  late int cost;
  late int count;
  late String image;
  late int id;
  late int donate;

  CompleteMedicalTollsModel(
      this.name,
      this.cost,
      this.count,
      this.image,
      this.donate,
      this.id
      );
}

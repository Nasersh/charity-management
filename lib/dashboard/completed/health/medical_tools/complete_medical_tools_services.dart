import 'dart:convert';

import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:http/http.dart' as http;
// <<<<<<< HEAD

// =======
import '../../../../config/config.dart';
// >>>>>>> 2c6a0d2 (new file)
import 'complete_medical_tools_model.dart';

class CompleteMedicalToolsServices {
  var message;
  var url = Uri.parse(
// <<<<<<< HEAD
      ServerConfig.domainNameServer + ServerConfig.completeMedical);
// =======
//       ServerConfig.domainNameServer + ServerConfig.completeMedical);
// >>>>>>> 2c6a0d2 (new file)
  FlutterSecureStorage storage = const FlutterSecureStorage();

  Future<List<CompleteMedicalTollsModel>> getMedicalTools() async {

    String? token = await storage.read(key: 'token');
    print(token);
    var response = await http.get(
      url,
      headers: {'Accept': 'application/json', 'Authorization': '$token'},
    );

    print(response.body);
    if (response.statusCode == 200) {
      List<CompleteMedicalTollsModel> inComMedicines = combineMedicines(json.decode(response.body));
      print(inComMedicines);
      return inComMedicines;
    } else {
      return [];
    }
  }

  List<CompleteMedicalTollsModel> combineMedicines(List<dynamic> response) {
    List<CompleteMedicalTollsModel> inComMedicines = [];

    for (int i = 0; i < response.length; i++) {

      print(i);
      inComMedicines.add(CompleteMedicalTollsModel(
        response[i]['name'],
        response[i]['cost'],
        response[i]['count'],
        response[i]['image'],
        response[i]['donate'],
        response[i]['id'],
      ));

    }
    return inComMedicines;
  }
  Future<String> deleteMedicalTool(int id) async {
    String? token = await storage.read(key: 'token');
    print(token);

    var url = Uri.parse(
// <<<<<<< HEAD
        ServerConfig.domainNameServer + ServerConfig.deleteMedicalTool + '$id');
// =======
//         ServerConfig.domainNameServer + ServerConfig.deleteMedicalTool + '$id');
// >>>>>>> 2c6a0d2 (new file)

    var response = await http.delete(url, headers: {
      'Authorization': 'Bearer $token ',
      'Accept': 'application/json'
    });

    String getResponse = response.body;
    print(getResponse);
    if (response.statusCode == 200) {
      return 'OK';
    } else {
      return 'NO';
    }
  }


  Future<String> deleteAllMedicalTool() async {
    String? token = await storage.read(key: 'token');
    print(token);

    var url = Uri.parse(
// <<<<<<< HEAD
        ServerConfig.domainNameServer + ServerConfig.deleteCompleteMedicalTool);
// =======
//         ServerConfig.domainNameServer + ServerConfig.deleteCompleteMedicalTool);
// >>>>>>> 2c6a0d2 (new file)

    var response = await http.delete(url, headers: {
      'Authorization': 'Bearer $token ',
      'Accept': 'application/json'
    });

    String getResponse = response.body;
    print(getResponse);
    if (response.statusCode == 200) {
      return 'OK';
    } else {
      return 'NO';
    }
  }


}

import 'package:get/get.dart';

import 'complete_medical_tools_model.dart';
import 'complete_medical_tools_services.dart';


class CompleteMedicalToolsController extends GetxController
{
  var medicalList =<CompleteMedicalTollsModel>[].obs;
  var medicals=<CompleteMedicalTollsModel>[];
  RxBool isTrue = false.obs;

  var id=0.obs;

  CompleteMedicalToolsServices service = CompleteMedicalToolsServices();
  var isloading=true.obs;

  @override
  void onInit() {
    showMedical();
    //loadProductsFromRepo(id.value);
    super.onInit();
  }

  void showMedical() async {
    try {
      isloading(true);
      medicals = await service.getMedicalTools();
      if (medicals != null) {
        medicalList.value = medicals;
      }
    } finally {
      isloading(false);
    }
  }


  Future<String> deleteMedicalTool(int id) async {

    String save;

    try {
      isloading(true);
      save = await service.deleteMedicalTool(id);
    }finally {
      isloading(false);
    }
    return save;
  }


  Future<String> deleteAllMedicalTool() async {

    String save;

    try {
      isloading(true);
      save = await service.deleteAllMedicalTool();
    }finally {
      isloading(false);
    }
    return save;
  }



}
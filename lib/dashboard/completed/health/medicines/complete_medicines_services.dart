import 'dart:convert';
// <<<<<<< HEAD

import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:http/http.dart' as http;
// =======
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:http/http.dart' as http;
import '../../../../config/config.dart';
// >>>>>>> 2c6a0d2 (new file)
import 'medicine_model.dart';

class CompleteMedicinesServices {
  var message;
  var url = Uri.parse(
// <<<<<<< HEAD
      ServerConfig.domainNameServer + ServerConfig.completeMedicine);
// =======
//
//       ServerConfig.domainNameServer + ServerConfig.completeMedicine);
// >>>>>>> 2c6a0d2 (new file)
  FlutterSecureStorage storage = const FlutterSecureStorage();

  Future<List<CompleteMedicineModel>> getMedicines() async {
    String? token = await storage.read(key: 'token');
    print(token);
    var response = await http.get(
      url,
      headers: {'Accept': 'application/json', 'Authorization': 'Bearer $token'},
    );

    print(response.body);
    if (response.statusCode == 200) {
       List<CompleteMedicineModel> inComMedicines = combineMedicines(json.decode(response.body));
       print(inComMedicines);
       return inComMedicines;
    } else {
      return [];
    }
  }

  List<CompleteMedicineModel> combineMedicines(List<dynamic> response) {
    List<CompleteMedicineModel> inComMedicines = [];

    for (int i = 0; i < response.length; i++) {

      print(i);
        inComMedicines.add(CompleteMedicineModel(
          response[i]['name'],
          response[i]['cost'],
          response[i]['count'],
          response[i]['image'],
          response[i]['donate'],
          response[i]['id'],
        ));

      }
    return inComMedicines;
  }


  Future<String> deleteAllMedicine() async {
    String? token = await storage.read(key: 'token');
    print(token);

    var url = Uri.parse(
// <<<<<<< HEAD
        ServerConfig.domainNameServer + ServerConfig.deleteCompleteMedicine);
// =======
//         ServerConfig.domainNameServer + ServerConfig.deleteCompleteMedicine);
// >>>>>>> 2c6a0d2 (new file)

    var response = await http.delete(url, headers: {
      'Authorization': 'Bearer $token ',
      'Accept': 'application/json'
    });

    String getResponse = response.body;
    print(getResponse);
    if (response.statusCode == 200) {
      return 'OK';
    } else {
      return 'NO';
    }
  }


  Future<String> deleteMedicine(int id) async {
    String? token = await storage.read(key: 'token');
    print(token);

    var url = Uri.parse(
// <<<<<<< HEAD
        ServerConfig.domainNameServer + ServerConfig.deleteMedicine + '$id');
// =======
//         ServerConfig.domainNameServer + ServerConfig.deleteMedicine + '$id');
// >>>>>>> 2c6a0d2 (new file)

    var response = await http.delete(url, headers: {
      'Authorization': 'Bearer $token ',
      'Accept': 'application/json'
    });

    String getResponse = response.body;
    print(getResponse);
    if (response.statusCode == 200) {
      return 'OK';
    } else {
      return 'NO';
    }
  }


}

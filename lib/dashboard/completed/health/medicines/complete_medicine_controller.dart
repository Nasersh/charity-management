import 'package:get/get.dart';

import 'complete_medicines_services.dart';
import 'medicine_model.dart';


class CompleteMedicinesController extends GetxController
{
  var medicinesList =<CompleteMedicineModel>[].obs;
  var medicines=<CompleteMedicineModel>[];
  RxBool isTrue = false.obs;

  var id=0.obs;

  CompleteMedicinesServices service = CompleteMedicinesServices();
  var isloading=true.obs;

  @override
  void onInit() {
    showMedicines();
    //loadProductsFromRepo(id.value);
    super.onInit();
  }

  void showMedicines() async {
    try {
      isloading(true);
      medicines = await service.getMedicines();
      medicines.forEach((element) {print(element.donate);});
      if (medicines != null) {
        medicinesList.value = medicines;
      }
    } finally {
      isloading(false);
    }
  }


  Future<String> deleteMedicine(int id) async {

    String save;

    try {
      isloading(true);
      save = await service.deleteMedicine(id);
    }finally {
      isloading(false);
    }
    return save;
  }


  Future<String> deleteAllMedicine() async {

    String save;

    try {
      isloading(true);
      save = await service.deleteAllMedicine();
    }finally {
      isloading(false);
    }
    return save;
  }


}
class CompleteMedicineModel {
  late String name;
  late int cost;
  late int count;
  late String image;
  late int id;
  late int donate;

  CompleteMedicineModel(
      this.name,
      this.cost,
      this.count,
      this.image,
      this.donate,
      this.id
      );
}

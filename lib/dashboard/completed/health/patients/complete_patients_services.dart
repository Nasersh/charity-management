import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:http/http.dart' as http;
// <<<<<<< HEAD
import '../../../../config/config.dart';
import '../../../../user/model/Patient.dart';
// >>>>>>> 2c6a0d2 (new file)




class CompletePatientsService {
  var message;
  var url = Uri.parse(
// <<<<<<< HEAD
      ServerConfig.domainNameServer + ServerConfig.completePatient);
// =======
//       ServerConfig.domainNameServer + ServerConfig.completePatient);
// >>>>>>> 2c6a0d2 (new file)
  FlutterSecureStorage storage = const FlutterSecureStorage();

  Future<List<Patient>> getPatient() async {
    String? token = await storage.read(key: 'token');
    print(token);
    var response = await http.get(url, headers: {
      'Accept': 'application/json',
      'Authorization': 'Bearer $token'
    },);
    print(response.statusCode);

    if (response.statusCode == 200) {
      var patients = patientFromJson(response.body);
      return patients.patients;
    }
    else {
      return [];
    }
  }


  Future<String> deletePatient(int id) async {
    String? token = await storage.read(key: 'token');
    print(token);

    var url = Uri.parse(
// <<<<<<< HEAD
        ServerConfig.domainNameServer + ServerConfig.deletePatient + '$id');
// =======
//         ServerConfig.domainNameServer + ServerConfig.deletePatient + '$id');
// >>>>>>> 2c6a0d2 (new file)

    var response = await http.delete(url, headers: {
      'Authorization': 'Bearer $token ',
      'Accept': 'application/json'
    });

    String getResponse = response.body;
    print(getResponse);
    if (response.statusCode == 200) {
      return 'OK';
    } else {
      return 'NO';
    }
  }


  Future<String> deleteAllPatient() async {
    String? token = await storage.read(key: 'token');
    print(token);

    var url = Uri.parse(
// <<<<<<< HEAD
        ServerConfig.domainNameServer + ServerConfig.deleteCompletePatients);
// =======
//         ServerConfig.domainNameServer + ServerConfig.deleteCompletePatients);
// >>>>>>> 2c6a0d2 (new file)

    var response = await http.delete(url, headers: {
      'Authorization': 'Bearer $token ',
      'Accept': 'application/json'
    });

    String getResponse = response.body;
    print(getResponse);
    if (response.statusCode == 200) {
      return 'OK';
    } else {
      return 'NO';
    }
  }



}

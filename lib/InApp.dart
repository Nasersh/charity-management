import 'package:charitysys/login/login.dart';
import 'package:charitysys/logout/logout_controller.dart';
import 'package:charitysys/secureStorage.dart';
import 'package:charitysys/user/about%20us/aboutus.dart';
import 'package:charitysys/user/bottom_bar/Education/Education.dart';
import 'package:charitysys/user/bottom_bar/Education/sponsership/my_guarantees/myguarantee.dart';
import 'package:charitysys/user/bottom_bar/Education/sponsership/sposership.dart';
import 'package:charitysys/user/bottom_bar/food/Food.dart';
import 'package:charitysys/user/bottom_bar/health/Health.dart';
import 'package:charitysys/user/box_donation/box.dart';
import 'package:charitysys/user/gallery/gallery.dart';
import 'package:charitysys/user/wallet/wallet_controller.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:get/get.dart';
import 'package:get/get_core/src/get_main.dart';
import 'package:url_launcher/url_launcher.dart';
import 'confetti_controller.dart';
import 'configrations/const/const.dart';

class InApp extends StatefulWidget {
  InApp({Key? key}) : super(key: key);

  @override
  State<InApp> createState() => _InAppState();
}

class _InAppState extends State<InApp> {
  var money;
  var _selectedIndex = 0;
  List<IconData> data = [
    FontAwesomeIcons.kitMedical,
    FontAwesomeIcons.bowlFood,
    FontAwesomeIcons.bookOpen,
    FontAwesomeIcons.donate,
  ];
  var screen = [
    Health(),
    Food(),
    Education(),
    Box(),
  ];
  WalletController controller = Get.put(WalletController());
  LogoutController logcontroller = Get.put(LogoutController());
  ConfController cf = Get.put(ConfController());
  WalletController wc = Get.put(WalletController());

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: kprimary,
        appBar: AppBar(
            iconTheme: IconThemeData(color: kprimary),
            backgroundColor: kblue,
            elevation: 0,
            centerTitle: true,
            actions: <Widget>[
              MaterialButton(
                  onPressed: () {
                    showModalBottomSheet(
                        context: context,
                        builder: (BuildContext context) {
                          return Container(
                            height: MediaQuery.of(context).size.height / 2,
                            decoration: const BoxDecoration(
                                color: kprimary,
                                borderRadius: BorderRadius.only(
                                    topRight: Radius.circular(25),
                                    topLeft: Radius.circular(25))),
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Padding(
                                  padding: const EdgeInsets.only(
                                      top: 15.0, right: 10, left: 10),
                                  child: TextField(
                                    onChanged: (value) {
                                      money = value;
                                    },
                                    keyboardType: TextInputType.number,
                                    textAlign: TextAlign.center,
                                    style: TextStyle(fontSize: 30),
                                  ),
                                ),
                                SizedBox(
                                  height:
                                      MediaQuery.of(context).size.height / 20,
                                ),
                                ElevatedButton(
                                    onPressed: () {
                                      wc.wallet.value =
                                          wc.wallet.value + int.parse(money);
                                      _charge(money);
                                    },
                                    child: Container(
                                      decoration: BoxDecoration(
                                          color: kstorm,
                                          borderRadius:
                                              BorderRadius.circular(20)),
                                      height:
                                          MediaQuery.of(context).size.height /
                                              14,
                                      width:
                                          MediaQuery.of(context).size.width / 3,
                                      child: const Center(
                                        child: Text(
                                          'charge wallet',
                                          style: TextStyle(
                                              color: kprimary, fontSize: 20),
                                        ),
                                      ),
                                    )),
                                SizedBox(
                                  height:
                                      MediaQuery.of(context).size.height / 40,
                                ),
                                ElevatedButton(
                                    onPressed: () {
                                      Get.back();
                                    },
                                    child: Container(
                                      decoration: BoxDecoration(
                                          color: kblue,
                                          borderRadius:
                                              BorderRadius.circular(20)),
                                      height:
                                          MediaQuery.of(context).size.height /
                                              15,
                                      width:
                                          MediaQuery.of(context).size.width / 4,
                                      child: const Center(
                                        child: Text(
                                          'cancel',
                                          style: TextStyle(color: kprimary),
                                        ),
                                      ),
                                    ))
                              ],
                            ),
                          );
                        });
                  },
                  child: const Center(
                      child: Text(
                    'charge wallet',
                    style: TextStyle(color: kprimary, fontSize: 16),
                  ))),
            ]),
        bottomNavigationBar: Padding(
          padding: const EdgeInsets.all(20),
          child: Material(
            elevation: 10,
            borderRadius: BorderRadius.circular(20),
            color: kprimary,
            child: Container(
              height: 50,
              width: double.infinity,
              child: ListView.builder(
                itemCount: data.length,
                // padding: EdgeInsets.symmetric(horizontal: 20),
                itemBuilder: (ctx, i) => Padding(
                  padding: EdgeInsets.symmetric(
                      horizontal: MediaQuery.of(context).size.width / 30),
                  child: GestureDetector(
                    onTap: () {
                      setState(() {
                        _selectedIndex = i;
                      });
                    },
                    child: AnimatedContainer(
                      duration: Duration(milliseconds: 300),
                      width: 60,
                      decoration: BoxDecoration(
                        border: i == _selectedIndex
                            ? const Border(
                                top: BorderSide(width: 3.0, color: kblue))
                            : null,
                        gradient: i == _selectedIndex
                            ? const LinearGradient(
                                colors: [kblue, kblue],
                                begin: Alignment.topCenter,
                                end: Alignment.bottomCenter)
                            : null,
                      ),
                      child: Icon(
                        data[i],
                        size: 30,
                        color: i == _selectedIndex ? kprimary : kblue,
                      ),
                    ),
                  ),
                ),
                scrollDirection: Axis.horizontal,
              ),
            ),
          ),
        ),
        drawer: Container(
          width: 250,
          child: Drawer(
            backgroundColor: kprimary,
            elevation: 20,
            child: Container(
              width: MediaQuery.of(context).size.width * .1,
              child: ListView(
                padding: EdgeInsets.zero,
                children: [
                  DrawerHeader(
                    decoration: const BoxDecoration(
                      color: kblue,
                     ),
                    child: Center(
                      child: Text('Hello',
                          style: TextStyle(
                            fontSize: 40,
                            color: kongreen,
                          )),
                    ),
                  ),
                  ListTile(
                    leading: const Icon(
                      Icons.person_outline,
                      color: kblue,
                    ),
                    title: const Text('my guarantees'),
                    onTap: () {
                      Get.to(MyGuarantees());
                    },
                  ),
                  const Divider(
                    height: 10,
                    thickness: 0.5,
                    color: Colors.grey,
                  ),
                  ListTile(
                    leading: const Icon(
                      Icons.star_border_outlined,
                      color: kblue,
                    ),
                    title: const Text('gallery'),
                    onTap: () {
                      Get.to(Gallery());
                    },
                  ),
                  const Divider(
                    height: 5,
                    thickness: 0.5,
                    color: Colors.grey,
                  ),
                  ListTile(
                    leading: Icon(
                      Icons.boy,
                      color: kblue,
                    ),
                    title: const Text('sponsorship'),
                    onTap: () {
                      Get.to(Sponsership());
                    },
                  ),
                  const Divider(
                    height: 10,
                    thickness: 0.5,
                    color: Colors.grey,
                  ),
                  ListTile(
                    leading: const Icon(
                      Icons.house,
                      color: kblue,
                    ),
                    title: const Text('About us'),
                    onTap: () {
                      Get.to(AboutUs());
                    },
                  ),
                  const Divider(
                    height: 5,
                    thickness: 0.5,
                    color: Colors.grey,
                  ),
                  ListTile(
                    leading: Icon(
                      Icons.call,
                      color: kblue,
                    ),
                    title: const Text('Contact'),
                    onTap: () {
                      _launchPhoneURL('0940749492');
                    },
                  ),
                  const Divider(
                    height: 5,
                    thickness: 0.5,
                    color: Colors.grey,
                  ),
                  ListTile(
                    leading: const Icon(
                      Icons.logout,
                      color: kblue,
                    ),
                    title: const Text('Log Out'),
                    onTap: () async {
                      logcontroller.LogoutData();
                    },
                  ),
                ],
              ),
            ),
          ),
        ),
        body: screen[_selectedIndex]);
  }

  void _charge(money) {
    controller.ChargeWallet('$money');
  }

  _launchPhoneURL(String phoneNumber) async {
    String url = 'tel://0940749492';
    launch(url);
  }
}

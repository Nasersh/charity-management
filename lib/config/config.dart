class ServerConfig {
  static const domainNameServer = 'http://192.168.1.109:8000';
  static const registerUser = '/user/register';
  static const showallPatients = '/user/patient/show';
  static const showallMedicine = '/user/medicine/show';
  static const showallFoodBascket = '/user/food_parcel/show';
  static const showallStationery = '/user/stationery/show';
  static const showallFoodStuff = '/user/food_section/show';
  static const showallEquip = '/user/medical_tools/show';
  static const showallkitchenNeeds = '/user/kitchen_set/show';
  static const Signin = '/user/login';
  static const Logout = '/user/logout';
  static const ShowWallet = '/user/wallet/show';
  static const ChargeWallet = '/user/wallet/charge';
  static const DonatePatient = '/user/patient/donate/';
  static const DonateFoodBascket = '/user/food_parcel/donate/';
  static const DonateStationery = '/user/stationery/donate/';
  static const DonateFoodStuff = '/user/food_section/donate/';
  static const DonateMed = '/user/medicine/donate/';
  static const DonateEquip = '/user/medical_tools/donate/';
  static const BoxDonate = '/user/direct_donate';
  static const DonateKitchenNeeds = '/user/kitchen_set/donate/';
  static const VolunteerSubmit = '/user/volunteer/submit';
  static const VolunteerResign = '/user/resignation/submit';
  static const showallStudent = '/user/guarantee/show';
  static const showMyStudent = '/user/guarantee/user_guarantee';
  static const DonateSponsership = '/user/guarantee/donate/';
  static const SearchStudents = '/user/guarantee/search';
  static const ShowMedicalTeam = '/admin/volunteer/medical_team';
  static const ShowCookTeam = '/admin/volunteer/food_service_team';
  static const ShowEducationTeam = '/admin/volunteer/educational_team';
  static const VerifyRandomCode = '/user/email_code';
  static const dismiss = '/admin/volunteer/dismissal/';
  static const allVolunteer = '/admin/volunteer/requests';
  static const showallposts = '/user/gallery/show';
  static const Buy = '/user/gallery/buy/';
  static String addPatient = "/admin/patient/add";
  static String inCompletePatient = '/admin/patient/show';
  static String addMedicine = '/admin/medicine/add';
  static String addStationery = '/admin/stationery/add';
  static String addMedicalTool = '/admin/medical_tools/add';
  static String updatePatient = '/admin/patient/update/';
  static String inCompleteMedicines = '/admin/medicine/show';
  static String updateMedicine = '/admin/medicine/update/';
  static String inCompleteMedicalTools = '/admin/medical_tools/show';
  static String updateMedicalTools = '/admin/medical_tools/update/';
  static String inCompleteStationery = '/admin/stationery/show';
  static String updateStationery = '/admin/stationery/update/';
  static String deleteStationery = '/admin/stationery/delete/';
  static String deletePatient = '/admin/patient/delete/';
  static String deleteMedicine = '/admin/medicine/delete/';
  static String deleteMedicalTool = '/admin/medical_tools/delete/';
  static String addGuarantee = "/admin/guarantee/add";
  static String inCompleteGuarantees = '/admin/guarantee/show/not_guaranteed';
  static String updateGuarantee = '/admin/guarantee/update/';
  static String deleteGuarantee = '/admin/guarantee/delete/';
  static String searchGuarantee = '/admin/guarantee/search';
  static String sortGuarantee = '/admin/guarantee/sort/';
  static String addFoodParcel = "/admin/food_parcel/add";
  static String addFoodSection = "/admin/food_section/add";
  static String addKitchenSets = "/admin/kitchen_set/add";
  static String inCompleteFoodParcels = '/admin/food_parcel/show';
  static String updateFoodParcel = '/admin/food_parcel/update/';
  static String deleteFoodParcel = '/admin/food_parcel/delete/';
  static String inCompleteFoodSections = '/admin/food_section/show';
  static String updateFoodSection = '/admin/food_section/update/';
  static String deleteFoodSection = '/admin/food_section/delete/';

  static String inCompleteKitchenSet = '/admin/kitchen_set/show';

  static String updateKitchenSet = '/admin/kitchen_set/update/';

  static String deleteKitchenSet = '/admin/kitchen_set/delete/';

  static String completeKitchenSet = '/admin/kitchen_set/complete';

  static String completeFoodSection = '/admin/food_section/complete';

  static String completePatient = '/admin/patient/complete';

  static String completeMedicine = '/admin/medicine/complete';

  static String completeMedical = '/admin/medical_tools/complete';

  static String deleteCompleteMedicine = '/admin/medicine/deleteComplete';
  static String showBoxDonation = '/admin/donate';

  static String deleteCompleteMedicalTool =
      '/admin/medical_tools/deleteComplete';
  static String takeOutBox = '/admin/takeout';

  static String deleteCompletePatients = '/admin/patient/deleteComplete';

  static String deleteCompleteFoodSection =
      '/admin/food_section/deleteComplete';

  static String deleteCompleteKitchenSet = '/admin/kitchen_set/deleteComplete';
  static String getBoxMoney = '/admin/charity_box/show';
  static String checkPassword = '/admin/charity_box/set_password';

  static String completeGuarantee = '/admin/guarantee//show/guaranteed';
  static String acceptVolunteer = '/admin/volunteer/accept/';
  static String rejectVolunteer = '/admin/volunteer/reject/';
  static String acceptResignDemand = '/admin/resignation/accept/';

  static String rejectResignDemand = '/admin/resignation/reject/';

  static String allResignDemands = '/admin/resignation/requests';

  static const like = '/user/gallery/like/';
}

import 'dart:math';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:lottie/lottie.dart';
import '../configrations/consts.dart';
import 'intro_controller.dart';

class IntroPage extends StatefulWidget {
  const IntroPage({Key? key}) : super(key: key);

  @override
  _IntroPageState createState() => _IntroPageState();
}

class _IntroPageState extends State<IntroPage> {
  @override


  IntroController controller = Get.put(IntroController());

  Widget build(BuildContext context) {

    Size size = MediaQuery.of(context).size;

    return SafeArea(
      child: Scaffold(
        body: Container(
          decoration: BoxDecoration(
              gradient: LinearGradient(
                  begin: Alignment.topLeft,
                  end: Alignment.bottomRight,
                  colors: [
                littleGrey,
                Colors.white.withOpacity(.3),
                deepGrey
              ])),
          child: Padding(
            padding: const EdgeInsets.all(12.0),
            child: PageView.builder(
              physics: const NeverScrollableScrollPhysics(),
              controller: controller.pageCont,
              itemBuilder: (context, index) => buildPage(
                  images[index], titles[index], subTitle[index], size),
              // }),
            ),
          ),
        ),
      ),
    );
  }

  Column buildPage(String image, String title, String subTitle, Size size) {
    return Column(
      children: [
        Expanded(
          child:
              Lottie.asset(image, width: size.width, height: size.height / 2),
        ),
        Column(
          children: [
            Text(
              title,
              style: const TextStyle(fontSize: 30, fontWeight: FontWeight.w900),
              textAlign: TextAlign.center,
            ),
            const SizedBox(height: 30),
            Padding(
              padding:const EdgeInsets.symmetric(horizontal: 3),
              child: Text(
                subTitle,
                style: const TextStyle(
                    fontSize: 20, fontWeight: FontWeight.bold, height: 1.3),
                textAlign: TextAlign.center,
              ),
            ),
            const SizedBox(height: 30),
            Center(
              child: Container(
                child: SizedBox(
                  height: 75,
                  width: 75,
                  child: Stack(
                    children: [
                      AnimatedIndicator(const Size(75, 75)),
                      GestureDetector(
                        onTap: controller.onNext,
                        child: Center(
                          child: Container(
                            width: 60,
                            height: 60,
                            decoration: const BoxDecoration(
                                color: Colors.blue, shape: BoxShape.circle),
                            child: const Icon(
                              Icons.arrow_forward_ios,
                              color: Colors.white,
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
            const SizedBox(height: 30),
          ],
        ),
      ],
    );
  }


  Widget AnimatedIndicator(Size size) {
    return CustomPaint(
      size: Size(size.width, size.height),
      painter: ProgressIndicator(progress: controller.progress.value),
    );
  }
}

class ProgressIndicator extends CustomPainter {
  double progress ;

  ProgressIndicator({required this.progress});

  @override
  void paint(Canvas canvas, Size size) {
    var line = Paint()
      ..style = PaintingStyle.stroke
      ..strokeWidth = 5
      ..color = Colors.blue;

    var circle = Paint()
      ..style = PaintingStyle.fill
      ..color = Colors.blue;

    var radians = (progress / 100) * 2 * pi;

    drawShape(canvas, line, circle, -pi / 2, radians, size);
  }

  @override
  bool shouldRepaint(covariant CustomPainter oldDelegate) => true;

  void drawShape(Canvas canvas, line, circle, start, sweepAngle, size) {
    final centerOffset = Offset(size.width / 2, size.height / 2);
    final double radius = min<double>(size.width, size.height) / 2;

    canvas.drawArc(Rect.fromCircle(center: centerOffset, radius: radius), start,
        sweepAngle, false, line);
  }
}

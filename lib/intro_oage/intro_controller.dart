import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../configrations/consts.dart';

class IntroController extends GetxController{

  late PageController pageCont ;
  late RxDouble progress;
  late RxInt curr;



  @override
  void onInit() {

    pageCont = PageController(initialPage: 0);
    progress = 0.0.obs;
    curr = 0.obs;

    super.onInit();
  }

  void onNext() {
    progress.value += 50;
    curr += 1;
    if (titles.length.isEqual(curr.value)){
      Get.offNamed("/login");
      return;
    }
    pageCont.nextPage(
        duration: const Duration(seconds: 1), curve: Curves.easeIn);
  }

}
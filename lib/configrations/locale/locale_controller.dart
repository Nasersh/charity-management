import 'package:flutter/cupertino.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:get/get.dart';

class MyLocalController extends GetxController{

  FlutterSecureStorage storage = const FlutterSecureStorage();

  String? str;
  Locale init = Get.deviceLocale!;

  void changeLang(String codeLang){

    Locale locale = Locale(codeLang);
    Get.updateLocale(locale);

  }


  @override
  void onInit() async {

    String? str = await storage.read(key: 'lang');
    init = (str == null) ? Get.deviceLocale! : const Locale("en");
    await storage.write(key: 'lang',value: init.toString());

    super.onInit();
  }

}
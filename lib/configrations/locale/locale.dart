import 'package:get/get.dart';

class MyLocale implements Translations{

  @override
  // TODO: implement keys
  Map<String, Map<String, String>> get keys => {
    "ar":{
      "pin_title":"التاكيدية",
      "error_pin":"هناك خطا",
      "error_pin_desc":"يرجى المحاولة مرة اخرى",
      "Syria":"سوريا",
      "America":"امريكا",
      "chooseLang":"اختر لغة",
      "changeLang":"غير اللغة",
      "eduGar":"كفالة \n          التعليم       ",
      "edu":"التعليم",
      "eduNeed":"لوازم التعليم",
      "eduNeed2":"لوازم \n          التعليم     ",
      "desG":"شارك معنا ولتقم بالتسجيل مباشرة اضغط هنا",
      "desN":"ساعد الايتام ولمعرفة احتياجاتهم اكمل مراحلهم الدراسية",
      "Sections":"اقسام"
    },
    "en":{
      "pin_title":"Pin Code Verification",
      "error_pin":"error occurred",
      "error_pin_desc":"please try again",
      "Syria":"Syria",
      "America":"America",
      "chooseLang":"Choose Language",
      "changeLang":"Change lang",
      "eduGar":"Education\n            Guarantee",
      "edu":"Education",
      "eduNeed":"Education needs",
      "eduNeed2":"Education \n               needs",
      "desG":"contact with us and go ahead to sign with us immediatly tap here",
      "desN":"help the orphans to get their needs to continue education level sir!",
      "Sections":"Sections"
    }
  };



}
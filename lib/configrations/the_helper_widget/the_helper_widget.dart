import 'package:charitysys/configrations/consts.dart';
import 'package:charitysys/dashboard/completed/education/complete_guarantee/consts.dart';
import 'package:flutter/material.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:get/get.dart';
import 'package:image_picker/image_picker.dart';
import 'package:lottie/lottie.dart';

import '../../dashboard/incomplete/Education/stationery/stationery_consts.dart';
import '../../dashboard/incomplete/Health/medicines/medicines_consts.dart';
import '../../dashboard/incomplete/Health/patients/add_patient/patient_consts.dart';
RxString certificationImage=''.obs;
class TheTop extends StatelessWidget {
  const TheTop({Key? key, required this.size}) : super(key: key);

  final Size size;

  @override
  Widget build(BuildContext context) {
    return Align(
      alignment: Alignment.topLeft,
      child: Container(
        width: size.width,
        margin: const EdgeInsets.only(top: 20, left: 20, right: 20),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Container(
              width: 70,
              height: 55,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  const Spacer(),
                  buildMenu(30, 7),
                  const SizedBox(height: 4),
                  buildMenu(24, 6),
                  const SizedBox(height: 4),
                  buildMenu(20, 5),
                  const Spacer(),
                ],
              ),
            ),
            Container(
              width: 70,
              height: 55,
              child: Lottie.asset("assets/edu/97965-wallet.json"),
            )
          ],
        ),
      ),
    );
  }

  Container buildMenu(double width, double height) {
    return Container(
      width: width,
      height: height,
      decoration: BoxDecoration(
        color: mainColor,
        borderRadius: BorderRadius.circular(2),
      ),
    );
  }
}

Widget getInput(int index, Size size, formKey) {
  return Container(
    alignment: Alignment.center,
    width: size.width * .7,
    height: 78,
    margin: const EdgeInsets.symmetric(horizontal: 20, vertical: 15),
    padding: const EdgeInsets.symmetric(horizontal: 10),
    decoration: BoxDecoration(
        color: Colors.grey.withOpacity(.2),
        borderRadius: BorderRadius.circular(10)),
    child: TextFormField(
      //key: formKey,
      controller: editors[index],
      decoration: InputDecoration(
        labelText: attributeName[index],
        border: InputBorder.none,
        icon: icons[index],
        iconColor: Colors.red,
        labelStyle: TextStyle(
            fontSize: 16, fontWeight: FontWeight.w600, color: mainColor),
      ),
      keyboardType: types[index],
      validator: (value) {
        if (index == 0 && value != null && value.length < 7) {
          return errorText[0];
        } else if (index == 1 && value != null && value.length < 10) {
          return errorText[1];
        } else if (index == 2 && value != null && value.length < 10)
          return errorText[2];
        else if (index == 3 &&
            value != null &&
            value.isNum &&
            int.parse(value) < 1000) {
          return errorText[3];
        } else if (index == 5 &&
            value != null &&
            value.isNum &&
            int.parse(value) < 100000) {
          return errorText[5];
        } else if (index == 4 &&
            value != null &&
            (value != 'male' && value != 'women')) {
          return errorText[4];
        }
      },
    ),
  );
}

bool checking() {
  bool isValidate = true;
  for (int i = 0; i <= 5; i++) {
    print(editors[i].toString());
    if (isValidate) {
      if (editors[i].text != '') {
        isValidate = formKey[i].currentState!.validate();
      }
    }
    if (!isValidate) {
      if (editors[i].text != '') {
        formKey[i].currentState!.validate();
      }
    }
  }
  print(isValidate);
  return isValidate;
}

RxString image = ''.obs;
RxBool isGet = false.obs;

Future<void> takeImage(ImageSource source) async {
  final picker = ImagePicker();
  final getImage = await picker.pickImage(source: source);
  if (getImage != null) {
    image.value = getImage.path;
    isGet.value = true;
  }
}

Widget getInputMedicine(int index, Size size, formKey) {
  return Container(
    alignment: Alignment.center,
    width: size.width * .8,
    height: 78,
    margin: const EdgeInsets.symmetric(horizontal: 20, vertical: 15),
    padding: const EdgeInsets.symmetric(horizontal: 10),
    decoration: BoxDecoration(
        color: Colors.grey.withOpacity(.2),
        borderRadius: BorderRadius.circular(10)),
    child: TextFormField(
      //key: formKey,
      controller: medEditors[index],
      decoration: InputDecoration(
        labelText: medAttributeName[index],
        border: InputBorder.none,
        icon: medicineIcons[index],
        iconColor: Colors.red,
        labelStyle: TextStyle(
            fontSize: 16, fontWeight: FontWeight.w600, color: mainColor),
      ),
      keyboardType: medTypes[index],
      validator: (value) {
        if (index == 0 && value != null && value.length < 7) {
          return medErrorText[0];
        } else if (index == 1 && value != null && value == 0) {
          return medErrorText[1];
        } else if (index == 2 && value != null && value == 0) {
          return medErrorText[2];
        }
      },
    ),
  );
}

class BuildStationerySizes extends StatelessWidget {
  const BuildStationerySizes({
    Key? key,
    required this.size,
    required this.controller,
  }) : super(key: key);

  final Size size;
  final controller;

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: size.width * .8,
      height: 78,
      child: ListView.builder(
          itemCount: 3,
          scrollDirection: Axis.horizontal,
          itemBuilder: (_, index) => Padding(
                padding: const EdgeInsets.all(8.0),
                child: GestureDetector(
                  onTap: () => controller.changeItem(index),
                  child: Obx(
                    () => Container(
                      width: size.width / 4,
                      height: 78,
                      decoration: BoxDecoration(
                        color: controller.currentItem.value == index
                            ? mainColor
                            : Colors.white.withOpacity(.2),
                        border: Border.all(
                          width: 2,
                          color: mainColor,
                        ),
                        borderRadius: BorderRadius.circular(20),
                      ),
                      child: Center(
                        child: Text(
                          stationerySizes[index],
                          style: TextStyle(
                              fontSize: 17,
                              fontWeight: FontWeight.bold,
                              color: controller.currentItem.value == index
                                  ? Colors.white
                                  : mainColor),
                        ),
                      ),
                    ),
                  ),
                ),
              )),
    );
  }
}

Widget getStationersInput(int index, Size size, formKey) {
  return Container(
    alignment: Alignment.center,
    width: size.width * .8,
    height: 78,
    margin: const EdgeInsets.symmetric(horizontal: 20, vertical: 15),
    padding: const EdgeInsets.symmetric(horizontal: 10),
    decoration: BoxDecoration(
        color: Colors.grey.withOpacity(.2),
        borderRadius: BorderRadius.circular(10)),
    child: TextFormField(
      //key: formKey,
      controller: stationeryEditors[index],
      decoration: InputDecoration(
        labelText: stationeryAttributeName[index],
        border: InputBorder.none,
        icon: stationeryIcons[index],
        iconColor: Colors.red,
        labelStyle: TextStyle(
            fontSize: 16, fontWeight: FontWeight.w600, color: mainColor),
      ),
      maxLines: index == 3 ? 4 : 1,
      keyboardType: stationeryTypes[index],
      validator: (value) {
        if (index == 0 && value != null && value.length < 7) {
          return stationeryErrorText[0];
        } else if (index == 1 && value != null && value == 0) {
          return stationeryErrorText[1];
        } else if (index == 2 && value != null && value == 0) {
          return stationeryErrorText[2];
        }else if (index == 3 && value != null && value.length < 7) {
          return stationeryErrorText[3];
        }
      },
    ),
  );
}





class TheAdminTop extends StatelessWidget {
  const TheAdminTop({Key? key, required this.size}) : super(key: key);

  final Size size;

  @override
  Widget build(BuildContext context) {
    return Align(
      alignment: Alignment.topLeft,
      child: Container(
        width: size.width,
        margin: const EdgeInsets.only(top: 20, right: 20),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [

                // Column(
                //   mainAxisAlignment: MainAxisAlignment.center,
                //   children: [
                //     const Spacer(),
                //     buildMenu(30, 7),
                //     const SizedBox(height: 4),
                //     buildMenu(24, 6),
                //     const SizedBox(height: 4),
                //     buildMenu(20, 5),
                //     const Spacer(),
                //   ],
                // ),

          ],))
    );
  }
}










//
//
//

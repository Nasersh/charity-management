import 'dart:convert';

import 'package:charitysys/services/local_notification.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:http/http.dart' as http;




void notificationEnsure(BuildContext context){

  AndroidNotificationChannel android = const AndroidNotificationChannel(
    "easyapproach",
    "channel",
    description: "our channel",
    importance: Importance.high,
    playSound: true,
  );
  FlutterLocalNotificationsPlugin _noti = FlutterLocalNotificationsPlugin();
  _noti.resolvePlatformSpecificImplementation<AndroidFlutterLocalNotificationsPlugin>()?.createNotificationChannel(android);

  FirebaseMessaging.instance.getInitialMessage();

  FirebaseMessaging.onMessage.listen((message) {
    print(message.notification!.title);
    print(message.notification!.body);

    LocalNotification.display(message);
  });
  FirebaseMessaging.onMessageOpenedApp.listen((message) {
    final route = message.data['route'];
    Navigator.of(context).pushNamed(route);
  });


}








Future<void> sendNoti(String title, String body, String id) async {
  var url = Uri.parse("https://fcm.googleapis.com/fcm/send");
  String serverToken =
      "AAAAaKtHjSM:APA91bFugKuYbtyKu1fHLDtCeCsr4zNsSzXCI1bkpo-t0GPvPBV5Ls4aQTkqEmQmrbSHa_rmzOg0cB-Ky0vd5hqz5hVJUWzM4WnV0Hp2NoGLZJuiYr_z763euWcBORazQXQWxnbU7TLR";

  var response = await http.post(
    url,
    headers: <String, String>{
      'Content-Type': 'application/json',
      'Authorization': 'key=$serverToken'
    },
    body: jsonEncode(<String, dynamic>{
      'notification': <String, dynamic>{
        'body': title,
        "title": body,
      },
      "priority": 'high',
      'data': <String, dynamic>{
        'click_action': 'FLUTTER_NOTIFICATION_CLICK',
        'id': id,
        'route': 'red'
      },
      "to": await FirebaseMessaging.instance.getToken()
      //fW6RuYM_QZ-pq6G-07tJG9:APA91bFUPVSyXV7X0d2mOkyvG5T9XEC8moKKDNgaVct0hNVW3T9uY0gNRTUDPPtBJ2WeIohfgjzyZ70aD7XBnm63-kgrIATNX_dmpFMiyPzXeTYYxB0OG804Yq1j09QuL6KcbZjIe9-4
    }),
  );
}

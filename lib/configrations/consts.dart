import 'package:flutter/material.dart';

Color deepGrey = const Color.fromRGBO(151, 167, 195, 1);
Color littleGrey = const Color.fromRGBO(151, 167, 195, 1);

List<String> images = [
  "assets/beddoctor.json",
  "assets/teaching1.json",
  "assets/food2.json",
];

List<String> titles = [
  "Welcome to Health Section",
  "Welcome to Education section",
  "Welcome to Food Section"
];

List<String> subTitle = [
  "in this Section we will show the medicine , equipments , operation needs",
  "in this Section we will show the stationery , education guarantee needs!",
  "in this Section we will show the general , food basket needs!",
];


TextStyle style = const TextStyle(
    color: Colors.white,
    fontWeight: FontWeight.bold,
    fontSize: 24,
    shadows: [
      BoxShadow(
          color: Colors.black26,
          spreadRadius: 3,
          blurRadius: 2,
          offset: Offset(5, 5))
    ]);


TextStyle title = const TextStyle(
    color: Colors.black,
    fontWeight: FontWeight.bold,
    fontSize: 30,
    shadows: [
      BoxShadow(
          color: Colors.black26,
          spreadRadius: 3,
          blurRadius: 2,
          offset: Offset(3, 3))
    ]);



TextStyle style1 = const TextStyle(
    color: Colors.black,
    fontWeight: FontWeight.bold,
    fontSize: 30,
    fontFamily: "Lobster",
    height: 2,
    shadows: [
      BoxShadow(
          color: Colors.black26,
          spreadRadius: 3,
          blurRadius: 2,
          offset: Offset(5, 5))
    ]);

Map<String, bool> check = {'syria': false, 'charity': false, 'rest': false};


List<Icon> icon = [
  const Icon(Icons.fastfood_sharp),
  const Icon(Icons.health_and_safety),
  const Icon(Icons.school),
  const Icon(Icons.settings),
];

List<Text> text = [
  const Text("Food"),
  const Text("Health"),
  const Text("Education"),
  const Text("Settings"),
];

List<Widget> color = [
  Container(color: Colors.green,),
  Container(color: Colors.red,),
  Container(color: Colors.yellow,),
  Container(color: Colors.blue,),
];


TextStyle subStyle = const TextStyle(
  color: Colors.white,
  fontSize: 18,
  fontWeight: FontWeight.w600,
  fontFamily: "Lobster",
);

TextStyle style2 = const TextStyle(
    color: Colors.black,
    fontWeight: FontWeight.w900,
    fontSize: 28,
    height: 1.2,
    shadows: [
      BoxShadow(
          color: Colors.black26,
          spreadRadius: 2,
          blurRadius: 2,
          offset: Offset(1,1))
    ]);


Color mainColor = const Color(0xff093d65);


TextStyle desc = const TextStyle(
  color: Colors.black,
  fontSize: 17
);

TextStyle descWh = const TextStyle(
    color: Colors.white,
    fontSize: 17
);



const TextStyle kHeading = TextStyle(
  fontSize: 55,
  fontWeight: FontWeight.bold,
  color: Colors.white,
);


const TextStyle kHeading2 = TextStyle(
  fontSize: 40,
  fontWeight: FontWeight.bold,
  color: Colors.white,
);

const TextStyle kBodyText = TextStyle(
  fontSize: 22,
  color: Colors.white,
);
const TextStyle ksubTitle = TextStyle(
  fontSize: 22,
  color: Colors.white,
  fontWeight: FontWeight.bold
);

TextStyle whiteWith32 = const TextStyle(
    color: Colors.white,
    fontWeight: FontWeight.w900,
    fontSize: 32,
    height: 1.2,
    shadows: const  [
      BoxShadow(
          color: Colors.black26,
          spreadRadius: 2,
          blurRadius: 2,
          offset: Offset(1,1))
    ]);

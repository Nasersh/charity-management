import 'package:email_validator/email_validator.dart';
import 'package:flutter/material.dart';

import '../const.dart';


class TextInput extends StatelessWidget {
   TextInput({
    Key? key,
    required this.icon,
    required this.hint,
    required this.inputType,
    required this.inputAction,
     required this.onchange,
     required this.validator,
     this.index = 0,

     this.textAlign=TextAlign.left,
  }) : super(key: key);
  final Function (String) onchange;
  final IconData icon;
   final String? Function(String?)? validator;
  final String hint;
  final TextInputType inputType;
  final TextInputAction inputAction;
  final TextAlign textAlign;
  final int index;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 12.0),
      child: Container(
        decoration: BoxDecoration(
          color: Colors.grey[600]!.withOpacity(0.5),
          borderRadius: BorderRadius.circular(16),
        ),
        child: TextFormField(
          validator: validator,
          onChanged: onchange,
          decoration: InputDecoration(
            errorStyle: const TextStyle(
              fontSize: 1,

            ),
            contentPadding: const EdgeInsets.symmetric(vertical: 20),
            border: InputBorder.none,
            hintText: hint,
            prefixIcon: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 20),
              child: Icon(
                icon,
                color: Colors.white,
                size: 20,
              ),
            ),
            hintStyle: kBodyText,
          ),
          style: kBodyText,
          keyboardType: inputType,
          textInputAction: inputAction,
        ),
      ),
    );
  }
}

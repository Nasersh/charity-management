

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';

import '../../../dashboard/completed/education/complete_guarantee/consts.dart';

Widget Buildcard(String name,int price,String grade)
{
  return  Container(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(15),
        boxShadow: [
          kDefaultShadow
        ],
        color: Colors.white,
      ),
      child: Column(
        children: [
          SizedBox(width: 8),
          Expanded(
            child: Padding(
              padding: const EdgeInsets.all(2.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [

                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Text(
                      name,
                      style: const TextStyle(
                        color: kstorm,
                        fontSize: 17,
                        fontWeight: FontWeight.bold,

                      ),
                      maxLines: 1,
                      overflow: TextOverflow.ellipsis,
                    ),
                  ),

                  Padding(
                      padding: EdgeInsets.all(6),
                    child:Text("you guaranteed this kid for one year you can check other kids",style: TextStyle(fontSize: 15),)
                  ),
                  SizedBox(
                    height: 50,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(
                        '$grade',style: TextStyle(color: kblue),
                        maxLines: 3,
                        overflow: TextOverflow.ellipsis,
                      ),
                      Text(
                        "\$${price}",
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                          color: kstorm
                        ),
                      ),
                    ],
                  ),

                ],
              ),
            ),
          )
        ],
      ),
  );
}

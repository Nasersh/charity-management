import 'package:charitysys/configrations/const/const.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class ElevatedButtonWidget extends StatelessWidget {
  ElevatedButtonWidget(
      {this.color = kstorm,
      this.border = false,
      this.isSelected = true,
      this.fontSize = 5,
      this.title = '',
      this.onPressed,
      Key? key})
      : super(key: key);

  Function()? onPressed;
  String title;
  int fontSize;
  Color? color;
  bool isSelected;
  bool border;

  @override
  Widget build(BuildContext context) {
    double getWidth = MediaQuery.of(context).size.width;
    double getHeight = MediaQuery.of(context).size.height;
    // SizeConfig().init(context);
    return Container(
      height: 50,
      child: ElevatedButton(
        style: ElevatedButton.styleFrom(
          // side: BorderSide(color: border?kGreenColor:Colors.transparent,),
          backgroundColor: isSelected ? color : Colors.white,
          foregroundColor: color == kBlackColor ? kGreyColor : const Color(0xff232323),

          elevation: 0,
          disabledBackgroundColor: kGreyColor,
          animationDuration: const Duration(milliseconds: 250),
          minimumSize: Size(double.infinity, getHeight * 0.045),
          maximumSize: Size(double.infinity, getHeight * 0.1),
          shape: const RoundedRectangleBorder(
            borderRadius: kButtonRadius, // <-- Radius
          ),
        ),
        // minWidth: double.infinity,
        //color: kMainDarkColor,
        onPressed: onPressed,
        child: Text(
          title,
          style: TextStyle(
            height: 1.5,
            color: isSelected ? Colors.white : Colors.grey,
            fontSize: 20,
          ),
        ),
      ),
    );
  }
}

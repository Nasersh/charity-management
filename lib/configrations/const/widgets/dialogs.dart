import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import '../const.dart';
import 'Elevated_button.dart';


class DialogsWidgets{

  void showYesNoDialog({
    required String title,
    required String noTitle,
    required String yesTitle,
    Widget? textWidget,
    required Function() onYesTap,
    required Function() onNoTap,
    required BuildContext context,
    Color color = kGreyColor,
  }) {

    double getWidth = MediaQuery.of(context).size.width;
    double getHeight = MediaQuery.of(context).size.height;
    var alertDialog = Dialog(
        backgroundColor: Colors.transparent,
        insetPadding: const EdgeInsets.all(15),
        child: ConstrainedBox(
          constraints: BoxConstraints(
            minHeight: getHeight*0.01,
          ),
          child: Container(
            width: double.infinity,
            decoration: const BoxDecoration(
                borderRadius: kWidgetBorderRadius,
                color: kBackGroundColor
            ),
            padding:  EdgeInsets.fromLTRB(getWidth*0.038,getWidth*0.1,getWidth*0.038,getWidth*0.038),
            child: Wrap(
              spacing : 10,
              //mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Center(
                  child: Builder(
                      builder: (context) {
                        if(textWidget != null){
                          return textWidget;
                        }
                        return Text(
                            title,
                        );
                      }
                  ),
                ),
                SizedBox(height: getWidth*0.1),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: [
                    Expanded(
                      child:ElevatedButtonWidget(
                        title: noTitle,
                        color: kBackGroundColor,
                        isSelected: false,
                        border: true,
                        onPressed: onNoTap,
                      ),

                    ),
                    SizedBox(width: getWidth*0.038,),
                    Expanded(
                      child:ElevatedButtonWidget(
                          title: yesTitle,
                          onPressed: onYesTap
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
        )
    );
    showDialog(
        context: context,
        builder: (BuildContext context) {
          return alertDialog;
        });
  }

  void showScaffoldSnackBar({
    required String title,
    required BuildContext context,
    Color color = kRedColor,
  }){
    ScaffoldMessenger.of(context).showSnackBar(
        SnackBar(
          behavior: SnackBarBehavior.floating,
          shape: const RoundedRectangleBorder(
            borderRadius: k5RadiusLowerPadding,
          ),
          content: Text(
            title,
            style: const TextStyle(
                height: 1.2,
                fontFamily: kNormalFont,fontSize: 15),),
          margin: const EdgeInsets.fromLTRB(20, 0, 20, 30),
          backgroundColor: color,
          duration: const Duration(milliseconds: 1500),
        ));
  }

  Future<void> showLoadingBox(BuildContext context) async {
    var alertDialog = Dialog(
        shape: const RoundedRectangleBorder(
            borderRadius: BorderRadius.all(Radius.circular(15))),
        backgroundColor: kGreenColor,
        insetPadding:  const EdgeInsets.fromLTRB(75,15,75,15),
        child: Container(
          decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(15),
              color: kGreenColor
          ),
          child: Container(
            width: double.infinity,
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(15),
                color: kGreenColor
            ),
            padding: const EdgeInsets.all(40),
            child: Wrap(
              children: [
                Center(
                  child: SvgPicture.asset(
                    'assets/images/soa white.svg',
                    width: 100,
                    height: 100,
                  ),
                ),
              ],
            ),
          ),
        )
    );
    showDialog(
        barrierDismissible: false,
        context: context,
        builder: (BuildContext context) {
          return WillPopScope(
              onWillPop: () async{
                return false;
              },
              child: alertDialog
          );
        });

    // Dismiss CircularProgressIndicator
  }

  void showCallSupervision(BuildContext context){
    double getWidth = MediaQuery.of(context).size.width;
    double getHeight = MediaQuery.of(context).size.height;
    showDialog(
        context: context,
        builder: (BuildContext dialogcontext) {
          return Dialog(
              backgroundColor: Colors.transparent,
              insetPadding: const EdgeInsets.symmetric(
                horizontal: 15,
              ),
              child: Wrap(
                  children:[ ConstrainedBox(
                    constraints: BoxConstraints(
                      minHeight: getHeight * 0.01,
                    ),
                    child: Container(
                      width: double.infinity,
                      decoration: const BoxDecoration(
                          borderRadius: kWidgetBorderRadius,
                          color: kBackGroundColor),
                      padding: EdgeInsets.fromLTRB(
                          getWidth * 0.038,
                          getWidth * 0.1,
                          getWidth * 0.038,
                          getWidth * 0.038),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        //mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          SvgPicture.asset('assets/images/smile.svg'),
                          SizedBox(
                            height: getHeight * .04,
                          ),
                          const Text("Please call supervision ",
                              style: TextStyle(
                                  fontFamily: kNormalFont,
                                  fontSize:
                                 15,
                                  color: kBlackColor,
                                  height: 1.2),
                              textAlign: TextAlign.center),
                          SizedBox(height: getWidth * 0.05),
                          Container(
                            //height: getHeight * .06,
                            child: ElevatedButtonWidget(
                              title: "Call and Exit",
                              color: kGreenColor,
                              isSelected: true,
                              border: true,
                              onPressed: () {
                                Navigator.of(dialogcontext).pop();
                              },
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                  ]));
        });
  }

}
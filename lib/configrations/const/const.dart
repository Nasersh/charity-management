import 'dart:ui';

import 'package:flutter/material.dart';

const kprimary = Color(0xfff6f7f9);
const kbrowm = Color(0xFFb5c2c7);
const kblue = Color(0xff093d65);
const kbblue = Color(0xffc5c6d0);
const kstorm = Color(0xFFF98D53);
const knude = Color(0xFFae9789);
const Color kBlackColor = Color(0xff202020);

const kDefaultPadding = 20.0;
const Color kGreyColor = Color(0xffE1E1E1);


var kongreen =Colors.white.withOpacity(.7);


const kDefaultShadow = BoxShadow(
  offset: Offset(0, 10),
  blurRadius: 5,
  color: Colors.black12, // Black color with 12% opacity
);


const TextStyle kHeading = TextStyle(
  fontSize: 55,
  fontWeight: FontWeight.bold,
  color: Colors.white,
);

const TextStyle kBodyText = TextStyle(
  fontSize: 18,
  color: Colors.white,
);

const kBorderRadius = BorderRadius.all(Radius.circular(30));
const kButtonRadius = BorderRadius.all(Radius.circular(8));



const kMainUrl = "https://soasystem.90-soft.com/parse";
const kApplicationId="soa";
const kRestKey = "soa12345";
final GlobalKey<NavigatorState> globalNavigatorKey = GlobalKey<NavigatorState>();

const Color kGreenColor = Color(0xff4BBCA7);
const Color kBackGroundColor = Color(0xffF0F0F0);
const kRedColor = Color(0xFFbc3631);
// #787878

const kTopBorderRadius = BorderRadius.only(
  topLeft:Radius.circular(30),
  topRight: Radius.circular(30),
);
const k5RadiusLowerPadding = BorderRadius.all(Radius.circular(4));
const kElevation = 8.0;

const kWidgetBorderRadius = BorderRadius.all(Radius.circular(15));

const kNormalFont = "CairoR";
const kLightFont = "CairoL";
const kBoldFont = "CairoB";
const kSemiBoldFont = "CairoSB";


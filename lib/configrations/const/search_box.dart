import 'package:flutter/material.dart';

import '../../dashboard/completed/education/complete_guarantee/consts.dart';

class SearchBox extends StatelessWidget {
  const SearchBox({
    required this.onChanged,
  }) ;

  final ValueChanged onChanged;

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.all(kDefaultPadding),
      padding: const EdgeInsets.symmetric(
        horizontal: kDefaultPadding,
        vertical: kDefaultPadding / 4,
      ),
      decoration: BoxDecoration(
        color: Colors.white,
        boxShadow: const [
          kDefaultShadow,
        ],
        borderRadius: BorderRadius.circular(20),
      ),
      child: TextField(
        onChanged: onChanged,
        style: TextStyle(color: kblue),
        cursorColor: kprimary,
        decoration: const InputDecoration(
          iconColor: kblue,
          enabledBorder: InputBorder.none,
          focusedBorder: InputBorder.none,
          icon: Icon(Icons.search,color: kprimary),
          hintText: 'Search',

        ),
      ),
    );
  }
}

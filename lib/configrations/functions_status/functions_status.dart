import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:lottie/lottie.dart';

class FunctionStatus extends StatelessWidget {
  FunctionStatus(
      {Key? key, this.controller, this.title = '', this.function = ''})
      : super(key: key);
  var controller;
  String title;
  String function;

  @override
  Widget build(BuildContext context) {
    Timer(const Duration(seconds: 3), () async {
      Get.back();
    });
    return SafeArea(
      child: Obx(
        () => Scaffold(
          body: controller.isTrue.value
              ? SizedBox(
                  width: double.infinity,
                  height: double.infinity,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      Lottie.asset('assets/ok_not/18052-ok-done-complete.json'),
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Text(
                          '$title $function successfully',
                          style: const TextStyle(
                              color: Colors.green,
                              fontSize: 30,
                              fontWeight: FontWeight.bold),
                          textAlign: TextAlign.center,
                        ),
                      )
                    ],
                  ),
                )
              : SizedBox(
                  width: double.infinity,
                  height: double.infinity,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      Lottie.asset(
                          'assets/ok_not/74894-failure-feedback-rejected.json'),
                      Padding(
                        padding: const EdgeInsets.all(50.0),
                        child: Text(
                          '$title did not $function try again',
                          style: const TextStyle(
                              color: Colors.red,
                              fontSize: 30,
                              fontWeight: FontWeight.bold),
                          textAlign: TextAlign.center,
                        ),
                      )
                    ],
                  ),
                ),
        ),
      ),
    );
  }
}

import 'dart:async';
import 'package:charitysys/configrations/consts.dart';
import 'package:charitysys/configrations/select_lang/select_lang_controller.dart';
import 'package:flutter/material.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:get/get.dart';
import '../locale/locale_controller.dart';


class SelectLang extends StatelessWidget {
  SelectLang({Key? key}) : super(key: key);

  SelectController selectController =
  Get.put<SelectController>(SelectController());

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    MyLocalController langController =
        Get.put<MyLocalController>(MyLocalController());

    return SafeArea(
      child: Scaffold(
        body: Column(
          children: [
            Container(
              width: size.width,
              height: size.height * .3,
              alignment: Alignment.center,
              decoration: BoxDecoration(
                color: mainColor,
                borderRadius: const BorderRadius.only(
                  bottomRight: Radius.circular(100),
                  bottomLeft: Radius.circular(100),
                ),
              ),
              child: Text(
                "chooseLang".tr,
                style: style,
                textAlign: TextAlign.center,
              ),
            ),
            Expanded(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  GestureDetector(
                        onTap: (){
                          selectController.click['S']!.value = !(selectController.click['S']!.value);
                          selectController.click['A']!.value = !(selectController.click['A']!.value);
                        },
                        child:
                            buildRowLang(size, 'assets/langs/syria.png', 'Syria'.tr,"S"),
                      ),
                  GestureDetector(
                      onTap: (){
                        selectController.click['A']!.value = !(selectController.click['A']!.value);
                        selectController.click['S']!.value = !(selectController.click['S']!.value);
                      },
                      child: buildRowLang(
                          size, 'assets/langs/america.png', 'America'.tr,"A"),
                    ),
                  GestureDetector(
                    onTap: (){
                      if(selectController.click['S']!.value == true){
                        langController.changeLang("ar");
                        Timer(
                            const Duration(seconds: 1),
                                ()async{
                                  FlutterSecureStorage storage = const FlutterSecureStorage();
                                  await storage.write(key: 'lang',value: 'ar');
                                  Get.toNamed('main_health');
                            }
                        );
                      }else{
                        langController.changeLang("en");
                        Timer(
                            const Duration(seconds: 1),
                                ()async{
                                  FlutterSecureStorage storage = const FlutterSecureStorage();
                                  await storage.write(key: 'lang',value: 'en');
                                  Get.toNamed('main_health');
                            }
                        );
                      }
                    },
                    child: Container(
                      width: size.width * .5,
                      height: size.width * .2,
                      alignment: Alignment.center,
                      decoration: BoxDecoration(
                          color: mainColor,
                          borderRadius: BorderRadius.circular(50)),
                      child: Text(
                        "changeLang".tr,
                        style: style,
                      ),
                    ),
                  )
                ],
              ),
            )
          ],
        ),
      ),
    );
  }

  buildRowLang(Size size, String image, String text,String type) {
    return Obx(
        ()=> Container(
        color: selectController.click[type]!.value != false ? Colors.blueGrey.withOpacity(.2) : null,
        padding: const EdgeInsets.all(10),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Padding(
              padding: EdgeInsets.symmetric(horizontal: size.width * .1),
              child: Container(
                //color: Colors.red,
                width: size.width * .25,
                height: size.width * .25,
                child: ClipRRect(
                  child: Image.asset(
                    image,
                    fit: BoxFit.cover,
                  ),
                  borderRadius: BorderRadius.circular(size.width * .3),
                ),
              ),
            ),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: size.width * .1),
              child: Container(
                child: Text(
                  text,
                  style: TextStyle(
                      color: mainColor, fontWeight: FontWeight.bold, fontSize: 30),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}

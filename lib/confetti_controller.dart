import 'package:get/get.dart';
import 'package:confetti/confetti.dart';

class ConfController extends GetxController
{

  late ConfettiController  topController;
  @override
  void onInit() {
    // TODO: implement onInit
     topController =  ConfettiController(duration: const Duration(seconds: 1));

    super.onInit();

  }

  @override
  void dispose() {

    // dispose the controller
    topController.dispose();
    super.dispose();
  }

}
import 'package:charitysys/configrations/const/widgets/Elevated_button.dart';
import 'package:charitysys/configrations/const/widgets/dialogs.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:get/get.dart';
import 'package:get/get_core/src/get_main.dart';
import 'package:animator/animator.dart';
import '../configrations/const/backgroundimage.dart';
import '../configrations/const/const.dart';
import '../configrations/const/widgets/password-input.dart';
import '../configrations/const/widgets/rounded-button.dart';
import '../configrations/const/widgets/text-input.dart';
import '../user/pin_verification/pin_controller.dart';
import 'login_controller.dart';


class LoginPage extends StatelessWidget {
  @override
  LoginController controller=Get.put(LoginController());
  PinController pincontroller=Get.put(PinController());
  final _formKey = GlobalKey<FormState>();

  Widget build(BuildContext context) {
    DialogsWidgets dialogsWidgets =DialogsWidgets();
    return Stack(
      children: [
        BackgroundImage(),
        Scaffold(
          backgroundColor: Colors.transparent,
          body: SingleChildScrollView(
            child: SafeArea(
              child: Column(
                children: [
                  const SizedBox(
                    height: 10,
                  ),
                  Container(
                    height: 150,
                    child: const Center(
                      child: Text(
                        'Orphans life',
                        style: kHeading,
                      ),
                    ),
                  ),
                   Obx(() {
                     child:
                    return SizedBox(
                       height: controller.tapped.value ? 10 : 120,
                     );
                   } ),
                  Container(
                    padding: const EdgeInsets.symmetric(horizontal: 25),
                    child: Column(
                      children: [
                        Form(
                          autovalidateMode: AutovalidateMode.onUserInteraction,
                          key: _formKey,
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.end,
                            children:  [
                              TextInput(
                                icon: FontAwesomeIcons.solidEnvelope,
                                hint: 'Email',
                                inputType: TextInputType.emailAddress,
                                inputAction: TextInputAction.next, onchange: (value ) {
                                  controller.email=value;
                                  pincontroller.email=value;
                                  emmail=value;

                                }, validator: (value ) {
                                if (value!.isValidEmail)
                                  return null;
                                  return 'Enter valid email';

                              },
                              ),
                              PasswordInput(
                                validator: (value){
                                  if(value!.length>3)
                                    return null;
                                  return 'your password is less than 3';
                                },
                                icon: FontAwesomeIcons.lock,
                                hint: 'Password',
                                inputAction: TextInputAction.done,
                                onchange: (value){
                                  controller.password=value;
                                  pincontroller.password=value;
                                  passsword=value;
                                  },
                              ),
                              Obx(() => controller.tapped.value
                                  ? Animator(
                                  duration:
                                  const Duration(milliseconds: 1000),
                                  tweenMap: {
                                    'motion': Tween<Offset>(
                                        begin: const Offset(500, 0),
                                        end: const Offset(0, 0))
                                  },
                                  builder: (_, anim, __) =>
                                      Transform.translate(
                                        offset: anim.getValue('motion'),
                                        child: SizedBox(
                                          height: 180,
                                          child: SingleChildScrollView(
                                            child: Obx(
                                                  () => Column(
                                                children: [
                                                  isSame.value
                                                      ? TextInput(
                                                    icon:
                                                    FontAwesomeIcons
                                                        .lock,
                                                    hint:
                                                    'Confirm Password',
                                                    inputType:
                                                    TextInputType
                                                        .visiblePassword,
                                                    inputAction:
                                                    TextInputAction
                                                        .next,
                                                    onchange:
                                                        (value) {
                                                      controller.confirmPassword = value;
                                                      pincontroller.confirmPassword = value;
                                                      confirmPasssword=value;
                                                    },
                                                    index: 2,
                                                    validator:
                                                        (value) {},
                                                  )
                                                      : TextInput(
                                                    icon:
                                                    FontAwesomeIcons
                                                        .lock,
                                                    hint:
                                                    'Confirm Password',
                                                    inputType:
                                                    TextInputType
                                                        .visiblePassword,
                                                    inputAction:
                                                    TextInputAction
                                                        .next,
                                                    onchange:
                                                        (value) {
                                                      controller.confirmPassword = value;
                                                      pincontroller.confirmPassword = value;
                                                    },
                                                    index: 2,
                                                    validator:
                                                        (value) {},
                                                  ),
                                                  TextInput(
                                                    icon: FontAwesomeIcons
                                                        .person,
                                                    hint: 'Name',
                                                    inputType: TextInputType
                                                        .emailAddress,
                                                    inputAction:
                                                    TextInputAction
                                                        .next,
                                                    onchange: (value) {
                                                      controller.name = value;
                                                      pincontroller.name = value;
                                                      namme=value;
                                                    },
                                                    validator: (value) {},
                                                  ),
                                                  TextInput(
                                                    onchange: (value) {
                                                      controller.phone = value;
                                                      pincontroller.phone = value;
                                                      phonne=value;
                                                    },
                                                    icon: FontAwesomeIcons
                                                        .phone,
                                                    hint: 'Phone Number',
                                                    inputType: TextInputType
                                                        .number,
                                                    inputAction:
                                                    TextInputAction
                                                        .next,
                                                    validator: (value) {},
                                                  ),
                                                ],
                                              ),
                                            ),
                                          ),
                                        ),
                                      ))
                                  : Container()),
                              !controller.tapped.value?const TextButton(onPressed: null, child:  Text(
                                'Forgot Password?',
                                style: kBodyText,
                              ),):
                              const TextButton(onPressed: null, child:  Text(
                                'Verify your email',
                                style: kBodyText,
                              ),)
                            ],
                          ),
                        ),
                        Column(
                            children: [
                              const SizedBox(
                                height: 70,
                              ),
                              Obx(
                                    () => ElevatedButtonWidget(
                                      color: kblue,
                                    title: controller.tapped.value
                                        ? 'register'
                                        : 'Login',
                                        onPressed:(){ controller.tapped.value
                                            ? clickSignUp
                                            : ClickSignin;
                                        if(!controller.loginStatus)
                                        {
                                          dialogsWidgets.showScaffoldSnackBar(title: 'wrong email or password', context: context);
                                        }
                                      }
                                    ),
                              ),
                              const SizedBox(
                                height: 50,
                              ),
                              GestureDetector(
                                onTap: controller.hide,
                                child: Obx(
                                      () => !controller.tapped.value
                                      ? Container(
                                    decoration: const BoxDecoration(
                                        border: Border(
                                          bottom: BorderSide(
                                              color: Colors.white, width: 1),
                                        )),
                                    child: TextButton(
                                        onPressed: controller.hide,
                                        child: const Text(
                                          'CreateNewAccount',
                                          style: kBodyText,
                                        )),
                                  )
                                      : TextButton(
                                      onPressed: controller.hide,
                                      child: const Text(
                                        'Get Back',
                                        style: kBodyText,
                                      )),
                                ),
                              ),
                              const SizedBox(
                                height: 30,
                              ),
                            ],
                          ),

                      ],
                    ),
                  )
                ],
              ),
            ),
          ),
        ),
      ],
    );
  }
  void ClickSignin() async {
    await  controller.SigninOnClick();
    print('${controller.role}');
  }

  void clickSignUp() async {
    if (controller.confirmPassword == controller.password) {
      isSame.value = true;
          pincontroller.sendcode();

    } else {
      isSame.value = false;
      return;
    }
   }
}
extension extString on String {
  bool get isValidEmail {
    final emailRegExp = RegExp(r"^[a-zA-Z0-9.]+@[a-zA-Z0-9]+\.[a-zA-Z]+");
    return emailRegExp.hasMatch(this);
  }

  bool get isValidName{
    final nameRegExp = new RegExp(r"^\s*([A-Za-z]{1,}([\.,] |[-']| ))+[A-Za-z]+\.?\s*$");
    return nameRegExp.hasMatch(this);
  }

  bool get isValidPassword{
    final passwordRegExp =
    RegExp(r'^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[!@#\><*~]).{3,}/pre>');
    return passwordRegExp.hasMatch(this);
  }

  bool get isNotNull{
    return this!=null;
  }

  bool get isValidPhone{
    final phoneRegExp = RegExp(r"^\+?0[0-9]{10}$");
    return phoneRegExp.hasMatch(this);
  }

}

import 'dart:convert';
import 'package:http/http.dart' as http;
import '../config/config.dart';
import '../config/userinfo.dart';
import '../secureStorage.dart';
import '../user/model/user.dart';
import 'login_controller.dart';
class LoginService
{
  var message;
  var token;
  var userid;
  var name;
  var isvolunteer;
  var url = Uri.parse(ServerConfig.domainNameServer + ServerConfig.Signin);

  Future <bool> Signin( Usermodel user ) async{
    var response = await http.post(
        url,headers: {
      'Accept' : 'application/json',
    },
        body: {
          'email': user.email,
          'password':user.password,
        }
    );
    print(response.statusCode);
    if(response.statusCode==200) {
      SecureStorage storage = SecureStorage();
      var jsonresponse = jsonDecode(response.body);
      signinrole = jsonresponse['message'];
      message = jsonresponse['message'];
      // print(Userinfo.Volunteer);
      // print('${Userinfo.Volunteer}');
      isvolunteer = jsonresponse['accepted'];
      Userinfo.USER_Role = message;
      storage.save('role', message);
      if (isvolunteer == '1')
    {
      Userinfo.Volunteer = 'true';
      print('here');
        print(Userinfo.Volunteer);
      // storage.save('isvolunteer', 'true');
    }
      token=jsonresponse['token'];
      userid=jsonresponse['users_id'];
      Userinfo.USER_TOKEN=token;
      await storage.save('token', Userinfo.USER_TOKEN);
      await storage.save('userid', Userinfo.USER_ID);
      await storage.save('name', Userinfo.USER_NAME);

      return true;
    }
    else if (response.statusCode == 401) {
      // var jsonResponse = jsonDecode(response.body);
      return false;
    }else if (response.statusCode == 422) {
      // var jsonResponse = jsonDecode(response.body);
      return false;
    }
    else if (response.statusCode == 500) {
      print('wrong');
      return false;
    }
    else {
      message = 'server error';
      return false;

    } }
}
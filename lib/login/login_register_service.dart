import 'dart:convert';
import 'package:get/get.dart';
import 'package:http/http.dart' as http;
import '../InApp.dart';
import '../config/config.dart';
import '../config/userinfo.dart';
import '../secureStorage.dart';
import '../user/model/user.dart';
class LoginRegisterService
{

  var message;
  var token;
  var userid;
  var name;


  Future <bool> registerAdmin(Usermodel user) async {
    var url =
    Uri.parse(ServerConfig.domainNameServer + ServerConfig.registerUser);
    print('service');
    print('${user.email}');

    var response = await http.post(url, headers: {}, body: {
      'name': user.name,
      'email': user.email,
      'password': user.password,
      'c_password':user.passwordConfirmation,
      'phone':user.phone
    });

    print(response.statusCode);
    if(response.statusCode==200)
    {
      var jsonresponse=jsonDecode(response.body);
      print('naser');

      message=jsonresponse['message'];
      token=jsonresponse['token'];
      userid=jsonresponse['users_id'];
      Userinfo.USER_TOKEN=token;
      SecureStorage storage=SecureStorage();
      await storage.save('token', Userinfo.USER_TOKEN);
      await storage.save('userid', Userinfo.USER_ID);
      await storage.save('name', Userinfo.USER_NAME);
      Get.off(InApp());
      return true;
    }
    else if (response.statusCode == 401) {
      var jsonResponse = jsonDecode(response.body);
      message = jsonResponse['error'];
      return false;
    }else if (response.statusCode == 422) {
      var jsonResponse = jsonDecode(response.body);
      message = jsonResponse['error'];

      return false;
    }
    else if (response.statusCode == 500) {
      print('wrong');
      return false;
    }
    else {
      message = 'server error';
      return false;

    } }

}
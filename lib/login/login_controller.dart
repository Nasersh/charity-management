import 'package:charitysys/login/login.dart';
import 'package:get/get.dart';
import '../InApp.dart';
import '../dashboard/dashboard.dart';
import '../secureStorage.dart';
import '../user/model/user.dart';
import 'login_register_service.dart';
import 'loginservice.dart';

var signinrole;

class LoginController extends GetxController {
  late String email;
  var Auth=true.obs;
   RxBool tapped=false.obs ;
  var password;
  var registerStatus;
  var name;
  var confirmPassword;
  var token;
  var role;
  var loginStatus;
  var phone='';
  var message;
  LoginRegisterService serviceReg = LoginRegisterService();
  var checkBoxStatus;
  var out = false.obs;
  late LoginService service;
  late SecureStorage storage;


  @override
  void onInit() async {
    storage = SecureStorage();
     token=await storage.read('token');
    role=await storage.read('role');
    await CheckToken();
    email ='';
    confirmPassword='';
    password='';
    loginStatus = false;
    checkBoxStatus = false;
    service = LoginService();
    serviceReg = LoginRegisterService();
    tapped = false.obs;
    super.onInit();
  }

  Future<void> SigninOnClick() async {
    Usermodel user = Usermodel(
      email: email,
      password: password,
    );
    loginStatus = await service.Signin(user);
    // print('$role');
    Auth.value=loginStatus;
    message = service.message;
    if(loginStatus){
      if( signinrole=='admin')
      {
        print('$signinrole 2');
        Get.off(Dashboard());
      }
      else if( signinrole=='user login successfully')
      {
        print('$signinrole 2');
        Get.off(InApp());
      }
    }
    }

  Future<void>CheckToken()async{

    // print(role);
  if(role=='admin')
  {
    Get.off(Dashboard());
    // Get.off(LoginPage());
   }
  else if(role=='user login successfully'){
      Get.off(InApp());
      // Get.off(LoginPage());
    }
  else
    {

    }

  }

  void hide(){

    tapped(!tapped.value);

  }
}


RxBool isSame = true.obs;

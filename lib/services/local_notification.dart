import 'package:firebase_messaging/firebase_messaging.dart';
import "package:flutter_local_notifications/flutter_local_notifications.dart";

class LocalNotification {
  static final FlutterLocalNotificationsPlugin _notiPlugin =
  FlutterLocalNotificationsPlugin();

  static void intialize() {
    InitializationSettings intSettings = const InitializationSettings(
      android: AndroidInitializationSettings(
        "@mipmap/ic_launcher"
      )
    );
    _notiPlugin.initialize(intSettings);
  }
  static void display(RemoteMessage message) async {

    NotificationDetails notificationDetails = const NotificationDetails(
      android: AndroidNotificationDetails(
        "easyapproach",
        "easyapproach channel",
        channelDescription: "our channel",
        importance: Importance.max,
        priority: Priority.high
      )
    );

    int id = DateTime.now().millisecondsSinceEpoch ~/ 1000;

    await _notiPlugin.show(id, message.notification!.title, message.notification!.body, notificationDetails);
  }
}

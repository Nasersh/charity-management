
import 'package:get/get.dart';

import '../login/login.dart';
import 'logout_service.dart';


class LogoutController extends GetxController{

  void LogoutData() async {

    LogoutService _service=LogoutService();
    _service.Logout();

  }
}
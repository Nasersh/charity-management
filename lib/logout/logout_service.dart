
import 'package:get/get.dart';
import 'package:get/get_core/src/get_main.dart';
import 'package:http/http.dart' as http;

import '../config/config.dart';
import '../login/login.dart';
import '../secureStorage.dart';


class LogoutService
{
  Logout() async {
    var urlshowproduct=Uri.parse(ServerConfig.domainNameServer+ServerConfig.Logout);

    var storage = SecureStorage();

    String? token = await storage.read('token');
    print(token);
    var response = await http.post(urlshowproduct,
        headers: {
          'Accept':'application/json',
          'Authorization' : 'Bearer $token'
        } );
    print(response.statusCode);
    // print(response.body);

    if(response.statusCode == 200){
       await storage.save('token','');
       Get.off(LoginPage());
       await storage.save('role','');
       print(await storage.read('token'));
      print('done');


    }

  }



}